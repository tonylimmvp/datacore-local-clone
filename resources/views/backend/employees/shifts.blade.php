@extends('backend.layouts.main')

@section('content')
    <div>
        <h2> {{ $employee->name }}'s Default Shift </h2>
        
        <hr> 

        {{-- Current Shift --}}
        <h4> Current Shift</h4>
        <p> Click to remove shift from default </p>

        @if(sizeof($employee->shifts)>0)
            <div class="row row-cols-2 row-cols-lg-3 row-cols-xl-5">
                @foreach($employee->shifts as $shift)
                    <div class="col mb-4">
                        <form method="POST" action="{{ route('coreui.employee-shift.delete') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="employee" value="{{$employee->id}}">
                            <input type="hidden" name="shift" value="{{$shift->id}}">
                            <button class="btn" type="submit">
                                <div class="card text-center">
                                    <div class="card-body">
                                        <h5 class="card-title">{{$shift->day}}</h5>
                                        <p class="card-text">
                                            {{$shift->start_time}} - {{$shift->end_time}}
                                        </p>
                                    </div>
                                </div>
                            </button>
                        </form>
                    </div>
                @endforeach
            </div>
        @else
            <div class="alert alert-info text-center m-5 pt-4">
                <p>{{$employee->name}} currently have no default shifts</p>
            </div>
        @endif
           
        <hr>

        {{-- Adding shift --}}
        <h4> Add Shift </h4>

        <p> Click to add shift to default shift</p>
        <div class="row row-cols-2 row-cols-lg-3 row-cols-xl-5">
        @foreach( App\WorkShift::availableShift($employee->id)->get() as $shift)
            <div class="col mb-4">
                <form method="POST" action="{{ route('coreui.employee-shift.create') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="employee" value="{{$employee->id}}">
                    <input type="hidden" name="shift" value="{{$shift->id}}">
                    <button class="btn" type="submit">
                        <div class="card text-center">
                            <div class="card-body">
                                <h5 class="card-title">{{$shift->day}}</h5>
                                <p class="card-text">
                                    {{$shift->start_time}} - {{$shift->end_time}}
                                </p>
                            </div>
                        </div>
                    </button>
                </form>
            </div>
        @endforeach
        </div>

    </div>
@endsection