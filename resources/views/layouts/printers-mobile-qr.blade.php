{{-- Print Modal --}}
{{-- Add Product Modal --}}

<div class="modal fade" id="printMobileQRModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title" id="exampleModalLabel">Print Mobile Order#<span id="printOrder">---</span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                @php $printers = App\LabelPrinter::all(); @endphp
                <h5 class="text-center">UPSTAIRS</h5>
                <div class="text-center">
                    <div class="btn-group">
                        @foreach($printers as $printer)
                            @if($printer->location == "UPSTAIRS")
                            <button class="btn btn-outline-secondary" onclick="printMobileQR({{$printer->id}},'{{route('print.mobileQR')}}')">
                                <img class="img-fluid" src="{{asset('img/'.$printer->type.'.png')}}" style="height:100px"><br>
                                {{$printer->type}}
                            </button>
                            @endif
                        @endforeach
                        {{-- Postek --}}
                        <a class="btn btn-outline-secondary" href='{{route('print.postekMobileQR',$order->order_id)}}'>
                            <img class="img-fluid" src="{{asset('img/postek.png')}}" style="height:100px"><br>
                            Postek
                        </a>
                    </div>
                </div>

                <hr>

                <h5 class="text-center">DOWNSTAIRS</h5>
                <div class="text-center">
                    <div class="btn-group">
                        @foreach($printers as $printer)
                            @if($printer->location == "DOWNSTAIRS")
                            <button class="btn btn-outline-secondary" onclick="printMobileQR({{$printer->id}},'{{route('print.mobileQR')}}')">
                                <img class="img-fluid" src="{{asset('img/'.$printer->type.'.png')}}"><br>
                                {{$printer->type}}
                            </button>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- End Print Modal --}}

@push('bottom-scripts')
<script>
    $(document).ready(function () {
        $('#printMobileQRModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var id = button.data('id') // Extract info from data-* attributes

            var modal = $(this)
            modal.find('#printOrder').text(id)
        });
    });

    function printMobileQR(printer_id,url){
        var order_id = $('#printOrder').text();
        var num_labels = 1;
        var printer_selected = printer_id;

        $.ajax({
            type: "get",
            url: url,
            data: {oid: order_id, printer: printer_id,num: num_labels},
            success: function( msg ) {
                if(msg.status == "failed"){
                    alert(msg.message);
                }
            },
            error: function (request, status, error) {
                alert(request.status + ':' + error + '. Please take picture and contact DevTeam.');
            }
        });

        $('#printMobileQRModal').modal('hide');
    }
</script>
@endpush