<?php

namespace App\Http\Middleware;
// namespace App\Http\Controllers\Auth;

use Closure;

class EmployeeRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ... $roles)
    {
        if (!\Auth::check()) // I included this check because you have it, but it really should be part of your 'auth' middleware, most likely added as part of a route group.
        return redirect('login');

        $user = \Auth::user();
        // dd($roles);
        foreach($roles as $role) {
            // Check if user has the role This check will depend on how your roles are set up
            // dd($user->hasRole($role));
            if($user->hasRole($role))
                return $next($request);
        }
        // TODO: Make unauthorize page 
        return redirect()->route('restricted');
    }
}
