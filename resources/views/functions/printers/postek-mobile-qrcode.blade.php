<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

    <style>
        body {
            margin: 0;
            padding: 0;
            background-color: #FAFAFA;
            font-family: "Raleway", sans-serif;
        }
        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }
        .page {
            width: 8.2cm;
            height: 8.3cm;
            margin: 0cm auto;
            border: 1px #D3D3D3 solid;
            border-radius: 0px;
            background: white;
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
            line-height: 4px;
        }

        @page {
            size: 5.5cm 6.3cm;
            margin: 0;
        }

        @media print {
            .page {
                margin: 0;
                border: initial;
                border-radius: initial;
                width: 6.3cm;
                min-height: 5.5cm;
                box-shadow: initial;
                background: initial;
                page-break-after: always;
            }
        }

        #container_style1{
            width: 301px;
            line-height: 0px;
            position: relative;
        }

        #header{
            text-align: left;
            margin-left:20px;
            padding-top:35px;
        }


        .info {
            text-align: left;
            margin-left:20px;

        }

        .name {
            font: 18pt "Raleway", sans-serif;
        }

        .qrcode-topright{
            position: absolute;
            top:0px;
            right:10px;
        }

        .qrcode-bottom{
            position: absolute;
            bottom:-5px;
            right:10px;
        }

    </style>
</head>

<div id='container_style1' class='page'>
    <div id="header">
        <h1 class="order_id">Mobile #{{$order->order_id}} </h1>
        <h3 class="order_reference">({{$order->reference}})</h3>
    </div>
    <div class="info">
        <h2 class="name">{{$order->customer()->firstname}} {{$order->customer()->lastname}}</h2>
        @if($order->customer()->company)<p style="margin-bottom:5px;">{{$order->customer()->company}}</p>@endif
        @if($order->customer()->phone)<p>{{$order->customer()->phone}}</p>@endif
        @if($order->customer()->email)<p>{{$order->customer()->email}}</p>@endif
    </div>

    <p class="qrcode-bottom"> {!! QRCode::size(130)->generate(route('orders.mobile',$order->order_id)) !!}</p>
</div>