@extends('frontend.layouts.main')

@include('layouts.datatables')

@push('top-scripts')
<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
@endpush

@push('css')
<style>
    h1 {
        text-align: center;
        font-family: 'Nunito', sans-serif;
        font-weight: 200;
    }

    h2 {
        font-family: 'Nunito', sans-serif;
        font-weight: 200; 
        text-align: center;
    }
    
    .product-image-thumbnail {
        display: block;
        width: 100px;
        height: auto;
    }

    td img {
        display: block;
        margin-left: auto;
        margin-right: auto;
    }
</style>
@endpush

@section('content')

    <br/>
    <h1>List of Store Supplies</h1>
    <hr/>

    @include('layouts.errors');

    <div style="text-align: center; justify-content: center; margin-bottom: 5px;">
        <button class="btn btn-secondary btn-sm btn-action mb-1" style="width:25%; font-size: 14px;"
            data-toggle="modal"
            data-target="#purchaseModal"
            data-url="{{route('store-supplies.purchaseSupply')}}">Order Product Manually
        </button>
    </div>

    <div class="table-responsive">
        <table id="mainTable" class="table table-hover table-dark table-bordered" width="100%">
            <thead>
                <tr>
                    <th scope="col"><span data-feather="list"></span> Name</th>
                    <th scope="col"><span data-feather="hash"></span> Quantity</th>
                    <th scope="col"><span data-feather="type"></span> Unit</th>
                    <th scope="col"><span data-feather="calendar"></span> Purchased Date</th>
                    <th scope="col"><span data-feather="calendar"></span> Updated At Date</th>
                    <th scope="col" style="display:none;"><span data-feather="activity"></span> Active?</th>
                    <th scope="col"><span data-feather="activity"></span> Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($supplies as $supply)
                    <tr>
                        <td>{{$supply->name}}</td>
                        <td>{{$supply->qty}}</td>
                        <td>{{$supply->unit}}</td>
                        <td>{{$supply->created_at}}</td>
                        <td>{{$supply->updated_at}}</td>
                        <td style="display:none;">{{$supply->isActive}}</td>
                        <td>
                            @auth
                                @if(Auth::user()->role->id == 1)
                                    <button class="btn btn-primary btn-sm btn-action mb-1"
                                        data-toggle="modal"
                                        data-target="#editModal"
                                        data-name="{{$supply->name}}"
                                        data-qty="{{$supply->qty}}"
                                        data-unit="{{$supply->unit}}"
                                        data-url="{{route('store-supplies.editSupply', $supply->id)}}">Edit
                                    </button>

                                    <form class="form-inline" method="POST" action="{{route('store-supplies.deleteSupply', $supply->id)}}" enctype="multipart/form-data">
                                        <input type="hidden" name="_method" value="POST">               
                                        {{ csrf_field() }}
                                        <button type="submit" class="btn btn-primary btn-sm btn-action mb-1">Delete</button><br>
                                    </form>
                                @endif
                            @endauth
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <hr/>

    {{-- Purchase Modal --}}
    <div class="modal fade" id="purchaseModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="POST" action="{{route('store-supplies.purchaseSupply')}}">
                	{{ csrf_field() }}
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Input Order Information</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>

					<div class="modal-body">

                        <label>Supply Name:</label>
                    	<input class="form-control" type="text" name="supply_name" id="supply_name">

						<label>Quantity:</label>
                    	<input class="form-control" type="number" name="qty" id="qty" required><br>

						<label>Unit:</label>
                        <select class="custom-select mr-sm-2 mb-2" name="unit" id="unit" required>
                            <option value="piece">Piece</option>
                            <option value="package">Package</option>
                            <option value="meter">Meter</option>
                            <option value="foot">Foot</option>
                            <option value="1/2lb">1/2LB</option>
                            <option value="box">Box</option>
                            <option value="roll">Roll</option>
                            <option value="winpos unit">Winpos Unit</option>
                        </select>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
						<a id="note-cancel" href="#">
						<button type="submit" class="btn btn-primary">Order Item</button>
						</a>
					</div>
        		</form>
            </div>
    	</div>
    </div>

    {{-- Edit Order Modal --}}
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="POST">
                <input type="hidden" name="_method" value="patch">
                	{{ csrf_field() }}
                    {{ method_field('PATCH') }}
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Edit Order</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>

					<div class="modal-body">

						<label>Product Name:</label>
                    	<input class="form-control" type="text" name="name" id="name" required><br/>

						<label>Ordered Quantity:</label>
                    	<input class="form-control" type="number" name="qty" id="qty" required><br/>

                    	<label>Unit:</label>
                    	<select class="custom-select mr-sm-2 mb-2" name="unit" id="unit" required>
                            <option value="piece">Piece</option>
                            <option value="package">Package</option>
                            <option value="meter">Meter</option>
                            <option value="foot">Foot</option>
                            <option value="1/2lb">1/2LB</option>
                            <option value="box">Box</option>
                            <option value="roll">Roll</option>
                            <option value="winpos unit">Winpos Unit</option>
                        </select>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>

						<button type="submit" class="btn btn-primary">Edit Supply</button>
					</div>
        		</form>
            </div>
    	</div>
    </div>


@endsection

@push('bottom-scripts')
    <script>
        jQuery(document).ready(function($) {
            $(".clickable-row").click(function() {
                window.location = $(this).data("href");
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#mainTable').DataTable({
                "scrollY":        "60vh",
                "scrollCollapse": true,
                "paging":         false,
                columnDefs: [ { orderable: false, targets: [

                ]}],
                "language": {
                    "search": "Filter records:"
                }
            });
        });
    </script>

    <script>
        $(document).ready(function () {
            $('#purchaseModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget)
                var url = button.data('url')

                var modal = $(this)
                modal.find('.modal-title').text('Purchase Modal')
                modal.find('form').attr('action',url)
            });
        });
    </script>

    <script>
        $(document).ready(function () {
            $('#editModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) 
                var name = button.data('name') 
                var qty = button.data('qty')
                var unit = button.data('unit')
                var url = button.data('url')

                var modal = $(this)
                modal.find('.modal-title').text('Edit Supply')
                modal.find('#name').val(name)
                modal.find('#qty').val(qty)
                modal.find('#unit').val(unit)
                modal.find('form').attr('action',url)
            });
        });
    </script>
@endpush