@extends('layouts.plain')

@section('content')
<div class="d-flex justify-content-center text-center" style="height:80vh">
        <div class="my-auto ">
            <img id="restricted-img" class="img img-fluid mb-3" src="{{asset('img/restricted.png')}}" alt="restricted">
            <h1>THIS PAGE IS RESTRICTED </h1>
            <h5>Please contact the development team if this is a bug.</h5>
        </div>
    </div>
@endsection