<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    //
    protected $guarded= [];

    public function service_types(){
        return $this->belongsToMany(ServiceType::class);
    }

}
