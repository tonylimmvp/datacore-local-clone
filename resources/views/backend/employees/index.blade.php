@extends('backend.layouts.main')

@include('layouts.datatables')

@section('content')
    <h1>Employees</h1>
    @if(Auth::user()->hasAccess('employee')->create)
    <div class="d-flex flex-row-reverse pt-3 pb-2 mb-3">
        <div class="btn-toolbar mb-2 mb-md-0">
        <div class="btn-group mr-2">
            <a href="{{route('coreui.employees.create')}}"><button class="btn btn-xs btn-primary" >Add Employee</button></a>
        </div>
        </div>
    </div>
    @endif
    <div class="table-responsive">
    <table id="employee_table" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
            <th class="th-sm">ID
            </th>
            <th class="th-sm">Username
            </th>
            <th class="th-sm">Name
            </th>
            <th class="th-sm">Email
            </th>
            <th class="th-sm">Role
            </th>
            <th class="th-sm">Start date
            </th>
            <th class="th-sm">Active
            </th>
            @if(Auth::user()->hasAccess('employee')->update || Auth::user()->hasAccess('employee')->delete)
            <th class="sorting_disabled">Action
            </th>
            @endif
            </tr>
        </thead>
        <tbody>
            @foreach($employees as $employee)
                <tr>
                    <td>{{$employee->id}}</td>
                    <td>{{$employee->username}}</td>
                    <td>{{$employee->name}}</td>
                    <td>{{$employee->email}}</td>
                    <td>{{$employee->role->name}}</td>
                    <td>{{$employee->created_at->toDateString()}}</td>
                    <td>@if($employee->isActive) Yes @else No @endif</td>
                    @if(Auth::user()->hasAccess('employee')->update || Auth::user()->hasAccess('employee')->delete)
                    <td style="text-align:center">
                        @if(Auth::user()->hasAccess('employee')->update)
                            <a href="{{route('coreui.employees.edit',$employee->id)}}"><button class="btn btn-sm btn-primary mb-2">Edit</button></a>
                            <a href="{{route('coreui.employee-permission.edit',$employee->id)}}"><button class="btn btn-sm btn-warning mb-2">Permissions</button></a>
                        @endif

                        @if(Auth::user()->hasAccess('work_shifts')->update && $employee->isActive && !$employee->ignoreShift() && $employee->role->id != 5)
                            <a href="{{route('coreui.employees.shifts',$employee->id)}}"><button class="btn btn-sm btn-light mb-2">Shift</button></a>
                        @endif

                        @if(Auth::user()->hasAccess('employee')->delete && $employee->role->id != 1)
                            @if($employee->isActive)
                            <div>
                            <button class="btn btn-sm btn-danger mb-2" 
                                data-toggle="modal" 
                                data-target="#deleteModal" 
                                data-id="{{ $employee->id }}" 
                                data-name="{{ $employee->name }}"
                                data-delete_url="{{ route('coreui.employees.deactivate',$employee->id) }}">Deactivate</button>
                            </div>
                            @else
                            <form action="{{ route('coreui.employees.activate',$employee->id) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('PATCH') }}
                                <button class="btn btn-sm btn-success mb-2">Activate</button>
                            </form>
                            @endif
                        @endif
                    </td>
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
      
{{-- Delete Modal --}}
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Deactivate User?</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Are you sure you want to deactivate <span id="employee-name"> Name </span>?
        </div>
        <div class="modal-footer">
            <form id="deactivate-form" method="POST">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-danger">Deactivate Employee</button>
            </form>
        </div>
        </div>
    </div>
</div>
     
@endsection

@push('bottom-scripts')
<script type="text/javascript">
    $(document).ready(function () {
        $('#employee_table').DataTable({
            "language": {
                "search": "Filter records:"
            }
            @if(Auth::user()->hasAccess('employee')->update || Auth::user()->hasAccess('employee')->delete)
            columnDefs: [ { orderable: false, targets: [6] } ]
            @endif
        });
        $('.dataTables_length').addClass('bs-select');
    });
</script>
<script>
$(document).ready(function () {
    $('#deleteModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var id = button.data('id') // Extract info from data-* attributes
        var employee = button.data('name') // Extract info from data-* attributes
        var delete_url = button.data('delete_url')
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-title').text('Delete User: ' + employee)
        modal.find('#employee-name').text(employee)
        $('#deactivate-form').attr('action',delete_url)
    });
});
</script>

@endpush