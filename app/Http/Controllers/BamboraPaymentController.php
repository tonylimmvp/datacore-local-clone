<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Mail;

class BamboraPaymentController extends Controller
{
    protected $merchant_id;
    protected $report_api_key;
    protected $payment_api_key;
    protected $api_version;
    protected $platform;

    public function __construct() { 
        $this->merchant_id = env('BAMBORA_MERCHANT_ID'); 
        $this->report_api_key = env('BAMBORA_REPORT_API'); 
        $this->payment_api_key = env('BAMBORA_PAYMENT_API'); 
        $this->api_version = env('BAMBORA_API_VERSION'); 
        $this->platform = env('BAMBORA_PLATFORM'); 
    }

    public function index(){
        //Create Beanstream Gateway
        $beanstream = new \Beanstream\Gateway(  
            $this->merchant_id, 
            $this->report_api_key, 
            $this->platform, 
            $this->api_version
        );

        //example search criteria data
        $search_criteria = array(
            'name' => 'Search', // or 'Search',
            'start_date' => Carbon::now()->subMonth()->format("Y-m-d"),
            'end_date' => Carbon::now()->addDay()->format("Y-m-d"),   
            'start_row' => '1',
            'end_row' => '100',
            // 'criteria' => array(
            //     'field' => '1',
            //     'operator' => '%3D',
            //     'value' => ''
            // )
        );

        // dd($beanstream);

        if($search_criteria){
            try {
                $result = $beanstream->reporting()->getTransactions($search_criteria);
                $result = json_decode(json_encode($result));

            } catch (\Beanstream\Exception $e) {
                
                /*
                * Handle transaction error, $e->code can be checked for a
                * specific error, e.g. 211 corresponds to transaction being
                * DECLINED, 314 - to missing or invalid payment information
                * etc.
                */

                $result = (object)[];
                $result->error = $e->getMessage();
                $result->records = null;
            }

        }

        return view('frontend.payment-read',compact('result'));
    }

    public function payment(){
        return view('frontend.payment');
    }

    public function pay(Request $request){
        $rules = [
            'card_holder' => 'required',
            'card_number' => 'required',
            'expiry_month' => 'required',
            'expiry_year' => 'required',
            'cvv' => 'required',
            'order_number' => 'required',
            'amount' => 'required',
            'address' => 'required',
            'postal_code' => 'required',
            'city' => 'required',
            'province' => 'required',
            'country' => 'required'
        ];

        $customMessages = [
            'product_id.required' => "There must be at least 1 product."
         ];

        $this->validate($request, $rules, $customMessages);
        
        // Do personal validation
        $errors = [];

        // Check if amount over $0
        if($request->amount <= 0) {
            array_push($errors,'Your amount must be over $0');
        }
        
        // Validate Credit Card
        $card_number = str_replace(' ', '', $request->card_number);
        $card_number = str_replace('_', '', $card_number);

        if($request->card_number == "1" && strlen($card_number) != 15) {
            array_push($errors,'Your AMEX number is invalid.');
        }

        if(($request->card_number == "0" || $request->card_number == " 0 ") && strlen($card_number) != 16) {
            array_push($errors,'Your credit card number is invalid.');
        }

        // Validate Expiry date
        $month = $request->expiry_month;
        $year = $request->expiry_year;

        $expired = Carbon::parse("$year-$month")->isPast();

        if($expired){
            array_push($errors,'Your credit card number is expired. Please use a different card.');
        }

        // Validate CVV

        $cvv = str_replace('_', '', $request->cvv);
        
        if($request->card_number == "1" && strlen($cvv) != 4){
            array_push($errors,'Your AMEX cvv is invalid.');
        }

        if(($request->card_number == "0" || $request->card_number == " 0 ") && strlen($cvv) != 3){
            array_push($errors,'Your credit card cvv is invalid.');
        }

        // Reject submission if errors exist

        if(sizeof($errors)>0) {
            return back()->withInput($request->all())->withErrors($errors);
        }

        // End of custom validation

        // Check for optional fields 
        if($request->email){
            $email = $request->email;
        }else{
            $email = "";
        }

        if($request->phone){
            $phone = $request->phone;
        }else{
            $phone = "";
        }

        if($request->address){
            $address = $request->address;
        }else{
            $address = "";
        }

        if($request->address2){
            $address2 = $request->address2;
        }else{
            $address2 = "";
        }

        if($request->postal_code){
            $postal_code = $request->postal_code;
        }else{
            $postal_code = "";
        }

        if($request->city){
            $city = $request->city;
        }else{
            $city = "";
        }

        if($request->province){
            $province = $request->province;
        }else{
            $province = "";
        }

        if($request->country){
            $country = $request->country;
        }else{
            $country = "";
        }
        
        // dd($card_number);

        //Create Beanstream Gateway
        $beanstream = new \Beanstream\Gateway($this->merchant_id, $this->payment_api_key, $this->platform, $this->api_version);

        // convert month to two digit
        $month = str_pad($request->expiry_month, 2, "0", STR_PAD_LEFT);

        // convert year to two digit
        $year = substr($request->expiry_year,2,3);

        $payment_data = array(
            'order_number' => $request->order_number,
            'amount' => $request->amount,
            'payment_method' => 'card',
            'card' => array(
                'name' => $request->card_holder,
                'number' => $card_number,
                'expiry_month' => $month,
                'expiry_year' => $year,
                'cvd' => $cvv
            ),
            'billing' => array(
                'name' => $request->card_holder,
                'address_line1'=> $address,
                'address_line2'=> $address2,
                'postal_code' => $postal_code,
                'city' => $city,
                'province' => $province,
                'country' => $country,
                'email_address' => $email,
                'phone_number' => $phone,
            )
        );

        // dd($payment_data);

        $complete = TRUE; //set to FALSE for PA

        if($payment_data){
        //Try to submit a Card Payment
            try {
                $result = $beanstream->payments()->makeCardPayment($payment_data, $complete);
                /*
                * Handle successful transaction, payment method returns
                * transaction details as result, so $result contains that data
                * in the form of associative array.
                */

                // Send email if email exist
                if($request->email){
                    $result["cardholder"] = $request->card_holder;
                    $result["email"] = $request->email;
                    \Mail::to($request->email)->bcc("support@leeselectronic.com")->send(new \App\Mail\BamboraCustomerReceipt($result));
                }

            } catch (\Beanstream\Exception $e) {
                
                /*
                * Handle transaction error, $e->code can be checked for a
                * specific error, e.g. 211 corresponds to transaction being
                * DECLINED, 314 - to missing or invalid payment information
                * etc.
                */
                // dd($e);

                return back()->withInput($request->all())->withErrors([$e->getMessage()]);
            }
        }

        return redirect('payments');
    }
}
