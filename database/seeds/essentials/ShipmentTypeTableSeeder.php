<?php

use Illuminate\Database\Seeder;

class ShipmentTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shipment_types')->insert([
            'name' => 'AIR',
        ]);

        DB::table('shipment_types')->insert([
            'name' => 'GROUND',
        ]);

        DB::table('shipment_types')->insert([
            'name' => 'SEA',
        ]);
    }
}
