<?php

use Illuminate\Database\Seeder;
use App\Order;


class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $order = new Order();
        $order->name = "Leon Cao";
        $order->phone = "7783181912";
        $order->email = "leon@leeselectronic.com";
        $order->employee_id = 3;
        $order->comments = Crypt::encryptString("Created by Seeder");
        $order->type = "backorder";
        $order->status_id = 1;
        $order->invoice_id = 11111111;
        $order->save();

        $order = new Order();
        $order->name = "Tony Lim";
        $order->phone = "6048751993";
        $order->email = "tony@leeselectronic.com";
        $order->employee_id = 5;
        $order->comments = Crypt::encryptString("Created by Seeder");
        $order->status_id = 1;
        $order->type = "special";
        $order->invoice_id = 22222222;
        $order->save();

        $order = new Order();
        $order->name = "Leon Cao";
        $order->phone = "7783181912";
        $order->email = "leon@leeselectronic.com";
        $order->employee_id = 3;
        $order->comments = Crypt::encryptString("Created by Seeder");
        $order->status_id = 1;
        $order->type = "special";
        $order->invoice_id = 33333333;
        $order->save();

        $order = new Order();
        $order->name = "Leon Cao";
        $order->phone = "7783181912";
        $order->email = "leon@leeselectronic.com";
        $order->employee_id = 3;
        $order->comments = Crypt::encryptString("Created by Seeder");
        $order->status_id = 1;
        $order->type = "backorder";
        $order->invoice_id = 33333333;
        $order->save();
    }
}
