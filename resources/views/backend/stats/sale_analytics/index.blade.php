@extends('backend.layouts.main')


@push('top-scripts')
<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
@endpush

{{-- Variable Declarations and Initializations --}}
@php use \App\Http\Controllers\AnalyticsController; @endphp
@php $currentYear = date('Y') + 0; @endphp
@php $rankQueryLimit = 50; @endphp

@push('css')
<style>
    h1 {
        text-align: center;
        font-family: 'Nunito', sans-serif;
        font-weight: 200;
    }

    h2 {
        font-family: 'Nunito', sans-serif;
        font-weight: 200; 
        text-align: center;
    }

    h3 {
        font-family: 'Nunito', sans-serif;
        font-weight: 200; 
        font-size: 20px;
        text-align: center;
    }
    
    .product-image-thumbnail {
        display: block;
        width: 100px;
        height: auto;
    }

    td img {
        display: block;
        margin-left: auto;
        margin-right: auto;
    }

    #doughnutChart {
        display: block;
        margin-left: auto;
        margin-right: auto;

    }

    .chart-container {
        position: relative;
        width: 50vw;
        height: 50vh;
        margin-left: auto;
        margin-right: auto;
    }
</style>
@endpush

@section('content')

{{-- Variables for Chart --}}
@php 
    $currentYear = date('Y') + 0; 
    $currentMonth = date('m') + 0; 
    $getRevenuePrev4Yrs = AnalyticsController::getCompanyYearlyRevenue($currentYear-4);
    $getRevenuePrev3Yrs = AnalyticsController::getCompanyYearlyRevenue($currentYear-3);
    $getRevenuePrev2Yrs = AnalyticsController::getCompanyYearlyRevenue($currentYear-2);
    $getRevenueLastYr = AnalyticsController::getCompanyYearlyRevenue($currentYear-1); 
    $getYTDRev = AnalyticsController::getCompanyYearlyRevenue($currentYear); 
    $getMTDRev = AnalyticsController::getCompanyMonthlyRevenue($currentYear, $currentMonth);
    $avgProductRevenue = AnalyticsController::getAverageProductRevenue($currentYear);

    $getCostPrev4Yrs = AnalyticsController::getCompanyYearlyCost($currentYear-4);
    $getCostPrev3Yrs = AnalyticsController::getCompanyYearlyCost($currentYear-3);
    $getCostPrev2Yrs = AnalyticsController::getCompanyYearlyCost($currentYear-2);
    $getCostLastYr = AnalyticsController::getCompanyYearlyCost($currentYear-1); 
    $getYTDCost = AnalyticsController::getCompanyYearlyCost($currentYear);
@endphp

<h2>Performance Analytics</h2>
<canvas id="perfChart" width="750px" height="200px"></canvas>
<script>
  //line
    var ctxLb = document.getElementById("perfChart").getContext('2d');
    var perfChart = new Chart(ctxLb, {
        type: 'line',
        data: {
            labels: [{{$currentYear-4}}, {{$currentYear-3}}, {{$currentYear-2}}, {{$currentYear-1}}, {{$currentYear}}],
            datasets: [{
                label: "Yearly Revenue",
                data: [{{$getRevenuePrev4Yrs}}, {{$getRevenuePrev3Yrs}}, {{$getRevenuePrev2Yrs}}, {{$getRevenueLastYr}}, {{$getYTDRev}}],
                backgroundColor: [
                'rgba(105, 0, 132, .2)',
                ],
                borderColor: [
                'rgba(200, 99, 132, .7)',
                ],
                borderWidth: 2
            },
            {
                label: "Yearly Cost",
                data: [{{$getCostPrev4Yrs}}, {{$getCostPrev3Yrs}}, {{$getCostPrev2Yrs}}, {{$getCostLastYr}}, {{$getYTDCost}}],
                backgroundColor: [
                'rgba(0, 137, 132, .2)',
                ],
                borderColor: [
                'rgba(0, 10, 130, .7)',
                ],
                borderWidth: 2
            }
            ]
        },
        options: {
            responsive: true
        }
    });

</script>

<hr>
<div class="chart-container">
    <canvas id="doughnutChart"></canvas>
</div>

<h3>Company YTD Revenue: $@formatMoney($getYTDRev)<h3>
<h3>Company YTD Cost: $@formatMoney($getYTDCost)</h3>
<h3>Company YTD Profit: $@formatMoney($getYTDRev - $getYTDCost)</h3>
<h3>Company MTD Revenue: $@formatMoney($getMTDRev)</h3>
<h3>Average Single Product Revenue (YTD) = $@formatMoney($avgProductRevenue)</h3>

<hr>
<h2>Revenue Analysis</h2>
<h3>Click on Barchart for more detail.</h3>
<canvas id="myChart" width="750px" height="200px"></canvas>
<script>
var ctx = document.getElementById("myChart");
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [{{$currentYear-4}}, {{$currentYear-3}}, {{$currentYear-2}}, {{$currentYear-1}}, {{$currentYear}}],
        datasets: [{
            label: 'Sales Revenue ($CAD)',
            data: [{{$getRevenuePrev4Yrs}}, {{$getRevenuePrev3Yrs}}, {{$getRevenuePrev2Yrs}}, {{$getRevenueLastYr}}, {{$getYTDRev}}],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
            ],
            borderWidth: 1
        }
        ]
    },
    options: {
        responsive: true,
        maintainAspectRatio: true,
        title: {
            display: true,
            text: 'Lee\'s Electronic Components Sales Y/Y'
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        },
        onClick: function(event){
            var activePoints = myChart.getElementsAtEvent(event);
            //Selected index will return 0 for the first bar, 1 for the second bar, etc...
            console.log(activePoints);
            var d = new Date();
            var currentYear = d.getFullYear();
            var selectedIndex = activePoints[0]._index;
            var year = currentYear-4 + selectedIndex;

            $("#modChart .modal-body").html('<canvas id="lineChart_'+ year +'" width="750" height="300"></canvas>');

            $(document).ready(function(){
            $.ajax({
                type: "GET",
                url: "{{route('analytics.monthRev')}}",
                data: {year: year},
                dataType: 'json',
                success: function(data) {
                    if(data.status == "success"){
                        
                            var ctxL = document.getElementById("lineChart_"+ year).getContext('2d');
                            var myLineChart = new Chart(ctxL, {
                                type: 'line',
                                data: {
                                labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                                datasets: [{
                                    label: "Monthly Revenue " + year,
                                    data: [data.monthly_revenues[0],data.monthly_revenues[1], data.monthly_revenues[2], data.monthly_revenues[3],data.monthly_revenues[4],data.monthly_revenues[5],data.monthly_revenues[6],data.monthly_revenues[7],data.monthly_revenues[8],data.monthly_revenues[9],data.monthly_revenues[10],data.monthly_revenues[11]],
                                    backgroundColor: [
                                        'rgba(105, 0, 132, .2)',
                                    ],
                                    borderColor: [
                                        'rgba(200, 99, 132, .7)',
                                    ],
                                    borderWidth: 2
                                    }  
                                ]
                                },
                                options: {
                                    responsive: true
                                }
                            });

                        $('#modChart').modal('show');
                    }
                }
            }); 
        });            
        }
    },
});

</script>

<hr>
<h2>Cost Analysis</h2>
<canvas id="costChart" width="750px" height="200px"></canvas>
<script>
    var ctxC = document.getElementById("costChart").getContext('2d');
    var costChart = new Chart(ctxC, {
        type: 'bar',
        data: {
        labels: [{{$currentYear-4}}, {{$currentYear-3}}, {{$currentYear-2}}, {{$currentYear-1}}, {{$currentYear}}],
        datasets: [{
            label: 'Cost ($CAD)',
            data: [{{$getCostPrev4Yrs}}, {{$getCostPrev3Yrs}}, {{$getCostPrev2Yrs}}, {{$getCostLastYr}}, {{$getYTDCost}}],
            backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)'
            ],
            borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)'
            ],
            borderWidth: 1
        }]
    },
        options: {
            title: {
                display: true,
                text: 'Lee\'s Electronic Components Purchase Cost'
            },
            scales: {
                yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                }]
            }
        }
  });

</script>
<hr>

{{-- Modal --}}
    <div class="modal fade" id="modChart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                    </button>
                </div>
                <div class="modal-body">
                    <canvas id="lineChart" width="750" height="300"></canvas>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('bottom-scripts')
    <script>
        jQuery(document).ready(function($) {
            $(".clickable-row").click(function() {
                window.location = $(this).data("href");
            });
        });
    </script>

    <script>
        var ctxD = document.getElementById("doughnutChart").getContext('2d');
        var myLineChart = new Chart(ctxD, {
            type: 'doughnut',
            data: {
                labels: ["Cost", "Revenue"],
                datasets: [{
                    data: [{{$getYTDCost}}, {{$getYTDRev}}],
                    backgroundColor: ["#F7464A", "#46BFBD"],
                    hoverBackgroundColor: ["#FF5A5E", "#5AD3D1"]
                    }]
                },
            options: {
                responsive: true,
                rotation: 1 * Math.PI,
                circumference: 1 * Math.PI,
                maintainAspectRatio: false
            }
        });
    </script>
@endpush