@extends('frontend.layouts.main')

@include('layouts.datatables')
@include('layouts.datepicker')

@push('top-scripts')
<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
@endpush

@push('css')
<style>
    h1 {
        text-align: center;
        font-family: 'Nunito', sans-serif;
        font-weight: 200;
    }

    h2 {
        font-family: 'Nunito', sans-serif;
        font-weight: 200; 
    }

    h3 {
        font-family: 'Nunito', sans-serif;
        font-weight: 200;
        font-size: 22px;
    }

</style>
@endpush

@section('content')
@include('layouts.errors')
<div class="container">
    <br/>
    <h1>Shipment #{{ $shipment->id }} Detail View Page</h1>
    <h1 style="color:#{{$shipment->status->color}}">Current Status: {{$shipment->status->name}}</h1>

    @if($shipment->isActive == false)
        <h2 style="color: red; text-align: center">Deactivated</h2>
    @endif

    <br/>

    <div class="form-row">
        <div class="input-group mr-sm-2 mb-2">
            <div class="input-group-prepend">
                <div class="input-group-text"><span data-feather="globe"></i></div>
            </div>
            <input class='form-control' type='text' name='shipment_type' value='{{ $shipment->type->name }}' readonly>

            <div class="input-group-prepend">
                <div class="input-group-text"><span data-feather="map"></div>
            </div>
            @if($shipment->isLocal == true)
                <input class='form-control' type='text' name='shipment_isLocal' value='LOCAL SHIPMENT' readonly>
            @else
                <input class='form-control' type='text' name='shipment_isLocal' value='NOT LOCAL SHIPMENT' readonly>
            @endif

            <div class="input-group-prepend">
                <div class="input-group-text"><span data-feather="truck"></div>
            </div>
            @if($shipment->isShipped == true)
                <input class='form-control' type='text' name='shipment_isLocal' value='SHIPPED' readonly>
            @else
                <input class='form-control' type='text' name='shipment_isLocal' value='NOT YET SHIPPED' readonly>
            @endif

        </div>
    </div>

    <hr/>
    
    <form method="POST" action="{{route('shipment-detail.save', $shipment->id)}}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="PATCH">

        <h3 class='shipment_label'>Purchase Order #: </h3>
        <div class="input-group mr-sm-2 mb-2">
            <div class="input-group-prepend">
                <div class="input-group-text"><span data-feather="tag"></div>
            </div>
            <input class='form-control' type='text' name='shipment_po' value='{{ $shipment->purchase_order }}'><br/>
        </div>

        <h3 class='shipment_label'>Created By: </h3>
        <div class="input-group mr-sm-2 mb-2">
            <div class="input-group-prepend">
                <div class="input-group-text"><span data-feather="users"></div>
            </div>
            <input class='form-control' type='text' name='shipment_created_by' value='{{ $shipment->employee->name }}' readonly><br/>
        </div>

        <h3 class='shipment_label'>Departure Date: </h3>
        <div class="input-group mr-sm-2 mb-2">
            <div class="input-group-prepend">
                <div class="input-group-text"><span data-feather="calendar"></div>
            </div>
            {{-- 96.43% to fit 100% of screen width with datepicker. --}}
            
            @if(($shipment->departure_date == NULL))
            <div class="input-group-text form-control p-0">
                <input class='form-control' type='text' id='datepicker' name='shipment_departure_date' placeholder="YYYY-MM-DD" value='{{ $shipment->departure_date }}'><br/>
                </div>
            @else
                <input class='form-control' type='text' name='shipment_departure_date' value='{{ $shipment->departure_date }}' readonly><br/>
            @endif
            
        </div>

        <h3 class='shipment_label'>Estimated Arrival Date: </h3>
        <div class="input-group mr-sm-2 mb-2">
            <div class="input-group-prepend">
                <div class="input-group-text"><span data-feather="calendar"></div>
            </div>
            <input class='form-control' type='text' name='shipment_estimated_date' value='{{ $shipment->estimated_arrival_date }}' readonly><br/>
        </div>

        <h3 class='shipment_label'>Actual Arrival Date: </h3>
        <div class="input-group mr-sm-2 mb-2 p-0">
            <div class="input-group-prepend">
                <div class="input-group-text"><span data-feather="calendar"></div>
            </div>
            
            <div class="input-group-text form-control p-0">
                <input class='form-control' type='text' id='datepicker' name='shipment_actual_date' value='{{ $shipment->actual_arrival_date }}'><br/>
            </div>
            
        </div>

        <h3 class='shipment_label'>Vendor:</h3>
        <div class="input-group mr-sm-2 mb-2">
            <div class="input-group-prepend">
                <div class="input-group-text"><span data-feather="map-pin"></div>
            </div>
            <input class='form-control' type='text' name='shipment_vendor' value='{{ $shipment->vendor->name }}' readonly><br/>
        </div>

        @php $couriers = App\Courier::all() @endphp
        @php $courierLength = sizeof($couriers) @endphp

        <h3 class='shipment_label'>Courier:</h3>
        <div class="input-group mr-sm-2 mb-2">
            <select class="custom-select mr-sm-2 mb-2" name="courier">
            @if(($shipment->courier->id) == 1)
                <option value='1' selected readonly>Please Select Courier</option>
                @for ($id = 2; $id <= $courierLength; $id++)
                    <option value="{{$id}}"> {{$couriers->find($id)->name}} </option>
                @endfor
            @else 
                <option value="{{$shipment->courier->id}}" selected readonly>{{$shipment->courier->name}} -- Current</option>
                @for ($id = 2; $id <= $courierLength; $id++)
                   <option value="{{$id}}">{{$couriers->find($id)->name}} </option>
                @endfor
            @endif
            </select>
        </div>

        <h3 class='shipment_label'>Tracking #:</h3>
        <div class="input-group mr-sm-2 mb-2">
            <div class="input-group-prepend">
                <div class="input-group-text"><span data-feather="map"></i></div>
            </div>
            <input class='form-control' type='text' name='shipment_tracking' value='{{ $shipment->tracking }}'><br/>
        </div>
        
        <h3 class='shipment_comments'>Comments: </h3>
        <textarea class='form-control' type='text' name='shipment_detail' readonly>{{ $shipment->comments }}</textarea><br/>

        <button type="submit" class="btn btn-info btn-sm mb-2" style="width: 25%">Save Information</button>
    </form>

    <hr/>

    <h2>Shipment Cart Information</h2>
    <div class="table-responsive">
        <table id="cartTable" class="table table-dark table-bordered" id="shipment-cart-table">
            <thead>
                <tr>
                    <th scope="col"><span data-feather="hash"></span> Product ID</th>
                    <th scope="col"><span data-feather="align-justify"></span> Product Description</th>
                    <th scope="col"><span data-feather="list"></span> Quantity</th>
                    <th scope="col"><span data-feather="type"></span> Unit</th>
                    {{-- <th scope="col"><span data-feather="truck"></span> Vendor</th> --}}
                    <th scope="col"><span data-feather="plus"></span> Remarks</th>
                    <th scope="col"><span data-feather="navigation"></span> Invoice Number</th>
                    <th scope="col"><span data-feather="message-square"></span> Comments</th>
                    <th scope="col"><span data-feather="user-check"></span> Employee</th>
                    <th scope="col"><span data-feather="anchor"></span> Activity</th>
                </tr>
            </thead>

            @php $carts = App\ShipmentCart::all()->where('shipment_id', '=', $shipment->id) @endphp
            <tbody>
            @foreach($carts as $cart)
                <tr id='cart_product1'>
                    <td>{{$cart->product_id}}</td>
                    <td>{{$cart->item_name}}</td>
                    <td>{{$cart->qty}}</td>
                    <td>{{$cart->unit}}</td>
                    {{-- <td>{{$cart->vendor}}</td> --}}
                    <td>{{$cart->vendor_remarks}}</td>
                    <td>{{$cart->invoice}}</td>
                    <td>{{$cart->comments}}</td>
                    <td>{{$cart->employee->name}}</td>
                    <td>
                    @auth
                        @if(Auth::user()->role->id == 1)
                            <button class="btn btn-primary btn-sm btn-action mb-1"
                                data-toggle="modal"
                                data-target="#editCartModal"
                                data-id="{{ $cart->product_id }}" 
                                data-name="{{ $cart->item_name}}"
                                data-qty="{{$cart->qty}}"
                                data-unit="{{$cart->unit}}"
                                data-shipment="{{$cart->shipment->id}}"
                                data-remarks="{{$cart->vendor_remarks}}"
                                data-invoice="{{$cart->invoice}}"
                                data-comments="{{$cart->comments}}"
                                data-url="{{route('shipment-cart.edit', $cart->id )}}">Edit
                            </button>

                            <form class="form-inline" method="POST" action="{{route('shipment-cart.delete', $cart->id)}}" enctype="multipart/form-data">
                                <input type="hidden" name="_method" value="POST">
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-primary btn-sm btn-action mb-1">Delete</button><br>
                            </form>
                        @endif
                    @endauth
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <br/>

    @auth
        @if(Auth::user()->role->id == 1)
            <button class="btn btn-info btn-sm mb-1" style="width: 25%"
                data-toggle="modal"
                data-target="#AddProductToCartModal"
                data-shipment="{{$shipment->id}}"

                data-url="{{route('shipment-cart.create')}}">Add Product
            </button>
        @endif
    @endauth

    <button class="btn btn-info btn-sm mb-1" style="width: 25%"
        data-toggle="modal"
        data-target="#importPOModal"
        data-shipment="{{$shipment->id}}"
        data-po="{{$shipment->purchase_order}}"

        data-url="{{route('shipment-cart.importPO')}}">Import PO Items
    </button>


    <form method="POST" action="{{route('shipment-cart.clear', $shipment->id)}}" enctype="multipart/form-data">
        <input type="hidden" name="_method" value="POST">
        {{ csrf_field() }}
        <button type="submit" class="btn btn-info btn-sm mb-1" style="width: 25%">Clear</button>
    </form>


    <br />
    <p>*To use Import PO Items function above make sure that integrations with Purchase Orders have been done.</p>

    <hr/>

    <h2>History and Comments</h2>
        <div class="table-responsive">
            <table id="historyTable" class="table table-dark table-bordered">
                <tr>   
                    <th><span data-feather="user-check"></span> Employee</th>
                    <th><span data-feather="activity"></span> Status</th>
                    <th><span data-feather="message-square"></span> Comments</th>
                    <th><span data-feather="calendar"></span> Date</th>
                </tr>
                @foreach($shipment_histories as $history)
                    <tr>
                        <td>{{$history->employee->name}}</td>
                        <td>{{$history->status->name}}</td>
                        <td>{{$history->comment}}</td>
                        <td>{{$history->created_at->format('D, M d, Y h:i A')}}</td>
                    </tr>
                @endforeach
            </table>    
        </div>

    @auth
        <form class="form-inline" method="POST" action="{{route('shipment-history.store')}}" enctype="multipart/form-data">
            {{ csrf_field() }}

                <div class="input-group mr-sm-2 mb-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><span data-feather="users"></i></div>
                    </div>
                    <input type="text" class="form-control" value="{{Auth::user()->name}}" disabled>
                    <input type="hidden" name="employee" value="{{Auth::user()->id}}">
                    <input type="hidden" name="shipment" value="{{$shipment->id}}">
                </div>

                <select class="custom-select mr-sm-2 mb-2" name="status" required>
                    <option value="{{$shipment->status->id}}">{{$shipment->status->name}} - Current</option>
                    @foreach($statuses as $status)
                        <option value="{{$status->id}}">{{$status->name}}</option>
                    @endforeach
                </select>

                <input type="text" class="form-control mb-2 mr-sm-2" placeholder="Enter Comment..." name="comment">
              
                <button type="submit" class="btn btn-info mb-2">Submit</button>
        </form>
    @endauth   

    <hr/>

    {{-- Easy Navigation Button --}}
    <div class="btn-group mr-2">
        <a href="{{route('shipment.show')}}"><button class="btn btn-sm btn-info" >Back to Shipment List</button></a>
    </div>

    <hr/>

</div>

    {{-- Edit Cart Modal --}}
    <div class="modal fade" id="editCartModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="POST">
                	{{ csrf_field() }}
                    <input type="hidden" name="_method" value="PATCH">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Edit Cart</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>

					<div class="modal-body">
						<label>Product ID:</label>
                    	<input class="form-control" type="text" name="id" id="id" readonly><br/>

						<label>Product Name:</label>
                    	<input class="form-control" type="text" name="name" id="name" readonly><br/>

						<label>Ordered Quantity:</label>
                    	<input class="form-control" type="text" name="qty" id="qty"><br/>

                    	<label>Ordered Unit:</label>
                    	<input class="form-control" type="text" name="unit" id="unit"><br/>

                        @php $shipments = App\Shipment::all() @endphp
                        <label>Select Shipment:</label>
                        <select class="custom-select mr-sm-2 mb-2" name="shipment" id="shipment" required>
                            @foreach($shipments as $shipment)
                                @if ($shipment->isActive)
                                    <option id="shipment_{{$shipment->id}}" value="{{$shipment->id}}">{{$shipment->id}}</option>
                                @endif
                            @endforeach
                        </select>
                        <br/>

                        <label>Remarks:</label>
                        <input class="form-control" type="text" name="remarks" id="remarks"><br/>

                    	<label>Invoice:</label>
                    	<input class="form-control" type="text" name="invoice" id="invoice"><br/>

						<label>Comments:</label>
                    	<textarea class="form-control" type="text" name="comments" id="comments"></textarea><br/>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>

						<button type="submit" class="btn btn-info">Save</button>
					</div>
        		</form>
            </div>
    	</div>
    </div>

    {{-- Add Product to Cart Modal --}}
    <div class="modal fade" id="AddProductToCartModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="POST">
                	{{ csrf_field() }}
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Add Product Into Cart</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>

					<div class="modal-body">
                        <label>Product ID:</label>
                        <div class="input-group-append">
                    	    <input class="form-control" type="text" name="id" id="modal_id">
                            <button class="btn btn-outline-secondary" type="button" onclick="autofill()"><span data-feather="align-left"></span></button>
                        </div><br/>

						<label>Product Name:</label>
                    	<input class="form-control" type="text" name="name" id="modal_name"><br/>

						<label>Quantity:</label>
                    	<input class="form-control" type="text" name="qty" id="qty"><br/>

                    	<label>Unit:</label>
                    	<select class="custom-select mr-sm-2 mb-2" name="unit" id="unit" required>
                            <option value="piece">Piece</option>
                            <option value="package">Package</option>
                            <option value="meter">Meter</option>
                            <option value="foot">Foot</option>
                            <option value="1/2lb">1/2LB</option>
                            <option value="box">Box</option>
                            <option value="roll">Roll</option>
                            <option value="winpos unit">Winpos Unit</option>
                        </select><br/>

                        <label>Select Shipment:</label>
                        <input class="form-control" type="text" name="shipment" id="shipment" readonly><br/>

                    	<label>Invoice:</label>
                    	<input class="form-control" type="text" name="invoice" id="invoice"><br/>

						<label>Comments:</label>
                    	<textarea class="form-control" type="text" name="comments" id="comments"></textarea><br/>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>

                        <button type="submit" class="btn btn-primary">Add Product To Cart</button>
					</div>
        		</form>
            </div>
    	</div>
    </div>


    {{-- Insert PO to  Modal --}}
    <div class="modal fade" id="importPOModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="POST">
                	{{ csrf_field() }}
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Please Confirm:</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>

					<div class="modal-body">

                        <label>Shipment:</label>
                        <input class="form-control" type="text" name="shipment" id="shipment" readonly><br/>

                    	<label>Purchase Order #:</label>
                    	<input class="form-control" type="text" name="po" id="po"><br/>

					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>

                        <button type="submit" class="btn btn-primary">Proceed</button>
					</div>
        		</form>
            </div>
    	</div>
    </div>

@endsection

@push('bottom-scripts')
    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"> </script>

    <script>
        $('#datepicker').datepicker({
            format: 'yyyy-mm-dd',
            uiLibrary: 'bootstrap4'
        });
    </script>

    <script>
    $(document).ready(function () {
        $('#editCartModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var id = button.data('id') // Extract info from data-* attributes
            var name = button.data('name') // Extract info from data-* attributes
            var qty = button.data('qty')
            var unit = button.data('unit')
            var shipment = button.data('shipment')
            var invoice = button.data('invoice')
            var remarks = button.data('remarks')
            var comments = button.data('comments')
            var url = button.data('url')
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('.modal-title').text('Edit Cart')
            modal.find('#id').val(id)
            modal.find('#name').val(name)
            modal.find('#qty').val(qty)
            modal.find('#unit').val(unit)
            modal.find('#shipment').val(shipment)
            $('#shipment_'+shipment).attr('selected',true)
            modal.find('#remarks').val(remarks)
            modal.find('#invoice').val(invoice)
            modal.find('#comments').val(comments)
            modal.find('form').attr('action',url)
        });
    });
    </script>

    {{-- Add Product to Cart Modal --}}
    <script>
        $(document).ready(function () {
            $('#AddProductToCartModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var shipment = button.data('shipment') // Extract info from data-* attributes
                var url = button.data('url')

                var modal = $(this)
                modal.find('.modal-title').text('Add Product to Shipment #: ' + shipment)
                modal.find('#shipment').val(shipment)
                modal.find('form').attr('action',url)
            });
        });
    </script>

    {{-- Import PO Items Modal --}}
    <script>
        $(document).ready(function () {
            $('#importPOModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var shipment = button.data('shipment') // Extract info from data-* attributes
                var po = button.data('po')
                var url = button.data('url')

                var modal = $(this)
                modal.find('.modal-title').text('Import PO Items to Shipment # ' + shipment)
                modal.find('#shipment').val(shipment)
                modal.find('#po').val(po)
                modal.find('form').attr('action',url)
            });
        });
    </script>

    <script>
        function autofill(){
            var pid = $('#modal_id').val();
            console.log(pid);
            $.ajax({
                type: "get",
                url: "{{ route('product.autofill') }}",
                data: {pid: pid},
                success: function( msg ) {
                    if(msg.status == "success"){
                        $('#modal_name').val(msg.name); 
                    }else{
                        alert(msg.message);
                    }
                },
                error: function (request, status, error) {
                    alert(request.status + ':' + error + '. Please take picture and contact DevTeam.');
                }
            });
        };
    </script>

@endpush