<tr>
    <td class="header">
        <a href="{{ $url }}">
            {{ $slot }}
        </a>
        <div id="company-info">
            <p>4131 Fraser Street Vancouver BC Canada V5V 4E9</p>
            <p><a href="tel:6048751993">(604) 875 - 1993</a> | <a href="mailto:support@leeselectronic.com">support@leeselectronic.com</a></p>
        </div>
    </td>
</tr>
