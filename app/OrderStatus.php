<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
    protected $fillable = [
        'name','color','position'
    ];
    public function orders() {
        return $this->hasMany(Order::class);
    }

    public static function getStatusName($status_id) {
        $query = static::where('id',$status_id)->first();

        $name = $query->name;

        return $name;
    }

    public static function getStatusColor($status_id) {
        $query = static::where('id',$status_id)->first();

        $color = $query->color;

        return $color;
    }
}
