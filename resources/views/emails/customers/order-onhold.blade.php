@component('mail::message')
Hello {{$order->name}},

@if($order->type == "backorder")
Your backorder has been put on hold.
@elseif($order->type == "phone")
Your phone order has been put on hold.
@elseif($order->type == "special")
Your special order has been put on hold.
@endif

Order #{{$order->id}} has been put on hold. If we have not contact you already, please call us at (604) 875-1993 for more information.

@component('mail::table')
| PID        | Name           | Price per Unit  | Quantity | 
| ---------- |:--------------:| :--------------:| :------: |
@foreach($order->products as $product)
| {{$product->id_product}} | {{$product->pivot->name}} | ${{$product->pivot->price}} | {{$product->pivot->quantity}} |
@endforeach
@endcomponent

Cheers,<br>
Lee's Electronic Components

@endcomponent
