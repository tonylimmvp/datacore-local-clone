@php

function convertBoStatus($status_id) {
    if($status_id == 0) {
        return "Created";
    }else if($status_id == 1) {
        return "Processing";
    }else if($status_id == 2) {
        return "Arrived";
    }else if($status_id == 3) {
        return "Done";
    }else if($status_id == 4) {
        return "Called";
    }else{
        return "NA";
    }
}

function boStatusColor($status_id) {
    if($status_id == 0) {
        return "#cebcbf";
    }else if($status_id == 1) {
        return "#e8961a";
    }else if($status_id == 2) {
        return "#6678b1";
    }else if($status_id == 3) {
        return "#0a1bea";
    }else if($status_id == 4) {
        return "#fa0057";
    }else{
        return "NA";
    }
}

function convertSpoStatus($status_id) {
    if($status_id == 0) {
        return "Created";
    }else if($status_id == 1) {
        return "Quote";
    }else if($status_id == 2) {
        return "Not Paid";
    }else if($status_id == 3) {
        return "Paid";
    }else if($status_id == 4) {
        return "Processing";
    }else if($status_id == 5) {
        return "Shipped";
    }else if($status_id == 6) {
        return "Arrived";
    }else if($status_id == 7) {
        return "Done";
    }else if($status_id == 8) {
        return "Called";
    }else{
        return "NA";
    }
}



function spoStatusColor($status_id) {
    if($status_id == 0) {
        return "#cebcbf";
    }else if($status_id == 1) {
        return "#bac";
    }else if($status_id == 2) {
        return "#ad0101";
    }else if($status_id == 3) {
        return "#00ba18";
    }else if($status_id == 4) {
        return "#e8961a";
    }else if($status_id == 5) {
        return "#4b01ad";
    }else if($status_id == 6) {
        return "#6678b1";
    }else if($status_id == 7) {
        return "#0a1bea";
    }else if($status_id == 8) {
        return "#fa0057";
    }else{
        return "NA";
    }
}

@endphp