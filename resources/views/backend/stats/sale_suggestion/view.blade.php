@extends('backend.layouts.main')

@push('top-scripts')
<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
@endpush

@push('css')
<style>
    h1 {
        text-align: center;
        font-family: 'Nunito', sans-serif;
        font-weight: 200;
    }

    h2 {
        font-family: 'Nunito', sans-serif;
        font-weight: 200; 
        text-align: center;
    }

    h3 {
        font-family: 'Nunito', sans-serif;
        font-weight: 200; 
        font-size: 20px;
        text-align: center;
    }
    
    .product-image-thumbnail {
        display: block;
        width: 100px;
        height: auto;
    }

    td img {
        display: block;
        margin-left: auto;
        margin-right: auto;
    }
</style>
@endpush

@section('content')

    <br/>
    <h1>Sale Promotions Suggestion View Page</h1>
    <hr/>

    <div class="table-responsive">

        <table class="table table-hover table-dark table-bordered" width="100%">
            <thead>
                <tr>
                    <th scope="col"><span data-feather="hash"></span> Product ID</th>
                    <th scope="col"><span data-feather="cart"></span> Vendor</th>
                    <th scope="col"><span data-feather="dollar-sign"></span> Price</th>
                    <th scope="col"><span data-feather="dollar-sign"></span> Cost</th>
                    <th scope="col"><span data-feather="up-arrow"></span> SRP Margin</th>
                
                    <th scope="col"><font color="#00cc66"><span data-feather="down-arrow"></span> Max Sale Percentage</font></th>
                    <th scope="col"><font color="#00cc66"><span data-feather="dollar-sign"></span> Max Adjusted Price</font></th>
                    <th scope="col"><font color="#00cc66"><span data-feather="check"></span> Profit Margin</font></th>              
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{$suggestion->product_id}}</td>
                    <td>{{$suggestion->vendor->name}}</td>
                    <td>${{$price = $suggestion->price}}</td>
                    <td>${{$cost = $suggestion->cost}}</td>
                    <td>{{$srp = (number_format((($price-$cost)/$price)*100, 0))}}%</td>

                    <td style="display:none">{{$discInDecimal = $srp / 100}}</td>

                    <td><font color="#00cc66">{{$srp}}%</font></td>
                    <td><font color="#00cc66">${{$discountedPrice = number_format($price - ($discInDecimal * $price), 2)}}<font></td>
                    <td><font color="#00cc66">{{(number_format((($discountedPrice-$cost)/$discountedPrice)*100, 0))}}%</font></td>
                </tr>
            </tbody>
        </table>
    </div>
    <hr/>

    <h1>Price Breaks Suggestion</h1>
    <hr/>

    <h3>Please Input Desired Quantity for Regular and Wholesale Price Breaks</h3>
    <div class="container" style="text-align:center">
        <form method="POST" action="{{route('sale-suggestion.storeQty', $suggestion)}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group input-group-append">
                <input type="text" class="form-control" id="reg_qty1" name="reg_qty1" placeholder="Quantity for Regular Sale 1" value="{{old('reg_qty1')}}">
                <input type="text" class="form-control" id="reg_qty2" name="reg_qty2" placeholder="Quantity for Regular Sale 2" value="{{old('reg_qty2')}}">
                <input type="text" class="form-control" id="reg_qty3" name="reg_qty3" placeholder="Quantity for Regular Sale 3" value="{{old('reg_qty3')}}">
            </div>
            <div class="form-group input-group-append">
                <input type="text" class="form-control" id="who_qty1" name="who_qty1" placeholder="Quantity for Wholesale Sale 1" value="{{old('who_qty1')}}">
                <input type="text" class="form-control" id="who_qty2" name="who_qty2" placeholder="Quantity for Wholesale Sale 2" value="{{old('who_qty2')}}">
                <input type="text" class="form-control" id="who_qty3" name="who_qty3" placeholder="Quantity for Wholesale Sale 3" value="{{old('who_qty3')}}">
            </div>

            <button type="submit" class="btn btn-info btn-sm ml-2" style="width: 25%">Save Quantity Information</button>

            @include('layouts.errors')
        </form>
    <hr/>
    </div>


    <h2>Regular [20,40,60]</h2>
    @php $breaks = App\SalePriceBreak::where('product_id', $suggestion->product_id)->first(); @endphp
    <div class="table-responsive">
        <table class="table table-hover table-dark table-bordered" width="100%">
            <thead>
                <tr>
                    <th scope="col"> Quantity 1</th>
                    <th scope="col"> Sale Suggestion 1</th>
                    <th scope="col"> Quantity 2</th>
                    <th scope="col"> Sale Suggestion 2</th>
                    <th scope="col"> Quantity 3</th>
                    <th scope="col"> Sale Suggestion 3</th>
                </tr>
            </thead>
            <tbody>
            @if($breaks)
                <tr class="" data-href="{{url('/')}}">
                    <td>{{$breaks->regular_qty1}}+</td>
                    <td>{{$breaks->regular_suggestion1}}</td>
                    <td>{{$breaks->regular_qty2}}+</td>
                    <td>{{$breaks->regular_suggestion2}}</td>
                    <td>{{$breaks->regular_qty3}}+</td>
                    <td>{{$breaks->regular_suggestion3}}</td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>

    <h2>Wholesale [40,60,80]</h2>
    <div class="table-responsive">
        <table class="table table-hover table-dark table-bordered" width="100%">
            <thead>
                <tr>
                    <th scope="col"> Quantity 1</th>
                    <th scope="col"> Sale Suggestion 1</th>
                    <th scope="col"> Quantity 2</th>
                    <th scope="col"> Sale Suggestion 2</th>
                    <th scope="col"> Quantity 3</th>
                    <th scope="col"> Sale Suggestion 3</th>
                </tr>
            </thead>
            <tbody>
            @if($breaks)
                <tr class="" data-href="{{url('/')}}">
                    <td>{{$breaks->wholesale_qty1}}+</td>
                    <td>{{$breaks->wholesale_suggestion1}}</td>
                    <td>{{$breaks->wholesale_qty2}}+</td>
                    <td>{{$breaks->wholesale_suggestion2}}</td>
                    <td>{{$breaks->wholesale_qty3}}+</td>
                    <td>{{$breaks->wholesale_suggestion3}}</td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
    <hr/>

    <div class="container" style="text-align:center">
        <form method="POST" action="{{route('sale-suggestion.calculateBreaks', $suggestion->id)}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <button type="submit" class="btn btn-info btn-sm ml-2" style="width: 25%">Calculate Breaks</button>
            @include('layouts.errors')
        </form>
    </div>

@endsection

@push('bottom-scripts')
    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"> </script>

    <script>
        jQuery(document).ready(function($) {
            $(".clickable-row").click(function() {
                window.location = $(this).data("href");
            });
        });
    </script>
@endpush