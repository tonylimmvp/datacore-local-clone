<?php

namespace App\Http\Controllers;

use App\EmployeeRole;
use App\Permission;
use Illuminate\Http\Request;

class EmployeeRoleController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
        $roles = EmployeeRole::all();
        return view('backend.employee_roles.index',compact('roles'));
    }

    public function store(Request $request)
    {   
        $this->validate(request(), [
            'name' => 'required|unique:employee_roles',
        ]);

        $employee_role = new EmployeeRole();
        $employee_role->name = $request->name;
        $employee_role->save();
        
        $permissions = Permission::all();
        foreach($permissions as $permission){
            $employee_role->permissions()->attach($permission->id, [
                'create' => 0,
                'read' => 0,
                'update' => 0,
                'delete' => 0,
                ]);
        }

        session()->flash('message','You have successfully created ' . $employee_role->name . '. Please select permissions.');

        return redirect()->route('coreui.employee-role-permission.edit',$employee_role->id);
    }

    public function update(Request $request, EmployeeRole $employee_role)
    {
        $this->validate(request(), [
            'name' => 'required|unique:employee_roles',
        ]);

        $employee_role->name = $request->name;

        $employee_role->save();
        
        session()->flash('message','You have successfully updated the employee role to ' . $employee_role->name);

        return redirect()->route('coreui.employee-role',$employee_role->id);
    }

    public function destroy(EmployeeRole $employee_role)
    {
        //
    }
}
