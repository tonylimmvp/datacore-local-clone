<?php

namespace App\Http\Controllers;

use App\EmployeePermission;
use App\Employee;
use App\Permission;
use Illuminate\Http\Request;

class EmployeePermissionController extends Controller
{

    public function edit(Employee $employee)
    {

        return view('backend.employees.permissions.index',compact('employee'));
    }

    public function update(Request $request, Employee $employee)
    {

        $permissions = Permission::all();

        foreach($permissions as $i => $permission){
            $create = 0;
            if($request->create){
                foreach($request->create as $name){
                    if("$permission->id" == "$name"){
                        $create = 1;
                    }
                }
            }

            $read = 0;
            if($request->read){
                foreach($request->read as $name){
                    if("$permission->id" == "$name"){
                        $read = 1;
                    }
                }
            }

            $update = 0;
            if($request->update){
                foreach($request->update as $name){
                    if("$permission->id" == "$name"){
                        $update = 1;
                    }
                }
            }

            $delete = 0;
            if($request->delete){
                foreach($request->delete as $name){
                    if("$permission->id" == "$name"){
                        $delete = 1;
                    }
                }
            }

            $employee->permissions()->updateExistingPivot($permission->id,['create'=>$create , 'read'=>$read, 'update'=>$update, 'delete'=>$delete]);


        }

        // dd($employee->permissions);

        session()->flash('message','You have successfully updated ' . $employee->name . ' permissions.');

        return redirect()->route('coreui.employee-permission.edit',$employee->id);

    }

    public function destroy(EmployeePermission $employeePermission)
    {
        
    }
}
