<?php

namespace App\Http\Controllers;

use App\WorkShift;
use App\Employee;
use Illuminate\Http\Request;

class WorkShiftController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.shifts.index');
    }

    /**
     *  CUSTOM CODE
     */

    public function addEmployeeShift(Request $request){
        // Validate form
        $this->validate(request(), [
            'employee' => 'required',
            'shift' => 'required',
        ]);
        
        $employee = Employee::find($request->employee);
        $employee->shifts()->attach($request->shift);

        return back();
    }

    public function removeEmployeeShift(Request $request){
        // Validate form
        $this->validate(request(), [
            'employee' => 'required',
            'shift' => 'required',
        ]);

        $employee = Employee::find($request->employee);
        $employee->shifts()->detach($request->shift);       

        return back();
    }
}
