<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoreSupply extends Model
{
    protected $table='store_supplies';
    protected $connection='mysql';
    protected $guarded = [];
}
