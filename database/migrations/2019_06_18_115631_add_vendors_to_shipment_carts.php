<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVendorsToShipmentCarts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shipment_carts', function (Blueprint $table) {
            $table->string('vendor')->after("unit")->nullable();
            $table->string('vendor_remarks')->after("vendor")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shipment_carts', function (Blueprint $table) {
            $table->dropColumn(['vendor_remarks']);
            $table->dropColumn(['vendor']);
        });
    }
}
