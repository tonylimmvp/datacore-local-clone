@extends('frontend.layouts.main')

@push('css')

@endpush

@section('content')

<script src='https://libs.na.bambora.com/customcheckout/1/customcheckout.js'></script>

<div class="container">
  <h1>Bambora Payment Portal</h1>
    <form class="form" id="checkout-form" action="{{route('pay')}}" method="POST">
        {{csrf_field()}}
        <div class="form-group">
            <label for="card_holder" id="card-number-error">Card Holder</label>
            <input type="text" class="form-control" id="card-holder" name="card_holder" placeholder="Full Name" value="{{old('card_holder')}}" required/>
            <small id="holderHelp" class="form-text text-muted">Full name on credit card</small>
        </div>
        <div class="form-group">
            <label for="card_number" id="card-number-error">Card Number</label>
            <div class="input-group">
                <div class="input-group-prepend cursor-pointer" onclick="changeCardFormat()">
                    <span class="input-group-text"><i data-feather="credit-card"></i></span>
                    <input type="hidden" name="card_type" value="{{old('card_type'),"0"}}">
                    <span id="card-type" class="input-group-text">@if(old('card_type'))@if(old('card_type')==1) AMEX @else VISA/MC @endif @else VISA/MC @endif</span>
                </div>
                <input class="form-control" id="card-number" name="card_number" placeholder="Valid Card Number" value="{{old('card_number')}}" required/>
                <div class="input-group-append">
                    <span class="input-group-text"><i data-feather="lock"></i></span>
                </div>
            </div>
            <small id="cardHelp" class="form-text text-muted">Visa, Mastercard, and AMEX</small>
        </div>
        
        <div class="row">
        <div class="form-group col-9 col-sm-6">
            <label class="control-label" for="expiry_month">Expiration Date</label>
            <div class="row">
            <div class="col-6">
                <select class="form-control" name="expiry_month" id="expiry-month" required>
                    <option @if(!old('expiry_month')) selected @endif disabled>MM</option>
                    @for($month=1;$month<=12;$month++)
                    <option value="{{$month}}" @if(old('expiry_month') == $month) selected @endif>
                        {{ date("M", mktime(0, 0, 0, $month, 10)) }} ({{ date("m", mktime(0, 0, 0, $month, 10)) }})
                    </option>
                    @endfor
                </select>
            </div>
            <span class="align-self-center">/</span>
            <div class="col-5">
                <select class="form-control" name="expiry_year" required>
                    <option @if(!old('expiry_year')) selected @endif disabled>YY</option>
                    @php $year_current = Carbon\Carbon::now()->year; @endphp
                    @for($year=$year_current;$year<=$year_current+10;$year++)
                    <option value="{{$year}}" @if(old('expiry_year') == $year) selected @endif>
                        {{$year}}
                    </option>
                    @endfor
                </select>
            </div>
        </div>
        </div>

        <div class="form-group col-3 col-sm-6">
            <label for="cvv" id="cvv-error">CVV</label>
            <input id="cvv" name="cvv" class="form-control col-xs-6" value="{{old('cvv')}}" size="3" min-length="3" required/>
        </div>

        </div>
        <div class="row">
            <div class="form-group col-sm-6">
                <label for="order_number" id="order-number-error">Order Number</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i data-feather="hash"></i></span>
                    </div>
                    <input type="number" class="form-control" id="order-number" name="order_number" placeholder="WinPOS Order Number" value="{{old('order_number')}}" required/>
                </div>
            </div>
            <div class="form-group col-sm-6">
                <label for="amount" id="amount-error">Amount</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i data-feather="dollar-sign"></i></span>
                    </div>
                    <input type="number" class="form-control" id="amount" name="amount" placeholder="Amount" value="{{old('amount')}}" min="0" step="0.01" required/>
                </div>
            </div>
        </div>

        <hr>

        {{-- Billing Information --}}
        <h2>Billing Information</h2>

        <div class="row">
            <div class="form-group col-12">
                <label for="address">Address</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><span data-feather="home"></span></div>
                    </div>
                    <input type="text" class="form-control" id="address" name="address" placeholder="E.g 4131 Fraser Street" value="{{ old('address') }}" required>
                </div>
                <div class="input-group mt-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><span data-feather="hash"></span></div>
                    </div>
                    <input type="text" class="form-control" id="address2" name="address2" placeholder="E.g Unit 101 <optional>" value="{{ old('address2') }}">
                </div>
            </div>
            
        </div>

        <div class="row">
            <div class="form-group col-6 col-md-4">
                <label for="city">City</label>
                <div class="input-group">
                    <input type="text" class="form-control" id="city" name="city" placeholder="E.g Vancouver" value="{{ old('city') }}" maxlength="36" required>
                </div>
            </div>
            <div class="form-group col-6 col-md-2">
                <label for="province" >Province/State</label>
                <div class="input-group">
                    <input type="text" class="form-control" id="province" name="province" placeholder="--" value="{{ old('province') }}" maxlength="2" required>
                </div>
            </div>
            <div class="form-group col-6 col-md-3">
                <label for="country">Country</label>
                <div class="input-group">
                    <select class="form-control" id="country" name="country" required>
                        <option value="CA">Canada</option>
                    </select>
                </div>
            </div>
            <div class="form-group col-6 col-md-3">
                <label for="postal">Postal Code</label>
                <div class="input-group">
                    <input type="text" class="form-control" id="postal_code" name="postal_code" placeholder="E.g V5V 4E9" value="{{ old('postal_code') }}" maxlength="7" required>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-sm-6">
                <label for="phone">Phone Number</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i data-feather="phone"></i></span>
                    </div>
                    <input type="number" class="form-control" id="phone" name="phone" placeholder="E.g. 6048751993" value="{{old('phone')}}" required/>
                </div>
            </div>
            <div class="form-group col-sm-6">
                <label for="email" id="email-error">Email Address</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i data-feather="mail"></i></span>
                    </div>
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" value="{{old('email')}}" required/>
                    
                </div>
                <small>This email is for customer E-receipt</small>
            </div>
        </div>
        
        <div class="form-group">
            <input id="pay-button" type="submit" class="btn btn-primary" value="Pay" />
        </div>

    </form>
    <hr>
    @include('layouts.errors')

</div>

@endsection

@push('bottom-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.1.60/inputmask/jquery.inputmask.js"></script>
<script>
    $(document).ready(function(){
        if($('input[name="card_type"]').val() == "1"){
            $('#card-number').inputmask('9999 999999 99999');
        }else{
            $('#card-number').inputmask('9999 9999 9999 9999');
        }
        $('#cvv').inputmask('9{1,4}');
        $('#province').inputmask('a{1,2}')
    });

    function changeCardFormat(){
        if($('#card-type').html() == "VISA/MC" || $('#card-type').html() == " VISA/MC "){
            $('input[name="card_type"]').val('1')
            $('#card-type').html('AMEX')
            $('#card-number').inputmask('9999 999999 99999');
        }else{
            $('input[name="card_type"]').val('0')
            $('#card-type').html('VISA/MC')
            $('#card-number').inputmask('9999 9999 9999 9999');
        }
    }

    $("#checkout-form").submit(function( event ) {
        openLoader();
    });
</script>

@endpush