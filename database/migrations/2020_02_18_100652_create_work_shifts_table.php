<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkShiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_shifts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('day',100)->nullable();
            $table->string('start_time',50)->nullable();
            $table->string('end_time',50)->nullable();
        });

        // Default Values
        App\WorkShift::create([
            'id' => '1',
            'day' => 'MONDAY',
            'start_time' => '09:00:00',
            'end_time' => '17:00:00'
        ]);

        App\WorkShift::create([
            'id' => '2',
            'day' => 'TUESDAY',
            'start_time' => '09:00:00',
            'end_time' => '17:00:00'
        ]);

        App\WorkShift::create([
            'id' => '3',
            'day' => 'WEDNESDAY',
            'start_time' => '09:00:00',
            'end_time' => '17:00:00'
        ]);

        App\WorkShift::create([
            'id' => '4',
            'day' => 'THURSDAY',
            'start_time' => '09:00:00',
            'end_time' => '17:00:00'
        ]);

        App\WorkShift::create([
            'id' => '5',
            'day' => 'FRIDAY',
            'start_time' => '09:00:00',
            'end_time' => '17:00:00'
        ]);

        App\WorkShift::create([
            'id' => '6',
            'day' => 'SATURDAY',
            'start_time' => '09:00:00',
            'end_time' => '17:00:00'
        ]);

        App\WorkShift::create([
            'id' => '7',
            'day' => 'MONDAY',
            'start_time' => '10:00:00',
            'end_time' => '18:00:00'
        ]);

        App\WorkShift::create([
            'id' => '8',
            'day' => 'TUESDAY',
            'start_time' => '10:00:00',
            'end_time' => '18:00:00'
        ]);

        App\WorkShift::create([
            'id' => '9',
            'day' => 'WEDNESDAY',
            'start_time' => '10:00:00',
            'end_time' => '18:00:00'
        ]);

        App\WorkShift::create([
            'id' => '10',
            'day' => 'THURSDAY',
            'start_time' => '10:00:00',
            'end_time' => '18:00:00'
        ]);

        App\WorkShift::create([
            'id' => '11',
            'day' => 'FRIDAY',
            'start_time' => '10:00:00',
            'end_time' => '18:00:00'
        ]);
        // End of initialization
        
        Schema::create('employee_work_shift', function (Blueprint $table) {
            $table->unsignedInteger('employee_id');
            $table->unsignedInteger('work_shift_id');
            $table->primary(['employee_id','work_shift_id']);

            $table->foreign('employee_id')->references('id')->on('employees');
            $table->foreign('work_shift_id')->references('id')->on('work_shifts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   
        Schema::dropIfExists('employee_work_shift');
        Schema::dropIfExists('work_shifts');
    }
}
