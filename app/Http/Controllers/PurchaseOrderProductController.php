<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PurchaseOrder;
use App\PurchaseOrderProduct;

class PurchaseOrderProductController extends Controller
{
    public function upload()
    {
        return view('backend.purchases.product-upload');
    }

    public function store(Request $request)
    {   
       
        $this->validate(request(), [
            'csv_file' => 'required|file|mimes:txt,csv,xlsx',
            'year' => 'required'
        ]);

        $year = $request->year;

        $V1_columns = array("PO Num",
                            "Rec Date",
                            "Item Code",
                            "Description",
                            "Qty PO",
                            "Qty Rec",
                            "Unit Cost",
                            "Qty*Unit Cost",
                            "Other Cost",
                            "Avg Item Cost",
                            "Total Item Cost"
                            );

        $path = $request->file('csv_file')->getRealPath();

        $handle = fopen($path, "r");

        $line_num = 0;
        $header = false;
        
        while ( (($data = fgetcsv($handle,0, ",","\"")) !== FALSE)) {
            // echo $data[0];
            if($data[0]==$V1_columns[0]){
                $header = true;
            }
            if($header){
                // Validate columns
                if($line_num == 0){

                    for($index=0 ; $index < count($V1_columns); $index++){

                        $data_token = $data[$index];
                        $header_token = $V1_columns[$index];
							// echo $data[$index];
						if(strcmp($data[$index], $V1_columns[$index]) != 0){
                            session()->flash('error','Header did not match, please consult the development team.');
                            return back();
                        }
                    }
     
                    $all_po = PurchaseOrder::whereRaw("year(created_at) = $year")->get();
                   
                    foreach($all_po as $po){
                        foreach($po->products as $product){
                            $product->delete();
                        }
                    }
                    
                }
                else{

                    $check = PurchaseOrder::whereRaw("year(created_at) = $year")->where('po_id', $data[0])->first();
                    
                    if($check){
                        $duplicate = PurchaseOrderProduct::where('po_id',$data[0])->where('product_id',$data[2])->where('name',$data[3])->first();

                        if($duplicate){
                            $po_qty = $duplicate->po_qty + $data[4];
                            $rec_qty = $duplicate->rec_qty + $data[5];
                            $unit_cost = $data[6];
                            $unit_qty_cost = $data[7];
                            $other_cost = $data[8];
                            $avg_cost = $data[9];
                            $total_cost = $data[10];
                        }else{
                            $po_qty = $data[4];
                            $rec_qty = $data[5];
                            $unit_cost = $data[6];
                            $unit_qty_cost = $data[7];
                            $other_cost = $data[8];
                            $avg_cost = $data[9];
                            $total_cost = $data[10];
                        }
                        
                        $po_product = PurchaseOrderProduct::updateOrCreate(
                            [
                                'po_id' => $data[0],
                                'product_id' => $data[2],
                                'name' => $data[3]
                            ],
                            [   
                                'po_qty' => $po_qty,
                                'rec_qty' => $rec_qty,
                                'unit_cost' => $unit_cost,
                                'unit_qty_cost' => $unit_qty_cost,
                                'other_cost' => $other_cost,
                                'avg_cost' => $avg_cost,
                                'total_cost' => $total_cost
                            ]
                        );
                    }

                }

                $line_num++;

            }
        }

        if(!$header){
            session()->flash('error','Could not find header, please check the file you uploaded.');
            return back();
        }

        session()->flash('message',"You have successfully imported/updated vendors");

        return back();
    }
    
}
