{{-- Setting up Variables --}}
@php

$SETTING_DEBUG_STOP_PRINT = 0;
$ALLOW_ACCESS = 0;

$printer_ip = "172.103.240.239";
$ITEM_NUMBER_OF_LABELS = 0;
$printer_port = 9101; // default is downstairs godex
$printerNumber = 2; //default printer is downstairs godex

@endphp

{{-- Validation - Check if ip is allowed to print --}}
@php
$accepted_ips = App\StoreIP::all();
foreach($accepted_ips as $ip){
    if($_SERVER['REMOTE_ADDR'] == $ip->ip_address){
        $ALLOW_ACCESS = 1;

        $printer_ip = $ip->ip_address;
    }
}

if($_SERVER['REMOTE_ADDR'] == "192.168.1.93" || $_SERVER['REMOTE_ADDR'] == "127.0.0.1" ){
    $ALLOW_ACCESS = 1;
    // $printer_ip = "208.110.116.114";
}

@endphp

{{-- Print Command --}}
@php
if($ALLOW_ACCESS == 1){
    // validate pid first
	if(isset($_GET['oid']) && isset($_GET['printer'])){
        $ORDER_ID = $_GET['oid'];

        $order = App\MobileOrder::find($ORDER_ID);

        /**************************************************************
        *
        *  Validate PID
        *
        ***************************************************************/

            if($order){
            
            /**************************************************************
            *
            *  Validate Printer Number
            *
            ***************************************************************/

            if(is_numeric($_GET['printer'])){
                $printerNumber = $_GET['printer'];
            }

            $printer = App\LabelPrinter::find($printerNumber);
            
            if($printer && $printer->isActive){

                $printer_port = $printer->port;
            
                /**************************************************************
                *
                *  Validate Number of labels
                *
                ***************************************************************/
                if(isset($_GET['num'])){
                    if(is_numeric($_GET['num'])){
                        $ITEM_NUMBER_OF_LABELS = $_GET['num'];
                    }
                }

                if($SETTING_DEBUG_STOP_PRINT==0){

                /**************************************************************
                *
                *  Start Print Settings by opening socket
                *
                ***************************************************************/

                    error_reporting(E_ALL);

                    echo "<h2>TCP/IP Connection</h2>\n";

                    /* Get the port for the WWW service. */
                    $service_port = getservbyname('www', 'tcp');

                    /* Get the IP address for the target host. */
                    $address = gethostbyname($printer_ip);

                    /* Create a TCP/IP socket. */
                    $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
                    if ($socket === false) {
                        echo "socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n";
                    } else {
                        echo "OK.\n";
                    }

                    //echo "Attempting to connect to '$address' on port '$service_port'...";
                    echo "Attempting to connect ...";
                    $result = socket_connect($socket, $address, $printer_port);
                    if ($result === false) {
                        echo "socket_connect() failed.\nReason: ($result) " . socket_strerror(socket_last_error($socket)) . "\n";
                    } else {
                        echo "OK.\n";
                    }
                    

                /**************************************************************
                *
                *  Godex / Small Label Print Setting
                *
                ***************************************************************/
                    
                    if($printer->type == "godex" && $printer->size == "small"){
                        //$in = "HEAD / HTTP/1.1\r\n";
                        //$in .= "Host: www.example.com\r\n";
                        //$in .= "Connection: Close\r\n\r\n";

                        $in = "^Q25,3,0\r\n";
                        $in .= "^W31\r\n";
                        $in .= "^H10\r\n";
                        $in .= "^P1\r\n";
                        $in .= "^S2\r\n";
                        $in .= "^AD\r\n";
                        $in .= "^C1\r\n";//number of labels
                        $in .= "^R0\r\n";
                        $in .= "~Q+0\r\n";
                        $in .= "^O0\r\n";
                        $in .= "^D0\r\n";
                        $in .= "^E12\r\n";
                        $in .= "~R255\r\n";
                        $in .= "^L\r\n";
                        $in .= "Dy2-me-dd\r\n";
                        $in .= "Th:m:s\r\n";
                        $in .= "Dy2-me-dd\r\n";
                        $in .= "Th:m:s\r\n";

                        $in .= "AD,20,8,1,1,0,0,Mobile #$order->order_id\r\n"; // Order Number
                        $in .= "AZ1,20,45,1,1,0,0,". $order->customer()->firstname . " " . $order->customer()->lastname . "\r\n"; // Order name
                        if(isset($order->customer()->phone)){
                            if($order->customer()->phone != "6048751993"){
                                $in .= "AZ1,20,60,1,1,0,0,".$order->customer()->phone."\r\n";
                            }
                        }
                       
                        $url = route("orders.mobile",$order->order_id);

                        $in .= "W140,90,5,2,M0,8,3,".strlen($url).",0\r\n";
                        $in .= $url ."\r\n";
                        $in .= "E\r\n";
                    }
                    
                /**************************************************************
                *
                *  Zebra Big Label Setting
                *
                ***************************************************************/
                    
                    else if ($printer->type == "zebra" && $printer->size == "large"){
                        //////////////
                        //basic command list
                        // ^FX is comment
                        // ^FO is placement of text  
                        // ^FD is data
                        // ^FS seperator
                        // 
                        
                        $in = "^XA\r\n";
                        $in .= "^PQ1\r\n";//number of labels
                        
                        $in .= "^FX size: 5.5cm 6.3cm\r\n";
                        $in .= "^FO100,25\r\n";
                        $in .= "^CFA,50\r\n";
                        $in .= "^FDMobile #".$order->order_id."\r\n";
                        $in .= "^FS\r\n";

                        $in .= "^FX Order Reference\r\n";
                        $in .= "^FO100,80\r\n";
                        $in .= "^CFA,35\r\n";
                        $in .= "^FD($order->reference)^FS\r\n";
                        
                        $in .= "^FX Customer Name\r\n";
                        $in .= "^FO100,120\r\n";
                        $in .= "^CFA,30\r\n";
                        $in .= "^FD". $order->customer()->firstname . " " . $order->customer()->lastname ."^FS\r\n";
                        
                        if(isset($order->customer()->company)){
                            $in .= "^FX Customer Company\r\n";
                            $in .= "^FO100,150\r\n";
                            $in .= "^CFA,30\r\n";
                            $in .= "^FD". $order->customer()->company ."^FS\r\n";

                            if(isset($order->customer()->phone)){
                                $in .= "^FX Customer Phone\r\n";
                                $in .= "^FO100,180";
                                $in .= "^FD".$order->customer()->phone."^FS\r\n";
                            }
                        }else{
                            if(isset($order->customer()->phone)){
                                $in .= "^FX Customer Phone\r\n";
                                $in .= "^FO100,150";
                                $in .= "^FD".$order->customer()->phone."^FS\r\n";
                            }
                        }      
                        
                        $in .= "^FX QR CODE\r\n";
                        $in .= "^FT300,500\r\n";
                        $in .= "^BQN,2,6\r\n";
                        $in .= "^FH\^FDLA,". route('orders.mobile',$order->order_id) . "\r\n";
                        $in .= "^FS";
                        
                        $in .= "^XZ\r\n";
                 
                    }
                    

                /**************************************************************
                *
                *  Zebra Small Label Setting
                *
                ***************************************************************/

                    else if($printer->type == "zebra" && $printer->size == "small"){

                        //////////////
                        //basic command list
                        // ^FX is comment
                        // ^FO is placement of text  
                        // ^FD is data
                        // ^FS seperator
                        // 

                        $in = "^XA\r\n";

                        $in .= "^FX Order Info\r\n";
                        $in .= "^CFA,28\r\n";
                        $in .= "^FO300,20\r\n";
                        $in .= "^FDMobile #". $order->order_id ."\r\n";
                        $in .= "^FS\r\n";
                        
                        $in .= "^FX Customer Name \r\n";
                        $in .= "^CFA,18\r\n";
                        $in .= "^FO300,50\r\n";
                        $in .= "^FD". $order->customer()->firstname . " " . $order->customer()->lastname ."\r\n";
                        $in .= "^FS\r\n";

                        if(isset($order->customer()->phone)){
                            if($order->customer()->phone != "6048751993"){
                                $in .= "^FX Customer Phone\r\n";
                                $in .= "^FO300,70";
                                $in .= "^FD". $order->customer()->phone ."^FS\r\n";
                            }
                        }
                                    
                        $in .= "^FX QR CODE\r\n";
                        $in .= "^FT420,200\r\n";
                        $in .= "^BQN,2,3\r\n";
                        $in .= "^FH\^FDLA,". route('orders.mobile',$order->order_id) . "\r\n";
                        $in .= "^FS";

                        $in .= "^XZ\r\n";
                        
                    }

                    // End of printing options
                    

                /**************************************************************
                *
                *  Close Socket
                *
                ***************************************************************/

                    echo "<br/>input:";
                    echo "<br/>$in<br/>";

                    $out = '';

                    echo "Sending HTTP HEAD request...";
                    socket_write($socket, $in, strlen($in));
                    echo "OK.\n";

                    // echo "Reading response:\n\n";
                    // while ($out = socket_read($socket, 2048)) {
                    // 	echo $out;
                    // }

                    echo "Closing socket...<br>";
                    socket_close($socket);
                    echo "OK.\n\n";
                }
                // End of DEBUG = 0 
            }else{
                echo "Error: Could not find printer or printer is inactive <br> ";
            }

        }else{//no such pid
            echo "Error: Could not find such product<br />";
        }

    }else{
        echo "Error: Could not find printer or product.<br />";
    }

}else{
// no access
echo $_SERVER['REMOTE_ADDR'] . "<br>";
echo "Error: Invalid location";	
}

@endphp
