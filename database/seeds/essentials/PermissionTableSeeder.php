<?php

use Illuminate\Database\Seeder;
use App\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = new Permission();
        $permission->name = "employee";
        $permission->save();

        $permission = new Permission();
        $permission->name = "permission";
        $permission->save();

        $permission = new Permission();
        $permission->name = "employee_role";
        $permission->save();

        $permission = new Permission();
        $permission->name = "sales_history";
        $permission->save();

        $permission = new Permission();
        $permission->name = "order";
        $permission->save();

        $permission = new Permission();
        $permission->name = "order_status";
        $permission->save();

        $permission = new Permission();
        $permission->name = "order_suggestion";
        $permission->save();

        $permission = new Permission();
        $permission->name = "rma";
        $permission->save();

        $permission = new Permission();
        $permission->name = "rma_status";
        $permission->save();

        $permission = new Permission();
        $permission->name = "search";
        $permission->save();

        $permission = new Permission();
        $permission->name = "order_product";
        $permission->save();

        $permission = new Permission();
        $permission->name = "product";
        $permission->save();

        $permission = new Permission();
        $permission->name = "product_note";
        $permission->save();

        $permission = new Permission();
        $permission->name = "product_related";
        $permission->save();

        $permission = new Permission();
        $permission->name = "vendors";
        $permission->save();

        $permission = new Permission();
        $permission->name = "statistics";
        $permission->save();

        $permission = new Permission();
        $permission->name = "purchase_orders";
        $permission->save();

        $permission = new Permission();
        $permission->name = "banner";
        $permission->save();

        $permission = new Permission();
        $permission->name = "cost";
        $permission->save();

        $permission = new Permission();
        $permission->name = "category";
        $permission->save();

        $permission = new Permission();
        $permission->name = "brands";
        $permission->save();

        $permission = new Permission();
        $permission->name = "shipment";
        $permission->save();

        $permission = new Permission();
        $permission->name = "order_email";
        $permission->save();
        
        $permission = new Permission();
        $permission->name = "quotation";
        $permission->save();

        $permission = new Permission();
        $permission->name = "notifier";
        $permission->save();
    }
}
