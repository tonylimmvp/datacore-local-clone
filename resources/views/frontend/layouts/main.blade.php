@include('layouts.config')
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- CSRF Token --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <title>{{ config('app.name') }} @yield('title')</title>

     {{-- Bootstrap core CSS --}}
    <link href="{{ asset('css/compiled/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/compiled/custom.css')}}">

    {{-- Custom styles for this template --}}
    <link href="{{ asset('css/dashboard.css')}}" rel="stylesheet">
    <link href="{{ asset('css/compiled/frontend_navtop.css')}}" rel="stylesheet">
    <link href="{{asset('css/compiled/side-modal.css')}}" rel="stylesheet">

    @stack('css')

    {{-- Custom scripts --}}
    @stack('top-scripts')

</head>
<body>
<div id="wrapper">
  <div id="header">
    @include('frontend.layouts.top-nav')
  </div>

  <div id="content">

    @include('layouts.loader')
    @include('layouts.flash')

    <main role="main" class="col-12">
        @auth
        @include('layouts.side-panel')
        @endif
        @yield('content')
    </main>

    {{-- Modals --}}
    @include('layouts.message-modal')
    @include('layouts.printers')
    @include('layouts.signature-pad')
    
  </div>

  <footer id="main-footer">
      <p>Copyright &copy; {{Carbon\Carbon::now()->year}} Lee's Electronic Components Ltd (2005). All rights reserved.</p>
  </footer>
</div>

{{-- Scripts --}}
{{-- <script src="{{ asset('js/compiled/app.js') }}"></script> --}}
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

{{-- Icons --}}
<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
<script>
  feather.replace()
</script>

{{-- Custom JS --}}
<script src="{{asset('js/compiled/custom.js')}}"></script>

@stack('bottom-scripts')

<script type="text/javascript">
  $(document).ready(function () {
    $('a[rel="tooltip"]').tooltip({
      container: 'body'
    });
  });

  $("[data-toggle='tooltip']").tooltip({html:true,sanitize:false});
</script>

<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@10.19.0/dist/lazyload.min.js"></script>

<script>
    var myLazyLoad = new LazyLoad({
      elements_selector: ".lazy",
      load_delay: 300 
    });
</script>

</body>
</html>
