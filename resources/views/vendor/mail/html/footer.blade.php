<tr>
    <td>
        <table class="footer" align="center" width="570" cellpadding="0" cellspacing="0">
            <tr>
                <td class="social-icons" align="center">
                    <a href="https://facebook.com/leeselectroniccomponents"><img src="{{ asset('img/social-icons/facebook.png') }}"></a>
                    <a href="https://twitter.com/lees_electronic"><img src="{{ asset('img/social-icons/twitter.png') }}"></a>
                    <a href="https://www.instagram.com/lees_electronic/"><img src="{{ asset('img/social-icons/instagram.png') }}"></a>
                    <a href="https://www.youtube.com/channel/UCt32cjrR9kY_0u62YPGro5w"><img src="{{ asset('img/social-icons/youtube.png') }}"></a>
                    <a href="https://www.instructables.com/member/leeselectronic/"><img src="{{ asset('img/social-icons/instructable.png') }}"></a>
                </td>
            </tr>
            <tr>
                <td class="content-cell" align="center">
                    {{ Illuminate\Mail\Markdown::parse($slot) }}
                </td>
            </tr>
        </table>
    </td>
</tr>
