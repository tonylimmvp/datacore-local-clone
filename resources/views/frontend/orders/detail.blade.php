@extends('frontend.layouts.main')

@isset($_GET['print'])
@if($_GET['print']=='yes')
    <script>
        window.print();
    </script>
@endif
@endisset

@section('content')
    <div class="container">
    <div class="text-center">
        <h1> Order #{{$order->id}}</h1>
        <h5>[{{$order->type}}]</h5>
        @if($order->invoice_id == "1234567")
            <span class="badge badge-danger no-print">UNPAID</span>
        @endif
        <br>
        <button class="btn btn-primary btn-sm d-lg-none no-print" style="position: absolute; right:5vw; top:100px"
                type="button"
                data-toggle="modal"
                data-target="#printOrderQRModal"
                data-id= "{{$order->id}}">Print QR</button>
        <button class="btn btn-primary btn-sm d-none d-lg-block no-print" style="position: absolute; right:25vw; top:100px" 
                type="button"
                data-toggle="modal"
                data-target="#printOrderQRModal"
                data-id= "{{$order->id}}">Print QR</button>
    </div>
    <hr>
    <form method="POST" action="{{route('orders.update-invoice',$order->id)}}">
        <div class="form-row pb-2">
            {{csrf_field()}}
            {{method_field('PATCH')}}
            <label class="col-sm-2" for="name">Invoice #:</label>
            <div class="input-group col-sm-10 p-0">
            <input type="text" class="form-control" @if($order->invoice_id)value={{$order->invoice_id}}@else placeholder=1234567A @endif name="invoice" @guest disabled @endguest>
                <div class="input-group-append">
                    <button class="btn btn-primary no-print" @guest disabled @endguest>Update</button>
                </div>
            </div>
        </div>
    </form>

    <form method="POST" action="{{route('orders.update-payment')}}">
            <div class="form-row pb-2 no-print">
                {{csrf_field()}}
                {{method_field('PATCH')}}
                <label class="col-sm-2" for="name">Payment #:</label>
                <div class="input-group col-sm-10 p-0">
                <input type="text" class="form-control" @if($order->payment_id)value={{$order->payment_id}}@else placeholder="" @endif name="payment_id" @guest disabled @endguest>
                <input type="hidden" name="order_id" value="{{$order->id}}">
                    <div class="input-group-append">
                        <button class="btn btn-primary no-print" @guest disabled @endguest>Update</button>
                    </div>
                </div>
            </div>
        </form>

    <form method="POST" action="{{route('orders.update-tracking',$order->id)}}">
        <div class="form-row pb-2 @if($order->tracking_id == '') no-print @endif">
            {{csrf_field()}}
            {{method_field('PATCH')}}
            <label class="col-sm-2" for="name">Tracking #:</label>
            <div class="input-group col-sm-10 p-0">
                <input type="text" class="form-control" @if($order->tracking_id)value={{$order->tracking_id}}@else placeholder="" @endif name="tracking" @guest disabled @endguest>
                <div class="input-group-append">
                    <button class="btn btn-primary no-print" @guest disabled @endguest>Update</button>
                </div>
            </div>
            
        </div>
    </form>

    <div class="form-row pb-2">
        <label class="col-sm-2" for="name">NAME:</label>
        <input type="text" class="form-control col-sm-10" value="{{$order->name}}" name="name" disabled>
    </div>

    <div class="form-row pb-2">
        <label class="col-sm-2" for="phone">PHONE:</label>
        <div class="input-group col-sm-10 pl-0 pr-0">
        <input type="text" class="form-control pl-1" value="{{$order->phone}}" name="phone" disabled>
        @if($order->phone_ext)
        <div class="input-group-prepend">
            <div class="input-group-text"><span>Ext.</span></div>
        </div>
        <input type="text" class="form-control" value="{{$order->phone_ext}}" name="phone_ext" disabled>
        @endif
        </div>
    </div>

    <div class="form-row pb-2">
        <label class="col-sm-2" for="email">EMAIL:</label>
        <input type="text" class="form-control col-sm-10" value="{{$order->email}}" name="email" disabled>
    </div>

    <div class="form-row pb-2">
        <label class="col-sm-2" for="sales">SALES REP:</label>
        <input type="text" class="form-control col-sm-10" value="{{$order->sales->name}}" name="sales" disabled>
    </div>

    <div class="form-row pb-2">
        <label class="col-sm-2" for="type">TYPE:</label>
        <input type="text" class="form-control col-sm-10" value="{{$order->type}}" name="type" disabled>
    </div>

    <div class="form-row pb-2">
        <label class="col-sm-2" for="date">CREATED ON:</label>
        <input type="text" class="form-control col-sm-10" value="{{$order->created_at->format('D, M d, Y h:i A')}}" name="date" disabled>
    </div>

    <div class="form-row pb-2">
        <label class="col-sm-2" for="status">CURRENT STATUS:</label>
        <input type="text" class="form-control col-sm-10" value="{{$order->currentStatus->name}}" name="status" disabled>
    </div>

    <form method="POST" action="{{route('orders.update-comment',$order->id)}}">
        <div class="form-row pb-2 no-print">
            {{csrf_field()}}
            {{method_field('PATCH')}}
            <label class="col-sm-2" for="status">COMMENTS:</label>
            <div class="input-group col-sm-10 p-0">
            <textarea type="text" class="form-control " name="comment" @if(!Auth::user()->hasAccess('order')->update) disabled @endif>@if($order->comments){{Crypt::decryptString($order->comments) }}@endif</textarea>
            <div class="input-group-append">
                <button class="btn btn-primary" @if(!Auth::user()->hasAccess('order')->update) disabled @endif>Update</button>
            </div>
            </div>
        </div>
    </form>
    
    @if(sizeof($related_phone)>0)
    <div class="row pb-2 no-print">
        <label class="col-sm-2">RELATED PHONE</label>
        <div class="col-sm-10">
        @foreach($related_phone as $related_order)
        <a href="{{route('orders.detail',$related_order->id)}}"><button class="btn btn-sm mb-1" style="color:white;background-color:{{$related_order->currentStatus->color}}">Order #{{$related_order->id}}</button></a>
        @endforeach
        </div>
    </div>
    @endif

    @if(sizeof($related_invoice)>0)
    <div class="row pb-2 no-print">
        <label class="col-sm-2">RELATED INVOICES</label>
        <div class="col-sm-10">
        @foreach($related_invoice as $related_order)
            <a href="{{route('orders.detail',$related_order->id)}}"><button class="btn btn-sm mb-1" style="color:white;background-color:{{$related_order->currentStatus->color}}">Order #{{$related_order->id}}</button></a>
        @endforeach
        </div>
    </div>
    @endif

    @if($order->invoice_id == "1234567")
    <div class="alert text-center">
            <span class="alert alert-danger">This order is UNPAID</span>
    </div>
    @endif

    <h5>CART:</h5>
        <div class="table-responsive">
        <table class="table table-bordered">
            <tr>   
                {{-- <th></th> --}}
                <th>PID</th>
                <th>Name</th>
                <th>Price</th>
                <th>Quantity</th>
                @if(Auth::user()->hasAccess('order_product')->read && Auth::user()->hasAccess('vendors')->read)
                <th class="no-print">Vendor</th>
                <th class="no-print">Vendor Remark</th>
                <th class="no-print"><span class="feather" data-feather="shopping-cart"></span></th>
                <th class="no-print"><span class="feather" data-feather="shopping-bag"></span></th>
                <th class="no-print">Action</th>
                @endif
            </tr>
            @foreach($order->products as $product)
                <tr>
                    @if(Auth::user()->hasAccess('order_product')->update)
                        <form action="{{ route('order-product.update-vendor',$product->pivot->id) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('PATCH') }}
                            <td>{{ $product->id_product }}</td>
                            <td>{{ $product->pivot->name }}</td>
                            <td>${{ $product->pivot->price }} </td>
                            <td>{{ $product->pivot->quantity }} </td>
                            @if(Auth::user()->hasAccess('order_product')->read && Auth::user()->hasAccess('vendors')->read)
                            <td class="no-print">
                                <select id="vendor{{$product->pivot->id}}" class="form-control form-control-sm order-product-vendor" name="vendor">
                                    <option value="">--</option>
                                    @foreach(\App\Vendor::all() as $vendor)
                                        <option value="{{$vendor->reference}}" @if($product->pivot->vendor == $vendor->reference) selected @endif>{{$vendor->name}}</option>
                                    @endforeach
                                </select>
                                @if($product->pivot->vendor)
                                    <small>Current: {{$product->pivot->vendor}}</small>
                                @endif
                            </td>
                            <td class="no-print">
                                <input id="remarks{{$product->pivot->id}}" class="form-control form-control-sm order-product-vendor-remark" type="text" name="vendor_remark" value="{{ $product->pivot->vendor_remark }}">
                                @if($product->pivot->vendor_remark)
                                    <small>Current: {{$product->pivot->vendor_remark}}</small>
                                @endif
                            </td>
                            @endif
                            <td class="no-print">
                                @if(!($product->id_product > 0 && $product->id_product < 10))
                                    @if($product->pivot->inCart)
                                    <span class="feather" data-feather="check" style="color:green"></span>
                                    @else
                                    <span class="feather" data-feather="x" style="color:red"></span>
                                    @endif
                                @endif
                            </td>
                            <td class="no-print">
                                @if(!($product->id_product > 0 && $product->id_product < 10))
                                    @if($product->pivot->isPurchased)
                                    <span class="feather" data-feather="check" style="color:green"></span>
                                    @else
                                    <span class="feather" data-feather="x" style="color:red"></span>
                                    @endif
                                @endif
                            </td>
                            <td class="text-center no-print">
                            <button class="btn btn-sm btn-secondary mb-2">Update</button>
                        </form>
                        @if(Auth::user()->hasAccess('shipment')->update)
                            <button class="btn btn-sm btn-secondary mb-2"
                                data-toggle="modal"
                                data-target="#purchaseAndShipModal"
                                data-id="{{ $product->id_product }}" 
                                data-orderpid="{{ $product->pivot->id }}" 
                                data-name="{{ $product->name }}"
                                data-url="{{route('order-suggestion.purchaseAndShip', $product->id_product)}}">Purchase and Ship
                            </button>
                        @endif
                    @else
                        <td>{{ $product->id_product }}</td>
                        <td>{{ $product->pivot->name }}</td>
                        <td>${{ $product->pivot->price }} </td>
                        <td>{{ $product->pivot->quantity }} </td>
                        @if(Auth::user()->hasAccess('order_product')->read && Auth::user()->hasAccess('vendors')->read)
                        <td class="no-print">{{ $product->pivot->vendor }}</td>
                        <td class="no-print">{{ $product->pivot->vendor_remark }}</td>
                        @endif
                        <td class="no-print">
                            @if(!($product->id_product > 0 && $product->id_product < 10))

                                @if($product->pivot->inCart)
                                <span class="feather" data-feather="check" style="color:green"></span>
                                @else
                                <span class="feather" data-feather="x" style="color:red"></span>
                                @endif
                            @endif
                        </td>
                        <td class="no-print">
                            @if(!($product->id_product > 0 && $product->id_product < 10))
                                @if($product->pivot->isPurchased)
                                <span class="feather" data-feather="check" style="color:green"></span>
                                @else
                                <span class="feather" data-feather="x" style="color:red"></span>
                                @endif
                            @endif
                        </td>
                    @endif
    
                    </td>
                </tr>
            @endforeach
            
        </table>
        </div>

        <br/>
        {{-- Notes and Comments --}}
        <div id="orderNotes" class="no-print">
            @php $notes = $order->note; @endphp
            <h5>NOTES AND COMMENTS:</h5>
            <div style="text-align:right">
				<button class="btn btn-primary mb-1"
                        title="Create New Note"
						data-toggle="modal"
						data-target="#createNoteModal"><span data-feather="file-plus"></i>
				</button>
			</div>
            @include('frontend.note.view', ['notes' => $notes], ['order' => $order])
        </div>

        <div class="no-print">
            <h5>STATUS HISTORY:</h5>
            <div class="table-responsive">
            <table class="table table-bordered">
                <tr>   
                    <th>Employee</th>
                    <th class="text-center">Status</th>
                    <th class="no-print">Comments</th>
                    <th>Date</th>
                </tr>
                @foreach($status_history as $status)
                <tr>
                    <td>{{$status->sales->name}}</td>
                    <td class="text-center"><span class="order-status" style="background:{{$status->status->color}};">{{$status->status->name}}</span></td>
                    <td class="no-print">{{$status->comment}}</td>
                    <td>{{$status->created_at->format('D, M d, Y h:i A')}}</td>      
                </tr>
                @endforeach
            </table>
            </div>
        </div>

        {{-- Status History Form --}}
        @auth
        <form class="form-inline no-print" method="POST" action="{{route('order-history.store')}}" enctype="multipart/form-data">
            {{ csrf_field() }}

                <div class="input-group mr-sm-2 mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text"><span data-feather="users"></i></div>
                  </div>
                                       
                    {{-- Sales Rep --}}
                    @if(Auth::user()->role->id==5)
                    
                    <select class="form-control custom-select employee-dropdown" name="employee" required>
                        <option value="" selected disabled>Please Select An Employee...</option>
                        @foreach($employees as $employee)
                            <option value="{{$employee->id}}">{{$employee->name}}</option>
                        @endforeach
                    </select>

                    @else

                    <input class="form-control" type="text" value="{{Auth::user()->name}}" disabled>
                    <input type="hidden" name="employee" value="{{Auth::user()->id}}">

                    @endif

                    <input type="hidden" name="order" value="{{$order->id}}">
                </div>

                <select class="custom-select mr-sm-2 mb-2" name="status" required>
                    <option value="" selected disabled>Please Select Status...</option>
                    @foreach($statuses as $status)
                        @if(!Auth::user()->hasAccess('order_product')->update && ($status->id == 2 || $status->id == 7)) 
                        <option value="{{$status->id}}" hidden>{{$status->name}}</option>
                        @else
                        <option value="{{$status->id}}">{{$status->name}}</option>
                        @endif 
                    @endforeach
                </select>

                <input type="text" class="form-control mb-2 mr-sm-2" placeholder="Enter Comment..." name="comment">
              
                <button type="submit" class="btn btn-primary mb-2" onclick="validateSubmission()" >Submit</button>
              </form>
              
        @endauth
        @guest
                <p class="alert alert-info no-print">Note: To change the status please log in: <a class="btn btn-primary btn-sm ml-2" href="{{ route('login')}}">Login Here</a></p>
        @endguest
        
        <div id="signatureDiv" class="mt-3">
            <h5 @if(!$order->isSigned()) class="no-print" @endif>CUSTOMER SIGNATURE:</h5>
            @if($order->isSigned())
                <img class="signature-img pb-3" src="{{ url('storage/invoices/order'. $order->id . '.jpg') }}" style="border-bottom: #c7c7c7 1px solid">
            <p class="mt-2" style="color:#8a8a8a">Picked up by: {{$order->picked_up_by}} on {{$order->lastSigned()}}</p>
            @else
                <button class='btn btn-sm btn-primary no-print'
                    type='button' 
                    data-toggle='modal' 
                    data-target='#signatureModal' 
                    data-id='{{$order->id}}'
                    data-name='{{$order->name}}'
                    data-type='order'
                    >Order Pick Up</button>
            @endif
        </div>

        @include('layouts.errors')

        <div class="footer text-center pt-5">
            <img class="img-fluid" src="{{asset('img/lees_logo_large.png')}}" width="300px;">
            <h2>Thank you!</h2>

            <p>We will contact you when the order is ready. Please keep this as your receipt!</p>

            <p>Please note, we only keep orders for three (3) months after the first initial call. We do not hold liability for discarding orders older than 3 months.</p>

            <p>4131 Fraser St. Vancouver BC V5V 4E9 Canada | 604 - 875 - 1993 | www.leeselectronic.com </p>
        </div>
    </div>

    {{-- Order Note Modal --}}
    <div class="modal fade" id="createNoteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{ route('order-notes.store', $order->id)}}" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create New Note</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <label>Order ID: </label>
                        <input class="form-control" type="integer" name="orderid" id="note-orderid" value="{{$order->id}}" readonly>
                        <hr/>

                        <br/>

                        <label>Comments:</label>
                        <textarea class="form-control" type="text" name="comments" id="note-comments"></textarea>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <a id="note-cancel" href="#">
                        <button type="submit" class="btn btn-primary">Create Note</button>
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- Purchase and Ship Modal --}}
    <div class="modal fade" id="purchaseAndShipModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="POST">
                	{{ csrf_field() }}
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Insert Order into Shipment</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>

					<div class="modal-body">
						<label>Product ID:</label>
                    	<input class="form-control" type="text" name="id" id="id" readonly><br/>

						<label>Product Name:</label>
                    	<input class="form-control" type="text" name="name" id="name" readonly><br/>

                        <label>Vendor:</label>
                        <input class="form-control" type="text" name="vendor" id="vendor" readonly><br/>

                        <label>Remark:</label>
                    	<input class="form-control" type="text" name="remarks" id="remarks" readonly><br/>

						<label>Ordered Quantity:</label>
                    	<input class="form-control" type="text" name="qty" id="qty"><br/>

                    	<label>Ordered Unit:</label>
                    	<select class="custom-select mr-sm-2 mb-2" name="unit" id="unit" required>
                            <option value="piece">Piece</option>
                            <option value="package">Package</option>
                            <option value="meter">Meter</option>
                            <option value="foot">Foot</option>
                            <option value="1/2lb">1/2LB</option>
                            <option value="box">Box</option>
                            <option value="roll">Roll</option>
                            <option value="winpos unit">Winpos Unit</option>
                        </select><br/>

                        @php $shipments = App\Shipment::all() @endphp
                        <label>Select Shipment:</label>
                        <select class="custom-select mr-sm-2 mb-2" name="shipment" id="shipment" required>
                            @foreach($shipments as $shipment)
                                @if ($shipment->isActive)
                                    <option value="{{$shipment->id}}">{{$shipment->id}} | [{{$shipment->status->name}}] | {{$shipment->vendor->name}} | 
                                        @if ($shipment->isLocal == '1')
                                            LOCAL
                                        @else
                                            NOT LOCAL
                                        @endif
                                        | {{$shipment->type->name}}
                                    </option>
                                @endif
                            @endforeach
                        </select>
                        <br/>
                        <p class='smallNote'> If no shipment is available, create one <a href="{{route('shipment.create')}}" target="_blank">here.</a>
                            <a href="javascript:window.location.href=window.location.href"> Refresh page.</a>
                        </p>

                    	<label>Invoice:</label>
                    	<input class="form-control" type="text" name="invoice" id="invoice"><br/>

						<label>Comments:</label>
                    	<textarea class="form-control" type="text" name="comments" id="comments"></textarea><br/>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>

						<button type="submit" class="btn btn-primary">Purchase Suggestion</button>
					</div>
        		</form>
            </div>
    	</div>
    </div>

    @include('layouts.printers-order-qr')

@endsection


@push('bottom-scripts')
<script>
    function validateSubmission() {
        var submit = true;
        if($('select[name="employee"] option:selected').val() == "" || $('select[name="status"] option:selected').val() == ""){
            submit = false;
        }
        if(submit){
            openLoader();
        }
        return submit;
    }
    $(document).ready(function () {
        $('#purchaseAndShipModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var id = button.data('id') // Extract info from data-* attributes
            var orderProductID = button.data('orderpid') // Extract info from data-* attributes
            var name = button.data('name') // Extract info from data-* attributes
            var vendor = $("#vendor" + orderProductID).val()
            var remarks = $("#remarks" + orderProductID).val()
            var url = button.data('url')

            var modal = $(this)
            modal.find('.modal-title').text('Insert Item: ' + name)
            modal.find('#id').val(id)
            modal.find('#name').val(name)
            modal.find('#vendor').val(vendor)
            modal.find('#remarks').val(remarks)
            modal.find('form').attr('action',url)
        });
    });
</script>

@endpush