@component('mail::message')

@if($order->type == "backorder")
# A new backorder has been created!
@elseif($order->type == "special")
# A new special order has been created!
@elseif($order->type == "phone")
# A new phone order has been created!
@endif

**Customer:** {{$order->name}}<br>
**Phone:** {{$order->phone}} @if($order->phone_ext), Ext.{{$order->phone_ext}} @endif<br>
**Email:** {{$order->email}}<br>
**Invoice:** {{$order->invoice_id}}

@component('mail::table')
| PID        | Name           | Price per Unit  | Quantity | 
| ---------- |:--------------:| :--------------:| :------: |
@foreach($order->products as $product)
| {{$product->id_product}} | {{$product->pivot->name}} | ${{$product->pivot->price}} | {{$product->pivot->quantity}} |
@endforeach
@endcomponent

@component('mail::button', ['url' => route('orders.detail',$order->id)])
View Order
@endcomponent

@endcomponent
