<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleSuggestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_suggestions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id');
            $table->decimal('price', 9, 2);
            $table->decimal('cost', 9, 2);
            $table->unsignedInteger('vendor_id')->nullable();

            $table->decimal('sale_percentage1')->nullable();
            $table->decimal('adj_price1')->nullable();
            $table->decimal('profit_margin1')->nullable();

            $table->decimal('sale_percentage2')->nullable();
            $table->decimal('adj_price2')->nullable();
            $table->decimal('profit_margin2')->nullable();

            $table->decimal('sale_percentage3')->nullable();
            $table->decimal('adj_price3')->nullable();
            $table->decimal('profit_margin3')->nullable();

            $table->decimal('max_sale_percentage')->nullable();
            $table->decimal('max_sale_adj_price')->nullable();
            $table->decimal('max_sale_profit_margin')->nullable();
            
            $table->integer('employee_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_suggestions');
    }
}
