<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Capacitor;

class CapacitorController extends Controller
{
    public function index(Request $request){
        if(isset($request->voltage) && isset($request->volt_sign) && isset($request->capacitance) && isset($request->cap_sign)){
            $capacitors = Capacitor::filter(request(['voltage','volt_sign','capacitance','cap_sign']))->get();
        }elseif(isset($request->voltage) && isset($request->volt_sign)){
            $capacitors = Capacitor::filter(request(['voltage','volt_sign']))->get();
        }elseif(isset($request->capacitance) && isset($request->cap_sign)){
            $capacitors = Capacitor::filter(request(['capacitance','cap_sign']))->get();
        }else{
            $capacitors = Capacitor::all();
        }

        return view('frontend.capacitor',compact('capacitors'));
    }

    public function update(){
        $capacitors = Product::whereRaw("name LIKE '%cap%' AND
                                        (name LIKE '%uf%' OR
                                        name LIKE '%nf%' OR
                                        name LIKE '%pf%' OR
                                        name LIKE '%MFD%')")
                                ->where("id_product",">","6")
                                ->where("id_product","!=","100")
                                ->where("disabled","N")->get();

        foreach($capacitors as $capacitor){
            $this->parseCapacitor($capacitor);
        }

        return back();
    }

    // Function
    public function parseCapacitor($capacitor){
        $name_array = explode(' ', $capacitor->name);
        
        $has_v = false;
        $has_uf = false;
        $has_temp = false;

        $voltage = '';
        $capacitance = '';
        $temperature = null;

        $capacitors = [];
        
        foreach($name_array as $token){
            // Voltage
            if(preg_match("/^[0-9]*\.*[0-9]*V/i",$token)){
                $has_v = true;
                if(preg_match_all('/^(\d+)\.*[0-9]*/m', $token, $m)){
                    $voltage = floatval($token);
                }
            }

            // Kilovolts;
            if(preg_match("/^[0-9]*\.*[0-9]*KV/i",$token)){
                $has_v = true;
                if (preg_match_all('/^(\d+)\.*[0-9]*/m', $token, $m)){
                        $voltage = floatval($token) * 1000;
                   }
            }

            //UF/MFD
            else if(preg_match("/^[0-9]*\.*[0-9]*(UF|uf|MFD)/i",$token)){
                $has_uf = true;
                if (preg_match_all('/^(\d+)\.*[0-9]*/m', $token, $m)){
                       $capacitance = floatval($token);
                }
            }

            
            // PF
            else if(preg_match("/^[0-9]*\.*[0-9]*(PF|pf)/i",$token)){
                $has_uf = true;
                if (preg_match_all('/^(\d+)\.*[0-9]*/m', $token, $m)){
                       $capacitance = floatval($token)/1000000;
                   }
            }

            // NF
            else if(preg_match("/^[0-9]*\.*[0-9]*(NF|nf)/i",$token)){
                $has_uf = true;
                if (preg_match_all('/^(\d+)\.*[0-9]*/m', $token, $m)){
                       $capacitance = floatval($token)/1000;
                   }
            }

            // Farad
            else if(preg_match("/^[0-9]*\.*[0-9]*(F|f)/i",$token)){
                $has_uf = true;
                if (preg_match_all('/^(\d+)\.*[0-9]*/m', $token, $m)){
                    $capacitance = floatval($token) *1000000;   
                }
            }

            // Temperature
            else if(preg_match("/^[0-9]*C$/i",$token)){
                $has_temp = true;
                if (preg_match_all('/^(\d+)/m', $token, $m)){
                    $temperature = floatval($token);
                }
            }

            if($has_v && $has_uf){
                $cap = Capacitor::updateOrCreate(
                    ['product_id' => $capacitor->id_product],
                    [
                        'voltage' => $voltage,
                        'capacitance' => $capacitance,
                        'temperature' => null
                    ]
                );
        
                $cap->save();
            }
        } 
    }
}
