@include('layouts.datatables')

@push('css')

<style>
.tooltip-inner {
    max-width: none;
    background:white;
    border:1px solid lightgrey;
    -webkit-box-shadow: 0px 3px 3px 0px rgba(0,0,0,0.3);
    -moz-box-shadow: 0px 3px 3px 0px rgba(0,0,0,0.3);
    box-shadow: 0px 3px 3px 0px rgba(0,0,0,0.3);
    color:#333
}

.order-links {
    color: #666;
    cursor: pointer;
}

.order-links:hover {
    color:#3399ff
}
</style>
@endpush

@include('functions.old-order-functions')

@if(isset($_GET['status'])) 
<h5 class="ml-4 mb-4">
    <span data-feather="corner-down-right"></span>
    <span class="badge badge-info" style="background-color:{{App\OrderStatus::getStatusColor($_GET['status'])}}">{{ App\OrderStatus::getStatusName($_GET['status']) }} Orders</span>
</h5> 
@elseif(isset($_GET['order']))
<h5 class="ml-4 mb-4">
    <span data-feather="corner-down-right"></span>
    <span class="badge badge-danger text-capitalize"> {{$_GET['order']}} </span>
</h5> 
@elseif(isset($term))
<h5 class="ml-4 mb-4">
    <span data-feather="corner-down-right"></span>
    <span class="badge badge-info"> Search Term: {{ $term }} </span>
</h5>
@endif
<div class="card-deck mb-1 text-center">
    <div class="card mb-4 shadow-sm">
        <div class="card-header">
        <h4 class="my-0 font-weight-normal">ALL ORDERS</h4>
        </div>
        <a href="{{route('orders')}}">
        <div class="card-body" @if(!isset($type)||(isset($type)&&$type=="1")) style="background:#078aff17" @endif>
        <h1 class="card-title pricing-card-title"> {{ $orderCount->all_orders }} <small class="text-muted"></small></h1>
        </div>
        </a>
    </div>
    <div class="card mb-4 shadow-sm">
        <div class="card-header">
        <h4 class="my-0 font-weight-normal">ALL SPECIAL ORDERS</h4>
        </div>
        <a href="{{route('orders.special')}}">
        <div class="card-body" @if(isset($type) && $type=="2") style="background:#078aff17" @endif>
        <h1 class="card-title pricing-card-title"> {{ $orderCount->special }} </h1>
        </div>
        </a>
    </div>
    <div class="card mb-4 shadow-sm">
        <div class="card-header">
        <h4 class="my-0 font-weight-normal">ALL BACKORDERS</h4>
        </div>
        <a href="{{route('orders.backorder')}}">
            <div class="card-body" @if(isset($type) && $type=="3") style="background:#078aff17" @endif>
                <h1 class="card-title pricing-card-title"> {{ $orderCount->backorders }} </h1>
            </div>
        </a>
    </div>
    <div class="card mb-4 shadow-sm">
        <div class="card-header">
        <h4 class="my-0 font-weight-normal">ALL PHONE ORDERS</h4>
        </div>
        <a href="{{route('orders.phoneorder')}}">
            <div class="card-body" @if(isset($type) && $type=="4") style="background:#078aff17" @endif>
                <h1 class="card-title pricing-card-title"> {{ $orderCount->phone }} </h1>
            </div>
        </a>
    </div>
</div>

<div>
    <form action="{{route('order.search')}}" method="post" class="mb-3">
        {{ csrf_field() }}
        <div class="input-group">
            <input class="form-control" name="term" value="@isset($term){{ $term }}@endisset" required>
            <div class="input-group-append">
                <button class="btn btn-primary" type="submit">Search</button>
            </div>
        </div>
    </form>
</div>

    <a href="?">
            <button class="btn btn-primary m-1 mb-4">
                All
            </button>
    </a>

@foreach($statusFilters as $filter)
    <a href="?status={{$filter->id}}">
        <button class="btn btn-primary m-1 mb-4">
            {{$filter->name}}
        </button>
    </a>
@endforeach

    <a href="?order=unpaid">
        <button class="btn btn-danger m-1 mb-4">
        Unpaid <span class="badge badge-secondary">{{$unpaid_size}}</span>
        </button>
    </a>

    <div class="table-responsive">
        <table id="orders-table" class="table table-bordered table-hover" width=100%>
            <thead>
                <tr>
                    <th>ORDER #</th>
                    <th>Invoice #</th>
                    <th>Customer</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Sales Rep</th>
                    <th>Type</th>
                    <th>Created</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach($orders as $order)
                <tr data-toggle="tooltip" 
                    title=" <table class='table table-sm table-bordered mt-3' >
                                <thead>
                                    <tr>
                                        <th>PID</th>
                                        <th>Name</th>
                                        <th>Quantity</th>
                                        <th>Price per Unit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($order->products as $product)
                                        <tr>
                                            <td>{{$product->id_product}}</td>
                                            <td>{{$product->pivot->name}}</td>
                                            <td>{{$product->pivot->quantity}}</td>
                                            <td>${{$product->pivot->price}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>">
                    <td> <a class="order-links" href="{{route('orders.detail',$order->id)}}">{{$order->id}}</a> </td>
                    <td @if($order->invoice_id == '1234567') class="alert-danger"@endif>@if($order->invoice_id){{$order->invoice_id}}@else NA @endif</td>
                    <td>{{$order->name}}</td>
                    <td>{{$order->phone}} @if($order->phone_ext) <span style="color:gray">Ext.{{$order->phone_ext}} </span>@endif</td>
                    <td>{{$order->email}}</td>
                    <td>{{$order->sales->name}}</td>
                    <td class="order-links" onclick="window.location='@if($order->type == 'special') {{route('orders.special')}} @elseif($order->type == 'backorder') {{route('orders.backorder')}} @endif'">{{$order->type}}</td>
                    <td>{{$order->created_at->format('M d, Y')}}</td>
                    <td class="text-center"><span class="order-status" style="background:{{$order->currentStatus->color}};">{{$order->currentStatus->name}}</span></td>
                </tr>

                @endforeach
            </tbody>
        </table>
    </div>

<hr>
{{-- Online orders --}}

@php 
    if(!isset($online)){
        $online = App\OnlineOrder::orderBy('id_order','desc')->whereNotIn('current_state',[4,5,6,7,17])->get(); 
    }
@endphp

<h2>Online Orders</h2>

<div class="table-responsive">
        <table id="online-table" class="table responsive table-bordered table-hover" width=100%>
            <thead>
                <tr>
                    <th>ORDER #</th>
                    <th>Reference</th>
                    <th>Customer</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Payment</th>
                    <th>Created</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach($online as $order)
                <tr>
                    <td> <a class="order-links" href="{{route('orders.online',$order->id_order)}}">{{$order->id_order}}</a> </td>
                    <td>{{$order->reference}}</td>
                    <td>{{$order->addressBilling()->firstname}} {{$order->addressBilling()->lastname}}</td>
                    <td>{{$order->addressBilling()->phone}} </td>
                    <td>{{$order->customer()->email}}</td>
                    <td>{{$order->payment}}</td>
                    <td>{{$order->date_add}}</td>
                    <td class="text-center"><span class="order-status" style="background:{{$order->statusColor()}}">{{$order->statusName()}}</span></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

<hr>

{{-- Mobile orders --}}

@php 
    if(!isset($mobile)){
        $mobile = App\MobileOrder::orderBy('order_id','desc')->whereNotIn('current_state',[3,4,5,7])->get(); 
    }
@endphp

<h2>Mobile Orders</h2>

<div class="table-responsive">
        <table id="mobile-table" class="table responsive table-bordered table-hover" width=100%>
            <thead>
                <tr>
                    <th>ORDER #</th>
                    <th>Reference</th>
                    <th>Customer</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Created</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach($mobile as $order)
                <tr>
                    <td> <a class="order-links" href="{{route('orders.mobile',$order->order_id)}}">{{$order->order_id}}</a> </td>
                    <td>{{$order->reference}}</td>
                    <td>{{$order->customer()->firstname}} {{$order->customer()->lastname}}</td>
                    <td>{{$order->customer()->phone}} </td>
                    <td>{{$order->customer()->email}}</td>
                    <td>{{$order->created_at}}</td>
                    <td class="text-center"><span class="order-status" style="background:{{$order->status->color}}">{{$order->status->name}}</span></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

<hr>

@isset($type)
    {{-- Fastsearch Orders --}}
    
    <h2>Orders from Old Fastsearch</h2>

    <a target="_blank" href="{{config("custom.web_url")."/fastsearch_lite/order_query.php"}}" class="btn btn-primary">All Orders</a>
    <a target="_blank" href="{{config("custom.web_url")."/fastsearch_lite/order.php"}}" class="btn btn-primary">Special Orders</a>
    <a target="_blank" href="{{config("custom.web_url")."/fastsearch_lite/backorder.php"}}" class="btn btn-primary">Backorders</a>

@endisset

{{-- Print Order Modal --}}
@if($orders = session('print'))
    <div class="modal fade" id="printOrderModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Print Orders</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    @foreach($orders as $order)
                        <a href="{{route('orders.detail',$order)}}?print=yes" target="_blank"><button class="btn btn-primary mb-2">Print Order #{{$order}}</button></a><br>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endif

@push('bottom-scripts')
    <script type="text/javascript">
        
            $('#orders-table').DataTable(
                {
                    scrollY:        "50vh",
                    scrollX:        true,
                    scrollCollapse: true,
                    paging:         false,
                    "displayLength": 25,
                    "order": [[ 0, "desc" ]],
                    "language": {
                        "search": "Filter records:"
                    }
                }
            );

            $('#online-table').DataTable(
                {
                    scrollY:        "50vh",
                    scrollX:       true,
                    scrollCollapse: true,
                    paging:         false,
                    "displayLength": 25,
                    "order": [[ 0, "desc" ]],
                    "language": {
                        "search": "Filter records:"
                    }
                }
            );

            $('#mobile-table').DataTable(
                {
                    scrollY:        "50vh",
                    scrollX:       true,
                    scrollCollapse: true,
                    paging:         false,
                    "displayLength": 25,
                    "order": [[ 0, "desc" ]],
                    "language": {
                        "search": "Filter records:"
                    }
                }
            );

    </script>

@if(session('print'))
<script type="text/javascript">
    $(window).on('load',function(){
        $('#printOrderModal').modal('show');
    });
</script>
@endif

@endpush
    
