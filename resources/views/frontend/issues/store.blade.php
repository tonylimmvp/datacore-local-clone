@extends('frontend.layouts.main')

@push('css')
<style>
    .table{
        display:none;
    }
    #sidepanelModal .table{
        display:table;
    }
</style>
@endpush

@section('content')

    <h1>ISSUES</h1>

     {{------------------------------------------- 
    ----
    ----    Product Without Price
    ----
    ---------------------------------------------}}

    <h2>Product Without Price</h2>
    @php
        $products = App\Product::where('price',0)->where('disabled','N')->whereNotIn('id_product',[100])->where('id_product','>',16)->get();
    @endphp

    <button class="btn btn-sm btn-primary" onclick="toggle('no_price')">Toggle</button>

    <span>
        {{sizeof($products)}} Results found.
    </span>

    <table id="no_price" class="table table-bordered mt-2">
        <thead>
            <th>PID</th>
            <th>Name</th>
            <th>Quantity</th>
        </thead>
        <tbody>
            @foreach($products as $product)
                <tr>
                    <td>{{ $product->id_product }}</td>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->quantity }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    {{------------------------------------------- 
    ----
    ----    Product with negative quantity
    ----
    ---------------------------------------------}}

    <h2>Product With Negative Quantity</h2>
    @php
        $products = App\Product::where('quantity','<','0')->where('disabled','N')->whereNotIn('id_product',[100])->where('id_product','>',16)->get();
    @endphp

    <button class="btn btn-sm btn-primary" onclick="toggle('neg_qty')">Toggle</button>

    <span>
        {{sizeof($products)}} Results found.
    </span>

    <table id="neg_qty" class="table table-bordered mt-2">
        <thead>
            <th>PID</th>
            <th>Name</th>
            <th>Quantity</th>
        </thead>
        <tbody>
            @foreach($products as $product)
                <tr>
                    <td>{{ $product->id_product }}</td>
                    <td>
                        {{ $product->name }}
                        @auth
                        {{-- Orders for product --}}
                        @if($product->orders)
                            @foreach($product->orders as $order)
                                @if($order->currentStatus->name != "Done")
                                    <br>
                                    <a href="{{route('orders.detail',$order->id)}}" class="badge badge-secondary">
                                        Order #{{$order->id}} - {{$order->name}}
                                    </a>
                                @endif
                            @endforeach
                        @endif
                        @endauth
                    </td>
                    <td>{{ $product->quantity }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

     {{------------------------------------------- 
    ----
    ----    Product Without Names
    ----
    ---------------------------------------------}}

    <h2>Product Without Name</h2>
    @php
        $products = App\Product::where('name','')->where('disabled','N')->where('id_product','>',16)->get();
    @endphp

    <button class="btn btn-sm btn-primary" onclick="toggle('no_name')">Toggle</button>

    <span>
        {{sizeof($products)}} Results found.
    </span>

    <table id="no_name" class="table table-bordered mt-2">
        <thead>
            <th>PID</th>
            <th>Name</th>
            <th>Quantity</th>
        </thead>
        <tbody>
            @foreach($products as $product)
                <tr>
                    <td>{{ $product->id_product }}</td>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->quantity }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    {{------------------------------------------- 
    ----
    ----    Product With No Storage Location
    ----
    ---------------------------------------------}}

    <h2>Product With No Storage Location</h2>
    @php
        $products = App\Product::where('fraser_loc','')->where('disabled','N')->where('id_product','>',16)->get();
    @endphp

    <button class="btn btn-sm btn-primary" onclick="toggle('no_loc')">Toggle</button>

    <span>
        {{sizeof($products)}} Results found.
    </span>

    <table id="no_loc" class="table table-bordered mt-2">
        <thead>
            <th>PID</th>
            <th>Name</th>
            <th>Quantity</th>
            <th>Storage Location</th>
        </thead>
        <tbody>
            @foreach($products as $product)
                <tr>
                    <td>{{ $product->id_product }}</td>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->quantity }}</td>
                    <td>{{ $product->fraser_loc }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

     {{------------------------------------------- 
    ----
    ----    Product With No Shelf Location
    ----
    ---------------------------------------------}}

    <h2>Product With No Shelf Location</h2>
    @php
        $products = App\Product::where('shelf_loc','')->where('disabled','N')->where('id_product','>',16)->get();
    @endphp

    <button class="btn btn-sm btn-primary" onclick="toggle('no_shelf_loc')">Toggle</button>

    <span>
        {{sizeof($products)}} Results found.
    </span>

    <table id="no_shelf_loc" class="table table-bordered mt-2">
        <thead>
            <th>PID</th>
            <th>Name</th>
            <th>Quantity</th>
            <th>Shelf Location</th>
        </thead>
        <tbody>
            @foreach($products as $product)
                <tr>
                    <td>{{ $product->id_product }}</td>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->quantity }}</td>
                    <td>{{ $product->shelf_loc }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    {{------------------------------------------- 
    ----
    ----    Duplicate Shelf Location
    ----
    ---------------------------------------------}}

    <h2>Products With Duplicate Shelf Location</h2>
    @php
        $locations = App\Product::selectRaw('shelf_loc, COUNT(*) as duplicate')->groupBy('shelf_loc')->havingRaw('duplicate >1  && duplicate < 5 && shelf_loc != "" && shelf_loc LIKE "S%-%"')->get();
    @endphp

    <button class="btn btn-sm btn-primary" onclick="toggle('duplicate_shelf')">Toggle</button>

    <span>
        {{sizeof($locations)}} Results found.
    </span>

    <table id="duplicate_shelf" class="table table-bordered mt-2">
        <thead>
            <th>Shelf Location</th>
            <th>Amount of Duplicate Items</th>
            <th>Items in location</th>
        </thead>
        <tbody>
            @foreach($locations as $loc)
                <tr>
                    <td>{{ $loc->shelf_loc }}</td>
                    <td>{{ $loc->duplicate }}</td>
                    <td>
                        @php $products = App\Product::where("shelf_loc",$loc->shelf_loc)->get(); @endphp
                        @foreach($products as $product)
                            <span data-toggle="tooltip" data-placement="bottom" title="{{$product->name}}">{{$product->id_product}},</span>
                        @endforeach
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    {{------------------------------------------- 
    ----
    ----    Disabled Items 
    ----
    ---------------------------------------------}}

    <h2>Disabled Products</h2>
    @php
        $products = App\Product::where('disabled','Y')->get();
    @endphp

    <button class="btn btn-sm btn-primary" onclick="toggle('disabled_products')">Toggle</button>

    <span>
        {{sizeof($products)}} Results found.
    </span>

    <table id="disabled_products" class="table table-bordered mt-2">
        <thead>
            <th>PID</th>
            <th>Name</th>
        </thead>
        <tbody>
            @foreach($products as $product)
                <tr>
                    <td>{{$product->id_product}}</td>
                    <td>{{$product->name}}</td>
                    
                </tr>
            @endforeach
        </tbody>
    </table>

    {{------------------------------------------- 
    ----
    ----    Obsoleted Products
    ----
    ---------------------------------------------}}

    <h2>Obsoleted and Active Products</h2>
    @php
        $notes = App\ProductNote::where('type_id','3')->get();
        $notes_count = 0;
    @endphp

    <button class="btn btn-sm btn-primary" onclick="toggle('obsolete_products')">Toggle</button>

    <table id="obsolete_products" class="table table-bordered mt-2">
        <thead>
            <th>PID</th>
            <th>Name</th>
        </thead>
        <tbody>
            @foreach($notes as $note)
                @if($note->product->isActive)
                    @php
                        $notes_count += 1;
                    @endphp
                <tr>
                    <td> <a href="{{config('custom.web_url')}}/product/{{$note->product->id_product}}.html" target="_blank">{{$note->product->id_product}}</a></td>
                    <td>{{$note->product->name}}</td>
                </tr>
                @endif
            @endforeach
        </tbody>
    </table>

    <span>
        {{$notes_count}} Results found.
    </span>

    {{------------------------------------------- 
    ----
    ----    Sales Restriction Products
    ----
    ---------------------------------------------}}

    <h2>Sales Restriction and Active Products</h2>
    @php
        $notes = App\ProductNote::where('type_id','8')->get();
        $notes_count = 0;
    @endphp

    <button class="btn btn-sm btn-primary" onclick="toggle('restricted_products')">Toggle</button>

    <table id="restricted_products" class="table table-bordered mt-2">
        <thead>
            <th>PID</th>
            <th>Name</th>
        </thead>
        <tbody>
            @foreach($notes as $note)
                @if($note->product->isActive)
                    @php
                        $notes_count += 1;
                    @endphp
                <tr>
                    <td><a href="{{config('custom.web_url')}}/product/{{$note->product->id_product}}.html" target="_blank">{{$note->product->id_product}}</a></td>
                    <td>{{$note->product->name}}</td>
                </tr>
                @endif
            @endforeach
        </tbody>
    </table>
    <span>
        {{$notes_count}} Results found.
    </span>

    {{------------------------------------------- 
    ----
    ----    Product with Cost Higher than Price
    ----
    ---------------------------------------------}}

    <h2>Margin Error (Selling Price < Cost)</h2>
    @php 
        $products = DB::connection('leeselectronic')
                        ->table('table_1')
                        ->select("id_product", "name", "price", "recent_cost")
                        ->where('disabled', 'N')
                        ->where('price', '<', 'recent_cost') 
                        ->limit(100)
                        ->get();                     
    @endphp

    <button class="btn btn-sm btn-primary" onclick="toggle('margin_error')">Toggle</button>

    <span>
        {{sizeof($products)}} Results found (Limited to 100).
    </span>

    <table id="margin_error" class="table table-bordered mt-2">
        <thead>
            <th>PID</th>
            <th>Name</th>
            <th>Price</th>
            <th>Recent Cost</th>
        </thead>
        <tbody>
            @foreach($products as $product)
                <tr>
                    <td>{{$product->id_product}}</td>
                    <td>{{$product->name}}</td>
                    <td>{{$product->price}}</td>
                    <td>{{$product->recent_cost}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    @include('layouts.error-flash')
    
@endsection

@push('bottom-scripts')
<script>
    function toggle(table){
        $('#' + table).toggle();
    }

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>

@endpush