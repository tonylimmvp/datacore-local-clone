@extends('backend.layouts.main')

@section('content')
    <div class="container d-flex justify-content-center" style="height:80vh">
        <div class="my-auto text-center">
            <p><b>Company Related Statistics:</b></p>
            <a href="{{route('sale-analytics.index')}}"><button class="btn btn-info">Performance Analytics</button></a>
            <hr>

            <p><b>Product Related Statistics</b></p>
            <a href="{{route('product-analytics.index')}}"><button class="btn btn-info">Product Margin Ranking</button></a>
            {{-- <a href="{{route('product-analytics.sales')}}"><button class="btn btn-info">Product Sale Analytics</button></a> --}}
            <a href="{{route('development')}}"><button class="btn btn-info">Product Sale Analytics</button></a>
            <a href="{{route('sale-suggestion')}}"><button class="btn btn-info">Sale/Discount Suggestion</button></a>
        </div>
    </div>
@endsection
