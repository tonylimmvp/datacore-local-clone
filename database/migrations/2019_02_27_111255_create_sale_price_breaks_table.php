<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalePriceBreaksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_price_breaks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');

            $table->integer('regular_qty1')->nullable();
            $table->integer('regular_qty2')->nullable();
            $table->integer('regular_qty3')->nullable();

            $table->decimal('regular_suggestion1')->nullable();
            $table->decimal('regular_suggestion2')->nullable();
            $table->decimal('regular_suggestion3')->nullable();

            $table->integer('wholesale_qty1')->nullable();
            $table->integer('wholesale_qty2')->nullable();
            $table->integer('wholesale_qty3')->nullable();
 
            $table->decimal('wholesale_suggestion1')->nullable();
            $table->decimal('wholesale_suggestion2')->nullable();
            $table->decimal('wholesale_suggestion3')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_price_breaks');
    }
}
