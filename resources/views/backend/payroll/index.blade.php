@extends('backend.layouts.main')

@section('content')
    <div>
        <h1>Payroll Report</h1>
        <hr>

        @foreach($periods as $period)
            <h4> {{ $period->period() }} </h4>
            <table class="table table-bordered">
                <thead>
                    <th>Employee</th>
                    <th>Vacation Days Used</th>
                    <th>Days Off</th>
                    <th>Total Hours</th>
                    @if(Auth::user()->hasAccess('events')->read)
                    <th>Actions</th>
                    @endif
                </thead>
                <tbody>
                    @foreach (App\Payroll::range($period->date_from,$period->date_to)->get() as $report)
                    <tr>
                        <td>{{ $report->employee->name }}</td>
                        <td>                                 
                            {{ App\Event::vacationsBetweenFor($report->date_from,$report->date_to,$report->employee->id) }}
                        </td>
                        <td> {{ count(App\Event::daysOffBetweenFor($report->date_from,$report->date_to,$report->employee->id)->get())}}</td>
                        <td>{{ $report->total_time }}</td>
                        @if(Auth::user()->hasAccess('events')->read)
                        <td class="text-center">
                            <a class="btn btn-primary" href="{{ route('coreui.schedule-employee',$report->employee->id) }}">
                                <span class="d-none d-sm-block">Schedule</span>
                                <span class="d-block d-sm-none" data-feather="calendar"></span>
                            </a>
                        </td>
                        @endif
                    </tr>
                    @endforeach  
                </tbody>     
            </table>
            <hr>
        @endforeach
 
    </div>
@endsection
