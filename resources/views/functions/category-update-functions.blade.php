<script>
/*
        Check Item
            - This checks if item is in database
*/

// Declaring variables to show message when typing
var typingTimer;
var doneTypingInterval = 10;
var finaldoneTypingInterval = 500;

$('input.category-input').keydown(function () {
    clearTimeout(typingTimer);
    var input = $(this).closest('td').find('.category-name');
    

    typingTimer = setTimeout(function () {
        input[0].innerHTML = 'Typing...';
    //     $(this).find('.category-name').innerHTML = 'Typing...';
    }, doneTypingInterval);
    
});

$('.category-input').keyup(function () {
    clearTimeout(typingTimer);
    var category_id = $(this).val();
    var input = $(this).closest('td').find('.category-name');
    
    typingTimer = setTimeout(function () {
        $.ajax({
            url:"{{route('category.autofill')}}",
            data:'_token={{csrf_token()}}&category_id='+category_id,
            type: 'POST',
            dataType: 'json',
            success: function(response){
                
                if(response.status=="success"){			
                    input[0].innerHTML = category_id + " - " + response.name;					
                }else if(response.status=="failed"){
                    input[0].innerHTML = "Category #" + category_id + " does not exist.";
                }else{
                    //change popup information!
                    input[0].innerHTML = "Error! Please contact developer.";		
                }
            },
            error: function(e){
                input[0].innerHTML = "Category Search Failed...";
            }
        });
    }, finaldoneTypingInterval);
    
});

$("form.categoryForm").submit(function(e) {
        var form = $(this);
        var url = form.attr('action');
    
        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            dataType: 'json',
            success: function(data){
                // header
                if(data.status == "success"){
                    var header = '<span data-feather="check-circle" style="width:50px;height:50px;stroke:white;"></span>';
                    $('#messageModal .modal-header').css("background-color","#6ba984");

                    form.html('<span style="color:green">Successfully Updated!</span>')
                }else{
                    var header = '<span data-feather="alert-circle" style="width:50px;height:50px;stroke:white;"></span>';
                    $('#messageModal .modal-header').css("background-color","#e66262");
                }
                
                $('#messageModal .modal-title').html(header);

                feather.replace()

                // body
                $('#messageModal .modal-body #message').html(data.message);
                $('#messageModal').modal('show');

            },
            error: function(request, status, error){

                var header = '<span data-feather="alert-circle" style="width:50px;height:50px;stroke:white;"></span>';
                $('#messageModal .modal-header').css("background-color","#e66262");

                $('#messageModal .modal-title').html(header);

                feather.replace()

                $('#messageModal .modal-body #message').html(error);
                $('#messageModal').modal('show');
            }
        });
    
        e.preventDefault(); // avoid to execute the actual submit of the form.
    });
</script>