@extends('frontend.layouts.main')
@push('css')
<link rel="stylesheet" href="{{asset("css/phonebook.css")}}">   
@endpush
@section('content')

<div class="title m-b-md">
    Phonebook
</div>
@include('layouts.errors')
<div class="row">
    

<form class="company-form form-group has-error" method="POST" action='{{route("phonebook-add")}}'>
        {{csrf_field()}}

        <h2>Add New Company:</h2>
        <div class="form-group">
            <label for="name">Name: <span class = "required-field">*required</span></label>
            <input type="text" name="name" id="name" required placeholder="eg: Lees Electronic" >
        </div>
        <div class="form-group">
            <label for="address">Address:</label>
            <input type="text" name="address" id="address" placeholder="eg: 4131 Fraser Street Vancouver BC V5V 3E9">
        </div>

        <div class="form-group">
            <label for="telephone">Telephone #: </label>
            <input type="tel" name="telephone" id="telephone" placeholder="eg: 6048751993">
        </div>

        <div class="form-group">
            <label for="site">Site:</label>
            <input type="test" name="site" id="site" placeholder="eg: https://leeselectronic.com">
        </div>

        <div class="form-group">
            <label for="maps">Maps Link:</label>
            <input type="text" name="maps" id="maps">
        </div>

        <div class="form-group">
            <label for="description">Short Description: <span class= "required-field">*required</span></label>
            <textarea name="description" class="form-control" id="description" cols="30" rows="10" style="resize :none" required></textarea>
        </div>

        <div class= "filters form-group">
            <label>Filters:</label>    
            @foreach ($filters as $filter)
                @if($filter->isActive)
                    <div class= "input-group-txt col-md-1 row">
                    <input type="checkbox" id="{{$filter->name}}"name= "{{$filter->name}}" class= "col-md-6" value = "{{$filter->id}}">
                        <label for="{{$filter->name}}" class= "col-md-6">{{$filter->name}}</label>
                    </div>
                @endif
            @endforeach
        </div>
        
        <input type="submit" class="btn btn-primary btn-lg" value="Add" >
        <input type="reset" class="btn btn-default btn-lg" value="Clear">

    </form>
    @endsection