<?php

namespace App\Http\Controllers;

use App\QuotationProduct;
use App\Quotation;
use Illuminate\Http\Request;

class QuotationProductController extends Controller
{

    public function update(Request $request, QuotationProduct $quotationProduct)
    {
        $this->validate(request(), [
            'pid' => 'required|exists:leeselectronic.table_1,id_product',
            'name' => 'required',
            'price' => 'required|numeric|min:0.01',
            'quantity' => 'required|numeric|min:1'
        ]);

        $quotationProduct->product_id = $request->pid;
        $quotationProduct->name = $request->name;
        $quotationProduct->price = $request->price;
        $quotationProduct->quantity = $request->quantity;
        $quotationProduct->vendor = $request->vendor;
        $quotationProduct->vendor_remark = $request->vendor_remark;
        $quotationProduct->save();

        $quotationProduct->quotation->touch();

        session()->flash('message',"You have successfully updated an item.");

        return back();
    }

    public function destroy(QuotationProduct $quotationProduct)
    {
        $quotationProduct->delete();

        session()->flash('message',"You have successfully deleted the item.");

        return back();
    }

    public function add(Quotation $quotation, Request $request){

        $this->validate(request(), [
            'pid' => 'required|exists:leeselectronic.table_1,id_product',
            'name' => 'required',
            'price' => 'required|numeric|min:0.01',
            'quantity' => 'required|numeric|min:1'
        ]);
        
        $product = new QuotationProduct();
        $product->quotation_id = $quotation->id;
        $product->product_id = $request->pid;
        $product->name = $request->name;
        $product->price = $request->price;
        $product->quantity = $request->quantity;
        $product->save();

        $product->quotation->touch();

        session()->flash('message',"You have successfully added the item.");

        return back();
    }

    /***********************************************************
     * 
     *  API
     * 
     ***********************************************************/

    public function addProduct(Request $request)
    {   
        $validator = \Validator::make($request->all(), [
            'pid' => 'required|exists:leeselectronic.table_1,id_product',
            'name' => 'required',
            'price' => 'required|numeric|min:0.01',
            'quantity' => 'required|numeric|min:1'
        ]);

        if ($validator->fails())
        {   
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        $product = new QuotationProduct();
        $product->quotation_id = $request->quote_id;
        $product->product_id = $request->pid;
        $product->name = $request->name;
        $product->price = $request->price;
        $product->quantity = $request->quantity;
        $product->save();

        $product->quotation->touch();

        return response()->json(['success'=>'You have successfully added ' . $request->pid]);

    }
}
