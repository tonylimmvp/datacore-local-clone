<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderSuggestion extends Model
{
    /**
     * The table associated with the model.
    */

    protected $connection='mysql';
    protected $table='order_suggestions';
    protected $guarded=[];

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

}
