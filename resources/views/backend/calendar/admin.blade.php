@extends('backend.layouts.main')

@include('layouts.fullcalendar')
@include('layouts.datepicker')

@section('content')
    <div id="calendar-tools" class="card card-body mb-4">
        <div class="row">
            <div class="col-3">
                <a href="{{ route('coreui.schedule')}}">
                    <button class="btn btn-primary" type="button">    
                        View Personal
                    </button>
                </a>
            </div>
        <div class="text-center col-6">
            <h2>All Schedules</h2>
        </div>
        <div class="text-right col-3">
            @if(Auth::user()->hasAccess('events')->create)
            <button class="btn btn-primary mb-2" data-toggle="modal" data-target="#createEventModal">Create Event</button>
            @endif
        </div>
        </div>
    </div>
    <div id="calendar"></div>

    @include('backend.calendar.schedule-admin')

@endsection