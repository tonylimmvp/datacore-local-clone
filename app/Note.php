<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Employee;

class Note extends Model
{
    /**
     * The table associated with the model.
    */
    protected $connection = "mysql";
    protected $table='notes';
    protected $guarded=[];

    public function online(){
        return $this->belongsToMany('App\OnlineOrder', 'datacore.online_notes', 'note_id', 'order_id');
    }

    public function order(){
        return $this->belongsToMany('App\Order', 'order_notes');
    }

    public function mobile(){
        return $this->belongsToMany('App\MobileOrder', 'datacore.mobile_notes', 'note_id', 'order_id');
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }

    public function handlingEmployee()
    {
        return $this->belongsTo(Employee::class, 'handledBy');
    }
}