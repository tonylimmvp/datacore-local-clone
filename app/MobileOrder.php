<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use DB;

class MobileOrder extends Model
{
    protected $connection = 'leeselectronic';
    protected $table = 'la_order';
    protected $primaryKey = 'order_id';

    public $incrementing = false;
    public $timestamps = false;

    protected $searchable = [
        'la_order.order_id',
        'la_order.reference',
        'la_user.name_first',
        'la_user.name_last',
        'la_user.email',
        'la_order_product.id_product',
        'table_1.name'
    ];

    public function products(){
        return $this->belongsToMany(Product::class, 'la_order_product','order_id','id_product')->withPivot('qty');
    }

    public function status(){
        return $this->hasOne(MobileOrderStatus::class,'order_state_id','current_state');
    }

    public function statusHistory(){
        return $this->hasMany(MobileOrderStatusHistory::class,'order_id');
    }

    public function courier(){
        return $this->hasOne(Courier::class,'id', 'courier_id');
    }

    public static function sales($employee_id){
        $employee = Employee::find($employee_id)->name;
        return $employee;
    }

    public function note(){
        return $this->belongsToMany('App\Note', 'datacore.mobile_notes', 'order_id', 'note_id');
    }

    /*************************************************************
     * 
     *          Order Functions
     * 
     *************************************************************/

    public static function newOrders(){
        $orders = static::whereIn('current_state',[0,1])->get();
        return $orders;
    }

    public function getTrackingURL(){
        if($this->shipping_number != ""){
            $url = $this->courier->url . $this->shipping_number;
        }else{
            $url = null;
        }
    
        return $url;
    }

    /*************************************************************
     * 
     *          Signature Functions
     * 
     *************************************************************/
    public function isSigned(){
        $file = Storage::disk('public')->exists('invoices/mobile'.$this->order_id.'.jpg');
        return $file;
     }

     public function lastSigned(){
        $url = url('/storage/invoices/mobile').$this->order_id.'.jpg';
        $c = curl_init($url);
        curl_setopt($c, CURLOPT_HEADER, 1); // Include the header    
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_FILETIME, 1);   
        curl_exec($c);
        $result = curl_getinfo($c);   

        return date('m/d/Y',(int)$result['filetime']);
     }

    /*************************************************************
     * 
     *         Customer Information
     * 
     *************************************************************/

    public function customer(){
        $customer = DB::connection('leeselectronic')->table('la_user')->selectRaw('user_id,name_first as firstname,name_last as lastname,email,phone')->where('user_id',$this->user_id)->first();
        return $customer;
    }

     /*************************************************************
     * 
     *          Order Address
     * 
     *************************************************************/

    public function addressBilling(){
        if($this->address_billing_id == 0){
            $address = null;
        }else{
            $address = DB::connection('leeselectronic')->table('la_address')->where('address_id',$this->address_billing_id)->first();
        }
        
        return $address;
    }

    public function addressShipping(){
        if($this->address_shipping_id == 0){
            $address = null;
        }else{
            $address = DB::connection('leeselectronic')->table('la_address')->where('address_id',$this->address_shipping_id)->first();
        }
        
        return $address;
    }


    public static function stateToName($state_id){
        $state = DB::connection('leeselectronic')->table('la_state')->where('state_id',$state_id)->first();
        return $state->name;
    }

    public static function stateToISO($state_id){
        $state = DB::connection('leeselectronic')->table('la_state')->where('state_id',$state_id)->first();
        return $state->iso_code;
    }

    ////////////////////////////////////////////
    //
    // Search
    //
    //////////////////////////////////////////

    protected function getWhereClause($term)
    {
        // removing symbols used by MySQL
        $reservedSymbols = ['<', '>', '@', '(', ')', '~','\''];
        $term = str_replace($reservedSymbols, '', $term);
        $whereClause = '';

        $words = explode(' ', $term);

        foreach($words as $word_id=>$word){
            if($word_id > 0){
                $whereClause .= ") AND (";
            }else{
                $whereClause .= "(";
            }
            foreach ($this->searchable as $col_id=>$column){
                if($col_id==0){
                    $whereClause .= "$column LIKE '%$word%' "  ; 
                }else{
                    $whereClause .= "OR $column LIKE '%$word%' "  ; 
                }
            }

            if($word_id + 1 == sizeof($words)){
                    $whereClause .= ")";
                }
        }
        return $whereClause;
    }

    public function scopeSearch($query, $term)
    {
        $columns = implode(',',$this->searchable);
        
        $whereClause = $this->getWhereClause($term);

        $query->selectRaw(' `la_order`.*, 
                            `la_order_product`.*,
                            `la_user`.`name_first` as firstname,
                            `la_user`.`name_last` as lastname,
                            `la_user`.`email`,
                            `table_1`.`name`')
                ->join('la_order_product','la_order.order_id','=','la_order_product.order_id')
                ->join('la_user','la_order.user_id','=','la_user.user_id')
                ->join('table_1','la_order_product.id_product','table_1.id_product')
                ->whereRaw($whereClause);
                
        return $query;
    }

    // END SEARCH

}
