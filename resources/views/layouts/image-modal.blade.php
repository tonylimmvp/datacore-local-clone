<div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
                <img src="#" class="img-fluid">
            </div>
        </div>
    </div>
</div>

@push('bottom-scripts')
<script>
    $(document).ready(function () {
        $('#imageModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var image = button.data('src') // Extract info from data-* attributes

            var modal = $(this)
            modal.find('img').attr('src', image)
        });
    });
</script>
@endpush