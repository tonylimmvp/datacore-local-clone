<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Payroll extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    /*************************************************
     * 
     * Eloquent Relationships
     * 
     *************************************************/
    
    public function employee(){
        return  $this->belongsTo(Employee::class);
    }

    /*************************************************
     * 
     * Scopes 
     * 
     *************************************************/
    
    public function scopePeriods($query){
        $query->groupBy('date_from','date_to')->orderBy('date_to','desc');
    }
    
    public function scopeRange($query,$start,$end){
        $query->where('date_from',$start)->where('date_to',$end);
    }

    /*************************************************
     * 
     * Functions
     * 
     *************************************************/

    public function period(){
        $start = Carbon::parse($this->date_from)->format('M d, Y');
        $end = Carbon::parse($this->date_to)->format('M d, Y');
        return  "$start - $end";
    }

    
}
