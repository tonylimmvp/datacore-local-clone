@push('css')
<link href='https://use.fontawesome.com/releases/v5.0.6/css/all.css' rel='stylesheet'>
<link href="{{asset('/assets/fullcalendar/packages/core/main.css')}}" rel="stylesheet"/>
<link href="{{asset('/assets/fullcalendar/packages/daygrid/main.css')}}" rel="stylesheet"/>
<link href="{{asset('/assets/fullcalendar/packages/timegrid/main.css')}}" rel="stylesheet"/>
<link href="{{asset('/assets/fullcalendar/packages/list/main.css')}}" rel="stylesheet"/>
<link href="{{asset('/assets/fullcalendar/packages/bootstrap/main.css')}}" rel="stylesheet"/>
@endpush

@push('bottom-scripts')
<script src="{{asset('assets/fullcalendar/packages/core/main.js')}}"></script>
<script src="{{asset('assets/fullcalendar/packages/interaction/main.js')}}"></script>
<script src="{{asset('assets/fullcalendar/packages/daygrid/main.js')}}"></script>
<script src="{{asset('assets/fullcalendar/packages/timegrid/main.js')}}"></script>
<script src="{{asset('assets/fullcalendar/packages/list/main.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="{{asset('assets/fullcalendar/packages/moment/main.js')}}"></script>
<script src="{{asset('assets/fullcalendar/packages/bootstrap/main.js')}}"></script>
@endpush