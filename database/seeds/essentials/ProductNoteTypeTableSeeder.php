<?php

use Illuminate\Database\Seeder;
use App\Permission;

class ProductNoteTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_note_types')->insert([
            'name' => 'General',
            'color' => 'C0C0C0', //silver
        ]);

        DB::table('product_note_types')->insert([
            'name' => 'Purchased',
            'color' => '50C878', //Emerald
            'isAutoInput' => '1',
        ]);
        
        DB::table('product_note_types')->insert([
            'name' => 'Obsoleted',
            'color' => 'c21525', //ruby
            'isAutoInput' => '1',
        ]);

        DB::table('product_note_types')->insert([
            'name' => 'Missing',
            'color' => 'ffd700',
        ]);
        
        DB::table('product_note_types')->insert([
            'name' => 'AdminOnly',
            'color' => 'ffcc99', //light orange
            'isAutoInput' => '1',
        ]);
        
        DB::table('product_note_types')->insert([
            'name' => 'StaffOnly',
            'color' => '00ffff', //aqua
            'isAutoInput' => '1',
        ]);

        DB::table('product_note_types')->insert([
            'name' => 'Renew Picture',
            'color' => 'ffccff', //light purple
        ]);

        DB::table('product_note_types')->insert([
            'name' => 'Sales Restriction',
            'color' => 'cc66ff', //purple
            'isAutoInput' => '1',
        ]);

        DB::table('product_note_types')->insert([
            'name' => 'Shipment Found',
            'color' => '009999', //?
            'isAutoInput' => '1',
        ]);
        
    }
}