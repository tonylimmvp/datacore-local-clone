<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoreIP extends Model
{
    protected $table = 'store_ips';

    public static function hasIP($ip){
        $results = StoreIP::where('ip_address',$ip)->get();
        if(sizeof($results)>0){
            return true;
        }else{
            return false;
        }
    }
}
