<p align="center"><img width="250px" src="https://leeselectronic.com/img/leeselectronic-logo-1529448909.jpg"></p>


# About Datacore

Datacore is an in-house application customized for Lee's Electronic Components. Datacore is a revamped software of it's earlier software (Fastsearch), originally created by Cheng Cao and later adapted by Leon Cao. Fastsearch was made up with pure PHP which made MVC and some common tasks, such as routing and API calls, difficult to do. The creators of Datacore, Leon Cao and Tony Lim, noticed these struggles and decided to recreate the software and focus the software on a PHP framework called Laravel which you can find more information about below.

## Pulling Project

### Setting up local server

To set up Datacore on your local device, please make sure you have a local server ([MAMP](https://www.mamp.info/en/downloads/)/[WAMP](http://www.wampserver.com/en/)) set up. Once set up, you can clone the git project into your local server directory.

For windows computer, it is recommended that you download a console emulator such as [cmder](https://cmder.net/).

Since our server is running on nginx, I suggust setting up your local development with nginx server.

#### Adding Nginx Config

Once you pull the project, be sure to update the your nginx config file by adding the following:

```
location /datacore/public/ {
        index index.html index.php;
        
        try_files $uri $uri/ /datacore/public/index.php?$query_string;	
    }
```

### Installing dependencies

When you finish setting up your server, you will need to install the project dependencies. You will need [composer](https://getcomposer.org/) and [npm](https://www.taniarascia.com/how-to-install-and-use-node-js-and-npm-mac-and-windows/) installed before continuing. 

```console
composer install
npm install
```

Once installed, you will need to run all the webpack files so that we can have all our css and js.

```console
npm install run dev
```

### Setting up config
Once done installing all the dependencies, you will need to set up your environment file which will have all your configurations. To copy a template of the environment file, copy the .env.example and name it .env

```console
cp .env.example .env
```

Once copied we will need to generate a Laravel key.

```console
php artisan key:generate
```

Now we can go ahead and edit the file using your favourite file editor.


```console
vim .env
```

There are a few key elements we have to update here. 

```env
# this is for datacore database
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=8889
DB_DATABASE=datacore
DB_USERNAME=root
DB_PASSWORD=root
DB_SOCKET=

# this is for leeselectronic database (for products/orders)
DB_CONNECTION_2=mysql
DB_HOST_2=127.0.0.1
DB_PORT_2=8889
DB_DATABASE_2=leeselectronic
DB_USERNAME_2=root
DB_PASSWORD_2=root
DB_SOCKET_2=

# this is for the mailing, without this any functions with emailing will break
MAIL_DRIVER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=
MAIL_PASSWORD=
MAIL_ENCRYPTION=null

# this is for links for the website
WEB_URL=http://localhost:insert_port/insert_website_directory/

```

### Exporting/Importing database
 
Now that we have our config file set up, we will need to import our database. Before we start importing we will need to migrate all of datacore migrations. To do so we will need to first create the *datacore* database. 

Once created, we will go onto a terminal and run the following command:

```
php artisan migrate
```

Now that the migration is completed, we can go ahead and import both *datacore* and *leeselectronic* database.


<hr>

# About Laravel

Laravel is a web application framework with expressive, elegant syntax. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, storage, caching and broadcasting to name a few. Laravel is accessible, yet powerful, providing tools needed for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of any modern web application framework, making it a breeze to get started learning the framework.

If you're not in the mood to read, [Laracasts](https://laracasts.com) contains over 1100 video tutorials on a range of topics including Laravel, modern PHP, unit testing, JavaScript, and more. Boost the skill level of yourself and your entire team by digging into our comprehensive video library.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).


Testing