@extends('backend.layouts.main')

@include('layouts.datatables')

@section('content')
    <h1>Employee Roles</h1>

    @if(Auth::user()->hasAccess('employee_role')->create)
    <div class="d-flex flex-row-reverse pt-3 pb-2 mb-3">
        
            <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <button class="btn btn-xs btn-primary"
                        data-toggle="modal" 
                        data-target="#createModal" >Add New Role</button>
            </div>
            </div>
    </div>
    @endif
    
    <table id="role-table" class="table table-striped table-bordered " cellspacing="0" width="100%">
        <thead>
            <tr>
            <th class="th-sm">ID</th>
            <th class="th-sm">Name</th>
            @if(Auth::user()->hasAccess('employee_role')->update)<th class="text-center">Action</th>@endif
            </tr>
        </thead>
        <tbody>
            @foreach($roles as $role)
                <tr>
                    <td>{{$role->id}}</td>
                    <td>{{$role->name}}</td>
                    @if(Auth::user()->hasAccess('employee_role')->update)
                    <td class="text-center">
                            <a href="{{ route('coreui.employee-role-permission.edit',$role->id)}}">
                                <button class="btn btn-sm btn-warning mb-2">
                                    Permissions
                                </button>
                            </a>
                                {{-- <button class="btn btn-sm btn-danger mb-2" 
                                data-toggle="modal" 
                                data-target="#deleteModal" 
                                data-id="{{ $role->id }}" 
                                data-name="{{ $role->name }}"
                                data-delete_url="{{ route('coreui.employees.delete',$role->id) }}">Delete</button> --}}
                    </td>
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>


    {{-- Create Modal --}}

    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <form action="{{ route('coreui.employee-role.store') }}" method="POST">
                    {{ csrf_field() }}
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Create New Employee Role</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    <label>Employee Role Name:</label>
                    <input class="form-control" type="text" name="name" id="role-name">
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                  <a id="employee-delete" href="#">
                  <button type="submit" class="btn btn-primary">Create New Employee Role</button>
                  </a>
                </div>
            </form>
              </div>
            </div>
          </div>

    @include('layouts.error-flash')
          
     
@endsection

@push('bottom-scripts')

<script type="text/javascript">
    $(document).ready(function () {
        $('#role-table').DataTable({
            "language": {
                "search": "Filter records:"
            }
            @if(Auth::user()->hasAccess('employee_role')->update)
            columnDefs: [ { orderable: false, targets: [2] } ]
            @endif
        });
    });
</script>

@endpush