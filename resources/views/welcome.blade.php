@extends('frontend.layouts.main')

@push('css')
    {{-- Styles --}}
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        .toc-disclaimer a {
            color: #000000;
            text-decoration: underline;
        }

        .toc-disclaimer a:hover{
            color: #3399ff;
        }
    </style>
@endpush
    
@section('content')
    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="title m-b-md">
                DataCore
            </div>
            <div class="toc-disclaimer">
                <h5>By using DataCore, the user has adhered, understood, and complied to <a href="{{route('toc')}}" target="_blank">DataCore / Fastsearch Usage Terms of Conditions.</a></h5>
            </div>
        </div>
    </div>
@endsection