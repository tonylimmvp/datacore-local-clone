<?php

namespace App\Http\Controllers;
use App\Product;
use App\ProductNote;
use App\ProductRelation;
use App\ProductPriceChange;
use Auth;
use DB;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function autofill(Request $request){
        // dd($request->all());
        $pid = $request->pid;
        $product = Product::find($pid);
        if($product){
            $response = array(
                'status' => 'success',
                'pid' => $product->id_product,
                'name' => $product->name,
                'price' => $product->price,
                'quantity' => $product->quantity,
                'vendor' => $product->vendor,
                'remarks' => $product->vendor_remarks
                );
        }else{
            $response = array(
                'status' => 'failed',
                'message' => 'Item does not exist',
            );
        }
        
        return response()->json($response);
    }

    public function exportNewProducts()
    {

        function arrayToCsv( array &$fields, $delimiter = ';', $enclosure = '"', $encloseAll = true, $nullToMysqlNull = false ) {
            $delimiter_esc = preg_quote($delimiter, '/');
            $enclosure_esc = preg_quote($enclosure, '/');
        
            $output = array();
            foreach ( $fields as $field ) {
                if ($field === null && $nullToMysqlNull) {
                    $output[] = 'NULL';
                    continue;
                }
        
                // Enclose fields containing $delimiter, $enclosure or whitespace
                if ( $encloseAll || preg_match( "/(?:${delimiter_esc}|${enclosure_esc}|\s)/", $field ) ) {
                    $output[] = $enclosure . str_replace($enclosure, $enclosure . $enclosure, $field) . $enclosure;
                }
                else {
                    $output[] = $field;
                }
            }
        
            return implode( $delimiter, $output );
        }
        
        ob_start();
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=exported_new_items.csv');

        ob_end_flush();
        
        // create a file pointer connected to the output stream
        $output_file = fopen('php://output', 'w');
        
        $head_array = array('ID', 'REFERENCE', 'NAME','SHORT_DESC', 'PRICE', 'QUANTITY', 'VENDOR');
        
        $head_row = arrayToCsv($head_array)."\n";
        fwrite($output_file, $head_row);
        
        // fetch the data
        $products = Product::newProducts();
        foreach($products as $product){
            $array= array(
                $product->id_product,
                $product->id_product,
                $product->name,
                $product->name,
                $product->price,
                $product->quantity,
                $product->vendor
            );
            $formatted_products = arrayToCsv($array) . "\n";
            fwrite($output_file, $formatted_products);
        }
    }


    public function profile(Product $product)
    {
        $notes = ProductNote::where('product_id', '=', $product->id_product)->get();
        $images = $product->getAllImagesUrl();
   
        return view('frontend.products.profile', compact('product', 'notes', 'images'));
    }

    public function handleNote(ProductNote $note, Product $product)
    {
        $productNote = ProductNote::find($note->id);
        $productNote->isHandled = '1';
        $productNote->handledDate = NOW();
        $productNote->handledBy = Auth::user()->id;

        $productNote->save();

        return redirect()->back();
    }

    public function createRelationship(Product $product, Request $request)
    {
        $relation = ProductRelation::updateOrCreate([
            'product_id' => $product->id_product,
            'related_product_id' => $request->relationship_pid,
            'related_product_name' => $request->relation_product_name,
            'related_vendor' => $request->relation_vendor,
            'related_vendor_remarks' => $request->relation_vendor_remarks,
            'comments' => $request->relationship_comments,
        ]);

        $relation->save();

        $relation2 = ProductRelation::updateOrCreate([
            'product_id' => $request->relationship_pid,
            'related_product_id' => $product->id_product,
            'related_product_name' => $product->name,
            'related_vendor' => $product->vendor,
            'related_vendor_remarks' => $product->vendor_remarks,
        ]);

        $relation2->save();

        return redirect()->back();
    }

    public function deleteRelationship(ProductRelation $relation)
    {
        $activeRelation = ProductRelation::find($relation->id)->delete();
        $activeRelation = ProductRelation::all();
        return back();
    }

    // Search 
    public function search() {
        $hold_vendor = session()->get('hold_vendor',false);
        return view('frontend.search',compact('hold_vendor'));
    }
    
    public function searchResults(Request $request) {
        $term = $request->term;
        if($term){
            $products = Product::search("$term")->paginate('100');
        }
        session()->put('hold_vendor',$request->hold_vendor);
        $hold_vendor = session()->get('hold_vendor',false);
        return view('frontend.search',compact('products','term','hold_vendor'));
    }

    public function obsoleteAjax(Product $product){
        // dd($product);
        if (!$product->isObsoleted()){
            $note = new ProductNote();
            $note->created_at = NOW();
            $note->type_id = 3;
            $note->product_id = $product->id_product;
            $note->comments = "Obsoleted on [". $note->created_at ."] by {". Auth::user()->name ."}";
            $note->employee_id = Auth::user()->id;
    
            $note->save();
            return json_encode(['status'=>'success','message'=>'Obsoleting Successful']);

        }else{
            return json_encode(['status'=>'success','message'=>'Product already Obsoleted.']);
        }

    }

    public function updateWeight(Product $product, Request $request){
        
        if(!$request->weight){
            return json_encode(['status'=>'failed','message'=>'Why you no fat.']);
        }

        // table_1

        $product->weight = $request->weight;
        $product->save();

        // ps_product

        $product->online->weight = $request->weight;
        $product->online->save();

        //ps_product_product_attribute
        DB::connection('leeselectronic')->table('ps_product_attribute')->where('id_product',$product->id_product)->update(['weight' => $request->weight]);

        //ps_product_product_attribute_shop
        DB::connection('leeselectronic')->table('ps_product_attribute_shop')->where('id_product',$product->id_product)->update(['weight' => $request->weight]);

        return json_encode(['status'=>'success','message'=>'Successfully updated weight.']);
    }

    public function updatePriceChanges(){
        // -- Scan Table 1 for all product prices. --
        // Obtaining id_product and price from leeselectronic_new.table_1
        $products = Product::where('disabled','N')->where('stockable','Y')->get();

        $count = 0;

        //Check last price in datacore with table_1
        foreach($products as $product){

            // Check if price change exist, if not we need to create new one
            if(sizeof($product->priceChanges)>0){
                $productLastPrice = $product->priceChanges->last()->new_price;
            }else{
                $productLastPrice = 0;
            }

            // check current price
            $productCurrentPrice = $product->price;
            
            //If changes exist, then insert to DB. OR changes does not exist
            if(($productCurrentPrice != (string) $productLastPrice)){
                $product_details = new ProductPriceChange();
                $product_details->product_id = $product->id_product;
                $product_details->product_name = $product->name;
                $product_details->old_price = $productLastPrice;
                $product_details->new_price = $product->price;
                $product_details->created_at = NOW();
                $product_details->save();
                $count++;
            }
        }
        
        session()->flash('message','There has been ' . $count . ' products with price changes.');

        return back();
    }

    public function editRelationship(Request $request, ProductRelation $relation)
    {
        $relation->comments = $request->comments;
        $relation->vendor_remarks = $request->remarks;
        $relation->save();

        return back();
    }
}