@extends('backend.layouts.main')

@include('layouts.datatables')

@section('content')

<h1>Vendor List</h1>

<table id="vendor_table" class="table table-bordered">
    <thead>
    <tr>
        <th>VID</th>
        <th>Vendor Code</th>
        <th>Comapny Name</th>
        <th>Address</th>
        <th style="min-width:140px">Contact</th>
        <th>Comments</th>
        <th>Active</th>
        <th>No. of Products</th>
    </tr>
    </thead>
    <tbody>
        @foreach($vendors as $vendor)
            <tr>
                <td><a href="{{ route('coreui.vendors.products',$vendor->id) }}">{{$vendor->id}}</a></td>
                <td>{{$vendor->reference}}</td>
                <td>{{$vendor->name}}</td>
                <td>
                    @if($vendor->address)
                    {{$vendor->address}}<br>
                    {{$vendor->city}}, {{$vendor->province}} {{$vendor->postal}} <br>
                    {{$vendor->country}}
                    @endif
                </td>
                <td> 
                    {{$vendor->contact}}<br>
                    Phone: {{$vendor->phone}}<br>
                    Cell: {{$vendor->cell}}<br>
                    Fax: {{$vendor->fax}}<br>
                    Email: {{$vendor->email}}<br>
                    <form class="urlForm" action="{{ route('vendors.update-url') }}" method="POST">
                        {{csrf_field()}}
                        <div class="input-group input-group-sm">

                            <input type="hidden" value="{{$vendor->id}}" name="vendor_id">
                            <div class="input-group-prepend">
                                <label class="input-group-text">URL</label>
                            </div>
                        
                            <input class="form-control" value="{{$vendor->url}}" name="url">
                            <div class="input-group-append">
                                <button class="btn btn-sm btn-primary"><span data-feather="chevron-right"></span></button>
                            </div>
                        </div>
                    </form>
                </td>
                <td>{{$vendor->comment}}</td>
                <td>
                    @if($vendor->isDisabled)
                    <span style="color:red">Disabled</span>
                    @else
                    <span style="color:darkcyan">Active</span>
                    @endif
                </td>
                <td>
                    {{sizeof($vendor->products->where('disabled','N'))}}
                </td>
            </tr>
        @endforeach
    </tbody>

</table>

@endsection

@push('bottom-scripts')

<script type="text/javascript">
        $(document).ready(function () {
            $('#vendor_table').DataTable(
                {
                    "language": {
                        "search": "Filter records:"
                    }
                }
            );
        });
        
        $("form.urlForm").submit(function(e) {
            var form = $(this);
            var url = form.attr('action');
        
            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(), // serializes the form's elements.
                dataType: 'json',
                success: function(data){
                    // header
                    if(data.status == "success"){
                        var header = '<span data-feather="check-circle" style="width:50px;height:50px;stroke:white;"></span>';
                        $('#messageModal .modal-header').css("background-color","#6ba984");
                    }else{
                        var header = '<span data-feather="alert-circle" style="width:50px;height:50px;stroke:white;"></span>';
                        $('#messageModal .modal-header').css("background-color","#e66262");
                    }
                    
                    $('#messageModal .modal-title').html(header);

                    feather.replace()

                    // body
                    $('#messageModal .modal-body #message').html(data.message);
                    $('#messageModal').modal('show');

                },
                error: function(request, status, error){
                    $('#notifierModal').modal('hide');

                    var header = '<span data-feather="alert-circle" style="width:50px;height:50px;stroke:white;"></span>';
                    $('#messageModal .modal-header').css("background-color","#e66262");

                    $('#messageModal .modal-title').html(header);

                    feather.replace()

                    $('#messageModal .modal-body #message').html(error);
                    $('#messageModal').modal('show');
                }
            });
        
            e.preventDefault(); // avoid to execute the actual submit of the form.
        });

</script>
@endpush