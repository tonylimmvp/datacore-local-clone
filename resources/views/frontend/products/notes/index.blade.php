@extends('frontend.layouts.main')

@push('top-scripts')
<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
@endpush

@push('css')
<style>
    h1 {
        text-align: center;
        font-family: 'Nunito', sans-serif;
        font-weight: 200;
    }

    h2 {
        font-family: 'Nunito', sans-serif;
        font-weight: 200; 
        text-align: center;
    }

    .flex-center {
        align-items: center;
        display: flex;
        justify-content: center;
    }

    .position-ref {
        position: relative;
    }

</style>
@endpush

@section('content')

    <br/>
    <h1>Profile for #{{$product->id_product}} for {{$product->name}}</h1>
    <hr/>

    <div class="table-responsive">
        <table class="table table-hover table-dark table-bordered" width="100%">
            <thead>
                <tr>
                    <th scope="col"><span data-feather="activity"></span> Type</th>
                    <th scope="col"><span data-feather="align-justify"></span> Notes</th>
                    <th scope="col"><span data-feather="user"></span> By</th>
                    <th scope="col"><span data-feather="calendar"></span> Time</th>
                    <th scope="col"><span data-feather="check-square"></span> Handled?</th>
                    <th scope="col"><span data-feather="list"></span> Actions</th>                   
                </tr>
            </thead>

            <tbody>
            @foreach ($notes as $note)
                @if (!($note->isHandled))
                    <tr>
                        <td><font color="#{{$note->type->color}}">{{ $note->type->name }}</font></td>
                        <td>{{ $note->comments }}</td>
                        <td>{{ $note->employee->name }}</td>
                        <td>{{ $note->created_at }}</td>
                        <td>{{ $note->isHandled }}</td>
                        <td>
                            <form class="form-inline" method="POST" action="{{route('notes.delete', $note->id)}}" enctype="multipart/form-data">
                                <input type="hidden" name="_method" value="POST">
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-primary btn-sm btn-action mb-1">Delete</button><br>
                            </form>
                        </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    <hr/>
@endsection

@push('bottom-scripts')
<script>
    jQuery(document).ready(function($) {
        $(".clickable-row").click(function() {
            window.location = $(this).data("href");
        });
    });
</script>
@endpush