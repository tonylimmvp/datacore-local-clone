@extends('frontend.layouts.main')

@include('layouts.datatables')

@push('top-scripts')
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
@endpush

@push('css')
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .position-ref {
            position: relative;
        }

        .links > a {
            color: #000000;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .panel-title a{
            color: #000000;
            font-size: 20px;
        }

        #toc_panel li{
            color: #000000;
            font-size: 16px;
        }
    </style>
@endpush

@section('content')
    <div class="position-ref">
    <section>
        <div class="page-header" id="toc">
            <h2>DataCore / Fastsearch Usage Terms and Conditions 
            <br/>
            <small>Revision: 2019/07/13</small></h2>
        </div>
        <hr/>
        
        <div class="panel-group" id="toc_panel">
        
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">
                        <a href="#collapse-2" data-toggle="collapse" data-parent="#toc_panel">
                            <b>1. Legality</b>
                        </a>
                    </div>
                    <div id="collapse-2" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ol>
                                <li style="list-style-type:lower-alpha">
                                    The web platforms DataCore and Fastsearch (hereby referred to as “aforementioned platforms”) are copyrighted directly by its authors and Lee’s Electronic Components (2005) Ltd. (hereby referred to as "Lee's Electronic" or "Lee's Electronic Components")
                                    All rights such as reproduction, distribution, and/or displaying any information for personal/commercial gain are strictly reserved without written consent of the author(s):
                                    <ol>
                                        <li style="list-style-type:lower-roman">
                                            Tony Lim: tony@leeselectronic.com (DataCore)
                                        </li>
                                        <li style="list-style-type:lower-roman">
                                            Leon Cao: leon@leeselectronic.com (DataCore / Fastsearch)
                                        </li>
                                        <li style="list-style-type:lower-roman">
                                            Aidan Cao: cheng@leeselectronic.com (Fastsearch)
                                        </li>
                                    </ol>
                                </li>
                                <li style="list-style-type:lower-alpha">
                                    By using these web platforms, the users have adhered, understood, and accepted to every terms of conditions that are in “DataCore / Fastsearch Usage Terms and Conditions” document.
                                </li>
                                <li style="list-style-type:lower-alpha">
                                    Lee’s Electronic Components and the authors of these platforms are not responsible for physical, and/or emotional distress, and/or financial loss that the user may suffer for utilizing the aforementioned platforms.
                                </li>
                                <li style="list-style-type:lower-alpha">
                                    Reverse engineering any functions and/or implementations for personal and/or commercial gain without the written consent of the aforementioned platforms’ authors will result in prosecution of copyright infringement.
                                </li>
                                <li style="list-style-type:lower-alpha">
                                    Consent for aforementioned platform can only be provided by the author(s) of that particular platform. 
                                </li>
                                <li style="list-style-type:lower-alpha">
                                    Lee's Electronic Components can deny user access to the aforementioned platform for any reason without any prior warning. 
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">
                        <a href="#collapse-3" data-toggle="collapse" data-parent="#toc_panel">
                            <b>2. Information and Data</b>
                        </a>
                    </div>
                    <div id="collapse-3" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ol>
                                <li style="list-style-type:lower-alpha">
                                    Users of the following platforms agree to the following:
                                    <ol>
                                        <li style="list-style-type:lower-roman">
                                            Lee’s Electronic Components have ownership to data and information inputted by user during their active usage session of the platforms.
                                        </li>
                                        <li style="list-style-type:lower-roman">
                                            Users agree to provide the most accurate relevant information needed to perform inquiries and data requests. 
                                            Users are solely responsible for misinformation provided.
                                        </li>
                                    </ol>
                                </li>
                                <li style="list-style-type:lower-alpha">
                                    Lee’s Electronic Components hold the rights to use users networking information such as IP Address, MAC Address, origin and destination packets when user(s) utilizes DataCore and Fastsearch to protect the aforementioned platforms.
                                </li>
                                <li style="list-style-type:lower-alpha">
                                    Information provided by DataCore and Fastsearch are intended solely for suggestion purposes. 
                                    Lee’s Electronic Components is not responsible for wrong data outputted by the aforementioned platforms.
                                </li>
                                <li style="list-style-type:lower-alpha">
                                    Sharing of user accounts is strictly prohibited. 
                                    If abusive intention or behavior is detected, Lee’s Electronic Components has the rights to permanently remove user(s) access to these platforms by any means necessary.
                                </li>
                                <li style="list-style-type:lower-alpha">
                                    Users agree to ensure the privacy of data provided by the aforementioned platforms. 
                                    Abusive exploitation will result in permanent removal of access and/or occupational consequences.
                                </li>
                                <li style="list-style-type:lower-alpha">
                                    Circumventing user’s given authorization is strictly prohibited, unless otherwise approved by Lee’s Electronic Components' management teams.
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">
                        <a href="#collapse-4" data-toggle="collapse" data-parent="#toc_panel">
                            <b>3. Usage</b>
                        </a>
                    </div>
                    <div id="collapse-4" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ol>
                                <li style="list-style-type:lower-alpha">
                                    DataCore and Fastsearch are solely meant to be used to increase productivity efficiency, enforce procedures, and allow control of outputted data in conjunction to a human’s own actions. 
                                    <b style="text-decoration: underline">The aforementioned platforms are not meant to replace human actions and responsibilities.</b>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

            <br/>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">
                        <a href="#collapse-99" data-toggle="collapse" data-parent="#toc_panel">
                            <b>Glossary</b>
                        </a>
                    </div>
                    <div id="collapse-99" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul>
                                <li>
                                    DataCore - A full stack sales operation assistance platform based on Laravel and JavaScript.
                                     DataCore implements and enhances Fastsearch platform authored by Aidan Cao.
                                     Created by Tony Lim, and Leon Cao for Lee's Electronic Components (2005) Ltd.
                                </li>
                                <li>
                                    Fastsearch - A full stack PHP query based platform to efficiently search products in database.
                                     Created by Aidan (Cheng) Cao, and modified by Leon Cao and Tony Lim for Lee's Electronic Components (2005) Ltd.
                                </li>
                                <li>
                                    IP Address - Identification address of a device connected to the network.
                                     Local Area Network (LAN) IP address are a local IP Address that are not accessible to outside of the range of the source provider (modem/router).
                                     Wide Area Network (WAN) IP address are global IP address given to a device by Internet Service Provider (ISP).
                                     WAN IP address are the IP address used by the device when accessing the global network such as the world wide web.
                                </li>
                                <li>
                                    Lee's Electronic Components (2005) Ltd. - Electronic component retail based company in Vancouver, BC, Canada.
                                     Founded in 1993 as a separate entity to the current company which was acquired in 2005.
                                     Presently Lee's Electronic Components is one of Greater Vancouver's leading electronic components provider.
                                </li>
                                <li>
                                    MAC Address - A unique device identification used for communication technologies such as Wi-Fi and Bluetooth.
                                     Each device's unique MAC Address are provided by the manufacturer of the device's network card.
                                </li>
                                <li>
                                    Networking packets (origin/destination packets) - Transmission of data sent through network as a "packet" handled by the transmission control protocol (TCP).
                                     In TCP/IP protocol, these networking packets contain (not limited to) data of source IP address and destination IP address.
                                </li>
                                <li>
                                    Reverse Engineering - The process of deconstructing of an object with an intention to reveal its design, architecture, and/or to extract knowledge from the object.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <b style="text-decoration: underline">Lee's Electronic Components hold the rights to modify these terms and conditions without prior notice.</b>
        </div>
    </section>
    </div>

@endsection