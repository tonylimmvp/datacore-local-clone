<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $connection = 'leeselectronic';
    protected $table = 'ps_manufacturer';
    protected $primaryKey = 'id_manufacturer';
    public $incrementing = false;
}
