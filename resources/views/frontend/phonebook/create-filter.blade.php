@extends('frontend.layouts.main')
@push('css')
<link rel="stylesheet" href="{{asset("css/phonebook.css")}}">   
@endpush
@section('content')

<div class="title m-b-md">
    Phonebook
</div>
@include('layouts.errors')
<div class="row">
    

<form class="company-form form-group has-error" method="POST" action='{{route("phonebook-store-filter")}}'>
        {{csrf_field()}}

        <h2>Add New Filter:</h2>
        <div class="form-group">
            <label for="name">Name: <span class = "required-field">*required</span></label>
            <input type="text" name="name" id="name" required >
        </div>
        <input type="submit" class="btn btn-primary btn-lg" value="Add" >
        <input type="reset" class="btn btn-default btn-lg" value="Clear">

    </form>
    @endsection