<?php

use Illuminate\Database\Seeder;
use App\OrderProduct;

class OrderProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product = new OrderProduct();
        $product->timestamps = false;
        $product->order_id = 1;
        $product->product_id = 10997;
        $product->name = "Arduino Uno R3";
        $product->price = 33.00;
        $product->quantity = 1;
        $product->save();

        $product = new OrderProduct();
        $product->timestamps = false;
        $product->order_id = 1;
        $product->product_id = 100;
        $product->name = "Component";
        $product->price = 1.00;
        $product->quantity = 5;
        $product->save();

        $product = new OrderProduct();
        $product->timestamps = false;
        $product->order_id = 3;
        $product->product_id = 100;
        $product->name = "Component";
        $product->price = 5.00;
        $product->quantity = 2;
        $product->save();

        $product = new OrderProduct();
        $product->timestamps = false;
        $product->order_id = 2;
        $product->product_id = 100;
        $product->name = "Component";
        $product->price = 2.50;
        $product->quantity = 4;
        $product->save();

        $product = new OrderProduct();
        $product->timestamps = false;
        $product->order_id = 4;
        $product->product_id = 10998;
        $product->name = "Arduino Board";
        $product->price = 47.00;
        $product->quantity = 3;
        $product->save();
       
    }
}
