<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OnlineOrder;
use App\SidePanelOnlineNote;

use Auth;

class SidePanelController extends Controller
{
    public function deleteNote(SidePanelOnlineNote $sidePanelNote)
    {
        $activeNote = SidePanelOnlineNote::find($sidePanelNote->id)->delete();
        return redirect()->back();
    }

    public function editNote(Request $request, SidePanelOnlineNote $sidePanelNote)
    {
        $sidePanelNote->notes = $request->notes;
        $sidePanelNote->employee_id = Auth::user()->id;
        $sidePanelNote->updated_at = NOW();
        $sidePanelNote->save();

        return redirect()->back();
    }

    public function createNote(Request $request)
    {
        $sidePanelNote = new SidePanelOnlineNote();

        $sidePanelNote->online_id = $request->id;
        $sidePanelNote->notes = $request->notes;
        $sidePanelNote->employee_id = Auth::user()->id;
        $sidePanelNote->created_at = NOW();
        $sidePanelNote->save();

        return back();
    }

    public function fetchSidePanelNoteInfo(Request $request)
    {
        $noteID = $request->sidePanelNote;
        
        $sql = SidePanelOnlineNote::where('id', $noteID)->first();

        if($sql){
            $result = $sql;

        }else{
            return json_encode(array('status' => 'failed', 'message' => 'Order could not be found.'));
        }

        $last_array = array('status' => 'success', 'result_data' => $result);
        //Append employee's name to be obtained by JS
        $last_array["employee_name"] = $sql->employee->name;

        //dd($last_array);
        return json_encode($last_array);
    }

}
