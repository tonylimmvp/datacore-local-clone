<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductVendor extends Model
{
    use Traits\MultiplePrimaryKeys;

    protected $connection = 'mysql';
    protected $primaryKey = ['product_id','vendor_id'];
    protected $guarded = [];

    public $timestamps = false;
    public $incrementing = false;

}
