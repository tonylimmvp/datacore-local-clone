@extends('backend.layouts.main')

@include('layouts.datatables')

@section('content')
    <h1>Permissions</h1>

    <div class="d-flex flex-row-reverse pt-3 pb-2 mb-3">
        
            <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <button class="btn btn-xs btn-primary"
                        data-toggle="modal" 
                        data-target="#createModal" >Add Permission</button>
            </div>
            </div>
    </div>
    
    <table id="permission-table" class="table table-striped table-bordered " cellspacing="0" width="100%">
        <thead>
            <tr>
            <th class="th-sm">ID
            </th>
            <th class="th-sm">Name
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($permissions as $permission)
                <tr>
                    <td>{{$permission->id}}</td>
                    <td>{{$permission->name}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>


    {{-- Create Modal --}}

    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <form action="{{ route('coreui.permission.store')}}" method="POST">
                    {{ csrf_field() }}
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Create New Permission</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    <label>Permission Name:</label>
                    <input class="form-control" type="text" name="name" id="permission-name" required>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                  <a id="employee-delete" href="#">
                  <button type="submit" class="btn btn-primary">Create Permission</button>
                  </a>
                </div>
            </form>
              </div>
            </div>
          </div>



          @include('layouts.error-flash')

     
@endsection

@push('bottom-scripts')

<script type="text/javascript">
    $(document).ready(function () {
        $('#permission-table').DataTable(
            {
                "language": {
                    "search": "Filter records:"
                }
            }
        );
    });
</script>

@endpush