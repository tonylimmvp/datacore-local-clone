<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class IntegrationController extends Controller
{
    public function bannerIndex(){
        return view('backend.integrations.website.banner-message');
    }

    public function bannerMessage(Request $request){
        Storage::disk('public')->put('banner_message.txt', $request->message_body);
   
        return back();
    }
}
