<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class SalesHistory extends Model
{
    use Traits\MultiplePrimaryKeys;
    protected $connection = "mysql";
    protected $guarded = [];
    protected $primaryKey = ['product_id','month','year'];
    public $timestamps = false;
    public $incrementing = false;

    public function products(){
        return $this->belongsTo(Product::class, 'product_id','id_product');
    }

    public static function allQuarterly(){
        $sales = SalesHistory::selectRaw("quarter_table.product_id, quarter_table.year,
        MAX(CASE WHEN quarter_table.quarter = 1 THEN (total_qty) END) AS q1,
        MAX(CASE WHEN quarter_table.quarter = 2 THEN (total_qty) END) AS q2,
        MAX(CASE WHEN quarter_table.quarter = 3 THEN (total_qty) END) AS q3,
        MAX(CASE WHEN quarter_table.quarter = 4 THEN (total_qty) END) AS q4")
            ->join(
            DB::raw("(SELECT product_id, year,FLOOR((month-1)/3)+1 quarter, SUM(qty) as total_qty
                    FROM `sales_histories`
                    GROUP BY product_id, year, FLOOR((month-1)/3)+1) AS quarter_table"), function ($join) {
                         $join->on('sales_histories.product_id', '=', 'quarter_table.product_id')
                                ->on('sales_histories.year', '=', 'quarter_table.year');
                    })
            ->groupBy("quarter_table.product_id","quarter_table.year")
            ->get();
        return $sales;
    }

    public static function quarterly($product_id,$year){
        $sales = SalesHistory::selectRaw("quarter_table.product_id, quarter_table.year,
        MAX(CASE WHEN quarter_table.quarter = 1 THEN (total_qty) END) AS q1,
        MAX(CASE WHEN quarter_table.quarter = 2 THEN (total_qty) END) AS q2,
        MAX(CASE WHEN quarter_table.quarter = 3 THEN (total_qty) END) AS q3,
        MAX(CASE WHEN quarter_table.quarter = 4 THEN (total_qty) END) AS q4")
            ->join(
            DB::raw("(SELECT product_id, year,FLOOR((month-1)/3)+1 quarter, SUM(qty) as total_qty
                    FROM `sales_histories`
                    GROUP BY product_id, year, FLOOR((month-1)/3)+1) AS quarter_table"), function ($join) {
                         $join->on('sales_histories.product_id', '=', 'quarter_table.product_id')
                                ->on('sales_histories.year', '=', 'quarter_table.year');
                    })
            ->where("quarter_table.product_id","=",$product_id)
            ->groupBy("quarter_table.product_id","quarter_table.year")
            ->having("quarter_table.year","=",$year)
            ->first();
        return $sales;
    }
    
    public static function quarterlyByYear($year){
        $sales = SalesHistory::selectRaw("quarter_table.product_id, quarter_table.year,
        MAX(CASE WHEN quarter_table.quarter = 1 THEN (total_qty) END) AS q1,
        MAX(CASE WHEN quarter_table.quarter = 2 THEN (total_qty) END) AS q2,
        MAX(CASE WHEN quarter_table.quarter = 3 THEN (total_qty) END) AS q3,
        MAX(CASE WHEN quarter_table.quarter = 4 THEN (total_qty) END) AS q4")
            ->join(
            DB::raw("(SELECT product_id, year,FLOOR((month-1)/3)+1 quarter, SUM(qty) as total_qty
                    FROM `sales_histories`
                    GROUP BY product_id, year, FLOOR((month-1)/3)+1) AS quarter_table"), function ($join) {
                         $join->on('sales_histories.product_id', '=', 'quarter_table.product_id')
                                ->on('sales_histories.year', '=', 'quarter_table.year');
                    })
            ->groupBy("quarter_table.product_id","quarter_table.year")
            ->having("quarter_table.year","=",$year)
            ->limit(20) //Used for debugging purposes only to not overload the system.
            ->get();
        return $sales;
    }

    public static function quarterlyByYearRank($year){
        $sales = SalesHistory::selectRaw("quarter_table.product_id, quarter_table.year,
        MAX(CASE WHEN quarter_table.quarter = 1 THEN (total_qty) END) AS q1,
        MAX(CASE WHEN quarter_table.quarter = 2 THEN (total_qty) END) AS q2,
        MAX(CASE WHEN quarter_table.quarter = 3 THEN (total_qty) END) AS q3,
        MAX(CASE WHEN quarter_table.quarter = 4 THEN (total_qty) END) AS q4")
            ->join(
            DB::raw("(SELECT product_id, year,FLOOR((month-1)/3)+1 quarter, SUM(qty) as total_qty
                    FROM `sales_histories`
                    GROUP BY product_id, year, FLOOR((month-1)/3)+1) AS quarter_table"), function ($join) {
                         $join->on('sales_histories.product_id', '=', 'quarter_table.product_id')
                                ->on('sales_histories.year', '=', 'quarter_table.year');
                    })
            ->groupBy("quarter_table.product_id","quarter_table.year")
            ->having("quarter_table.year","=",$year)
            ->limit(20)
            ->get();
        return $sales;
    }

    public static function quarterlyByProduct($product_id){
        $sales = SalesHistory::selectRaw("quarter_table.product_id, quarter_table.year,
        MAX(CASE WHEN quarter_table.quarter = 1 THEN (total_qty) END) AS q1,
        MAX(CASE WHEN quarter_table.quarter = 2 THEN (total_qty) END) AS q2,
        MAX(CASE WHEN quarter_table.quarter = 3 THEN (total_qty) END) AS q3,
        MAX(CASE WHEN quarter_table.quarter = 4 THEN (total_qty) END) AS q4")
            ->join(
            DB::raw("(SELECT product_id, year,FLOOR((month-1)/3)+1 quarter, SUM(qty) as total_qty
                    FROM `sales_histories`
                    GROUP BY product_id, year, FLOOR((month-1)/3)+1) AS quarter_table"), function ($join) {
                         $join->on('sales_histories.product_id', '=', 'quarter_table.product_id')
                                ->on('sales_histories.year', '=', 'quarter_table.year');
                    })
            ->where("quarter_table.product_id","=",$product_id)
            ->groupBy("quarter_table.product_id","quarter_table.year")
            ->get();
        return $sales;
    }

}
