<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\OrderSuggestion;
use App\Employee;
use App\EmployeeRole;
use App\ProductNote;
use App\ProductNoteType;
use App\ShipmentCart;
use App\Notifier;
use Carbon\Carbon;
use Auth;

class OrderSuggestionController extends Controller
{
    public function index(){
        $order_suggestions = OrderSuggestion::all();

        return view('frontend.order-suggestion.index', compact('order_suggestions'));
    }

    public function create(Product $product)
    {
        $suggestion = new OrderSuggestion();

        //Database Insert to order_suggestions db.
        $suggestion->product_id = $product->id_product;
        $suggestion->product_name = $product->name;
        $suggestion->store_qty = $product->quantity;
        $suggestion->recent_cost = $product->recent_cost;
        $suggestion->vendor = $product->vendor;
        $suggestion->remarks = $product->vendor_remarks;
        $suggestion->recent_cost = $product->recent_cost;
        $suggestion->employee_id = Auth::user()->id;
        $suggestion->created_at = NOW();

        $suggestion->save();

        $order_suggestions = OrderSuggestion::all();

        //This will return to the previous url and current scroll location
        return redirect(url()->previous().'#'.$product->id_product);
    }

    public function purchase(Product $product, Request $request)
    {
        // Validate Purchase and Hold
        $this->validate(request(), [
            'qty' => 'required|numeric|min:1',
            'unit' => 'required'
        ]);

        $note = new ProductNote();
        $note->type_id = 2;
        $note->product_id = $product->id_product;
        $note->qty = $request->qty;
        $note->unit = $request->unit;
        $note->comments = $request->comments;
        $note->employee_id = Auth::user()->id;

        $note->save();

        if($product->orderSuggestion){
            $product->orderSuggestion->delete();
        }

        return redirect()->back();
    }

    public function delete(OrderSuggestion $suggestion)
    {
        $order_suggestions = OrderSuggestion::where('product_id', '=', $suggestion->product_id)->delete();
        $order_suggestions = OrderSuggestion::all();
        return redirect()->route('order-suggestion',compact('order_suggestions'));
    }

    public function obsolete(Product $product)
    {
        $note = new ProductNote();
        $note->created_at = NOW();
        $note->type_id = 3;
        $note->product_id = $product->id_product;
        $note->comments = "Obsoleted on [". $note->created_at ."] by {". Auth::user()->name ."}";
        $note->employee_id = Auth::user()->id;

        $note->save();

        $order_suggestions = OrderSuggestion::where('product_id', '=', $product->id_product)->delete();
        
        return redirect()->back();
    }

    public function purchaseAndShip(Request $request, Product $product)
    {
        $this->validate(request(), [
            'id' => 'required|integer|exists:leeselectronic.table_1,id_product',
            'qty' => 'required|numeric|min:1',
            'unit' => 'required'
        ]);

        $note = new ProductNote();
        $note->type_id = 2;
        $note->product_id = $request->id;
        $note->qty = $request->qty;
        $note->unit = $request->unit;
        $note->comments = $request->comments;
        $note->employee_id = Auth::user()->id;
        $note->isHandled = '1';
        $note->updated_at = NOW();
        $note->handledBy = Auth::user()->id;
        $note->handledDate = NOW();

        $note->save();
        
        $shipmentNote = new ProductNote();
        $shipmentNote->type_id = 9;
        $shipmentNote->product_id = $request->id;
        $shipmentNote->qty = $request->qty;
        $shipmentNote->unit = $request->unit;
        
        $shipmentNote->employee_id = Auth::user()->id;
        $shipmentNote->created_at = NOW();
        $shipmentNote->save();

        $cart = new ShipmentCart();
        $cart->shipment_id = $request->shipment;
        $cart->product_id = $request->id;
        $cart->item_name = $request->name;
        $cart->qty = $request->qty;
        $cart->unit = $request->unit;
        $cart->vendor = $request->vendor;
        $cart->vendor_remarks = $request->remarks;
        $cart->comments = $request->comments;
        $cart->invoice = $request->invoice;
        $cart->employee_id = Auth::user()->id;
        $cart->created_at = NOW();
        $cart->note_id = $shipmentNote->id;

        $cart->save();


        if($product->orderSuggestion){
            $product->orderSuggestion->delete();
        }

        // return redirect()->route('shipment.show');
        return redirect()->back();
    }

    public function editOrder(ProductNote $note, Request $request)
    {
        $this->validate(request(), [
            'qty' => 'required|numeric|min:1',
            'unit' => 'required'
        ]);

        $note->product_id = $request->id;
        $note->product->name = $request->name;
        $note->qty = $request->qty;
        $note->unit = $request->unit;
        $note->comments = $request->comments;

        $note->employee_id = Auth::user()->id;

        $note->save();

        return redirect()->route('ordered-products');
    }

    public function insertToOrderedProducts(Request $request)
    {
        $this->validate(request(), [
            'product_id' => 'required|integer|exists:leeselectronic.table_1,id_product',
            'qty' => 'required|numeric|min:0',
        ]);

        $note = new ProductNote();
        $note->type_id = 2;
        $note->product_id = $request->product_id;

        $productID = $request->product_id;

        if($request->name) {
            $note->name = $request->name;
        } else {
            $productName = Product::find($productID)->name;
            $note->name = $productName;
        }

        $note->qty = $request->qty;
        $note->unit = $request->unit;
        $note->comments = $request->comments;
        $note->employee_id = Auth::user()->id;

        $note->save();

        return redirect()->route('ordered-products');
    }

    public function addToOrderSuggestion(Request $request) 
    {
        if(!$request->pid){
            return json_encode(['status'=>'failed','message' => 'No product ID found.']);
        }
        //$product = Product::find($request->pid);
        $product = Product::where('id_product', $request->pid)->first();

        if(!$product){
            return json_encode(['status'=>'failed','message' => 'Product ID does not exist.']);
        }

        $suggestion = OrderSuggestion::firstOrCreate(
            ['product_id' => $request->pid],
            ['product_name' => $product->name, 
            'store_qty' => $product->quantity, 
            'recent_cost' => $product->recent_cost, 
            'vendor' => $product->vendor, 
            'remarks' => $product->vendor_remarks, 
            'employee_id' => Auth::user()->id]
        );

        $suggestion->touch();

        return json_encode(['status'=>'success']);
    }

    //This is a one time integration system to consolidate notifier list to order suggestion list
    public function consolidateNotifierToOrderSuggestion() 
    {
        $notifiers = Notifier::all();

        //Iterate each notifier, get the id_product, and enter it into order-suggestion
        foreach($notifiers as $notifier){
            $product = Product::where('id_product', $notifier->id_product)->first();

            $suggestion = OrderSuggestion::firstOrCreate(
                ['product_id' => $notifier->id_product],
                ['product_name' => $product->name, 
                'store_qty' => $product->quantity, 
                'recent_cost' => $product->recent_cost, 
                'vendor' => $product->vendor, 
                'remarks' => $product->vendor_remarks, 
                'employee_id' => 1]
            );
        }
    }

    public function ignoreSuggestionAJAX(Request $request) {
        if(!$request->suggestion_id){
            return json_encode(['status'=>'failed','message' => 'No product ID found.']);
        }
        $suggestion = OrderSuggestion::find($request->suggestion_id);

        if(!$suggestion){
            return json_encode(['status'=>'failed','message' => 'Suggestion ID does not exist.']);
        }

        $suggestion->delete();
        
        return json_encode(['status'=>'success']);
    }

    public function removeWhenGreaterThanThreeMonths() 
    {
        $suggestions = OrderSuggestion::where('created_at', '<=', Carbon::now()->subDays(90)->toDateTimeString())->get();

        foreach($suggestions as $suggestion){
            $suggestion->delete();
        }
        
        return redirect()->route('order-suggestion');
    }

    public function removeByQty() 
    {        
        $suggestions = OrderSuggestion::all();

        foreach($suggestions as $suggestion){
            //Get Product's current qty:
            $currentQuantity = $suggestion->product->quantity;

            //Get Quantity on suggestion creation.
            $suggestionQuantity = $suggestion->store_qty;

            if($currentQuantity > $suggestionQuantity){
                $suggestion->delete();
            }
        }
        
        return redirect()->route('order-suggestion');
    }
}
