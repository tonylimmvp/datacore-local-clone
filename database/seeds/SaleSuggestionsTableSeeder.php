<?php

use Illuminate\Database\Seeder;

class SaleSuggestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sale_suggestions')->insert([
            'product_id' => '10997',
            'price' => '33.00',
            'cost' => '20.02',
            'vendor_id' => '1024',

            'sale_percentage1' => '0.05',
            'adj_price1' => '1.65',
            'profit_margin1' => '0.361',

            'sale_percentage2' => '0.10',
            'adj_price2' => '3.30',
            'profit_margin2' => '0.326',

            'sale_percentage3' => '0.25',
            'adj_price3' => '8.25',
            'profit_margin3' => '0.191',

            'employee_id' => '1',
            'created_at' => NOW()
        ]);
    }
}
