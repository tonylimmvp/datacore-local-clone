<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Order;
use App\OnlineOrder;

class PrinterController extends Controller
{
    public function printLabel(){
        return view('functions.printers.index');
    }
    
    public function printQuantityLabel(){
        return view('functions.printers.quantity');
    }

    public function printNoPriceLabel(){
        return view('functions.printers.noprice');
    }

    public function printOrderQRCode(){
        return view('functions.printers.order-qrcode');
    }

    public function printOnlineOrderQRCode(){
        return view('functions.printers.online-qrcode');
    }

    public function printMobileOrderQRCode(){
        return view('functions.printers.mobile-qrcode');
    }
    
    public function printPostek(Product $product, Request $request){
        return view('functions.printers.postek',compact('product', 'request'));
    }

    public function printPostekQuantity(Product $product, Request $request){
        return view('functions.printers.postek-quantity',compact('product', 'request'));
    }

    public function printPostekNoPrice(Product $product, Request $request){
        return view('functions.printers.postek-noprice',compact('product', 'request'));
    }

    public function printPostekOrderQRCode(Order $order, Request $request){
        return view('functions.printers.postek-order-qrcode', compact('order','request'));
    }

    public function printPostekOnlineOrderQRCode(OnlineOrder $order, Request $request){
        return view('functions.printers.postek-online-qrcode', compact('order','request'));  
    }

    public function printPostekMobileOrderQRCode(OnlineOrder $order, Request $request){
        return view('functions.printers.postek-mobile-qrcode', compact('order','request'));  
    }
    
}
