@component('mail::message')

## A new quotation has been created by {{$quotation->employee->name}}.

**Customer:** {{$quotation->name}}<br>
**Phone:** {{$quotation->phone}}@if($quotation->phone_ext), Ext.{{$quotation->phone_ext}} @endif<br>
**Email:** {{$quotation->email}}<br>

@component('mail::table')
| PID        | Name           | Price per Unit  | Quantity | 
| ---------- |:--------------:| :--------------:| :------: |
@foreach($quotation->products as $product)
| {{$product->id_product}} | {{$product->pivot->name}} | ${{$product->pivot->price}} | {{$product->pivot->quantity}} |
@endforeach
@endcomponent

@component('mail::button', ['url' => route('quotations')])
View Quotation
@endcomponent

@endcomponent
