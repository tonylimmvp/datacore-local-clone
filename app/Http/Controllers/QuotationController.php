<?php

namespace App\Http\Controllers;

use App\Quotation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\QuotationProduct;
use App\Employee;
use Mail;
use App\Mail\QuotationCreateCustomer;
use App\Mail\QuotationCreateEmployee;

class QuotationController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
        $quotations = Quotation::with('products')->filter(request(['expired']))->latest()->get();

        return view('frontend.quotations.index',compact('quotations'));
    }

    public function create()
    {
        $employees = Employee::isActive()->get();
        return view('frontend.quotations.create',compact('employees'));
    }

    public function store(Request $request)
    {
        $rules = [
            'product_id.*' => 'required|exists:leeselectronic.table_1,id_product',
            'product_price.*' => 'between:0,9999.99',
            'name' => 'required',
            'phone' => 'required|min:10',
            'email' => 'required|email',
            'employee' => 'required|exists:employees,id'
        ];

        $customMessages = [
            'product_id.required' => "There must be at least 1 product.",
            'product_id.exists' => "One or more item(s) does not exist."
         ];

        $this->validate($request, $rules, $customMessages);

        // dd($request->all());

        // END OF VALIDATIONS

        $phone = preg_replace('/\D+/', '', $request->phone);

        $quotation = new Quotation();
        $quotation->name = $request->name;
        $quotation->phone = $phone;
        $quotation->phone_ext = $request->phone_ext;
        $quotation->email = $request->email;
        if($request->comments){
            $quotation->comments = Crypt::encryptString($request->comments);
        }
        if($request->internal){
            $quotation->lego = Crypt::encryptString($request->internal);
        }
        $quotation->employee_id = $request->employee;

        $quotation->save();

        // Products
        for($i = 0 ; $i < sizeof($request->product_id) ;$i++){
            $product = QuotationProduct::create([
                'product_id' => $request->product_id[$i],
                'name' => $request->product_name[$i],
                'price' => $request->product_price[$i],
                'quantity' => $request->product_quantity[$i],
                'vendor' => $request->product_vendor[$i],
                'vendor_remark' => $request->product_vendor_remark[$i],
                'quotation_id' => $quotation->id
            ]);
            $product->timestamps = false;
            $product->save();   
        }

        $quotations = Quotation::all();

        // Emails
        Mail::to($quotation->email)->send(new QuotationCreateCustomer($quotation));

        // check who opt in for emails
        $quotationMailList = [];

        foreach(Employee::where('id','!=',$quotation->employee->id)->get() as $employee){
            if($employee->hasAccess('quotation_email')->read){
                array_push($quotationMailList,$employee->email);
            }
        }

        if(config('mail.host','smtp.mailtrap.io') != 'smtp.mailtrap.io'){
            \Mail::to($quotation->employee->email)
                    ->bcc($quotationMailList)
                    ->send(new \App\Mail\QuotationCreateEmployee($quotation));
        }else{
            \Mail::to($quotation->employee->email)
            ->send(new \App\Mail\QuotationCreateEmployee($quotation));
        }

        return redirect()->route('quotations',compact('quotations'));
    }

    public function updateComment(Request $request, Quotation $quotation)
    {   
        $quotation->comments = Crypt::encryptString($request->comments);
        $quotation->save();

        $quotation->touch();

        session()->flash('message','You have updated the comment for Quotation #' . $quotation->id);

        return back();
    }

    public function updateLego(Request $request, Quotation $quotation)
    {
        $quotation->lego = Crypt::encryptString($request->internal);
        $quotation->save();

        $quotation->touch();

        session()->flash('message','You have updated the internal comment for Quotation #' . $quotation->id);

        return back();
    }

    public function update(Request $request, Quotation $quotation)
    {
        //
    }

    public function destroy(Quotation $quotation)
    {
        //
    }
}
