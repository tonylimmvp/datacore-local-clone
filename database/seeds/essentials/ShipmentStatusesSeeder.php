<?php

use Illuminate\Database\Seeder;

class ShipmentStatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Seeding rma_status table begins
        DB::table('shipment_statuses')->insert([
            'name' => 'Pending / In Progress',
            'color' => 'B7D9E5', //Aqua
            'position' => '1',
        ]);

        DB::table('shipment_statuses')->insert([
            'name' => 'Delayed / On Hold',
            'color' => 'ffff00', //yellow
            'position' => '2',
        ]);

        DB::table('shipment_statuses')->insert([
            'name' => 'Shipped',
            'color' => '66FF66', //Emerald
            'position' => '3',
        ]);

        DB::table('shipment_statuses')->insert([
            'name' => 'Cancelled',
            'color' => 'FF0000', //Red
            'position' => '4',
        ]);

        DB::table('shipment_statuses')->insert([
            'name' => 'Arrived',
            'color' => 'b76e79', //Rose Gold
            'position' => '5',
        ]);  

        DB::table('shipment_statuses')->insert([
            'name' => 'Done',
            'color' => '00FF00', //Green
            'position' => '6',
        ]);  
    }
}
