@extends('frontend.layouts.main')

@include('layouts.datatables')

@push('top-scripts')
<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
@endpush

@push('css')
<style>
    h1 {
        text-align: center;
        font-family: 'Nunito', sans-serif;
        font-weight: 200;
    }

    h2 {
        font-family: 'Nunito', sans-serif;
        font-weight: 200; 
        text-align: center;
    }

    .flex-center {
        align-items: center;
        display: flex;
        justify-content: center;
    }

    .position-ref {
        position: relative;
    }

	.product_images {
		width: 250px;
		height: auto;
	}

	.product-image-thumbnail {
        display: block;
        width: 100px;
        height: auto;
    }

    td img {
        display: block;
        margin-left: auto;
        margin-right: auto;
    }

</style>
@endpush

@section('content')

    <h1>Profile for #{{$product->id_product}} for {{$product->name}}
	</h1>
	<hr/>
	
	<div class="form-inline">
		<div class="input-group">
			<input id="profile_product" type="number" class="form-control" value="{{$product->id_product}}"> 
			<button type="button" class="input-group-append btn btn-primary" onclick="searchProductProfile()"> Search </button>
		</div>
	</div>

	<h2>Images</h2>
	<div class="position-ref flex-center">
		@foreach($images as $image)
			<img class="product_images" src='{{$image}}'>
		@endforeach
	</div>
	<hr/>

	<h2>Information:</h2>
	<div class="table_responsive">
	<table id="products-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th class="th-sm">ID</th>
				<th class="th-sm">Name</th>
				<th class="th-sm">Price</th>
				@auth @if(Auth::user()->hasAccess('statistics')->read && Auth::user()->hasRole('SuperAdmin'))<th class="th-sm">Margin</th>@endif @endauth
				<th class="th-sm">Quantity</th>
				<th class="th-sm"> Shelf Location</th>
				@auth <th class="th-sm"> Storage Location</th> @endauth
				@auth  @if(Auth::user()->hasAccess('vendors')->read)<th class="th-sm">Vendor</th>@endif @endauth
				<th class="th-sm">Actions</th>
				<th class="th-sm">Category</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="searchID">
					<a href="{{config('custom.web_url')}}/product/{{$product->id_product}}.html" target="_blank">
					{{-- <a href="{{route('product-profile.show', $product->id_product)}}"> --}}
						{{$product->id_product}}
					</a>
				</td>

				<td>{{$product->name}}
					@auth
					@include('layouts.product-name-icons')

					{{-- Orders for product --}}
					@if($product->orders)
						@foreach($product->orders as $order)
							@if($order->currentStatus->name != "Done")
								<br>
								<a href="{{route('orders.detail',$order->id)}}">
									<button class="btn btn-sm btn-secondary m-1">
										Order #{{$order->id}} - {{$order->name}}
									</button>
								</a>
							@endif
						@endforeach
					@endif

					{{-- show quotations if exit --}}
					@if($product->quotations)
						@foreach($product->quotations as $quote)
							@if(!$quote->isExpired)
								<br>
								<a href="{{route('quotations')}}">
									<button class="btn btn-sm btn-info m-1">
										Quote #{{$quote->id}} - {{$quote->name}}
									</button>
								</a>
							@endif
						@endforeach
					@endif

				@endauth
				</td>
				<td>
					{{-- check if on sale --}}
					@if($product->onSale())
						@foreach($product->onlineSales as $sale)
							@if($sale->onSale())
								<span style="text-decoration: line-through;">
									${{$product->price}}
								</span>
								<p style="font-size:18px;color:red;">${{$sale->salePrice()}}</p>
							@endif
						@endforeach
					@else
						<span>
							${{$product->price}}
						</span>
					@endif

					@if($product->online)
						@if($product->online->unity)
							/{{$product->online->unity}}
						@endif
					@endif
					
				</td>
				@php $margin = number_format(($product->price - $product->recent_cost) / $product->price, 3); @endphp
				@auth @if(Auth::user()->hasAccess('statistics')->read && Auth::user()->hasRole('SuperAdmin'))<td>{{($margin)*100}}%</td> @endif @endauth
				<td>{{$product->quantity}}</td>
				<td><span class="cursor-pointer" onclick="displayShelfLoc('{{$product->shelf_loc}}')">{{$product->shelf_loc}}</span></td>

				{{-- LOCATION --}}
				@auth
				<td class="text-center">
					{{$product->locations()}} 
				</td>
				@endauth
				{{-- END OF LOCATION --}}

				@auth @if(Auth::user()->hasAccess('vendors')->read)
				<td class="text-center lightTable">
					@if(sizeof($product->vendors)>0)
					@foreach($product->vendors as $index=>$vendor)
						@if($index>0)
							<hr>
						@endif

						<a href="{{ route('coreui.vendors.products',$vendor->id) }}">{{$vendor->reference}}</a>
						@if($vendor->pivot->remark)
							| 
							@if($vendor->url)
								<a href="{{$vendor->url . $vendor->pivot->remark}}" target="_blank">{{$vendor->pivot->remark}} </a>
							@else
								{{$vendor->pivot->remark}} 
							@endif
						@endif

					@endforeach
					@else
						{{$product->vendor}} @if($product->vendor_remarks) | {{$product->vendor_remarks}} @endif
					@endif
				</td>
				@endif @endauth
				<td width="100px">   
                    <div class="container text-center">

                    @auth
					
					@if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Supervisor'))
						<button id='obsoleteBtn{{$product->id_product}}' 
							type="button" 
							class="btn btn-secondary btn-sm btn-action mb-1" 
							onclick="obsolete('{{route('product.obsolete', $product->id_product)}}', '{{$product->id_product}}')" 
							@if($product->isObsoleted()) disabled @endif>Obsoleting
						</button><br>
					@endif

                    <button class="btn btn-secondary btn-sm btn-action mb-1" onclick="addToBoard({{$product->id_product}},this)" @if($product->restock || $product->isObsoleted()) disabled @endif>Restock</button><br>
                    @endauth

                    {{-- <button class="btn btn-primary btn-sm btn-action mb-1">Verify Quantity</button><br> --}}
					
					<button class="btn btn-secondary btn-sm btn-action mb-1" data-toggle="modal" data-target="#notifierModal" data-id="{{ $product->id_product }}" data-qty="{{$product->quantity}}"
						@if($product->isObsoleted())
							disabled
						@endif
						>Notifier</button><br>

                     {{--   Performing Unique Constraint Check
                            Check whether in the database such pid exist. 
                            If exist then not unique     --}}
                    @auth
                    @php 
                        $uniquity = DB::table('order_suggestions')->where('product_id', '=', $product->id_product)->count(); 
                    @endphp

                    @if ($uniquity == '1' || $product->isObsoleted())
                        <form class="form-inline" method="POST" action="{{route('order-suggestion.create', $product->id_product)}}" enctype="multipart/form-data">
                        <input type="hidden" name="_method" value="POST">
                        
                        {{ csrf_field() }}

                        <button type="submit" class="btn btn-secondary btn-sm btn-action mb-1" disabled>Add to Order Suggestion</button><br>
                        </form>
                    
                    @elseif ($uniquity == '0')
                        <form class="form-inline" method="POST" action="{{route('order-suggestion.create', $product->id_product)}}" enctype="multipart/form-data">
                        <input type="hidden" name="_method" value="POST">
                        
                        {{ csrf_field() }}

                        <button type="submit" class="btn btn-secondary btn-sm btn-action mb-1">Add to Order Suggestion</button><br>
                        </form>
                    
                    @else
                        <button class="btn btn-secondary btn-sm btn-action mb-1" disabled>Database Error on uniquity.</button><br>
                    @endif

                    @endauth
                    
                    @auth
                    <button class="btn btn-secondary btn-sm btn-action"
                    type="button"
                    data-toggle="modal"
                    data-target="#printModal"
                    data-id= "{{$product->id_product}}">Print Labels</button><br>
                    @endauth
                </div>
                </td>
                <td class="categoryCol">
                    @if($product->online)
                        @if($product->getCategoryUrl($product->online->id_category_default && $product->online->id_category_default != 2))
                            @auth<p>ID SET TO: {{$product->online->id_category_default}}</p>@endauth
                            <a href="{{ $product->getCategoryUrl($product->online->id_category_default) }}">
                                {{$product->getCategoryName($product->online->id_category_default)}}
                            </a>
                        @else
                            @auth
                            @include('layouts.category-update-form')
                            @endauth
                        @endif
                    @else
                        <p>This is a new item. Please upload items to change category.</p>
                    @endif
                </td>
		</tbody>
	</table>
	<hr/>

	<h2>Notes</h2>
    <div class="table-responsive">
        <table id="notesTable" class="table table-hover table-dark table-bordered" width="100%">
            <thead>
                <tr>
                    <th scope="col"><span data-feather="activity"></span> Type</th>
                    <th scope="col"><span data-feather="align-justify"></span> Notes</th>
                    <th scope="col"><span data-feather="user"></span> By</th>
                    <th scope="col"><span data-feather="calendar"></span> Time</th>
					<th scope="col"><span data-feather="check-square"></span> Handled?</th>
					@if(Auth::user()->employee_role_id != 5)
					<th scope="col"><span data-feather="list"></span> Actions</th>    
					@endif               
                </tr>
            </thead>

            <tbody>
            @foreach ($notes as $note)
				<tr>
					<td><font color="#{{$note->type->color}}">{{ $note->type->name }}</font></td>
					<td>{{ $note->comments }}</td>
					<td>{{ $note->employee->name }}</td>
					<td>{{ $note->created_at }}</td>
					<td>
						@if ($note->isHandled)
							Yes *by {{$note->handlingEmployee->name}}* on [{{$note->handledDate}}]
						@else 
							No 
						@endif 
					</td>
					@if(Auth::user()->hasAccess('product_note')->update)
					<td>
						<form class="form-inline" method="POST" action="{{route('notes.handle', $note->id)}}" enctype="multipart/form-data">
							<input type="hidden" name="_method" value="POST">
							{{ csrf_field() }}
							@if ($note->isHandled)
								<button type="submit" class="btn btn-secondary btn-sm btn-action mb-1" disabled>Handled</button><br>
							@else
								<button type="submit" class="btn btn-secondary btn-sm btn-action mb-1">Handle</button><br>
							@endif
						</form>

						@if ($note->isHandled)
							<form class="form-inline" method="POST" action="{{route('notes.delete', $note->id)}}" enctype="multipart/form-data">
								<input type="hidden" name="_method" value="POST">
								{{ csrf_field() }}
								<button type="submit" class="btn btn-secondary btn-sm btn-action mb-1">Delete Note</button><br>
							</form>
						@endif
					</td>
					@endif
				</tr>
            @endforeach
            </tbody>
		</table>
		
		@if(Auth::user()->employee_role_id == 5)
		<div class="alert alert-info">
			<p>Note: To handle notes, please log in to your personal account. <a class="btn btn-sm btn-secondary" href="{{route('logout')}}">Logout</a></p>
		</div>
		@endif

		<br/>
		@if(Auth::user()->hasAccess('product_note')->create)
			<div style="text-align:center">
				<button class="btn btn-secondary btn-action"
						style="width: 25%"
						data-toggle="modal"
						data-target="#createNoteModal">Add New Note
				</button>
			</div>
		@endif

    </div>
	<br/>
	<hr/>
	@if($product->online)
	@if(Auth::user()->hasAccess('online_product_setting')->read)
	<h2>Online Product Settings</h2>
		
		<div class="container text-center">
			<form class="unityForm" action="{{ route('product.updateUnity') }}" method="POST">
				{{csrf_field()}}
				<input type="hidden" name="id_product" value="{{$product->id_product}}">
				<div class="input-group col-lg-6 offset-lg-3">
				<input class="form-control form-control-sm" value="@if(old('unity')){{old('unity')}}@else{{$product->online->unity}}@endif" name="unity">
					<div class="input-group-append">
						<button class="btn btn-secondary btn-action btn-sm">Update Unity</button>
					</div>
				</div>
				
			</form>
			<div class="custom-control custom-switch">
				<input type="checkbox" class="custom-control-input" id="online_only" name="online_only" @if($product->online->isOnlineOnly()) checked @endif @if(!Auth::user()->hasAccess('online_product_setting')->update) disabled @endif>
				<label class="custom-control-label" for="online_only">Online Only</label>
			</div>
			<div class="custom-control custom-switch">
				<input type="checkbox" class="custom-control-input" id="allow_backorder" name="allow_backorder" @if($product->online->allowBO()) checked @endif  @if(!Auth::user()->hasAccess('online_product_setting')->update) disabled @endif>
				<label class="custom-control-label" for="allow_backorder">Allow Backorder</label>
			</div>
		</div>
	<hr/>
	@endif
	@endif

	{{-- END OF ONLINE SETTINGS  --}}
	
	<h2>Related Product(s)</h2>
		@if(Auth::user()->hasAccess('product_related')->create)
		<div class="container">
			<form method="POST" action="{{route('product-relationship.create', $product->id_product)}}" enctype="multipart/form-data">
				{{csrf_field()}}
				<div class="form-group input-group-append" style="text-align:center">
					<input type="text" class="form-control" id="relationship_pid" name="relationship_pid" placeholder="Enter Product ID" value="{{old('relationship_pid')}}" required>
						<div class="input-group-append">
							<button class="btn btn-outline-secondary" type="button" onclick="autofill()"><span data-feather="align-left"></span></button>
						</div>
				</div>

				<div class="form-group input-group-append" style="text-align:center">
					<input type="text" class="form-control" id="relation_product_name" name="relation_product_name" placeholder="Click on autofill button to obtain name." value="{{old('relation_product_name')}}" required>
					<input type="text" class="form-control" id="relation_vendor" name="relation_vendor" placeholder="Click on autofill button to obtain vendor." value="{{old('relation_vendor')}}">
					<input type="text" class="form-control" id="relation_vendor_remarks" name="relation_vendor_remarks" placeholder="Click on autofill button to obtain remarks." value="{{old('relation_vendor_remarks')}}">
				</div>

				<div class="form-group input-group-append" style="text-align:center">
					<input type="text" class="form-control" id="relationship_comments" name="relationship_comments" placeholder="Enter Comment" value="{{old('relationship_comments')}}">
				</div>

				<div class="form-group" style="text-align:center">
					<button type="submit" class="btn btn-info ml-2" style="width: 25%">Create Relationship</button>
				</div>
			</form>
		</div>
		@endif

		<br/>
	    <div class="table-responsive">
        <table class="table table-hover table-dark table-bordered" width="100%">
            <thead>
                <tr>
                    <th scope="col"><span data-feather="hash"></span> Related Product ID</th>
					<th scope="col"><span data-feather="info"></span> Related Product Name</th>
					<th scope="col"><span data-feather="image"></span> Related Product Image</th>
					@if(Auth::user()->hasAccess('vendors')->read)
					<th scope="col"><span data-feather="truck"></span> Vendor</th>
					<th scope="col"><span data-feather="list"></span> Remarks</th>
					@endif
                    <th scope="col"><span data-feather="message-square"></span> Relationship Comments</th> 
					<th scope="col"><span data-geather="activity"></span> Activity</th>            
                </tr>
            </thead>

            <tbody>
			@php $relatedProducts = App\ProductRelation::where('product_id', '=', $product->id_product)->get() @endphp
			@php $temp_product =  $product @endphp
				@foreach($relatedProducts as $relatedProduct)
					@php $product = $relatedProduct->relatedProduct @endphp
					<tr>
						<td>{{$relatedProduct->relatedProduct->id_product}}</td>
						<td>{{$relatedProduct->related_product_name}}
                        	@include('layouts.product-name-icons')
						</td>
						<td><img class="product-image-thumbnail img-thumbnail" src="{{ $relatedProduct->relatedProduct->image_url() }}"></td>
						@if(Auth::user()->hasAccess('vendors')->read)
							<td>{{$relatedProduct->related_vendor}}</td>
							<td>{{$relatedProduct->related_vendor_remarks}}</td>
						@endif
						<td>{{$relatedProduct->comments}}</td>
						<td>
							@if(Auth::user()->hasAccess('shipment')->update || Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Supervisor'))
								<button class="btn btn-primary btn-sm btn-action mb-1"
									data-toggle="modal"
									data-target="#purchaseAndShipModal"

									data-id="{{ $relatedProduct->relatedProduct->id_product }}" 
									data-name="{{ $relatedProduct->related_product_name}}"

									data-url="{{route('order-suggestion.purchaseAndShip', $relatedProduct->relatedProduct->id_product)}}">Purchase and Ship
								</button>

								{{-- Ordered button and form begin here --}}
								<button class="btn btn-primary btn-sm btn-action mb-1"
									data-toggle="modal"
									data-target="#purchaseModal"

									data-remarks="{{$relatedProduct->related_vendor_remarks}}"
									data-url="{{route('order-suggestion.purchase', $relatedProduct->relatedProduct->id_product )}}">Purchase and Hold
								</button>
							@endif

							@if(Auth::user()->hasAccess('product_related')->update)
								<button class="btn btn-primary btn-sm btn-action mb-1"
									data-toggle="modal"
									data-target="#editRelationshipModal"
									data-remarks="{{$relatedProduct->related_vendor_remarks}}"
									data-comments="{{$relatedProduct->comments}}"
									data-url="{{route('product-relationship.edit', $relatedProduct->id )}}">Edit
								</button>
							@endif
								
							@if(Auth::user()->hasAccess('product_related')->delete)
								<form class="form-inline" method="POST" action="{{route('product-relationship.delete', $relatedProduct->id)}}" enctype="multipart/form-data">
									<input type="hidden" name="_method" value="POST">
									{{ csrf_field() }}
									<button type="submit" class="btn btn-primary btn-sm btn-action mb-1">Delete</button><br>
								</form>
							@endif
						</td>
					</tr>

				@endforeach
			@php $product =  $temp_product @endphp
            </tbody>
		</table>

	<hr/>
	{{-- END OF RELATED PRODUCTS --}}

    {{--
		Monthly Sales Table
		Credit: Cheng Cao, Fastsearch Lite v3.0
		Converted by Tony Lim (Lee's Electronic Components)
	--}} 
	@if(Auth::user()->hasAccess('sales_history')->read)

		@php
			function sumArrayTotal($arrayToBeTotaled){
				//print_r($arrayToBeTotaled);
				$total = 0;
				foreach($arrayToBeTotaled as $key=>$value){
					
					$total = $total + intval($value);
				}
				
				return $total;
			}
		@endphp

		<h2>Sales History</h2>
		
		@include('layouts.sales-history')

		<hr/>
	
	@endif
	{{-- End of Sales History --}}

	{{-- Price Breaks --}}

	<h2>Price Breaks</h2>
	<div class="table-responsive">
        <table class="table table-hover table-dark table-bordered" width="100%">
            <thead>
                <tr>
                    <th scope="col"><span data-feather="pie-chart"></span> Quantity</th>
					<th scope="col" style="display:none"><span data-feather="dollar-sign"></span> Total Discount Amount</th>
                    <th scope="col"><span data-feather="arrow-down"></span> Discount</th>
                    <th scope="col"><span data-feather="dollar-sign"></span> Discount Per Unit</th>
					<th scope="col"><span data-feather="dollar-sign"></span> Discounted Price Per Unit</th>
					<th scope="col"><span data-feather="dollar-sign"></span> Total Minimum Break Price</th>
					<th scope="col" style="display:none"><span data-feather="dollar-sign"></span> Regular Price Total</th>    
                </tr>
            </thead>

            <tbody>
				<tr>
					@if(($product->qty_break_1) != 0)
						<td>{{$qty1 = $product->qty_break_1}}</td>
						<td style="display:none">${{$discAmtTotal1 = $product->qty_break_amount_1}}</td>
						<td>{{$discAmtTotal1 / $qty1 *100}}%</td>
						<td>-${{$discPerUnit1 = number_format($discAmtTotal1 / $qty1, 2)}}</td>
						<td>${{$discountedPrice1 = number_format($product->price - $discPerUnit1, 2)}}</td>
						<td>${{number_format($qty1 * $discountedPrice1, 2)}}</td>
						<td style="display:none">{{$product->price * $qty1}}</td>
					@endif
				</tr>
				<tr>
					@if(($product->qty_break_2) != 0)
						<td>{{$qty2 = $product->qty_break_2}}</td>
						<td style="display:none">${{$discAmtTotal2 = $product->qty_break_amount_2}}</td>
						<td>{{$discAmtTotal2 / $qty2 *100}}%</td>
						<td>-${{$discPerUnit2 = number_format($discAmtTotal2 / $qty2, 2)}}</td>
						<td>${{$discountedPrice2 = number_format($product->price - $discPerUnit2, 2)}}</td>
						<td>${{number_format($qty2 * $discountedPrice2, 2)}}</td>
						<td style="display:none">{{$product->price * $qty2}}</td>
					@endif
				</tr>
				<tr>
					@if(($product->qty_break_3) != 0)
						<td>{{$qty3 = $product->qty_break_3}}</td>
						<td style="display:none">${{$discAmtTotal3 = $product->qty_break_amount_3}}</td>
						<td>{{$discAmtTotal3 / $qty3 *100}}%</td>
						<td>-${{$discPerUnit3 = number_format($discAmtTotal3 / $qty3, 2)}}</td>	
						<td>${{$discountedPrice3 = number_format($product->price - $discPerUnit3, 2)}}</td>
						<td>${{number_format($qty3 * $discountedPrice3, 2)}}</td>
						<td style="display:none">{{$product->price * $qty3}}</td>
					@endif
				</tr>
            </tbody>
        </table>
    </div>
	<hr/>

	{{-- Product Price Changes --}}
	@if(Auth::user()->hasRole('SuperAdmin'))
		<h2>Price Changes History</h2>
		<div class="table-responsive">
			<table class="table table-hover table-dark table-bordered" width="100%">
				<thead>
					<tr>
						<th scope="col"><span data-feather="hash"></span> Product ID</th>
						<th scope="col"><span data-feather="list"></span> Product Name</th>
						<th scope="col"><span data-feather="dollar-sign"></span> Old Price</th>
						<th scope="col"><span data-feather="dollar-sign"></span> New Price</th>
						<th scope="col"><span data-feather="calendar"></span> Date</th>
					</tr>
				</thead>
				<tbody>
					@php $ProductPrices = App\ProductPriceChange::where('product_id', '=', $product->id_product)->get() @endphp

					@foreach($ProductPrices as $ProductPrice)
						<tr>
							<td>{{$ProductPrice->product_id}}</td>
							<td>{{$ProductPrice->product_name}}</td>
							<td>$@formatMoney($ProductPrice->old_price)</td>
							<td>$@formatMoney($ProductPrice->new_price)</td>
							<td>{{$ProductPrice->created_at}}</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<hr>
	@endif


	{{-- Product Warranty --}}
	<h2>Product Warranty</h2>
	<div class="table-responsive">
        <table class="table table-hover table-dark table-bordered" width="100%">
            <thead>
                <tr>
                    <th scope="col"><span data-feather="pie-chart"></span> Warranty Length (Months)</th>
					@if(Auth::user()->hasAccess('vendors')->read)
						<th scope="col"><span data-feather="truck"></span> Vendor</th>
					@endif
					{{-- <th scope="col"><span data-feather="message-square"></span> Comment</th> --}}
                </tr>
            </thead>

            <tbody>
				<tr>
					<td>{{$product->warranty_month}}</td>
					@if(Auth::user()->hasAccess('vendors')->read)
						<td>{{$product->vendor}}</td>
					@endif	
					{{-- <td>Dummy field for comment. Implement later.</td> --}}
				</tr>
            </tbody>
        </table>
    </div>

	<hr/>

    {{-- New Note Modal --}}

	@if(Auth::user()->hasAccess('product_note')->create)
	@php $noteTypes = App\ProductNoteType::where('isAutoInput', '0')->get() @endphp

    <div class="modal fade" id="createNoteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{ route('notes.store')}}" method="POST">
                	{{ csrf_field() }}
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Create New Note</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>

					<div class="modal-body">
						<label>Product ID: </label>
                    	<input class="form-control" type="integer" name="id_product" id="note-productid" value="{{$product->id_product}}" readonly>
						<hr/>

						<label>Note Type:</label>
						<select class="custom-select mr-sm-2 mb-2" name="type_id" required>
							@foreach($noteTypes as $type)
								<option value="{{$type->id}}">{{$type->name}}</option>
							@endforeach
						</select>
						<br/>

						<label>Comments:</label>
                    	<textarea class="form-control" type="text" name="comments" id="note-comments"></textarea>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
						<a id="note-cancel" href="#">
						<button type="submit" class="btn btn-primary">Create Note</button>
						</a>
					</div>
        		</form>
            </div>
    	</div>
	</div>
	@endif

    @include('layouts.error-flash')

	{{-- Purchase Modal --}}
    <div class="modal fade" id="purchaseModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="POST">
                	{{ csrf_field() }}
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Input Order Information</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>

					<div class="modal-body">
						<label>Remarks:</label>
						<input class="form-control" type="text" name="remarks" id="remarks" readonly><br/>

						<label>Quantity:</label>
                    	<input class="form-control" type="text" name="qty" id="qty"><br/>

						<label>Unit:</label>
                        <select class="custom-select mr-sm-2 mb-2" name="unit" id="unit" required>
                            <option value="piece">Piece</option>
                            <option value="package">Package</option>
                            <option value="meter">Meter</option>
                            <option value="foot">Foot</option>
                            <option value="1/2lb">1/2LB</option>
                            <option value="box">Box</option>
                            <option value="roll">Roll</option>
                            <option value="winpos unit">Winpos Unit</option>
                        </select>

						<label>Comments:</label>
                    	<textarea class="form-control" type="text" name="comments" id="note-comments"></textarea><br/>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
						<a id="note-cancel" href="#">
						<button type="submit" class="btn btn-primary">Order Item</button>
						</a>
					</div>
        		</form>
            </div>
    	</div>
    </div>

	{{-- Purchase and Ship Modal --}}
    <div class="modal fade" id="purchaseAndShipModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="POST">
                	{{ csrf_field() }}
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Insert Order into Shipment</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>

					<div class="modal-body">
						<label>Product ID:</label>
                    	<input class="form-control" type="text" name="id" id="id" readonly><br/>

						<label>Product Name:</label>
                    	<input class="form-control" type="text" name="name" id="name" readonly><br/>

						<label>Ordered Quantity:</label>
                    	<input class="form-control" type="text" name="qty" id="qty"><br/>

                    	<label>Ordered Unit:</label>
                    	<select class="custom-select mr-sm-2 mb-2" name="unit" id="unit" required>
                            <option value="piece">Piece</option>
                            <option value="package">Package</option>
                            <option value="meter">Meter</option>
                            <option value="foot">Foot</option>
                            <option value="1/2lb">1/2LB</option>
                            <option value="box">Box</option>
                            <option value="roll">Roll</option>
                            <option value="winpos unit">Winpos Unit</option>
                        </select><br/>

                        @php $shipments = App\Shipment::all() @endphp
                        <label>Select Shipment:</label>
                        <select class="custom-select mr-sm-2 mb-2" name="shipment" id="shipment" required>
                            @foreach($shipments as $shipment)
                                @if ($shipment->isActive)
                                    <option value="{{$shipment->id}}">{{$shipment->id}} | [{{$shipment->status->name}}] | {{$shipment->vendor->name}} | 
                                        @if ($shipment->isLocal == '1')
                                            LOCAL
                                        @else
                                            NOT LOCAL
                                        @endif
                                        | {{$shipment->type->name}}
                                    </option>
                                @endif
                            @endforeach
                        </select>
                        <br/>   

                    	<label>Invoice:</label>
                    	<input class="form-control" type="text" name="invoice" id="invoice"><br/>

						<label>Comments:</label>
                    	<textarea class="form-control" type="text" name="comments" id="comments"></textarea><br/>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>

						<button type="submit" class="btn btn-primary">Purchase Suggestion</button>
					</div>
        		</form>
            </div>
    	</div>
    </div>

	{{-- Edit Relationship Modal --}}
    <div class="modal fade" id="editRelationshipModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="POST">
                	{{ csrf_field() }}
                    <input type="hidden" name="_method" value="PATCH">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Edit Relationship</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>

					<div class="modal-body">
                        <label>Remarks:</label>
                        <input class="form-control" type="text" name="remarks" id="remarks"><br/>

						<label>Comments:</label>
                    	<textarea class="form-control" type="text" name="comments" id="comments"></textarea><br/>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>

						<button type="submit" class="btn btn-info">Save</button>
					</div>
        		</form>
            </div>
    	</div>
    </div>

	@include('layouts.image-modal')
	@include('layouts.side-panel')
	@include('layouts.notifier')
	@include('layouts.shelf-location')

@endsection

@push('bottom-scripts')

<script>
    jQuery(document).ready(function($) {
        $(".clickable-row").click(function() {
            window.location = $(this).data("href");
        });
    });
</script>

<script>
    function autofill(){
                var pid = $('#relationship_pid')[0].value;
                console.log(pid);
                $.ajax({
                    type: "get",
                    url: "{{ route('product.autofill') }}",
                    data: {pid: pid},
                    success: function( msg ) {
                        if(msg.status == "success"){
                            $('#relation_product_name')[0].value = msg.name
                            $('#relation_vendor')[0].value = msg.vendor
							$('#relation_vendor_remarks')[0].value = msg.remarks
                        }else{
                            alert(msg.message);
                        }
                    },
                    error: function (request, status, error) {
                        alert(request.status + ':' + error + '. Please take picture and contact DevTeam.');
                    }
                });
            };
</script>

{{-- Datatable --}}
    <script type="text/javascript">
        $(document).ready(function () {
            $('#notesTable').DataTable({
                "scrollY":        "60vh",
                "scrollCollapse": true,
                "paging":         true,
				"pageLength": 5,
				"aLengthMenu": [[5, 10, 20, -1], [5, 10, 20, "All"]],
                columnDefs: [ { orderable: false, targets: [
                    5
                ]}],
				"language": {
                    "search": "Filter records:"
                },
				"order": [[ 3, "desc" ]]
            });
        });
    </script>

   <script>
		$(document).ready(function () {
			$('#purchaseModal').on('show.bs.modal', function (event) {
				var button = $(event.relatedTarget) 
				var url = button.data('url')
				var remarks = button.data('remarks')
				var modal = $(this)
				modal.find('#remarks').val(remarks)
				modal.find('form').attr('action',url)

			});
		});
    </script>

	<script>
		$(document).ready(function () {
			$('#purchaseAndShipModal').on('show.bs.modal', function (event) {
				var button = $(event.relatedTarget) // Button that triggered the modal
				var id = button.data('id') // Extract info from data-* attributes
				var name = button.data('name') // Extract info from data-* attributes
				var url = button.data('url')

				var modal = $(this)
				modal.find('.modal-title').text('Insert Item: ' + name)
				modal.find('#id').val(id)
				modal.find('#name').val(name)
				modal.find('form').attr('action',url)
			});
		});
    </script>

	<script>
		function addToBoard(pid,btn) {
			btn.disabled = true;

			$.ajax({
				type: "get",
				url: "{{ route('restock.add') }}",
				data: {pid: pid},
				dataType: 'json',
				success: function( msg ) {
					console.log(msg)
					console.log(msg.status)
					if(msg.status == "success"){
						alert("You have successfully added PID#" + pid + " into the restock board.");
					}else{
						alert(msg.message);
						btn.disabled = true;
					}
				},
				error: function (request, status, error) {
					alert(request.status + ':' + error + '. Please take picture and contact DevTeam.');
					btn.disabled = true;
				}
			});
		}
	</script>

	<script>
		function searchProductProfile() {
			var pid = $('#profile_product').val();
			console.log(pid);
			location.href = "{{url('/product/profile/')}}/" + pid;
		}

		$("#profile_product").on('keypress',function(e) {
			if(e.which == 13) {
				searchProductProfile();
			}
		});
	</script>

	<script>
        function obsolete(url, pid){
            $.ajax({
                type: "post",
                url: url,
                data: '_token={{csrf_token()}}', 
                dataType: 'json',
                success: function( msg ) {
                    document.getElementById('obsoleteBtn' + pid).disabled = true;
                    alert("Obsoleting Successful.");
                },
                error: function (request, status, error) {
                    alert(request.status + ':' + error + '. Obsoleting failed. Please contact DevTeam.');
                }
            });
        }
	</script>

	<script>
		$('#online_only').bind('change', function(){
			var online = $(this).is(":checked");
			if(online){
				online = 1;
			}else{
				online = 0;
			}
			$.ajax({
				type: "POST",
				url: "{{route('online-product.updateOnlineOnly')}}",
				data: { _token:"{{csrf_token()}}", product_id: "{{$product->id_product}}", online: online }, // serializes the form's elements.
				dataType: 'json',
				success: function(data) {
					var message = "";
					if(data.status == "success"){
						if(data.status == "success"){
                        var header = '<span data-feather="check-circle" style="width:50px;height:50px;stroke:white;"></span>';
						$('#messageModal .modal-header').css("background-color","#6ba984");
						message = "Success! ";
                    }else{
                        var header = '<span data-feather="alert-circle" style="width:50px;height:50px;stroke:white;"></span>';
						$('#messageModal .modal-header').css("background-color","#e66262");
						message = "An error has occured! ";
                    }

						$('#messageModal .modal-title').html(header);

						feather.replace()

						$('#messageModal .modal-body #message').html(message + data.message);
						$('#messageModal').modal('show');
					}else{
						var header = '<span data-feather="alert-circle" style="width:50px;height:50px;stroke:white;"></span>';
						$('#messageModal .modal-header').css("background-color","#e66262");

						$('#messageModal .modal-title').html(header);

						feather.replace()

						$('#messageModal .modal-body #message').html(data.message);
						$('#messageModal').modal('show');
					}
				},
				error: function(request, status, error){

					var header = '<span data-feather="alert-circle" style="width:50px;height:50px;stroke:white;"></span>';
					$('#messageModal .modal-header').css("background-color","#e66262");

					$('#messageModal .modal-title').html(header);

					feather.replace()

					$('#messageModal .modal-body #message').html(error);
					$('#messageModal').modal('show');

					form.html('<span style="color:red">Update Failed. Contact DevTeam. </span>');
				}
			}); 

		});
		$('#allow_backorder').bind('change', function(){
			var allow = $(this).is(":checked");
			if(allow){
				allow = 1;
			}else{
				allow = 2;
			}
			$.ajax({
				type: "POST",
				url: "{{route('online-product.updateBO')}}",
				data: { _token:"{{csrf_token()}}", product_id: "{{$product->id_product}}", backorder_status: allow }, // serializes the form's elements.
				dataType: 'json',
				success: function(data) {
					var message = "";
					if(data.status == "success"){
						if(data.status == "success"){
                        var header = '<span data-feather="check-circle" style="width:50px;height:50px;stroke:white;"></span>';
						$('#messageModal .modal-header').css("background-color","#6ba984");
						message = "Success! ";
                    }else{
                        var header = '<span data-feather="alert-circle" style="width:50px;height:50px;stroke:white;"></span>';
						$('#messageModal .modal-header').css("background-color","#e66262");
						message = "An error has occured! ";
                    }

						$('#messageModal .modal-title').html(header);

						feather.replace()

						$('#messageModal .modal-body #message').html(message + data.message);
						$('#messageModal').modal('show');
					}else{
						var header = '<span data-feather="alert-circle" style="width:50px;height:50px;stroke:white;"></span>';
						$('#messageModal .modal-header').css("background-color","#e66262");

						$('#messageModal .modal-title').html(header);

						feather.replace()

						$('#messageModal .modal-body #message').html(data.message);
						$('#messageModal').modal('show');
					}
				},
				error: function(request, status, error){

					var header = '<span data-feather="alert-circle" style="width:50px;height:50px;stroke:white;"></span>';
					$('#messageModal .modal-header').css("background-color","#e66262");

					$('#messageModal .modal-title').html(header);

					feather.replace()

					$('#messageModal .modal-body #message').html(error);
					$('#messageModal').modal('show');

					form.html('<span style="color:red">Update Failed. Contact DevTeam. </span>');
				}
			});
		});
	</script>

	<script>
        $("form.unityForm").submit(function(e) {
            var form = $(this);
            var url = form.attr('action');
            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(), // serializes the form's elements.
                dataType: 'json',
                success: function(data) {
                    if(data.status == "success"){
                        location.reload()
                    }else{
                        form.html('<span style="color:red">Update Failed. ' + data.message + '</span>');
                    }
                },
                error: function(request, status, error){

                    var header = '<span data-feather="alert-circle" style="width:50px;height:50px;stroke:white;"></span>';
                    $('#messageModal .modal-header').css("background-color","#e66262");

                    $('#messageModal .modal-title').html(header);

                    feather.replace()

                    $('#messageModal .modal-body #message').html(error);
                    $('#messageModal').modal('show');

                    form.html('<span style="color:red">Update Failed. Contact DevTeam. </span>');
                }
            }); 

            e.preventDefault(); // avoid to execute the actual submit of the form.
        });

    </script>

	<script>
    $(document).ready(function () {
        $('#editRelationshipModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var remarks = button.data('remarks')
            var comments = button.data('comments')
            var url = button.data('url')
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('.modal-title').text('Edit Relationship')
            modal.find('#remarks').val(remarks)
            modal.find('#comments').val(comments)
            modal.find('form').attr('action',url)
        });
    });
    </script>

	@include('functions.category-update-functions')
@endpush