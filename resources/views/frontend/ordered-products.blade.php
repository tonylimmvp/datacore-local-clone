@extends('frontend.layouts.main')

@include('layouts.datatables')

@push('top-scripts')
<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
@endpush

@push('css')
<style>
    h1 {
        text-align: center;
        font-family: 'Nunito', sans-serif;
        font-weight: 200;
    }

    h2 {
        font-family: 'Nunito', sans-serif;
        font-weight: 200; 
        text-align: center;
    }
    
    .product-image-thumbnail {
        display: block;
        width: 100px;
        height: auto;
    }

    td img {
        display: block;
        margin-left: auto;
        margin-right: auto;
    }

</style>
@endpush

@section('content')

    @include('layouts.errors')

    <br/>
    <h1>Ordered Products and Hold List</h1>
    <hr/>

    <div style="text-align: center; justify-content: center; margin-bottom: 5px;">
        <button class="btn btn-secondary btn-sm btn-action mb-1" style="width:25%; font-size: 14px;"
            data-toggle="modal"
            data-target="#purchaseModal"
            data-url="{{route('ordered-products.insertToOP')}}">Order Product Manually
        </button>
    </div>

    @php $notes = App\ProductNote::all(); @endphp

        <div class="table-responsive">
            <table id="mainTable" class="table table-hover table-dark table-bordered" width="100%">
                <thead>
                    <tr>
                        {{-- <th>ID (DEBUG)</th> --}}
                        <th scope="col"><span data-feather="hash"></span> Product ID</th>
                        <th scope="col"><span data-feather="align-justify"></span> Product Name</th>
                        <th scope="col"><span data-feather="list"></span> Fraser Quantity</th>
                        <th scope="col"><span data-feather="list"></span> Ordered Quantity</th>
                        <th scope="col"><span data-feather="list"></span> Ordered Units</th>
                        <th scope="col"><span data-feather="image"></span> Product Image</th>
                        <th scope="col"><span data-feather="message-square"></span> Comments</th>
                        @auth 
                            @if(Auth::user()->role->id == 1)
                                @if(Auth::user()->hasAccess('cost')->read)<th scope="col"><span data-feather="dollar-sign"></span> Recent Cost</th>@endif
                                {{-- @if(Auth::user()->hasAccess('vendors')->read)<th scope="col"><span data-feather="shopping-cart"></span> Vendor</th> --}}
                                {{-- <th scope="col"><span data-feather="shopping-cart"></span> Remarks</th>@endif --}}
                                <th scope="col"><span data-feather="more-horizontal"></span> Related Products | Purchased?</th>
                                <th scope="col"><span data-feather="truck"></span> Quick Ship</th>
                            @endif
                        @endauth
                        <th scope="col"><span data-feather="user-check"></span> Staff Name</th>
                        <th scope="col"><span data-feather="calendar"></span> Purchased Date</th>
                        <th scope="col"><span data-feather="activity"></span> Activities</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($notes as $note)
                        @php $noteID = $note->id; @endphp
                        @if(!($note -> isHandled) && ($note->type_id == 2))
                            <tr>
                                {{-- <td>{{$note->id}}</td> --}}
                                <td><span style="font-weight:bold"><a href='{{url('https://leeselectronic.com/en/product/'. $note->product_id. '.html')}}' target='_blank'>{{ $note->product_id }}</a></span></td>
                                <td name="op_name{{$noteID}}" id="op_name{{$noteID}}">
                                        @if ($note->name)
                                            {{$note->name}}
                                        @else
                                            {{ $note->product->name }}
                                        @endif
                                        @php $product = $note->product; @endphp
                                        @include('layouts.product-name-icons')
                                </td>
                                <td>{{$note->product->qty_available}}</td>
                                <td><input class='form-control' type='text' name="op_qty{{$noteID}}" id="op_qty{{$noteID}}" value='{{$note->qty}}'></td>
                                <td>{{$note->unit}}</td>
                                <td>
                                    @if ($note->product_id != 100)
                                        <a href="{{$note->product->image_url()}}"><img class="product-image-thumbnail img-thumbnail" src="{{ $note->product->image_url() }}"></a>
                                    @else
                                        Image for #100 is not available.
                                    @endif
                                </td>
                                <td>{{$note->comments}}</td>
                                @auth 
                                    @if(Auth::user()->role->id == 1)
                                        @if(Auth::user()->hasAccess('cost')->read)<td>{{ $note->product->recent_cost }}</td>@endif
                                        {{-- @if(Auth::user()->hasAccess('vendors')->read)<td>{{ $note->product->vendor}}</td>
                                        <td>{{ $note->product->vendor_remarks}}</td>@endif --}}
                                        <td>
                                            @php $relations = App\ProductRelation::where('product_id', $note->product->id_product)->get() @endphp
                                            @foreach($relations as $relation)
            
                                                #{{$relation->related_product_id}} | 
                
                                                @php $shipmentNotes = App\ProductNote::where('product_id', $relation->related_product_id)->where('type_id', '9')->where('isHandled', '0')->get() @endphp
                                                @php $purchaseNotes = App\ProductNote::where('product_id', $relation->related_product_id)->where('type_id', '2')->where('isHandled', '0')->get() @endphp

                                                @if(($purchaseNotes->count() > 0) || ($shipmentNotes->count() > 0))
                                                    <span class="nameIcons" data-feather="check"></span>
                                                @else
                                                    <span class="nameIcons" data-feather="x"></span>
                                                @endif
                                                <br>
                                            @endforeach
                                        </td>
                                        <td>
                                            @php $shipments = App\Shipment::all() @endphp
                                            <select class="custom-select mr-sm-2 mb-2" name="shipment" id="op_shipment{{$noteID}}">
                                                @foreach($shipments as $shipment)
                                                    @if ($shipment->isActive)
                                                        <option value="{{$shipment->id}}">{{$shipment->id}} | [{{$shipment->status->name}}] | {{$shipment->vendor->name}} | 
                                                            @if ($shipment->isLocal == '1')
                                                                LOCAL
                                                            @else
                                                                NOT LOCAL
                                                            @endif
                                                            | {{$shipment->type->name}}
                                                        </option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </td>
                                    @endif
                                @endauth
                                <td>{{ $note->employee->name}}</td>
                                <td>{{ $note->created_at->diffForHumans()}}</td>
                                <td>
                                    @auth
                                        @if(Auth::user()->role->id == 1)
                                    {{-- Ordered button and form begin here --}}
                                            <button class="btn btn-primary btn-sm btn-action mb-1"
                                                data-toggle="modal"
                                                data-target="#insertShipmentModal"
                                                data-id="{{$note->product_id}}" 
                                                data-note="{{$note->id}}"
                                                data-name="{{$note->product->name}}"
                                                data-qty="{{$note->qty}}"
                                                data-unit="{{$note->unit}}"
                                                data-url="{{route('shipment-cart.insertToShipment', $note->id)}}">Enter into Shipment
                                            </button>

                                            <button class="btn btn-primary btn-sm btn-action mb-1"
                                                data-toggle="modal"
                                                data-target="#editOrderModal"
                                                data-id="{{ $note->product_id }}" 
                                                data-name="{{ $note->product->name }}"
                                                data-qty="{{$note->qty}}"
                                                data-unit="{{$note->unit}}"
                                                data-url="{{route('ordered-products.editOrder', $note->id )}}">Edit Order
                                            </button>

                                            <form class="form-inline" method="POST" action="{{route('ordered-products.delete', $note->id)}}" enctype="multipart/form-data">
                                                <input type="hidden" name="_method" value="POST">               
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-primary btn-sm btn-action mb-1">Delete Order</button><br>
                                            </form>
                                        @endif
                                    @endauth
                                </td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    </form>

    {{-- Purchase Modal --}}
    <div class="modal fade" id="purchaseModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="POST">
                	{{ csrf_field() }}
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Input Order Information</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>

					<div class="modal-body">
                        <label>Product ID:</label>
                    	<input class="form-control" type="text" name="product_id" id="product_id" required><br>

                        <label>*Product Name:</label>
                    	<input class="form-control" type="text" name="name" id="name">
                        <small style="font-size: 12px;">*Leave the field above blank for the system to automatically grab name.</small><br><br>

						<label>Quantity:</label>
                    	<input class="form-control" type="number" name="qty" id="qty" required><br>

						<label>Unit:</label>
                        <select class="custom-select mr-sm-2 mb-2" name="unit" id="unit" required>
                            <option value="piece">Piece</option>
                            <option value="package">Package</option>
                            <option value="meter">Meter</option>
                            <option value="foot">Foot</option>
                            <option value="1/2lb">1/2LB</option>
                            <option value="box">Box</option>
                            <option value="roll">Roll</option>
                            <option value="winpos unit">Winpos Unit</option>
                        </select>

						<label>Comments:</label>
                    	<textarea class="form-control" type="text" name="comments" id="note-comments"></textarea><br/>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
						<a id="note-cancel" href="#">
						<button type="submit" class="btn btn-primary">Order Item</button>
						</a>
					</div>
        		</form>
            </div>
    	</div>
    </div>

    {{-- Insert to Shipment Modal --}}
    <div class="modal fade" id="insertShipmentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="POST">
                	{{ csrf_field() }}
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Insert Ordered Product into Shipment</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>

					<div class="modal-body">
                    	<label>Product ID:</label>
                    	<input class="form-control" type="text" name="id" id="id" readonly><br/>

						<label>Product Name:</label>
                    	<input class="form-control" type="text" name="name" id="name" value="{{$note->name}}" readonly><br/>

						<label>Ordered Quantity:</label>
                    	<input class="form-control" type="number" name="qty" id="qty" value="{{$note->qty}}" required><br/>

                    	<label>Ordered Unit:</label>
                    	<input class="form-control" type="text" name="unit" id="unit" readonly><br/>

                        @php $shipments = App\Shipment::all() @endphp
                        <label>Select Shipment:</label>
                        <select class="custom-select mr-sm-2 mb-2" name="shipment" id="shipment" required>
                            @foreach($shipments as $shipment)
                                @if ($shipment->isActive)
                                    <option value="{{$shipment->id}}">{{$shipment->id}} | [{{$shipment->status->name}}] | {{$shipment->vendor->name}} | 
                                        @if ($shipment->isLocal == '1')
                                            LOCAL
                                        @else
                                            NOT LOCAL
                                        @endif
                                        | {{$shipment->type->name}}
                                    </option>
                                @endif
                            @endforeach
                        </select>
                        <br/>   

                    	<label>Invoice:</label>
                    	<input class="form-control" type="text" name="invoice" id="invoice"><br/>

						<label>Comments:</label>
                    	<textarea class="form-control" type="text" name="comments" id="comments"></textarea><br/>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
						<button type="submit" class="btn btn-primary">Ship Product</button>
					</div>
        		</form>
            </div>
    	</div>
    </div>

    {{-- Edit Order Modal --}}
    <div class="modal fade" id="editOrderModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="POST">
                <input type="hidden" name="_method" value="patch">
                	{{ csrf_field() }}
                    {{ method_field('PATCH') }}
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Edit Order</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>

					<div class="modal-body">
                    	<label>Product ID:</label>
                    	<input class="form-control" type="text" name="id" id="id" readonly><br/>

						<label>Product Name:</label>
                    	<input class="form-control" type="text" name="name" id="name" readonly><br/>

						<label>Ordered Quantity:</label>
                    	<input class="form-control" type="number" name="qty" id="qty"><br/>

                    	<label>Unit:</label>
                    	<select class="custom-select mr-sm-2 mb-2" name="unit" id="unit" required>
                            <option value="piece">Piece</option>
                            <option value="package">Package</option>
                            <option value="meter">Meter</option>
                            <option value="foot">Foot</option>
                            <option value="1/2lb">1/2LB</option>
                            <option value="box">Box</option>
                            <option value="roll">Roll</option>
                            <option value="winpos unit">Winpos Unit</option>
                        </select>

                        <label>Comments:</label>
                    	<textarea class="form-control" type="text" name="comments" id="comments"></textarea><br/>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>

						<button type="submit" class="btn btn-primary">Edit Order</button>
					</div>
        		</form>
            </div>
    	</div>
    </div>

@endsection

@push('bottom-scripts')
    
    <script>
    $(document).ready(function () {
        $('#purchaseModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) 
            var url = button.data('url')

            var modal = $(this)
            modal.find('.modal-title').text('Purchase Modal')
            modal.find('form').attr('action',url)
        });
    });
    </script>
    
    <script>
    $(document).ready(function () {
        $('#insertShipmentModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) 
            var id = button.data('id')
            var note_id = button.data('note')
            var name = $("#op_name" + note_id).text().trim()
            var qty = $("#op_qty" + note_id).val()
            var unit = button.data('unit')
            var shipment = $("#op_shipment" + note_id).val()
            var url = button.data('url')

            var modal = $(this)
            modal.find('.modal-title').text('Insert Item: ' + name)
            modal.find('#id').val(id)
            modal.find('#name').val(name)
            modal.find('#qty').val(qty)
            modal.find('#unit').val(unit)
            modal.find('#shipment').val(shipment)
            modal.find('form').attr('action',url)
        });
    });
    </script>

    <script>
    $(document).ready(function () {
        $('#editOrderModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) 
            var id = button.data('id') 
            var name = button.data('name') 
            var qty = button.data('qty')
            var unit = button.data('unit')
            var comments = button.data('comments')
            var url = button.data('url')

            var modal = $(this)
            modal.find('.modal-title').text('Edit Order')
            modal.find('#id').val(id)
            modal.find('#name').val(name)
            modal.find('#qty').val(qty)
            modal.find('#unit').val(unit)
            modal.find('#comments').val(comments)
            modal.find('form').attr('action',url)
        });
    });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#mainTable').DataTable({
                "scrollY":        "80vh",
                "scrollCollapse": true,
                "paging":         false,
                columnDefs: [ { orderable: false, targets: [
                    5, 10, 13
                ]}],
                "language": {
                    "search": "Filter records:"
                }
            });
        });
    </script>
@endpush