@if($error = session('error'))
    <div class="form-group mt-2 no-print">
        <div class="alert alert-danger">
            <ul>
                <li>{{ $error }}</li>
            </ul>
        </div> 
    </div>
@elseif(count($errors))
    <div class="form-group mt-2 no-print">
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach

            </ul>
        </div> 
    </div>
@endif