@extends('backend.layouts.main')

@section('content')
<table class="table table-bordered">
    @foreach(App\Employee::activeForShift()->with('shifts')->get() as $employee)
        <tr>
            <td>{{$employee->name}}</td>
            <td class="text-center">
            @if(sizeof($employee->shifts)>0)
            @foreach($employee->shifts as $shift)
                {{$shift->day}} - {{$shift->start_time}} - {{$shift->end_time}} <br>
            @endforeach
            @else
                <p class="m-0 p-0"> Currently no default shift </p>
            @endif
            </td>
            @if(Auth::user()->hasRole('SuperAdmin'))
            <td class="text-center"><a href="{{route('coreui.employees.shifts',$employee->id)}}"><button class="btn btn-primary">Update</button></a></td>
            @endif
        </tr>
    @endforeach
</table>
@endsection