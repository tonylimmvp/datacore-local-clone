<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_types', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name')->unique();

            $table->boolean('isActive')->default(true);

            $table ->timestamps();

        });

        Schema::create('company_service_type', function (Blueprint $table) {

            $table->integer('company_id');

            $table->integer('service_type_id');

            $table->primary(['company_id','service_type_id']);

            $table ->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_types');

        Schema::dropIfExists('company_service_type');
    }
}
