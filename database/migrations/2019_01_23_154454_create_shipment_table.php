<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShipmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipments', function (Blueprint $table) {
            $table->increments('id');
            $table->date('departure_date')->nullable();
            $table->date('estimated_arrival_date')->nullable();
            $table->date('actual_arrival_date')->nullable();
            $table->integer('employee_id');
            $table->unsignedInteger('type_id');
            $table->boolean('isLocal');
            $table->string('vendor_id');
            $table->boolean('isShipped');
            $table->boolean('isActive');
            $table->unsignedInteger('courier_id')->default('1');
            $table->string('tracking')->nullable();
            $table->text('comments')->nullable();
            $table->unsignedInteger('status_id')->default('1');
            $table->string('purchase_order')->nullable();
            $table->timestamps();

            $table->foreign('courier_id')->references('id')->on('couriers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipments');
    }
}
