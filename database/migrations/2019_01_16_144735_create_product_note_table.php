<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductNoteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_notes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('type_id');
            $table->integer('product_id');
            $table->integer('qty')->nullable();
            $table->string('unit')->nullable();
            $table->text('comments')->nullable();
            $table->unsignedInteger('employee_id');
            $table->boolean('isHandled')->default(false);
            $table->date('handledDate')->nullable();
            $table->unsignedInteger('handledBy')->default('1');
            $table->timestamps();

            $table->foreign('employee_id')->references('id')->on('employees');
            $table->foreign('type_id')->references('id')->on('product_note_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_notes');
    }
}
