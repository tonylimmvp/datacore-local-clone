@extends('layouts.app')

@push('top-scripts')
<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
@endpush

@push('css')
<style>
    h1 {
        text-align: center;
        font-family: 'Nunito', sans-serif;
        font-weight: 200;
    }

    h2 {
        font-family: 'Nunito', sans-serif;
        font-weight: 200; 
    }

    h3 {
        font-family: 'Nunito', sans-serif;
        font-weight: 200;
        font-size: 22px;
    }

</style>
@endpush

@section('content')
<div class="container">
    <br/>
    <h1>RMA #{{ $rma->id }}</h1>
    <h1 style="color:#{{$rma->status->color}}">Current Status: {{$rma->status->name}}</h1>

    @if($rma->isActive == false)
        <h2 style="color: red; text-align: center">Deactivated</h2>
    @endif

    <hr/>

    <h3 class='shipment_label'>Vendor:</h3>
    <div class="input-group mr-sm-2 mb-2">
        <div class="input-group-prepend">
            <div class="input-group-text"><span data-feather="map-pin"></div>
        </div>
        @if($rma->vendor)
            <input class='form-control' type='text' name='shipment_vendor' value='{{ $rma->vendor->name }}' readonly><br/>
        @else
           <input class='form-control' type='text' name='shipment_vendor' value='N/A' readonly><br/> 
        @endif
    </div>

    <h3 class='rma_label'>Creation Date: </h3>
    <div class="input-group mr-sm-2 mb-2">
        <div class="input-group-prepend">
            <div class="input-group-text"><span data-feather="calendar"></i></div>
        </div>
        <input class='form-control rma_detail_show' type='text' name='rma_detail_name' value='{{ $rma -> created_at ->diffForHumans() }}' disabled><br/>
    </div>
    
    <h3 class='rma_label'>Created By: </h3>
    <div class="input-group mr-sm-2 mb-2">
        <div class="input-group-prepend">
            <div class="input-group-text"><span data-feather="users"></i></div>
        </div>
        <input class='form-control rma_detail_show' type='text' name='rma_detail_name' value='{{ $rma -> employee -> name }}' disabled><br/>
    </div>
				
    <h3 class='rma_label'>Name: </h3>
    <div class="input-group mr-sm-2 mb-2">
        <div class="input-group-prepend">
            <div class="input-group-text"><span data-feather="meh"></i></div>
        </div>
        <input class='form-control rma_detail_show' type='text' name='rma_detail_name' value='{{ $rma -> name }}' disabled><br/>
    </div>

    <h3 class='rma_label'>Phone: </h3>
    <div class="input-group mr-sm-2 mb-2">
        <div class="input-group-prepend">
            <div class="input-group-text"><span data-feather="phone-outgoing"></i></div>
        </div>
        <input class='form-control rma_detail_show' type='text' name='rma_detail_name' value='{{ $rma -> phone }}' disabled><br/>
        @if($rma->phone_ext)
        <div class="input-group-prepend">
            <div class="input-group-text"><span>Ext.</span></div>
        </div>
        <input class='form-control rma_detail_show' type='text' name='rma_detail_ext' value='{{$rma->phone_ext}}' disabled><br/>
        @endif
    </div>
	

    <h3 class='rma_label'>Email: </h3>
    <div class="input-group mr-sm-2 mb-2">
        <div class="input-group-prepend">
            <div class="input-group-text"><span data-feather="mail"></i></div>
        </div>
        <input class='form-control rma_detail_show' type='text' name='rma_detail_name' value='{{ $rma -> email }}' disabled><br/>
    </div>
    
    <h3 class='rma_label'>Comments: </h3>
    <textarea class='form-control rma_detail_show' type='text' name='rma_detail_name' disabled>{{ $rma -> comments }}</textarea><br/>
    <hr/>

    <h2>Product Information</h2>
    <div class="table-responsive">
        <table class="table table-dark table-bordered">
            <thead>
                <tr>
                    <th scope="col"><span data-feather="hash"></span> Product ID</th>
                    <th scope="col"><span data-feather="align-justify"></span> Product Description</th>
                    <th scope="col"><span data-feather="list"></span> Quantity</th>
                    <th scope="col"><span data-feather="slack"></span> Invoice Number</th>
                    <th scope="col"><span data-feather="calendar"></span> Invoice Date</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <th scope="row">{{ $rma -> id_product }}</th>
                    <td>{{ $rma -> product_name }}</td>
                    <td>{{ $rma -> qty }}</td>
                    <td>{{ $rma -> invoice_num }}</td>
                    <td>{{ $rma -> invoice_date}}</td>
                </tr>
            </tbody>
        </table>
    </div>
    <hr/>

    <h2>History and Comments</h2>
        <div class="table-responsive">
            <table class="table table-dark table-bordered">
                <tr>   
                    <th><span data-feather="user-check"></span> Employee</th>
                    <th><span data-feather="activity"></span> Status</th>
                    <th><span data-feather="message-square"></span> Comments</th>
                    <th><span data-feather="calendar"></span> Date</th>
                </tr>
                @foreach($status_history as $status)
                <tr>
                    <td>{{$status->sales->name}}</td>
                    <td>{{$status->status->name}}</td>
                    <td>{{$status->comment}}</td>
                    <td>{{$status->created_at->format('D, M d, Y h:i A')}}</td>      
                </tr>
                @endforeach
            </table>    
        </div>

    @auth
        @if(Auth::user()->hasAccess('rma')->update)
        <form class="form-inline" method="POST" action="{{route('rma-history.store')}}" enctype="multipart/form-data">
            {{ csrf_field() }}

                <div class="input-group mr-sm-2 mb-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><span data-feather="users"></i></div>
                    </div>

                    @if(Auth::user()->employee_role_id == 5)
                        <select class="form-control custom-select employee-dropdown" name="employee" required>
                            <option value="" selected disabled>Please Select An Employee...</option>
                            @foreach(App\Employee::isActive()->get() as $employee)
                                <option value="{{$employee->id}}">{{$employee->name}}</option>
                            @endforeach
                        </select>
                    @else
                    <input  type="text" class="form-control" value="{{Auth::user()->name}}" disabled>
                    <input type="hidden" name="employee" value="{{Auth::user()->id}}">
                    @endif

                    <input type="hidden" name="rma" value="{{$rma->id}}">
                </div>

                <select class="custom-select mr-sm-2 mb-2" name="status" required>
                    <option value="" selected disabled>Please Select Status...</option>
                    @foreach($statuses as $status)
                        <option value="{{$status->id}}">{{$status->name}}</option>
                    @endforeach
                </select>

                <input type="text" class="form-control mb-2 mr-sm-2" placeholder="Enter Comment..." name="comment">
              
                <button type="submit" class="btn btn-sm btn-info mb-2">Submit</button>
        </form>   
        @endif

        @if(Auth::user()->hasAccess('rma')->update)
        <hr/>
        <h2>Actions</h2>
        <form class="form-inline" method="POST" action="{{route('rma.deactivate', $rma->id)}}" enctype="multipart/form-data">
            <input type="hidden" name="_method" value="patch">
            
            {{ csrf_field() }}

            <input type="hidden" name="isActive" value="0">

            <button type="submit" class="btn btn-sm btn-info mb-2">Deactivate RMA</button>
        </form>
        @endif

        <a href="{{route('rma.printLabel', $rma->id)}}">
            <button class='btn btn-sm btn-info mb-2'>Print Label</button>
        </a>
        
    @endauth

</div>

@endsection