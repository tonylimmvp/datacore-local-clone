@extends('frontend.layouts.main')

@section('content')
<div class="container">
    <h1>Create Order</h1>
    <form id="order-create-form" method="POST" action="{{route('orders.store')}}" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{-- Cart Information --}}
        <div class="card mb-3">
            <div class="card-body">
                <table id="order-products" class="table table-bordered table-responsive">
                    <thead>
                        <tr>
                            <th style="min-width:180px">PID</th>
                            <th style="min-width:250px">Name</th>
                            <th style="min-width:180px;width:180px">Price</th>
                            <th style="min-width:110px;width:110px;">Quantity</th>
                            <th style="min-width:150px">Vendor</th>
                            <th style="min-width:100px">Remark</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(old('product_id'))
                        @foreach(old('product_id') as $id => $product)
                            <tr id="prod{{$id}}">
                                <td class="input-group"><input type="number" class="form-control" name="product_id[]" placeholder="PID..." value="{{old('product_id')[$id]}}" required><div class="input-group-append"><button class="btn input-group-text btn-outline-secondary autofill" type="button"><span data-feather="align-left"></span></button></span></div></td>
                                <td><input class="form-control" name="product_name[]" placeholder="Item Name..." value="{{old('product_name')[$id]}}" required></td>
                                <td class="input-group"><div class="input-group-prepend"><span class="input-group-text">$</span></div><input type="number" step="0.01" class="form-control ml-auto" name="product_price[]" placeholder="Amount" onkeyup="calculateTotal()" value="{{old('product_price')[$id]}}" required></td>
                                <td><input type="number" class="form-control" name="product_quantity[]" onkeyup="calculateTotal()" value="{{old('product_quantity')[$id]}}" required></td>
                                <td>
                                    <select class="form-control" name="product_vendor[]">
                                        <option value="" selected>--</option>
                                        @foreach(\App\Vendor::all() as $vendor)
                                            <option value="{{$vendor->reference}}" @if(old('product_vendor')[$id] == $vendor->reference) selected @endif>{{$vendor->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td><input class="form-control" name="product_vendor_remark[]" placeholder="Vendor Remark..." value="{{old('product_vendor_remark')[$id]}}"></td>
                                <td><button class="btn btn-danger removeButton" type="button"><span data-feather="x"></span></button></td>
                            </tr>   
                        @endforeach
                        @else
                            <tr id="prod0">
                                <td class="input-group"><input type="number" class="form-control" name="product_id[]" placeholder="PID..." required><div class="input-group-append"><button class="btn input-group-text btn-outline-secondary autofill" type="button"><span data-feather="align-left"></span></button></span></div></td>
                                <td><input class="form-control" name="product_name[]" placeholder="Item Name..." value="" required></td>
                                <td class="input-group"><div class="input-group-prepend"><span class="input-group-text">$</span></div><input type="number" step="0.01" class="form-control ml-auto" name="product_price[]" placeholder="Amount" onkeyup="calculateTotal()" required></td>
                                <td><input type="number" class="form-control" name="product_quantity[]" onkeyup="calculateTotal()" required></td>
                                <td>
                                    <select class="form-control" name="product_vendor[]">
                                        <option value="" selected>--</option>
                                        @foreach(\App\Vendor::all() as $vendor)
                                            <option value="{{$vendor->reference}}">{{$vendor->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td><input class="form-control" name="product_vendor_remark[]" placeholder="Vendor Remark..."></td>
                                <td><button class="btn btn-danger removeButton" type="button"><span data-feather="x"></span></button></td>
                            </tr>   
                        @endif
                         
                    </tbody>
                    <tfoot>
                        <tr>
                            <td><span id="totalProducts">Total Items: 1</span></td>
                            <td><span id="totalPrice">Total Price: $0.00 </span></td>
                        </tr>
                    </tfoot> 
                </table>
                <button type="button" class="btn btn-primary" onclick="addProduct()">Add Product</button>
            </div>
        </div>


        {{-- Customer Information --}}
        <div class="form-group input-group">
                
            <label for="invoice">Invoice Number</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text"><span data-feather="hash"></i></div>
                </div>
                <input type="text" class="form-control" id="name" name="invoice" placeholder="Enter Invoice Number" value="{{ old('invoice') }}" required>
            </div>
            @auth @if(Auth::user()->hasAccess('payment')->create)<small>Haven't paid yet? <a href="{{route('checkout')}}" target="_blank">Pay with Bambora!</a> </small> @endif @endauth
           
        </div>

        <div class="form-group input-group"> 
                <label for="payment_id">Bambora Transaction ID</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><span data-feather="hash"></i></div>
                    </div>
                    <input type="text" class="form-control" id="payment_id" name="payment_id" placeholder="Enter Bambora TID (optional)" value="{{ old('payment_id',$request->tid) }}">
                </div>               
            </div>
        
        <div class="form-group">
            <label for="name">Name</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text"><span data-feather="smile"></i></div>
                </div>
                <input type="text" class="form-control" id="name" name="name" placeholder="Enter Customer Name" value="{{ old('name',$request->name)}}" required>
            </div>
        </div>
    
        <div class="form-group">
            <label for="phone">Phone</label>
           
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text"><span data-feather="phone"></span></div>
                </div>
                <input type="text" class="form-control" id="phone" name="phone" placeholder="E.g 6048751993" value="{{ old('phone',$request->phone) }}" required>
                <div class="input-group-prepend">
                    <div class="input-group-text"><span>Ext.</span></div>
                </div>
                <input type="text" class="form-control" id="phone_ext" name="phone_ext" pattern="\d*" maxlength="6" placeholder="(optional)" value="{{ old('phone_ext') }}">
            </div>
        </div>
    
        <div class="form-group">
            <label for="email">Email</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text"><span data-feather="mail"></i></div>
                </div>
                <input type="email" class="form-control" id="email" name="email" placeholder="Enter Email" value="{{ old('email',$request->email) }}">
            </div>
        </div>
        
        <div class="form-group">
            <label for="comments">Comments</label>
            <textarea type="textarea" class="form-control" id="comments" name="comments" placeholder="Enter Comment..." value="{{ old('comments') }}"></textarea>
        </div>

        {{-- Employee --}}
        <div class="form-group form-inline">
                <div class="input-group mr-sm-2 mb-2">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><span data-feather="users"></i></div>
                    </div>
                    @if(Auth::user()->employee_role_id == 5)
                        <select class="form-control custom-select employee-dropdown" name="employee" required>
                            <option value="" selected disabled>Please Select An Employee...</option>
                            @foreach($employees as $employee)
                                <option value="{{$employee->id}}">{{$employee->name}}</option>
                            @endforeach
                        </select>
                    @else
                    <input  type="text" class="form-control" value="{{Auth::user()->name}}" disabled>
                    <input type="hidden" name="employee" value="{{Auth::user()->id}}">
                    @endif
                </div>
            <button id="orderCreateButton" type="submit" class="btn btn-primary mb-2">Create Order</button>
        </div>

        <a class="pb-2" href="{{route('logout')}}">Not <b>{{Auth::user()->name}}?</b> Log out to log in!</a>
    
    
    @include('layouts.errors')
    
    </form>

</div>
    
@endsection

@push('bottom-scripts')
<script>
    function addProduct(){
        var i = $('#order-products').find('tbody>tr').length
        $('#order-products>tbody').append('<tr id="prod' + i + '">\
        <td class="input-group"><input type="number" class="form-control" name="product_id[]" placeholder="PID..." required><div class="input-group-append"><button class="btn input-group-text btn-outline-secondary autofill" type="button"><span data-feather="align-left"></span></button></span></div></td>\
        <td><input class="form-control" name="product_name[]" placeholder="Item Name..." required></td>\
        <td class="input-group"><div class="input-group-prepend"><span class="input-group-text" required>$</span></div>\
        <input type="number" step="0.01" class="form-control ml-auto" name="product_price[]" placeholder="Amount" onkeyup="calculateTotal()" required></td>\
        <td><input type="number" class="form-control" name="product_quantity[]" onkeyup="calculateTotal()" required></td>\
        <td>\
        <select class="form-control" name="product_vendor[]">\
            <option value="" selected>--</option>\
            @foreach(\App\Vendor::all() as $vendor)\
                <option value="{{$vendor->reference}}">{{$vendor->name}}</option>\
            @endforeach\
        </select></td>\
        <td><input class="form-control" name="product_vendor_remark[]" placeholder="Remark..."></td>\
        <td><button class="btn btn-danger removeButton" type="button"><span data-feather="x"></span></button></td></tr>');

        feather.replace()

        updateTableRow()
    }


    function updateTableRow(){
        var i = $('#order-products').find('tbody>tr').length
        $( "#order-products>tfoot #totalProducts").html("Total Items: " + i);
    }

    function calculateTotal(){
        var price=parseFloat(0);
        var qty = 1;
        $('input[name="product_price[]"]').each(function(index){
            var current_price = 0;
            if($(this).val()){
                var current_price = parseFloat($(this).val())
            }
            var quantity = $('input[name="product_quantity[]"]')[index].value
            if(!quantity){
                qty = 0;
            }
            price += current_price * quantity
        })

        if(qty == 1){
            $( "#order-products>tfoot #totalPrice").html("Total Price: $" + price.toFixed(2));
        }else{
            $( "#order-products>tfoot #totalPrice").html("Total Price: NA");
        }
        
    }

    $(document).ready(function(){
        $( "#order-products" ).on("click", ".autofill", function() {
            var tr = $(this).closest('tr');
            var pid = $(this).closest('tr').find("input")[0].value;
            console.log("Autofilling: " + pid);
            $.ajax({
                type: "get",
                url: "{{ route('product.autofill') }}",
                data: {pid: pid},
                success: function( msg ) {
                    if(msg.status == "success"){
                        tr.find('input')[0].value = pid
                        tr.find('input')[1].value = msg.name
                        tr.find('input')[2].value = msg.price
                        tr.find('select')[0].value = msg.vendor
                        tr.find('input')[4].value = msg.remarks
                    }else{
                        alert(msg.message);
                    }
                    calculateTotal();
                },
                error: function (request, status, error) {
                    alert(request.status + ':' + error + '. Please take picture and contact DevTeam.');
                }
            });
            updateTableRow();
        });
    });
    
    $( "#order-products" ).on("click", ".removeButton", function() {
        $( this ).closest("tr").remove();
        updateTableRow();
    });
</script>

<script>
$('form#order-create-form').on('submit',function(){
    $('#orderCreateButton').attr("disabled",true);
    $('#loader').toggle();
});
</script>
@endpush