@component('mail::message')
Hello {{$order->name}},

@if($order->type == "backorder")
Your backorder has arrived!
@elseif($order->type == "special")
Your special order has arrived!
@elseif($order->type == "phone")
Your phone order has arrived!
@endif

Order #{{$order->id}} has arrived and is ready for pick up. If your order is for ship-out, you will receive a second email with your tracking number.

@component('mail::table')
| PID        | Name           | Price per Unit  | Quantity | 
| ---------- |:--------------:| :--------------:| :------: |
@foreach($order->products as $product)
| {{$product->id_product}} | {{$product->pivot->name}} | ${{$product->pivot->price}} | {{$product->pivot->quantity}} |
@endforeach
@endcomponent

Cheers,<br>
Lee's Electronic Components

@endcomponent

