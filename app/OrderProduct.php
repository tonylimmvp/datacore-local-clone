<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;

class OrderProduct extends Model
{
    protected $table = 'order_product';
    protected $guarded = [];
    public $timestamps = false;

    public function order(){
        return $this->belongsTo(Order::class);
    }

    public function product(){
        return $this->belongsTo(Product::class,'product_id','id_product');
    }

    public function orders(){
        $orders = Order::join("order_product","orders.id","=","order_product.order_id")
                ->select('orders.id','order_product.id as order_product_id','orders.name','product_id','order_product.name as product_name','status_id')
                ->whereNotIn('status_id',[4,6,3,8,2])
                ->where('product_id',$this->product_id)
                ->where('order_product.name',$this->name)->get();
        return $orders;
    }
}
