@include('layouts.datatables')

{{-- Search --}}
{{-- <div class="row">
    <div class="col-12">
        <form method="post" action="{{route('order.search')}}">
            {{ csrf_field() }}
            <div class="input-group text-center mb-4">
                    <div class="input-group-prepend offset-1 offset-sm-2">
                        <select class="custom-select" name="type" id="inputGroupSelect01">
                            <option value="1" @if(isset($type) && $type==1) selected @endif>All</option>
                            <option value="2" @if(isset($type) && $type==2) selected @endif>Special</option>
                            <option value="3" @if(isset($type) && $type==3) selected @endif>Back</option>
                        </select>
                    </div>
                    <input type="text" class="form-control col-4 col-md-6" name="term" @if(isset($term)) value="{{$term}}" @endif required>
                    <div class="input-group-append">
                    <button type="submit" class="btn btn-primary">Search</button>
                    </div>
            </div>
        </form>
    </div>
</div> --}}

@foreach($statusFilters as $filter)

<a href="?status={{$filter->id}}">
    <button class="btn btn-primary m-1 mb-4">
        {{$filter->name}}
    </button>
</a>
@endforeach
<a href="?">
    <button class="btn btn-primary m-1 mb-4">
        All
    </button>
</a>

{{-- @foreach($orders as $order)
    <div class="card mb-3">
        <div class="card-header">
            <h5> Order <a href="{{route('orders.detail',$order->id)}}"> #{{$order->id}} </a>
                @if($order->type == "special")
                    (SPO)
                @elseif($order->type == "backorder")
                    (BO)
                @else
                    (ERROR)
                @endif
            </h5>
                    
        </div>
        <div class="card-body table-responsive">
            <table class="table table-bordered">
                <tr>
                    <th>Invoice #</th>
                    <th>Customer</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Sales Rep</th>
                    <th>Created</th>
                    <th>Status</th>
                </tr>
                <tr>
                    <td>@if($order->invoice_id){{$order->invoice_id}}@else NA @endif</td>
                    <td>{{$order->name}}</td>
                    <td>{{$order->phone}}</td>
                    <td>{{$order->email}}</td>
                    <td>{{$order->sales->name}}</td>
                    <td>{{$order->created_at->format('M d, Y')}}</td>
                    <td style="background:{{$order->currentStatus->color}}">{{$order->currentStatus->name}}</td>
                </tr>
                
            </table>

            <table class="table table-bordered">
                    <tr>
                        <th>PID</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Quantity</th>
                    </tr>
                    @foreach($order->products as $product)
                    <tr>
                        <td>{{$product->id_product}}</td>
                        <td>{{$product->pivot->name}}</td>
                        <td>${{$product->pivot->price}}</td>
                        <td>{{$product->pivot->quantity}}</td>
                    </tr>
                    @endforeach
                    
                </table>
        </div>
    </div>
    
    @endforeach --}}

    <div class="table-responsive">
        <table id="orders-table" class="table table-bordered">
            <thead>

            
            <tr>
                <th>ORDER #</th>
                <th>Invoice #</th>
                <th>Customer</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Sales Rep</th>
                <th>Created</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            @foreach($orders as $order)
            <tr>
                <td><a href="{{route('orders.detail',$order->id)}}">{{$order->id}}</a></td>
                <td>@if($order->invoice_id){{$order->invoice_id}}@else NA @endif</td>
                <td>{{$order->name}}</td>
                <td>{{$order->phone}}</td>
                <td>{{$order->email}}</td>
                <td>{{$order->sales->name}}</td>
                <td>{{$order->created_at->format('M d, Y')}}</td>
                <td style="background:{{$order->currentStatus->color}}">{{$order->currentStatus->name}}</td>
            </tr>

            @endforeach
        </tbody>
        </table>
    </div>

    @push('bottom-scripts')

        <script type="text/javascript">
            
                $('#orders-table').DataTable(
                    {
                        scrollY:        "60vh",
                        scrollX:        true,
                        scrollCollapse: true,
                        paging:         false,
                        "displayLength": 25,
                        "language": {
                            "search": "Filter records:"
                        }
                    }
                );
         
        </script>


    @endpush
    
