<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Note;
use App\OnlineOrder;
use App\Order;
use App\MobileOrder;
use Auth;
use Employee;
use EmployeeRole;
use DB;

class NoteController extends Controller
{
    public function viewOnline(OnlineOrder $onlineOrder){
        //We use generic variable $notes here so that the view page will return same variable name
        $order = $onlineOrder;
        $notes = $onlineOrder->note;

        //Variables used to distinguish which order is which in view page:
        $mobileOrder = null;
        $storeOrder = null;

        return view('frontend.note.view',compact('notes', 'order', 'onlineOrder', 'mobileOrder', 'storeOrder'));
    }

    public function viewOrder(Order $storeOrder){
        $order = $storeOrder;
        $notes = $storeOrder->note;

        //Variables used to distinguish which order is which in view page:
        $mobileOrder = null;
        $onlineOrder = null;

        return view('frontend.note.view',compact('notes', 'order', 'onlineOrder', 'mobileOrder', 'storeOrder'));
    }

    public function viewMobile(MobileOrder $mobileOrder){
        $order = $mobileOrder;
        $notes = $mobileOrder->note;

        //Variables used to distinguish which order is which in view page:
        $storeOrder = null;
        $onlineOrder = null;

        return view('frontend.note.view',compact('notes', 'order', 'onlineOrder', 'mobileOrder', 'storeOrder'));
    }

    public function storeOrderNote(Request $request, Order $storeOrder)
    {
        if(!empty($storeOrder)){

            $note = new Note();
            $note->employee_id = Auth::user()->id;
            $note->notes = $request->comments;
            $note->created_at = NOW();
            $note->save();
            
            $note->order()->attach($storeOrder->id);
        }

        return back();
    }

    public function storeOnlineOrderNote(Request $request, OnlineOrder $onlineOrder)
    {
        if(!empty($onlineOrder)){

            $note = new Note();
            $note->employee_id = Auth::user()->id;
            $note->notes = $request->comments;
            $note->created_at = NOW();
            $note->save();
            
            $note->online()->attach($onlineOrder->id_order);
        }

        return back();
    }

    public function storeMobileOrderNote(Request $request, MobileOrder $mobileOrder)
    {
        if(!empty($mobileOrder)){

            $note = new Note();
            $note->employee_id = Auth::user()->id;
            $note->notes = $request->comments;
            $note->created_at = NOW();
            $note->save();
            
            $note->mobile()->attach($mobileOrder->order_id);
        }

        return back();
    }

    public function deleteOrderNote(Note $note)
    {
        $activeNote = Note::find($note->id);
        $activeNote->order()->detach();
        $activeNote->delete();
        
        return redirect()->back();
    }

    public function deleteOnlineNote(Note $note)
    {
        $activeNote = Note::find($note->id);
        $activeNote->online()->detach();
        $activeNote->delete();
        
        return redirect()->back();
    }

    public function deleteMobileNote(Note $note)
    {
        $activeNote = Note::find($note->id);
        $activeNote->mobile()->detach();
        $activeNote->delete();
        
        return redirect()->back();
    }
}