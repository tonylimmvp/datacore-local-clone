<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuotationProduct extends Model
{
    protected $table = 'quotation_product';
    protected $guarded = [];
    public $timestamps = false;

    public function quotation(){
        return $this->belongsTo(Quotation::class);
    }
}
