<?php

use Illuminate\Database\Seeder;
use App\OrderStatusHistory;

class OrderStatusHistoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $order_status = new OrderStatusHistory();
        $order_status->order_id = 1;
        $order_status->employee_id = 1;
        $order_status->status_id = 1;
        $order_status->comment = "Created by Seed";
        $order_status->save();

        $order_status = new OrderStatusHistory();
        $order_status->order_id = 2;
        $order_status->employee_id = 1;
        $order_status->status_id = 1;
        $order_status->comment = "Created by Seed";
        $order_status->save();

        $order_status = new OrderStatusHistory();
        $order_status->order_id = 3;
        $order_status->employee_id = 2;
        $order_status->status_id = 1;
        $order_status->comment = "Created by Seed";
        $order_status->save();

        $order_status = new OrderStatusHistory();
        $order_status->order_id = 4;
        $order_status->employee_id = 4;
        $order_status->status_id = 1;
        $order_status->comment = "Created by Seed";
        $order_status->save();

        $order_status = new OrderStatusHistory();
        $order_status->order_id = 1;
        $order_status->employee_id = 3;
        $order_status->status_id = 2;
        $order_status->comment = "Created by Seed";
        $order_status->save();
    }
}
