<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\EmployeeRole;
use App\Permission;

class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {   
        $employees = Employee::all();

        return view('backend.employees.index',compact('employees'));
    }

    public function create()
    {
        $roles = EmployeeRole::all();

        return view('backend.employees.create',compact('roles'));
    }

    public function store(Request $request)
    {   
        // Validate form
        $this->validate(request(), [
            'name' => 'required',
            'username' => 'required|string|max:255|unique:employees',
            'role' => 'required|integer',
            'email' => 'required|email|unique:employees',
            'password' => 'required|confirmed'
        ]);

        // create employee

        $employee = Employee::create([
                'name' => request('name'),
                'username' => strtolower(request('username')),
                'email' => request('email'),
                'password' => bcrypt(request('password')),
                'employee_role_id' => (int)request('role')
            ]
        );

        foreach($employee->role->permissions as $permission){
            $employee->permissions()->attach($permission->id, [
                'create' => $permission->pivot->create,
                'read' => $permission->pivot->read,
                'update' => $permission->pivot->update,
                'delete' => $permission->pivot->delete,
                ]);
        }

        $employee->save();

        session()->flash('message','You have successfully created an account for ' . $employee->name . '. Please update the permissions.');

        return redirect()->route('coreui.employee-permission.edit',$employee->id);
    }

    public function show($id)
    {
        //
    }

    public function edit(Employee $employee)
    {   
        $current = \Auth::user();
        if($employee->id!=$current->id){
            if(!$current->hasRole("SuperAdmin")){
                session()->flash('message','The page you tried to reach is restricted');
                return redirect()->route('coreui.dashboard');
            }
        }
        $roles = EmployeeRole::all();
        return view('backend.employees.edit', compact('employee','roles'));
    }

    public function update(Request $request, Employee $employee)
    {   
        $current = \Auth::user();
        if($employee->id!=$current->id){
            if(!$current->hasRole("SuperAdmin")){
                session()->flash('message','The page you tried to reach is restricted');
                return redirect()->route('coreui.dashboard');
            }
        }

        // Validate form
        $this->validate(request(), [
            'name' => 'required',
            'role' => 'required|integer',
            'password' => 'confirmed'
        ]);

        $employee->name = $request->name;
        $employee->employee_role_id = $request->role;
        if($request->password){
            $employee->password = bcrypt($request->password);
        }
        $employee->save();

        if($current->hasRole("SuperAdmin")){
            session()->flash('message','You have successfully updated ' . $employee->name);
            return redirect()->route('coreui.employees');
        }else{
            session()->flash('message','You have successfully updated your account.');
            return redirect()->route('coreui.dashboard');
        }
        
    }

    public function destroy(Employee $employee)
    {
        $employee->delete();
        session()->flash('message','You have successfully deleted ' . $employee->name .'.');
        return redirect()->route('coreui.employees');
    }

    public function deactivate(Employee $employee)
    {
        $employee->isActive = false;
        $employee->save();
        session()->flash('message','You have successfully deactivated ' . $employee->name .'.');
        return redirect()->route('coreui.employees');
    }

    public function activate(Employee $employee) {
        $employee->isActive = true;
        $employee->save();
        session()->flash('message','You have successfully activated ' . $employee->name .'.');
        return redirect()->route('coreui.employees');
    }

    public function profile(Employee $employee){
        return view('backend.employees.profile.index',compact('employee'));
    }

    public function profile_update(Request $request, Employee $employee){
        // Validate form
        $this->validate(request(), [
            'name' => 'required',
            'password' => 'confirmed'
        ]);

        $employee->name = $request->name;

        if($request->password){
            $employee->password = bcrypt($request->password);
        }
        $employee->save();

        session()->flash('message','You have successfully updated your account.');
        
        return redirect()->route('coreui.dashboard');
    }

    public function shifts(Employee $employee){
        return view('backend.employees.shifts',compact('employee'));
    }
}
