<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vendor;
use App\PurchaseOrder;
use Carbon\Carbon;

class PurchaseOrderController extends Controller
{
    public function __construct(){
        return $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $purchases = PurchaseOrder::active()->filter($request->all())->orderBy('created_at','desc')->paginate('200');

        return view('backend.purchases.index',compact('purchases'));
    }

    public function products(PurchaseOrder $po_id)
    {
        $purchase = $po_id;

        return view('backend.purchases.show',compact('purchase'));
    }

    public function upload()
    {
        return view('backend.purchases.upload');
    }

    public function store(Request $request)
    {   
       
        $this->validate(request(), [
            'csv_file' => 'required|file|mimes:txt,csv,xlsx',
        ]);

        $V1_columns = array("P.O. Date",
                            "Ship Date",
                            "PO no.",
                            "PR no.",
                            "Vend.ID",
                            "Vend.Inv.",
                            "User ID",
                            "B.Tax Tot",
                            "A.Tax Tot",
                            "Discount",
                            "PST",
                            "GST",
                            "Currency"
                            );

        $path = $request->file('csv_file')->getRealPath();

        $handle = fopen($path, "r");

        $line_num = 0;
        $header = false;
        
        while ( (($data = fgetcsv($handle,0, ",","\"")) !== FALSE)) {
            // echo $data[0];
            if($data[0]==$V1_columns[0]){
                $header = true;
            }
            if($header){
                // Validate columns
                if($line_num == 0){

                    for($index=0 ; $index < count($V1_columns); $index++){

                        $data_token = $data[$index];
                        $header_token = $V1_columns[$index];
							// echo $data[$index];
						if(strcmp($data[$index], $V1_columns[$index]) != 0){
                            session()->flash('error','Header did not match, please consult the development team.');
                            return back();
                        }
                    }
                }
                else{
                    $vendor = Vendor::where('reference',$data[4])->first();
                    
                    if($vendor){
                        PurchaseOrder::updateOrCreate(
                            [
                                'po_id'=>$data[2]
                            ],
                            [
                                'pr_id'=>$data[3],
                                'vendor_id'=>$vendor->id,
                                'invoice'=>$data[5],
                                'user'=>$data[6],
                                'total_excl_tax'=>$data[7],
                                'total_incl_tax'=>$data[8],
                                'discount'=>$data[9],
                                'pst'=>$data[10],
                                'gst'=>$data[11],
                                'currency'=>$data[12],
                                'created_at'=>Carbon::parse($data[0]),
                                'received_at'=>Carbon::parse($data[1])
                            ]);
                    }

                }

                $line_num++;

            }
        }

        if(!$header){
            session()->flash('error','Could not find header, please check the file you uploaded.');
            return back();
        }

        session()->flash('message',"You have successfully imported/updated vendors");

        return back();
    }
}
