@extends('backend.layouts.main')

@include('layouts.datatables')

@push('css')
    <link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/3.0.0-beta.3/css/bootstrap-colorpicker.min.css" rel="stylesheet">
@endpush

@section('content')
    <h1>Order Statuses</h1>
    @if(Auth::user()->hasAccess('order_status')->create)
    <div class="d-flex flex-row-reverse pt-3 pb-2 mb-3">
      <div class="btn-toolbar mb-2 mb-md-0">
      <div class="btn-group mr-2">
          <a href="{{route('coreui.order-status.create')}}"><button class="btn btn-xs btn-primary" >Add Order Status</button></a>
      </div>
      </div>
    </div>
    @endif
    <table id="orderStatus" class="table table-striped table-bordered table-responsive" cellspacing="0" width="100%">
        <thead>
            <tr>
            <th class="th-sm">ID</th>
            <th class="th-sm">Name</th>
            <th class="th-sm">Color </th>
            <th class="th-sm">Position</th>
            @if(Auth::user()->hasAccess('order_status')->update || Auth::user()->hasAccess('order_status')->delete)
            <th class="sorting_disabled">Action
            </th>
            @endif
            </tr>
        </thead>
        <tbody>
            @foreach($statuses as $status)
                <tr>
                    <td>{{$status->id}}</td>
                    <td>{{$status->name}}</td>
                    <td> <div class="d-flex"><span class="pr-2">{{$status->color}}</span><span class="ml-auto border" style="width:35px;height:35px;background:{{$status->color}}"> </span> </div></td>
                    <td>{{$status->position}}</td>
                    @if(Auth::user()->hasAccess('order_status')->update || Auth::user()->hasAccess('order_status')->delete)
                    <td>
                        @if(Auth::user()->hasAccess('order_status')->update)
                        <button class="btn btn-xs btn-primary" 
                            data-toggle="modal" 
                            data-target="#editModal" 
                            data-id="{{$status->id}}" 
                            data-name="{{$status->name}}"
                            data-color="{{$status->color}}"
                            data-position="{{$status->position}}"
                            data-edit_url="{{route('coreui.order-status.update',$status->id)}}">Edit</button>
                        @endif
                        @if(Auth::user()->hasAccess('order_status')->delete)
                        <button class="btn btn-xs btn-danger" 
                                data-toggle="modal" 
                                data-target="#deleteModal" 
                                data-id="{{$status->id}}" 
                                data-name="{{$status->name}}"
                                data-delete_url="{{route('coreui.order-status.delete',$status->id)}}">Delete</button>
                        @endif
                    </td>
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>

    @include('layouts.errors')


{{-- Edit Modal --}}
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Update Order Status: <span id="status-name"> --- </span></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_method" value="PATCH">
                {{ csrf_field() }}
            <div class="modal-body">
              
                  <label for="name">Name: </label><input type="text" class="form-control" id="status-input" name="name" required>
                  <br>
                  <label for="color">Color: </label><input type="text" class="form-control" id="color-picker" name="color" required>
                  <br>
                  <label for="position">Position: </label><input type="number" class="form-control" id="status-position" name="position" required>
             
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-primary">Update Status</button>
            </div>
            </form>
          </div>
        </div>
      </div>
      
{{-- Delete Modal --}}
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Delete Order Status?</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              Are you sure you want to delete the status: <span id="status-name"> Status </span>?
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <a id="status-delete" href="#">
              <button type="button" class="btn btn-danger">Delete Status</button>
              </a>
            </div>
          </div>
        </div>
      </div>
     
@endsection

@push('bottom-scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/3.0.0-beta.3/js/bootstrap-colorpicker.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#orderStatus').DataTable({
            columnDefs: [ { orderable: false, targets: [3] } ],
            "language": {
              "search": "Filter records:"
            }
        });
        $('.dataTables_length').addClass('bs-select');
    });
    $('#color-picker').colorpicker({format:'hex'});
</script>

<script>
$(document).ready(function () {
    $('#editModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) 
        var id = button.data('id')
        var status = button.data('name') 
        var color = button.data('color') 
        var position = button.data('position') 
        var edit_url = button.data('edit_url')
        var modal = $(this)
        modal.find('#status-name').text(status)
        $('#status-input').val(status)
        $('#color-picker').val(color)
        $('#status-position').val(position)
        modal.find('form').attr('action',edit_url)
    });
});
</script>

<script>
$(document).ready(function () {
    $('#deleteModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) 
        var id = button.data('id')
        var status = button.data('name') 
        var delete_url = button.data('delete_url')
        var modal = $(this)
        modal.find('#status-name').text(status)
        modal.find('#status-delete').attr('href',delete_url)
    });
});
</script>


@endpush