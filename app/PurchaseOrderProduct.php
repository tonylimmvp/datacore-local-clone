<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrderProduct extends Model
{
    use Traits\MultiplePrimaryKeys;

    protected $connection = "mysql";
    protected $primaryKey = ['po_id','product_id','name'];
    protected $guarded = [];

    public $timestamps = false;
    public $incrementing = false;

    public function detail(){
        return $this->belongsTo(Product::class, 'product_id','id_product');
    }
}
