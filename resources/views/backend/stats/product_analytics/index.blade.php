@extends('backend.layouts.main')

@include('layouts.datatables')

@push('top-scripts')
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
@endpush

{{-- Variable Declarations and Initializations --}}
@php use \App\Http\Controllers\AnalyticsController; @endphp
@php
    $currentYear = date('Y') + 0; 
    $currentMonth = date('m') + 0;
    $rankQueryLimit = 50;
@endphp


@push('css')
    <style>
        h1 {
            text-align: center;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
        }

        h2 {
            font-family: 'Nunito', sans-serif;
            font-weight: 200; 
            text-align: center;
        }

        h3 {
            font-family: 'Nunito', sans-serif;
            font-weight: 200; 
            font-size: 20px;
            text-align: center;
        }
    </style>
@endpush

@section('content')

<h2>Product Margin Ranking</h2>
<h3>Price > 0, Recent Cost > 0.01, Product Not Disabled, Price >= Recent Cost</h3>
<hr>

@php 
    $products = DB::connection('leeselectronic')
                    ->table("table_1")
                    ->select("id_product", "name", "price", "recent_cost", DB::raw("((price - recent_cost)/price) as margin"))
                    ->where('disabled', 'N')
                    ->where('price', '>=', 'recent_cost') 
                    ->where('recent_cost','>',0.01)
                    ->orderBy('margin', 'ASC')
                    ->limit(100)
                    ->get(); 
@endphp

<div class="table-responsive">
    <table id="rankTable_1" class="table table-hover table-dark table-bordered" width="100%">
        <thead>
            <tr>
                <th scope="col"></th>
                <th scope="col"><span data-feather="hash"></span> Product ID</th>
                <th scope="col"><span data-feather="list"></span> Name</th>
                <th scope="col"><span data-feather="dollar-sign"></span> Price</th>
                <th scope="col"><span data-feather="dollar-sign"></span> Cost</th>      
                <th scope="col"><span data-feather="tag"></span> Margin</th>
            </tr>
        </thead>
        <tbody>
            @foreach($products as $key=>$product)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <th scope="row">{{ $product->id_product }}</th>
                    <td>{{ $product->name }}</td>
                    <td>${{ $product->price }}</td>
                    <td>${{ $product->recent_cost }}</td>
                    <td>{{$product->margin}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

<hr>

<h2>Product Sales Ranking</h2>
<hr>

@php $rankQueries = DB::select("SELECT product_id, year, SUM(qty) as total FROM datacore.sales_histories where year = $currentYear GROUP BY product_id, year ORDER BY total DESC LIMIT $rankQueryLimit"); @endphp

    <div class="table-responsive">
        <table id="rankTable_2" class="table table-hover table-dark table-bordered" width="100%">
            <thead>
                <tr>
                    <th scope="col"></th>
                    <th scope="col"><span data-feather="hash"></span> Product ID</th>
                    <th scope="col"><span data-feather="list"></span> Name</th>
                    <th scope="col"><span data-feather="dollar-sign"></span> Price</th>
                    <th scope="col"><span data-feather="tag"></span> Quantity Sold</th>
                </tr>
            </thead>
            <tbody>
                @foreach($rankQueries as $key=>$rankQuery)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <th scope="row">{{ $rankQuery->product_id }}</th>
                        <td>{{App\Product::getProductName($rankQuery->product_id)}}</td>
                        <td>${{App\Product::getProductPrice($rankQuery->product_id)}}</td>
                        <td>{{$rankQuery->total}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
<hr>

@php $productCount = App\Product::all()->count(); @endphp
@php $activeProductCount = App\Product::where('disabled', 'N')->count(); @endphp
<h3>Total Products: {{$productCount}}</h3>
<h3>Total Active Products: {{$activeProductCount}}</h3>

@endsection

@push('bottom-scripts')
    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"> </script>

    <script>
        jQuery(document).ready(function($) {
            $(".clickable-row").click(function() {
                window.location = $(this).data("href");
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#rankTable_1').DataTable({
                "scrollY":        "150vh",
                "scrollX":        true,
                "scrollCollapse": true,
                "paging":         false,
                columnDefs: [ { orderable: false, targets: [
                    2
                ]}],
                "language": {
                    "search": "Filter records:"
                }
            });
        });
    </script>

        <script type="text/javascript">
        $(document).ready(function () {
            $('#rankTable_2').DataTable({
                "scrollY":        "150vh",
                "scrollX":        true,
                "scrollCollapse": true,
                "paging":         false,
                columnDefs: [ { orderable: false, targets: [
                    2
                ]}],
                "language": {
                    "search": "Filter records:"
                }
            });
        });
    </script>

@endpush