<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\OrderProduct;
use App\Order;
use App\Product;
use App\ProductNote;
use App\OrderStatusHistory;

class OrderProductController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function updateVendor(Request $request, OrderProduct $orderProduct)
    {

        $orderProduct->vendor = $request->vendor;
        $orderProduct->vendor_remark = $request->vendor_remark;
        $orderProduct->save();

        $orderProduct->order->touch();

        session()->flash('message',"You have successfully updated an item.");

        return back();
    }

    public function purchase(OrderProduct $order_product, Request $request)
    {
        // Validate Purchase and Hold
        $this->validate(request(), [
            'qty' => 'required|numeric|min:1',
            'unit' => 'required'
        ]);
        
        $order_product->isPurchased = 1;
        $order_product->save();

        //check what to change order status to
        if($order_product->orders()){
            foreach($order_product->orders() as $order){
                if($order->isAllPurchased()){
                    // Ordered
                    if($order->status_id != 2){
                        $status_history = OrderStatusHistory::create([
                            'order_id' => $order->id,
                            'employee_id' => Auth::user()->id,
                            'status_id' => 2,
                            'comment' => "[AUTO GENERATED] All products are ordered for this order."            
                        ]);
                    
                        $status_history->save();

                        $order->status_id = 2;
                        $order->save();
                    }
                }else{
                    // Proccessing
                    if(!($order->status_id == 2 || $order->status_id == 7)){
                        $status_history = OrderStatusHistory::create([
                            'order_id' => $order->id,
                            'employee_id' => Auth::user()->id,
                            'status_id' => 7,
                            'comment' => "[AUTO GENERATED] Order partially ordered."            
                        ]);

                        $status_history->save();
                        $order->status_id = 7;
                        $order->save();
                    }
                }
            }
        }
        
        // Create ordered note
        $note = new ProductNote();
        $note->type_id = 2;
        $note->product_id = $order_product->product_id;
        $note->name = $request->name;
        $note->qty = $request->qty;
        $note->unit = $request->unit;
        $note->comments = $request->comments;
        $note->employee_id = Auth::user()->id;

        $note->save();

        return redirect()->back();
    }

    public function purchaseAndShip(Request $request, OrderProduct $order_product)
    {
        $this->validate(request(), [
            'id' => 'required|integer|exists:leeselectronic.table_1,id_product',
            'qty' => 'required|numeric|min:1',
            'unit' => 'required'
        ]);

        if($product->orders){
            dd($product->orders);
        }

        $note = new ProductNote();
        $note->type_id = 2;
        $note->product_id = $request->id;
        $note->qty = $request->qty;
        $note->unit = $request->unit;
        $note->comments = $request->comments;
        $note->employee_id = Auth::user()->id;
        $note->isHandled = '1';
        $note->updated_at = NOW();
        $note->handledBy = Auth::user()->id;
        $note->handledDate = NOW();

        $note->save();
        
        $shipmentNote = new ProductNote();
        $shipmentNote->type_id = 9;
        $shipmentNote->product_id = $request->id;
        $shipmentNote->qty = $request->qty;
        $shipmentNote->unit = $request->unit;
        
        $shipmentNote->employee_id = Auth::user()->id;
        $shipmentNote->created_at = NOW();
        $shipmentNote->save();

        $cart = new ShipmentCart();
        $cart->shipment_id = $request->shipment;
        $cart->product_id = $request->id;
        $cart->item_name = $request->name;
        $cart->qty = $request->qty;
        $cart->unit = $request->unit;
        $cart->vendor = $request->vendor;
        $cart->vendor_remarks = $request->remarks;
        $cart->comments = $request->comments;
        $cart->invoice = $request->invoice;
        $cart->employee_id = Auth::user()->id;
        $cart->created_at = NOW();
        $cart->note_id = $shipmentNote->id;

        $cart->save();


        if($product->orderSuggestion){
            $product->orderSuggestion->delete();
        }

        // return redirect()->route('shipment.show');
        return redirect()->back();
    }



}
