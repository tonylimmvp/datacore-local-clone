<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipmentCart extends Model
{
    protected $table='shipment_carts';
    protected $connection='mysql';
    protected $guarded = [];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function shipment()
    {
        return $this->belongsTo(Shipment::class, 'shipment_id');
    }

    public function employee(){
        return $this->belongsTo(Employee::class);
    }

    public function notes(){
        return $this->hasMany(ProductNote::class, 'id', 'note_id');
    }

}
