<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Vendor;
use App\ProductVendor;
use App\PurchaseOrder;
use App\PurchaseOrderProduct;
use DB;

class ProductVendorController extends Controller
{
    public function updateVendor() {
        //update vendor for product
        $products = Product::all();

        foreach($products as $product){
            if($product->vendor){
                $vendor = Vendor::where('reference',$product->vendor)->first();
                if($vendor){
                    $product_vendor = ProductVendor::updateOrCreate(
                        [   
                            'product_id'=>$product->id_product,
                            'vendor_id'=> $vendor->id    
                        ],
                        [   
                            'recent_cost'=>$product->recent_cost,
                            'remark'=>$product->vendor_remarks
                        ]
                    );
                }
            }
        }

        session()->flash('message','You have successfully updated product vendors.');

        return back();
    }

    public function updateVendorPrice() {
        //update vendor for product
        $products = Product::all();

        foreach($products as $product){
            if($product->vendor){
                $vendor = Vendor::where('vendor_id',$product->vendor)->first();
                if($vendor){
                    $product_vendor = ProductVendor::updateOrCreate(
                        [   'product_id'=>$product->id_product,
                            'vendor_id'=> $vendor->id    
                        ],
                        [   'recent_cost'=>$product->recent_cost
                        ]
                    );
                }
            }
        }

        session()->flash('message','You have successfully updated product vendors.');
        
        return back();
    }

    // SELECT purchase_orders.po_id, vendor_id, product_id, name, unit_cost, received_at FROM `purchase_orders` INNER JOIN purchase_order_products ON purchase_order_products.po_id = purchase_orders.po_id WHERE product_id != 100 && product_id > 20 && received_at != "1980-01-01" && vendor_id != 1074 GROUP BY product_id, vendor_id ORDER BY `received_at` DESC

    public function updateRecentCost()
    {   
        $products = PurchaseOrder::join('purchase_order_products','purchase_orders.po_id','=','purchase_order_products.po_id')
                                    ->selectRaw('purchase_orders.po_id, vendor_id, product_id, name, unit_cost, received_at')
                                    ->whereRaw('product_id != 100 && product_id > 20 && received_at != "1980-01-01"')
                                    ->whereNotIn('vendor_id',['1074','1073','1023'])
                                    ->orderBy('received_at','ASC')
                                    ->get();

        foreach($products as $product){
            $product_vendor = ProductVendor::updateOrCreate(
                [   
                    'product_id'=>$product->product_id,
                    'vendor_id'=> $product->vendor_id    
                ],
                [   
                    'recent_cost'=>$product->unit_cost
                ]
            );
        }

        session()->flash('message','You have successfully updated product vendors.');
        
        return back();
        
    }

    public function updateAverageCost()
    {   

        // $products = DB::select('SELECT po_table.vendor_id, po_table.product_id, name, recent_cost, AVG(unit_cost) as avg_cost
        //                         FROM (SELECT purchase_orders.po_id, vendor_id, product_id, name, unit_cost,received_at 
        //                         FROM `purchase_orders` 
        //                         INNER JOIN purchase_order_products ON purchase_order_products.po_id = purchase_orders.po_id 
        //                         WHERE product_id != 100 && product_id > 20 && received_at != "1980-01-01" && vendor_id NOT IN (1074,1073,1023)  
        //                         ORDER BY `received_at` DESC) as po_table
        //                         INNER JOIN product_vendors ON po_table.vendor_id = product_vendors.vendor_id && po_table.product_id = product_vendors.product_id
        //                         GROUP BY vendor_id, product_id');

        $products = DB::select('SELECT purchase_orders.po_id, vendor_id, product_id, name, unit_cost,AVG(unit_cost) as avg_cost, received_at 
                                FROM `purchase_orders` 
                                INNER JOIN purchase_order_products ON purchase_order_products.po_id = purchase_orders.po_id 
                                WHERE product_id != 100 && product_id > 20 && received_at != "1980-01-01" && vendor_id NOT IN (1074,1073,1023)
                                GROUP BY vendor_id, product_id');



        foreach($products as $product){
            $product_vendor = ProductVendor::updateOrCreate(
                [   
                    'product_id'=>$product->product_id,
                    'vendor_id'=> $product->vendor_id    
                ],
                [   
                    'avg_cost'=>$product->avg_cost
                ]
            );
        }

        session()->flash('message','You have successfully updated product vendors');
        
        return back();
        
    }

    public function updateRemark(Request $request) {

        if(!$request->pid||!$request->vendor_id){
            return json_encode(['status'=>'failed','message'=>'Missing Parameter.']);
        }
        
        $product_vendor = ProductVendor::where('product_id',$request->pid)->where('vendor_id',$request->vendor_id)->first();

        if($product_vendor){
            $product_vendor->remark = $request->remark;
            $product_vendor->save();

            return json_encode(['status'=>'success','message'=>'You have successfully updated the remark.']);
        }else{
            return json_encode(['status'=>'failed','message'=>'Product Vendor does not exist.']);
        }
        
    }
}
