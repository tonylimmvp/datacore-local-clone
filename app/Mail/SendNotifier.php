<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifier;

class SendNotifier extends Mailable
{
    use Queueable, SerializesModels;

    public $notifier;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Notifier $notifier)
    {
        $this->notifier = $notifier;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.send-notifier')
                    ->from('no_reply@leeselectronic.com','Lee\'s Electronic Components')
                    ->replyTo('support@leeselectronic.com')
                    ->subject('[Lee\'s Electronic Components] The product you were interested in is now available.');
    }
}
