<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRmaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rma_status', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('color');
            $table->integer('position');
        });

        Schema::create('rmas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('phone');
            $table->string('email');
            $table->integer('id_product');
            $table->string('product_name');
            $table->integer('qty');
            $table->unsignedInteger('employee_id');
            $table->unsignedInteger('status_id')->default('1');
            $table->string('invoice_num', 8);
            $table->string('invoice_date');
            $table->string('tracking_num', 50)->nullable();
            $table->text('comments', 255)->nullable();
            $table->boolean('isActive')->default(TRUE);
            $table->unsignedInteger('vendor_id')->nullable();
            $table->timestamps();

            $table->foreign('status_id')->references('id')->on('rma_status');
            $table->foreign('employee_id')->references('id')->on('employees');

        });

        Schema::create('rma_product', function (Blueprint $table) {
            $table->unsignedInteger('rma_id');
            $table->integer('product_id');
            $table->string('name');
            $table->decimal('price', 6, 2);
            $table->integer('qty');

            $table->foreign('rma_id')->references('id')->on('rmas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rma_product');
        Schema::dropIfExists('rmas');
        Schema::dropIfExists('rma_status');
    }
}
