<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Quotation;

class QuotationCreateEmployee extends Mailable
{
    use Queueable, SerializesModels;

    public $quotation;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Quotation $quotation)
    {
        $this->quotation = $quotation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.employees.quotation-create')
                    ->from('no_reply@leeselectronic.com','Lee\'s Electronic Components')
                    ->replyTo('support@leeselectronic.com')
                    ->subject("[Lee's Electronic Components] New Quotation #".$this->quotation->id);;
    }
}
