@component('mail::message')

@if($order->type == "backorder")
#The status of a backorder has been changed.

@elseif($order->type == "phone")
#The status of a phone order has been changed.

@elseif($order->type == "special")
#The status of a special order has been changed.
@endif

Order #{{$order->id}} status have been changed to:

<p style="text-align: center;
        font-size:30px;
        background-color:{{$order->currentStatus->color}};
        border-radius: 25px;
        color:white;
        box-shadow: 5px 5px 20px #6f6f6f;"> 
        {{$order->currentStatus->name}} 
    </p>

**Customer:** {{$order->name}}<br>
**Phone:** {{$order->phone}}<br>
**Email:** {{$order->email}}<br>
**Invoice:** {{$order->invoice_id}}

@component('mail::table')
| PID        | Name           | Price per Unit  | Quantity | 
| ---------- |:--------------:| :--------------:| :------: |
@foreach($order->products as $product)
| {{$product->id_product}} | {{$product->pivot->name}} | ${{$product->pivot->price}} | {{$product->pivot->quantity}} |
@endforeach
@endcomponent

@component('mail::button', ['url' => route('orders.detail',$order->id)])
View Order
@endcomponent

@endcomponent
