<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\OrderStatusHistory;
use App\Order;
use App\OnlineOrder;
use App\MobileOrderStatusHistory;
use App\MobileOrder;
use DB;
use Auth;

class SignatureController extends Controller
{

/************************
 * 
 * This saves online signatures
 * 
 * This should save the signature and give a refresh on the page.
 * If fail, it should give a JSON failure.
 * 
 *  */ 

    public function saveOnlineSignature(Request $request){
        $savedMessage = $this->save($request);

        if($savedMessage == "saved"){
            // if saved, we want to update order status to DONE
            // we first want to update order status history

            DB::connection('leeselectronic')->table('ps_order_history')->insert(
                [   
                    'id_employee' => Auth::user()->ps_user_id, 
                    'id_order' => $request->signature_order_id,
                    'id_order_state' => 17,
                    'date_add' => now(),
                ]
            );

            // Now update order to picked up
            $order = OnlineOrder::find($request->signature_order_id);

            if(!$order){
                return json_encode(["status"=>"failed","message"=>"Signature was not saved. Order not found."]);
            }

            $order->current_state = 17;
            $order->picked_up_by = $request->signature_pickup;
            $order->save();

            session()->flash('message','You have successfully updated the orders status.');

            // Grab url
            $url = route('orders.online',$order->id_order);

            return json_encode(["status"=>"success","url"=>"$url"]);

        }

        return json_encode(["status"=>"failed","message"=>"Signature was not saved. " . $savedMessage]);
    }

/************************
 * 
 * This saves DataCore Order signatures
 * 
 * This should save the signature and redirect to the changed order
 * If fail, it should give a JSON failure.
 * 
 *  */ 

    public function saveOrderSignature(Request $request){
        $savedMessage = $this->save($request);

        if(!isset($request->signature_pickup)){
            return json_encode(["status"=>"failed","message"=>"Signature was not saved. Pickup person was not specified."]);
        }

        if($savedMessage == "saved"){
            // if saved, we want to update order status to DONE
            // we first want to update order status history

            $status_history = OrderStatusHistory::create([
                'order_id' => $request->signature_order_id,
                'employee_id' => Auth::user()->id,
                'status_id' => 4,
                'comment' => "Order has been picked up by " . $request->signature_pickup . " and signed."           
            ]);
        
            $status_history->save();

            // Once status history created, we want to change order status as well

            $order = Order::find($request->signature_order_id);

            if(!$order){
                return json_encode(["status"=>"failed","message"=>"Signature was not saved. Order not found."]);
            }

            $order->status_id = 4; 
            $order->picked_up_by = $request->signature_pickup;

            $order->save();
            
            session()->flash('message','You have successfully updated the orders status.');

            return json_encode(["status"=>"success","url"=>route('orders.detail',$request->signature_order_id)]);

        }

        return json_encode(["status"=>"failed","message"=>"Signature was not saved. " . $savedMessage]);
        
    }

/************************
 * 
 * This saves Mobile Application Order signatures
 * 
 * This should save the signature and redirect to the changed order
 * If fail, it should give a JSON failure.
 * 
 *  */ 

public function saveMobileSignature(Request $request){
    $savedMessage = $this->save($request);

    if(!isset($request->signature_pickup)){
        return json_encode(["status"=>"failed","message"=>"Signature was not saved. Pickup person was not specified."]);
    }

    if($savedMessage == "saved"){
        // if saved, we want to update order status to DONE
        // we first want to update order status history

        $status_history = MobileOrderStatusHistory::create([
            'order_id' => $request->signature_order_id,
            'employee_id' => Auth::user()->id,
            'state_id' => 3,
            'date_stamp' => \Carbon\Carbon::now()->format('Y-m-d h:iA')
        ]);
    
        $status_history->save();

        // Once status history created, we want to change order status as well

        $order = MobileOrder::find($request->signature_order_id);

        if(!$order){
            return json_encode(["status"=>"failed","message"=>"Signature was not saved. Order not found."]);
        }

        $order->current_state = 3; 
        $order->picked_up_by = $request->signature_pickup;

        $order->save();
        
        session()->flash('message','You have successfully updated the orders status.');

        return json_encode(["status"=>"success","url"=>route('orders.mobile',$request->signature_order_id)]);

    }

    return json_encode(["status"=>"failed","message"=>"Signature was not saved. " . $savedMessage]);
    
}

/************************
 * 
 * This is a refactered function
 * 
 *  */ 

    function save(Request $request){

        if(!isset($request->signature_data)){
            return "Signature missing.";
        }

        if(!isset($request->signature_type)){
            return "Signature missing type.";
        }

        if(!isset($request->signature_order_id)){
            return "Signature missing order.";
        }

        $encoded_image = explode(",", $request->signature_data)[1];
        $decoded_image = base64_decode($encoded_image);

        $img_path = '/invoices/' . $request->signature_type . $request->signature_order_id;
        
        // If signature exist, then save a copy in case someone made a mistake
        if(Storage::disk('public')->exists($img_path . '.jpg')){
            Storage::disk('public')->put($img_path . '-old.jpg', Storage::disk('public')->get($img_path . '.jpg'));
        }

        // Store signature
        Storage::disk('public')->put($img_path . '.jpg', $decoded_image);

        return "saved";
    }

}
