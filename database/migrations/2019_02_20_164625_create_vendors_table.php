<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reference')->unique();
            $table->string('name');
            $table->string('contact')->nullable();
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('province')->nullable();
            $table->string('postal')->nullable();
            $table->string('country')->nullable();
            $table->string('phone')->nullable();
            $table->string('ext')->nullable();
            $table->string('fax')->nullable();
            $table->string('cell')->nullable();
            $table->string('email')->nullable();
            $table->string('url')->nullable();
            $table->text('comment')->nullable();
            $table->boolean('isDisabled')->default(true);
            $table->date('created_at');
        });

        DB::statement("ALTER TABLE vendors AUTO_INCREMENT = 1000;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors');
    }
}
