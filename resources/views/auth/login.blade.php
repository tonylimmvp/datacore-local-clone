<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Lee's Electronic Components Login</title>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        {{-- Fonts --}}
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        {{-- Styles --}}
        {{-- <link href="{{ asset('css/compiled/app.css') }}" rel="stylesheet"> --}}
        <link href="{{ asset('css/compiled/login.css') }}" rel="stylesheet">
        {{-- <link href="{{ asset('css/compiled/navtop.css') }}" rel="stylesheet"> --}}

        <style>
        html, body{
                background-image: url({{asset("/img/login-background.jpg")}}) 
        }
        </style>
    </head>
    <body>

    {{-- Bootstrap Login Form --}}
    <div class="flex-center position-ref full-height">

        <div class="content">
            {{-- Signin Form --}}
            <form class="form-signin" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}

                {{-- E-mail address box --}}
                <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                    <label for="username" class="sr-only">Username</label>
                    <input id="inputUsername" type="string" class="form-control" name="username" value="{{ old('username') }}" placeholder="Username/Email" required autofocus>

                        @if ($errors->has('username'))
                            <span class="help-block">
                                <strong>{{ $errors->first('username') }}</strong>
                            </span>
                        @endif
                </div>

                {{-- Password Box --}}
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="sr-only">Password</label>
                        <input id="inputPassword" type="password" class="form-control" name="password" placeholder="Password" required>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                </div>

                {{-- Remember me tick box --}}
                <div class="checkbox mb-3">
                    <label>
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                    </label>
                </div>

                {{-- include previous url --}}
                <input type="hidden" name="previous" value="{{ URL::previous() }}">

                <div class="signinButton">
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                </div>
            </form>

            @include('layouts.errors')

            {{-- Login Form Ends here --}}

            <p class="mt-5 mb-3">&copy; Lee's Electronic Components {{\Carbon\Carbon::now()->year}}. All rights reserved.</p>
        </div>
    </div>

    {{-- Bootstrap JS Scripts --}}
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
</body>
</html>
