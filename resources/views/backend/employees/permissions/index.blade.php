@extends('backend.layouts.main')

@include('layouts.datatables')

@section('content')

<h1> {{ $employee->name }}'s Permissions</h1>

<form action="{{ route('coreui.employee-permission.update',$employee->id) }}" method="POST">

        {{ csrf_field() }}
        {{ method_field('PATCH') }}

<table id="permissions-table" class="table" width="100%">
    <thead>
        <th>Name</th>
        <th class="text-center">Create</th>
        <th class="text-center">Read</th>
        <th class="text-center">Update</th>
        <th class="text-center">Delete</th>
        <th class="text-center">Select All</th>
    </thead>
    <tbody>
        @foreach($employee->permissions as $permission)
        <tr>
            <td><input name="name[]" type="hidden" value="{{$permission->name}}">{{ $permission->name}}</td>
            <td class="text-center"><input name="create[]" type="checkbox" value="{{$permission->id}}" @if($permission->pivot->create) checked @endif></td>
            <td class="text-center"><input name="read[]" type="checkbox" value="{{$permission->id}}" @if($permission->pivot->read) checked @endif></td>
            <td class="text-center"><input name="update[]" type="checkbox" value="{{$permission->id}}" @if($permission->pivot->update) checked @endif></td>
            <td class="text-center"><input name="delete[]" type="checkbox" value="{{$permission->id}}" @if($permission->pivot->delete)  checked @endif></td>
            <td class="text-center"><input id="select-all" type="checkbox" onclick="selectAll(this)"></td> 
        </tr>
        @endforeach
    </tbody>
</table>

<button class="btn btn-primary mt-4">Update Permissions</button>

</form>

@endsection

@push('bottom-scripts')

<script type="text/javascript">
    
        $('#permissions-table').DataTable(
            {
                scrollY:        "60vh",
                scrollX:        true,
                scrollCollapse: true,
                paging:         false,
                searching:      false,
                "displayLength": 25,
                "language": {
                    "search": "Filter records:"
                }
            }
        );
            
        function selectAll(source){
            var tr = $(source).closest('tr');
            if(source.checked) {   
                tr.find("input:checkbox").each(function() {
                    this.checked = true;                        
                });
            } else {
                tr.find("input:checkbox").each(function() {
                    this.checked = false;                       
                });
            }
        }
 
</script>
@endpush