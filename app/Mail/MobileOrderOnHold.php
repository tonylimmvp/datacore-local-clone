<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\MobileOrder;

class MobileOrderOnHold extends Mailable
{
    use Queueable, SerializesModels;

    public $order;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(MobileOrder $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.mobile.status.onhold')
                    ->from('no_reply@leeselectronic.com','Lee\'s Electronic Components')
                    ->replyTo('support@leeselectronic.com')
                    ->subject("[Lee's App Order] ".$this->order->reference." is on hold.");
    }
}
