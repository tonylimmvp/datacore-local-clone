<?php

namespace App\Http\Controllers;

use App\RestockProduct;
use Illuminate\Http\Request;
use App\Product;

class RestockProductController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index()
    {
        $restockList = RestockProduct::orderBy('created_at','asc')->get();
        return view('frontend.restock.index',compact('restockList'));
    }

    public function store(Request $request)
    {
        $rules = [
            'pid' => 'required|exists:leeselectronic.table_1,id_product|unique:restock_products,product_id',
        ];

        $customMessages = [
            'pid.required' => "The pid is required.",
            'pid.unique' => "PID#". $request->pid ." already exist on the restock board.",
            'pid.exists' => "The pid does not exist."
         ];

        $this->validate($request, $rules, $customMessages);
        
        $product = new RestockProduct();
        $product->product_id = $request->pid;

        if(Product::find($request->pid)->quantity <= 0){
            session()->flash('error','No quantity available for restock  (PID#' . $request->pid . ")");
            return back();
        }

        $product->save();

        session()->flash('message','You have successfully added item to the restock board.');

        return back();
    }

    public function destroy(RestockProduct $product)
    {
        $product->delete();
        session()->flash('message','You have successfully deleted ' . $product->info->name .'.');
        return back();
    }

    /********************************************************************************************************
     * 
     *      API
     * 
     */

     public function addToBoard(Request $request) {

        if(!$request->pid){
            return json_encode(['status'=>'failed','message' => 'No product ID found.']);
        }
        $check = Product::find($request->pid);

        if(!$check){
            return json_encode(['status'=>'failed','message' => 'Product ID does not exist.']);
        }

        $product = RestockProduct::firstOrCreate(['product_id'=>$request->pid]);
        $product->touch();

        return json_encode(['status'=>'success']);
     }

     public function removeFromBoard(Request $request) {

        if(!$request->pid){
            return json_encode(['status'=>'failed','message' => 'No product ID found.']);
        }
        $product = RestockProduct::find($request->pid);

        if(!$product){
            return json_encode(['status'=>'success','message' => 'Product not in restock board.']);
        }

        $product->delete();

        return json_encode(['status'=>'success','message' => 'Product successfully removed from restock board.']);
     }

}
