<?php

namespace App;

use DB;

class Issue
{
    public static function checkCategory(){
        $products = ProductOnline::where('id_category_default','2')
                        ->where('active',1)->get();

        $array = [];
        // dd($products);
        foreach($products as $product){
            if(sizeof($product->categories)>1){
                array_push($array,$product);
            }
        }
        
        return $array;

    }

    public static function checkEmptyCategory(){
        $products = ProductOnline::where('id_category_default','2')
                        ->where('active',1)->get();
        
        return $products;

    }

}