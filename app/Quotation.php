<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quotation extends Model
{
    protected $connection = "mysql";

    /*************************************
    *
    *       Eloquent Relations
    *
    **************************************/
    
    public function products(){
        $database = $this->getConnection()->getDatabaseName();
        return $this->belongsToMany(Product::class, "$database.quotation_product",'quotation_id','product_id')->withPivot('id','name','price','quantity','vendor','vendor_remark');
    }

    public function employee(){
        return $this->belongsTo(Employee::class);
    }

    /*************************************
     * 
     *      Functions
     *  
     **************************************/

    public static function checkExpired(){

        $expired_quotes = static::whereRaw("isExpired = 0 && DATE(created_at) < DATE_SUB(CURDATE(), INTERVAL 7 DAY)");

        if($expired_quotes->first()){
            foreach($expired_quotes->get() as $quote){
                $quote->isExpired = true;
                $quote->save();
            }
        }
    }

    // Filter
    public function scopeFilter($query, $filters){
        if(isset($filters['expired'])){
            $isExpired = $filters['expired'];
            $query->where('isExpired',$isExpired);
        }
    }
}
