<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipment extends Model
{
    protected $table='shipments';
    protected $connection='mysql';

    protected $guarded = [];
    
    public function employee(){
        return $this->belongsTo(Employee::class, 'employee_id');
    }

    public function type(){
        return $this->hasOne(ShipmentType::class,'id', 'type_id');
    }

    public function courier(){
        return $this->hasOne(Courier::class,'id', 'courier_id');
    }

    public function status(){
        return $this->belongsTo(ShipmentStatus::class, 'status_id');
    }

    public function history(){
        return $this->hasMany(ShipmentHistory::class);
    }

    public function cart(){
        return $this->hasMany(ShipmentCart::class);
    }

    public function vendor(){
        return $this->belongsTo(Vendor::class, 'vendor_id');
    }
}