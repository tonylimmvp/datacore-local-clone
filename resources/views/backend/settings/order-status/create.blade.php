@extends('backend.layouts.main')

@push('css')
    <link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/3.0.0-beta.3/css/bootstrap-colorpicker.min.css" rel="stylesheet">
@endpush

@section('content')

<h1 class="mt-2">Add New Order Status</h1>
<form method="POST" action="{{route('coreui.order-status.store')}}" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" id="name" name="name" placeholder="Enter Status Name" >
    </div>

    <div class="form-group">
        <label for="color">Color</label>
        <input type="text" class="form-control" id="color" name="color" placeholder="Enter Status Background Color" >

    </div>

<div class="form-group">
    <label for="position">Position</label>
    <input type="number" class="form-control" id="position" name="position" placeholder="Enter Position" >
</div>

<button type="submit" class="btn btn-primary">Submit</button>

@include('layouts.errors')

</form>

@endsection


@push('bottom-scripts')
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/3.0.0-beta.3/js/bootstrap-colorpicker.min.js"></script>
    <script type="text/javascript">
        $('#color').colorpicker({format:'hex'});
    </script>
@endpush