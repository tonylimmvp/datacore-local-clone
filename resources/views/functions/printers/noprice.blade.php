{{-- Setting up Variables --}}
@php

$SETTING_DEBUG_STOP_PRINT = 0;
$ALLOW_ACCESS = 0;
$printer_ip = "173.180.12.45";
$ITEM_NUMBER_OF_LABELS = 0;
$printer_port = 9101; // default is downstairs godex
$printerNumber = 2; //default printer is downstairs godex

@endphp

{{-- Validation - Check if ip is allowed to print --}}
@php
$accepted_ips = App\StoreIP::all();
foreach($accepted_ips as $ip){
    if($_SERVER['REMOTE_ADDR'] == $ip->ip_address){
        $ALLOW_ACCESS = 1;

        $printer_ip = $ip->ip_address;
    }
}

 if($_SERVER['REMOTE_ADDR'] == "127.0.0.1"){
     $ALLOW_ACCESS = 1;
    $printer_ip = "173.180.12.45";
}

@endphp


{{-- PHP FUNCTIONS --}}
@php

function customWordWarp($longstring, $maxLineLength){
    global $SETTING_DEBUG_STOP_PRINT;
	//echo "entered word warp";
	//echo $longstring;
	$arrayWords = explode(" ", $longstring);
	//echo $arrayWords;
	
    if($SETTING_DEBUG_STOP_PRINT)
    {print_r($arrayWords);}

	// Max size of each line
	//$maxLineLength = 18;

	// Auxiliar counters, foreach will use them
	$currentLength = 0;
	$index = 0;
	$arrayOutput = array();//initilize output array

	foreach($arrayWords as $word)
	{
		
		//echo "words: " . $word . "<br />";
		// +1 because the word will receive back the space in the end that it loses in explode()
		$wordLength = strlen($word) + 1;

		if( ( $currentLength + $wordLength ) <= $maxLineLength )
		{
            if(isset($arrayOutput[$index])){
				$arrayOutput[$index] .= $word . ' ';
			}else{
				$arrayOutput[$index] = $word . ' ';
			}

			$currentLength += $wordLength;
		}
		else
		{
			$index += 1;

			$currentLength = $wordLength;

			$arrayOutput[$index] = $word . ' ';
		}
	}
	
	return $arrayOutput;
}
@endphp

{{-- Print Command --}}
@php
if($ALLOW_ACCESS == 1){
    // validate pid first
	if(isset($_GET['pid']) && isset($_GET['printer'])){
        $ITEM_ID = $_GET['pid'];

        $product = App\Product::find($ITEM_ID);

        /**************************************************************
        *
        *  Validate PID
        *
        ***************************************************************/

            if($product){
            
            /**************************************************************
            *
            *  Validate Printer Number
            *
            ***************************************************************/

            if(is_numeric($_GET['printer'])){
                $printerNumber = $_GET['printer'];
            }

            $printer = App\LabelPrinter::find($printerNumber);
            
            if($printer || $printer->isActive){

                $printer_port = $printer->port;
            
                /**************************************************************
                *
                *  Validate Number of labels
                *
                ***************************************************************/
                if(isset($_GET['num'])){
                    if(is_numeric($_GET['num'])){
                        $ITEM_NUMBER_OF_LABELS = $_GET['num'];
                    }
                }

                /**************************************************************
                *
                *  Validate Date
                *
                ***************************************************************/
                
                $DATE = "";
                if(isset($_GET['date'])){
                    $date_is_set = $_GET['date'];
                    if($date_is_set == 'yes'){
                        $DATE = date("Wy");
                    }else{
                        $DATE = "";
                    }
                }

                /**************************************************************
                *
                *  Declaring Variables
                *
                ***************************************************************/
                $ITEM_NAME = $product->name;
                echo $ITEM_NAME . "<br />";
                $ITEM_PRICE = $product->price;
                $ITEM_LOC = $product->fraser_loc;

                $desc_array = customWordWarp($product->name, 20);
                $loc_array = customWordWarp($product->fraser_loc, 11);

                echo "<br />";
                print_r($desc_array);

                echo "<br /><br />";
                
                echo "NUMBER OF LABELS: ".$ITEM_NUMBER_OF_LABELS."<br />";

                if($SETTING_DEBUG_STOP_PRINT==0){

                /**************************************************************
                *
                *  Start Print Settings by opening socket
                *
                ***************************************************************/

                    error_reporting(E_ALL);

                    echo "<h2>TCP/IP Connection</h2>\n";

                    /* Get the port for the WWW service. */
                    $service_port = getservbyname('www', 'tcp');

                    /* Get the IP address for the target host. */
                    $address = gethostbyname($printer_ip);

                    /* Create a TCP/IP socket. */
                    $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
                    if ($socket === false) {
                        echo "socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n";
                    } else {
                        echo "OK.\n";
                    }

                    //echo "Attempting to connect to '$address' on port '$service_port'...";
                    echo "Attempting to connect ...";
                    $result = socket_connect($socket, $address, $printer_port);
                    if ($result === false) {
                        echo "socket_connect() failed.\nReason: ($result) " . socket_strerror(socket_last_error($socket)) . "\n";
                    } else {
                        echo "OK.\n";
                    }
                    

                /**************************************************************
                *
                *  Godex / Small Label Print Setting
                *
                ***************************************************************/
                    
                    if($printer->type == "godex" && $printer->size == "small"){
                        //$in = "HEAD / HTTP/1.1\r\n";
                        //$in .= "Host: www.example.com\r\n";
                        //$in .= "Connection: Close\r\n\r\n";

                        $in = "^Q25,3,0\r\n";
                        $in .= "^W31\r\n";
                        $in .= "^H10\r\n";
                        $in .= "^P1\r\n";
                        $in .= "^S4\r\n";
                        $in .= "^AD\r\n";
                        
                        if($ITEM_NUMBER_OF_LABELS > 0){
                            if($ITEM_NUMBER_OF_LABELS>20){
                                $ITEM_NUMBER_OF_LABELS = 20;
                            }
                            $in .= "^C".$ITEM_NUMBER_OF_LABELS."\r\n";//number of labels
                        }else{
                            $in .= "^C1\r\n";//number of labels
                        }

                        $in .= "^R0\r\n";
                        $in .= "~Q+0\r\n";
                        $in .= "^O0\r\n";
                        $in .= "^D0\r\n";
                        $in .= "^E12\r\n";
                        $in .= "~R200\r\n";
                        $in .= "^L\r\n";
                        $in .= "Dy2-me-dd\r\n";
                        $in .= "Th:m:s\r\n";

                        // name of product

                        if(isset($desc_array[0])){
                            $in .= "AZ1,20,8,1,1,0,0,". $desc_array[0] ."\r\n";
                        }
                        if(isset($desc_array[1])){
                            $in .= "AZ1,20,28,1,1,0,0,". $desc_array[1] ."\r\n";
                        }
                        if(isset($desc_array[2])){
                            $in .= "AZ1,20,48,1,1,0,0,". $desc_array[2] ."\r\n";
                        }
                        if(isset($desc_array[3])){
                            $in .= "AZ1,20,68,1,1,0,0,". $desc_array[3] ."\r\n";
                        }

                        // Barcode
                        $in .= "AB,20,135,1,1,0,0,PID: ". $ITEM_ID ."\r\n";
                        $in .= "BQ,20,97,2,7,35,0,0,". $ITEM_ID ."\r\n";
                        

                        if($DATE != ""){
                            //$in .= "AZ1,12,158,1,1,0,0,$DATE\r\n";
                            $in .= "AA,221,118,1,1,0,3,$DATE\r\n";
                        }
                        $in .= "E\r\n";
                    }
                    
                /**************************************************************
                *
                *  Zebra Big Label Setting
                *
                ***************************************************************/
                    
                    else if ($printer->type == "zebra" && $printer->size == "large"){
                        //////////////
                        //basic command list
                        // ^FX is comment
                        // ^FO is placement of text  
                        // ^FD is data
                        // ^FS seperator
                        // 
                        
                        $in = "^XA\r\n";
                        
                        if($ITEM_NUMBER_OF_LABELS > 0){
                            if($ITEM_NUMBER_OF_LABELS>5){
                                $ITEM_NUMBER_OF_LABELS = 5;
                            }
                            $in .= "^PQ".$ITEM_NUMBER_OF_LABELS."\r\n";//number of labels
                        }else{
                            $in .= "^PQ1\r\n";//number of labels
                        }
                        

                        $in .= "^FX size: 5.5cm 6.3cm\r\n";
                        $in .= "^FO100,25\r\n";
                        $in .= "^CFA,75\r\n";
                        $in .= "^FD#".$ITEM_ID."\r\n";
                        $in .= "^FS\r\n";
                        
                        // name of product

                        $in .= "^FX Description\r\n";
                        $in .= "^FO100,110\r\n";
                        $in .= "^CFA,30\r\n";
                        
                        if(isset($desc_array[0])){
                            $in .= "^FD".$desc_array[0]."^FS\r\n";
                        }
                        if(isset($desc_array[1])){
                            $in .= "^FO100,140^FD".$desc_array[1]."^FS\r\n";
                        }
                        if(isset($desc_array[2])){
                            $in .= "^FO100,170^FD".$desc_array[2]."^FS\r\n";
                        }
                        if(isset($desc_array[3])){
                            $in .= "^FO100,200^FD".$desc_array[3]."^FS\r\n";
                        }
                        // $in .= "^FO100,230^FD^FS\r\n";

                        $in .= "^FS\r\n";
                        
                        $in .= "^FX divider line\r\n";
                        $in .= "^FO100,370^GB420,1,4,^FS\r\n";
                        
                        $in .= "^FX BARCODE\r\n";
                        $in .= "^BY5,2,270\r\n";
                        $in .= "^FO100,380^BY2^BCN,65,Y,N,N^FD".$ITEM_ID."^FS\r\n";
                        
                        $in .= "^FX QTY\r\n";
					
					

                        $in .= "^CFA,20\r\n";
                        if(isset($loc_array[0])){
                            $in .= "^FO340,380^FDLOC: ".$loc_array[0]."^FS\r\n";
                        }
                        if(isset($loc_array[1])){
                            $in .= "^FO340,410^FD".$loc_array[1]."^FS\r\n";
                        }
                        if(isset($loc_array[2])){
                            $in .= "^FO340,440^FD".$loc_array[2]."^FS\r\n";
                        }
                        
                        $in .= "^FX date\r\n";
                        $in .= "^CFA,15\r\n";
                        $in .= "^FO290,480^FD".$DATE."^FS\r\n";
                        $in .= "^XZ\r\n";
                        
                    }
                    

                /**************************************************************
                *
                *  Zebra Small Label Setting
                *
                ***************************************************************/

                    else if($printer->type == "zebra" && $printer->size == "small"){

                        //////////////
                        //basic command list
                        // ^FX is comment
                        // ^FO is placement of text  
                        // ^FD is data
                        // ^FS seperator
                        // 

                        $in = "^XA\r\n";
                        
                        
                        $in .= "^PQ1\r\n";//number of labels
                        

                        $in .= "^FX Description\r\n";

                        $in .= "^CF0,20\r\n";
                        
                        if(isset($desc_array[0])){
                            $in .= "^FO300,20^FD".$desc_array[0]."^FS\r\n";
                        }
                        if(isset($desc_array[1])){
                            $in .= "^FO300,40^FD".$desc_array[1]."^FS\r\n";
                        }
                        if(isset($desc_array[2])){
                            $in .= "^FO300,60^FD".$desc_array[2]."^FS\r\n";
                        }
                        if(isset($desc_array[3])){
                            $in .= "^FO300,80^FD".$desc_array[3]."^FS\r\n";
                        }

                        $in .= "^FS\r\n";                                

                        $in .= "^FX BARCODE\r\n";
                        $in .= "^BY5,2,270\r\n";
                        $in .= "^FO300,100\r\n";
                        $in .= "^BY2^BCN,30,N,N,N\r\n";
                        $in .= "^FD".$ITEM_ID."^FS\r\n";

                        $in .= "^FX PID\r\n";
                        $in .= "^FO300,140\r\n";
                        $in .= "^CFA,18\r\n";
                        $in .= "^FDPID#".$ITEM_ID."\r\n";
                        $in .= "^FS\r\n";

                        $in .= "^FX date\r\n";
                        $in .= "^CFA,14\r\n";
                        $in .= "^FO510,100\r\n";
                        $in .= "^A0B,18,18\r\n";
                        $in .= "^FD".$DATE."\r\n";
                        $in .= "^FS\r\n";
                        $in .= "^XZ\r\n";
                        
                    }

                    // End of printing options
                    

                /**************************************************************
                *
                *  Close Socket
                *
                ***************************************************************/

                    echo "<br/>input:";
                    echo "<br/>$in<br/>";

                    $out = '';

                    echo "Sending HTTP HEAD request...";
                    socket_write($socket, $in, strlen($in));
                    echo "OK.\n";

                    // echo "Reading response:\n\n";
                    // while ($out = socket_read($socket, 2048)) {
                    // 	echo $out;
                    // }

                    echo "Closing socket...<br>";
                    socket_close($socket);
                    echo "OK.\n\n";
                }
                // End of DEBUG = 0 
            }else{
                echo "Error: Could not find printer or printer is inactive <br> ";
            }
            
        }else{//no such pid
            echo "Error: Could not find such product<br />";
        }

    }else{
        echo "Error: Could not find printer or product.<br />";
    }

}else{
// no access
echo $_SERVER['REMOTE_ADDR'] . "<br>";
echo "Error: Invalid location";	
}

@endphp
