<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderNotesPivotTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('online_notes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->nullable()->unsigned();
            $table->integer('note_id')->nullable()->unsigned();
        });

        Schema::create('order_notes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->nullable()->unsigned();
            $table->integer('note_id')->nullable()->unsigned();
        });

        Schema::create('mobile_notes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->nullable()->unsigned();
            $table->integer('note_id')->nullable()->unsigned();
        });

        Schema::table('online_notes', function($table) {
            $table->foreign('order_id')->references('id_order')->on('leeselectronic.ps_orders');
            $table->foreign('note_id')->references('id')->on('notes');
        });

        Schema::table('order_notes', function($table) {
            $table->foreign('order_id')->references('id')->on('orders');
            $table->foreign('note_id')->references('id')->on('notes');
        });

        Schema::table('mobile_notes', function($table) {
            $table->foreign('note_id')->references('id')->on('notes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mobile_notes');
        Schema::dropIfExists('order_notes');
        Schema::dropIfExists('online_notes');
    }
}