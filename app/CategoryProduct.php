<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryProduct extends Model
{
    protected $connection = "leeselectronic";
    protected $table = "ps_category_product";  
    protected $primaryKey = ['id_category', 'id_product'];

    protected $guarded = [];

    public $incrementing = false;
    public $timestamps = false;
}
