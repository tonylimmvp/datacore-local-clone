@extends('frontend.layouts.main')

@include('layouts.datatables')

@push('top-scripts')
<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
@endpush

@push('css')
<style>
    h1 {
        text-align: center;
        font-family: 'Nunito', sans-serif;
        font-weight: 200;
    }

    h2 {
        font-family: 'Nunito', sans-serif;
        font-weight: 200; 
        text-align: center;
    }
    
    .product-image-thumbnail {
        display: block;
        width: 100px;
        height: auto;
    }

    td img {
        display: block;
        margin-left: auto;
        margin-right: auto;
    }
</style>
@endpush

@section('content')

    <br/>
    <h1>{{$title}}</h1>
    <hr/>

    <div class="table-responsive">
        <table id="mainTable" class="table table-hover table-dark table-bordered" width="100%">
            <thead>
                <tr>
                    <th scope="col"><span data-feather="hash"></span> Shipment ID</th>
                    <th scope="col"><span data-feather="calendar"></span> Departure Date</th>
                    <th scope="col"><span data-feather="calendar"></span> Estimated Arrival Date</th>
                    <th scope="col"><span data-feather="calendar"></span> Actual Arrival Date</th>
                    <th scope="col"><span data-feather="activity"></span> Status</th>
                    <th scope="col"><span data-feather="user-check"></span> Employee</th>
                    <th scope="col"><span data-feather="globe"></span> Type</th>
                    <th scope="col"><span data-feather="map"></span> Local?</th>
                    @auth 
                        @if(Auth::user()->hasAccess('vendors')->read)
                            <th scope="col"><span data-feather="shopping-cart"></span> Vendor</th>
                        @endif
                    @endauth
                    <th scope="col"><span data-feather="hash"></span> PO#</th>
                    <th scope="col"><span data-feather="package"></span> Shipped?</th>
                    <th scope="col"><span data-feather="info"></span> Active?</th>
                    <th scope="col"><span data-feather="truck"></span> Courier</th>
                    <th scope="col"><span data-feather="target"></span> Tracking</th>
                </tr>
            </thead>
            <tbody>
                @foreach($shipments as $shipment)
                    <tr class="clickable-row" data-href="{{route('shipment.view', $shipment->id)}}">
                        <th scope="row">{{ $shipment->id }}</th>
                        <td>{{ $shipment->departure_date }}</td>
                        <td>{{ $shipment->estimated_arrival_date }}</td>
                        <td>{{ $shipment->actual_arrival_date }}</td>
                        <td style="color:#{{$shipment->status->color}}">{{ $shipment->status->name }}</td>
                        <td>{{ $shipment->employee->name }}</td>
                        <td>{{ $shipment->type->name }}</td>
                        <td>@if ($shipment->isLocal)Yes @else No @endif</td>
                        @auth 
                            @if(Auth::user()->hasAccess('vendors')->read)
                                @if($shipment->vendor)
                                    <td>{{ $shipment->vendor->reference }}</td>
                                @else
                                    <td>N/A</td>
                                @endif
                            @endif
                        @endauth
                        @if ($shipment->purchase_order)
                            <td>{{$shipment->purchase_order}}</td>
                        @else
                            <td></td>
                        @endif
                        <td>@if ($shipment->isShipped)Yes @else No @endif</td>
                        <td>@if ($shipment->isActive)Yes @else No @endif</td>
                        <td>
                            @if (($shipment->courier_id) != NULL)
                                {{ $shipment->courier->name}}
                            @endif
                        </td>
                        <td>
                        @if (($shipment->courier_id) != NULL)
                            <a href="{{$shipment->courier->url}}{{$shipment->tracking}}" target="_blank">{{ $shipment->tracking}}</a>
                        @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <hr/>

    <div class="btn-group mr-2">
        <a href="{{route('shipment.create')}}"><button class="btn btn-xs btn-info" >Create Shipment</button></a>
    </div>

    @if ($showAllState == 0)
        <div class="btn-group mr-2">
            <a href="{{route('shipment.show').'?showAllState=1'}}"><button class="btn btn-xs btn-info" >{{$buttonText}}</button></a>
        </div>
    @elseif ($showAllState == 1)
        <div class="btn-group mr-2">
            <a href="{{route('shipment.show').'?showAllState=0'}}"><button class="btn btn-xs btn-info" >{{$buttonText}}</button></a>
        </div>
    @endif

@endsection

@push('bottom-scripts')

    <script>
        jQuery(document).ready(function($) {
            $(".clickable-row").click(function() {
                window.location = $(this).data("href");
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#mainTable').DataTable({
                "scrollY":        "60vh",
                "scrollCollapse": true,
                "paging":         false,
                columnDefs: [ { orderable: false, targets: [

                ]}],
                "language": {
                    "search": "Filter records:"
                }
            });
        });
    </script>
@endpush