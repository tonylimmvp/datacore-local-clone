@extends('frontend.layouts.main')

@include('layouts.datatables')

{{-- Automated Checks --}}
@php 
    App\Quotation::checkExpired();
@endphp

@push('css')
<style>
    button:disabled {
        background-color: grey;
        pointer-events: none;
        cursor: not-allowed;
    }

    #notes_table td {
        margin: 0;
        padding: 0;
    }

    #notes_table th {
        margin: 0;
        padding: 0;
    }

</style>
@endpush

@section('content')

<div class="row">
    <div class="col-12">
        <form method="get" action="{{route('search.detail')}}">
            <div class="form-group row text-center">
                <input id="search_field" type="text" class="form-control col-7 col-md-6 offset-1 offset-md-2" name="term" @if(isset($term)) value="{{$term}}" @endif required>
                @if($hold_vendor)<input type="hidden" name="hold_vendor" value="{{$hold_vendor}}"> @endif
                <button type="submit" class="btn btn-primary col-3 col-md-3 col-lg-1" >Search</button>
            </div>
        </form>
        @auth
        @if(Auth::user()->hasAccess('vendors')->read)
        <form method="get" action="{{route('search.detail')}}">
            <input type="hidden" name="term" @if(isset($term)) value="{{$term}}" @endif required>
            @if($hold_vendor)
            <input type="hidden" name="hold_vendor" value="0"> 
            <button class="btn btn-info btn-sm" type="submit">Release Vendor</button>
            @else
            <input type="hidden" name="hold_vendor" value="1"> 
            <button class="btn btn-secondary btn-sm" type="submit">Hold Vendor</button>
            @endif
        </form>
        @endif
        @endauth
    </div>
</div>

{{-- Desktop View --}}
<div class="table_responsive d-none d-md-block">
    <table id="products-table" class="table table-striped table-hover table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="th-sm">ID</th>
                <th class="th-sm">Name</th>
                <th class="th-sm">Price</th>
                <th class="th-sm">Quantity</th>
                <th class="th-sm"> Shelf Location</th>
                <th class="th-sm"> Storage Location</th>
                @auth  @if(Auth::user()->hasAccess('vendors')->read && $hold_vendor)<th class="th-sm">Vendor</th>@endif @endauth
                <th class="th-sm">Image</th>
                <th class="th-sm">Actions</th>
                <th class="th-sm">Category</th>
            </tr>
        </thead>
        <tbody>
            @if(isset($products))
                @foreach ($products as $product)
                <tr @if($product->online) 
                        @if($product->online->isOnlineOnly())
                        style="background-color:rgba(234, 143, 26, 0.17)" 
                        @endif
                    @elseif($product->isRestricted())
                        style="background-color:rgba(92, 0, 210, 0.23);" 
                    @elseif($product->isObsoleted())
                        style="background-color:rgba(255, 47, 47, 0.09);" 
                    @endif>
                    <td class="searchID" id="{{$product->id_product}}">
                        <a href="{{config('custom.web_url')}}/product/{{$product->id_product}}.html" target="_blank">
                        {{-- <a href="{{route('product-profile.show', $product->id_product)}}"> --}}
                            {{$product->id_product}}
                        </a>
                    </td>

                    <td>{{$product->name}}  
                        @auth
                        @include('layouts.product-name-icons')
                        @endauth
                        
                        @if($product->online)
                            @if($product->online->isOnlineOnly())
                                <br>
                                <span class="badge badge-warning">ONLINE ONLY</span>
                            @endif
                        @endif

                        @auth
                        {{-- Orders for product --}}
                        @if($product->orders)
                            @foreach($product->orders as $order)
                                @if($order->currentStatus->name != "Done")
                                    <br>
                                    <a href="{{route('orders.detail',$order->id)}}" class="badge badge-secondary">
                                        Order #{{$order->id}} - {{$order->name}}
                                    </a>
                                @endif
                            @endforeach
                        @endif

                        {{-- show quotations if exit --}}
                        @if($product->quotations)
                            @foreach($product->quotations as $quote)
                                @if(!$quote->isExpired)
                                    <br>
                                    <a href="{{route('quotations')}}" class="badge badge-info">
                                        Quote #{{$quote->id}} - {{$quote->name}}
                                    </a>
                                @endif
                            @endforeach
                        @endif

                        {{--    Show applicable notes. 
                                If a note for this particular product exist. --}}
                        @if((sizeof($product->notes) > 0))
                            @auth
                            {{-- Only show note if your role is staff or above. --}}
                            @if(Auth::user()->hasAccess('product_note')->read)
                                {{-- Check if there is a non handled note, if true then show table headers. --}}
                                @if(($product->notes->where("isHandled", "=", "0")->count()) > 0)
                                    {{-- Iterate Each Note --}}
                                    @foreach($product->notes as $note)
                                        @if (!($note->isHandled))
                                        {{-- Purchased Note - Show qty and unit instead of comments.  --}}
                                            <br>
                                            <div class="btn btn-sm mb-1 mt-1" style="background-color:#{{$note->type->color}};@if($note->type_id == 3)color:white; @endif">
                                                <small>
                                                    @if(($note->type_id) == 2)
                                                        @if (Auth::user()->hasAccess('shipment')->delete)
                                                            <form class="form-inline" method="POST" action="{{route('notes.handle', $note->id)}}" enctype="multipart/form-data">
                                                                <input type="hidden" name="_method" value="POST">
                                                                {{ csrf_field() }}
                                                                <button type="submit" style="all: unset;">
                                                                    <b>{{$note->type->name}}:</b>
                                                                    <a style="text-decoration: none; color: black;">
                                                                        [{{$note->qty}} {{$note->unit}} by {{$note->employee->name}}] on [{{$note->created_at->format('Y-m-d')}}]
                                                                        @if ($note->comments != NULL)
                                                                            <b>Comments:</b> {{$note->comments}}
                                                                        @endif
                                                                    </a>
                                                                </button>
                                                            </form>
                                                        @else
                                                            <b>{{$note->type->name}}:</b>
                                                            <a style="text-decoration: none; color: black;">
                                                                [{{$note->qty}} {{$note->unit}} by {{$note->employee->name}}] on [{{$note->created_at->format('Y-m-d')}}]
                                                                @if ($note->comments != NULL)
                                                                    <b>Comments:</b> {{$note->comments}}
                                                                @endif
                                                            </a>
                                                        @endif
                                                    {{-- Obsolete Note --}}
                                                    @elseif (($note->type_id) == 3)
                                                        <b>{{$note->type->name}}:</b>
                                                        [{{$note->updated_at}}]
                                                    {{-- Shipment Found Note  --}}
                                                    @elseif((($note->type_id) == 9))
                                                        <b>{{$note->type->name}}:</b>
                                                        @if ($note->product->cart)
                                                            <a href="{{route('shipment.view', $note->cart->shipment->id)}}" style="text-decoration: none; color: black;">
                                                                Found in Shipment: #{{$note->cart->shipment->id}} (Qty: {{$note->cart->qty}} 
                                                                @if($note->cart->unit != 'winpos unit')
                                                                    {{$note->cart->unit}}@if($note->cart->qty > 1)s) @else ) @endif
                                                                @else
                                                                    )
                                                                @endif
                                                            </a>
                                                        @endif
                                                    @else
                                                        <b>{{$note->type->name}}:</b>
                                                        {{$note->comments}}
                                                    @endif
                                                </small>
                                            </div> 
                                        @endif
                                    @endforeach
                                @endif
                            @endif
                            @endauth
                        @endif
                        @endauth
                    </td>
                    {{-- Price --}}
                    <td>

                    @if($product->qty_break_1 != 0)
                        <div class='price-tooltip'>

                            {{-- check if on sale --}}
                            @if($product->onSale())
                                @foreach($product->onlineSales as $sale)
                                    @if($sale->onSale())
                                        <span style="text-decoration: line-through;">
                                            ${{$product->price}}
                                        </span>
                                        <p style="font-size:18px;color:red;">${{$sale->salePrice()}}</p>
                                    @endif
                                @endforeach
                            @else
                                <span>
                                    ${{$product->price}}
                                </span>
                            @endif
                            

                        <table class='price-tooltiptext'>
                        <thead>
                            <tr>
                                <th scope='col'>Quantity</th>
                                <th scope='col'>Discount</th>
                                <th scope='col'>Retail Price</th>
                                <th scope='col'>Total Price</th>
                            </tr>
                        </thead>

                        @php 
                        $qty_breaks = array(
                            $product->qty_break_1,
                            $product->qty_break_2,
                            $product->qty_break_3
                        );
                        $qty_break_price = array(
                            $product->qty_break_amount_1,
                            $product->qty_break_amount_2,
                            $product->qty_break_amount_3 
                        );
                        @endphp

                        @foreach($qty_breaks as $key => $qty_break )
                            @if($qty_break!=0)
                                <tr>
                                    <td> {{$qty_break}} </td>

                                    @php
                                    $total_before = $qty_break * $product->price;
                                    $total = $total_before - $qty_break_price[$key];
                                    
                                    $percentage = 1 - $total/$total_before;
                                    $percentage = "-" .round((float)$percentage * 100 ) . '%';
                                    
                                    $new_price = $total/$qty_break;

                                    @endphp

                                    <td>{{$percentage}}</td>

                                    <td>${{number_format($new_price, 2, '.', '')}}</td>
                                    <td>${{number_format($total, 2, '.', '')}}</td>

                                </tr>
                            @endif
                        @endforeach
                    

                        </table>

                        </div>
                    @else
                        {{-- check if on sale --}}
                        @if($product->onSale())
                            @foreach($product->onlineSales as $sale)
                                @if($sale->onSale())
                                    <span style="text-decoration: line-through;">
                                        ${{$product->price}}
                                    </span>
                                    <p style="font-size:18px;color:red;">${{$sale->salePrice()}}</p>
                                @endif
                            @endforeach
                        @else
                            <span>
                                ${{$product->price}}
                            </span>
                        @endif
                    @endif
                    
                    @if($product->online)
                        @if($product->online->unity)
                            /{{$product->online->unity}}
                        @endif
                    @endif

                    </td>

                    {{-- End of Price --}}

                    {{-- Quantity --}}
                    
                    @php
                    $fraser_qty = intval($product->fraser_quantity);
                    $qty_on_po = intval($product->qty_on_po);
                    $qty_on_so = intval($product->qty_on_so);
                    $qty_available = intval($product->qty_available);
                    @endphp

                    @if ($fraser_qty >= 0) 
                        <td @auth class='highlight-col' @endauth style='max-width:15px;'>
                    @else 
                        <td @auth class="highlight-col" @endauth style='background-color: red; max-width:15px;'>
                    @endif
                            
                    @auth
                    <div class='qty-tooltip' @if($qty_on_po!=0||$qty_on_so!=0) style='border-bottom:dotted 1px;' @endif>
                        {{$qty_available}}
                        <table class='qty-tooltiptext table table-sm'>
                            <tbody>
                            <tr>
                                <td>Quantity On Hand:</td>
                                <td>{{$fraser_qty}}</td>
                            </tr>
                            <tr>
                                <td>Quantity On PO:</td>
                                <td>{{$qty_on_po}}</td>
                            </tr>
                            <tr>
                                <td>Quantity On SO:</td>
                                <td>{{$qty_on_so}}</td>
                            </tr>
                            <tr>
                                <td>Quantity Available:</td>
                                <td>{{$qty_available}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    @if($qty_on_so > 0)
                    <span data-toggle="tooltip" data-title="Total quantity on Sales Order in WinPOS" data-placement="bottom">
                        <span class="badge badge-primary">{{$qty_on_so}}</span>
                    </span>
                    @endif
                    @if($qty_on_po > 0)
                    <span data-toggle="tooltip" data-title="Total quantity on Purchase Order in WinPOS" data-placement="bottom">
                        <span class="badge badge-secondary">{{$qty_on_po}}</span>
                    </span>
                    @endif
                        @if($product->online)
                        @if(sizeof($product->online->detail) > 0)
                        @php
                            $online_quantity = 0;
                            $online_tooltip = "Online order: ";
                            foreach($product->online->detail as $online_order){
                                $online_quantity = $online_order->pivot->product_quantity;
                                $online_tooltip .= "#" . $online_order->id_order . ", ";
                            }
                            $online_tooltip .= " requires " . $online_quantity ." quantity. Please reserve and create invoice on WinPOS.";
                        @endphp
                        <span data-toggle="tooltip" data-title="{{$online_tooltip}}" data-placement="bottom">
                            <span class="badge badge-danger">{{$online_quantity}}</span>
                        </span>
                        @endif
                        @endif
                    @endauth

                    @guest
                    {{$qty_available}}
                    @endguest
                    
                    </td>
                    {{-- END OF QUANTITY --}}

                    <td><span class="cursor-pointer" onclick="displayShelfLoc('{{$product->shelf_loc}}')">{{$product->shelf_loc}}</span></td>

                    {{-- LOCATION --}}
                    <td class="highlight-col text-center">
                        {{$product->locations()}}
                    </td>
                    {{-- END OF LOCATION --}}

                    @auth @if(Auth::user()->hasAccess('vendors')->read && $hold_vendor)
                    <td class="text-center lightTable">
                        @if(sizeof($product->vendors)>0)
                        @foreach($product->vendors as $index=>$vendor)
                            @if($index>0)
                                <hr>
                            @endif

                            <a href="{{ route('coreui.vendors.products',$vendor->id) }}">{{$vendor->reference}}</a>
                            @if($vendor->pivot->remark)
                                | 
                                @if($vendor->url)
                                    <a href="{{$vendor->url . $vendor->pivot->remark}}" target="_blank">{{$vendor->pivot->remark}} </a>
                                @else
                                    {{$vendor->pivot->remark}} 
                                @endif
                            @endif

                        @endforeach
                        @else
                            {{$product->vendor}} @if($product->vendor_remarks) | {{$product->vendor_remarks}} @endif
                        @endif
                    </td>
                    @endif @endauth

                    <td class="text-center" style="max-width:150px">    
                        @if($product->image_url())
                        <img class="lazy product-image" data-src="{{$product->image_url()}}" data-toggle="modal" data-target="#imageModal" width=150px height=150px>
                        @else
                            @if($product->online)
                                <a href="{{config('custom.web_url')}}/webui/index.php/product/form/{{$product->id_product}}">Upload</a>
                            @else
                                <p>New item, please upload item to update picture.</p>
                            @endif
                        @endif
                    </td>
                    <td width="100px">   
                        <div class="container text-center">
                        @auth
                        <a href="{{route('product-profile.show', $product->id_product)}}"><button class="btn btn-secondary btn-sm btn-action mb-1">Profile</button></a><br>
                        @endauth

                        @if($product->quantity > 0)
                        @auth
                        @if(!$product->isObsoleted())
                        <button class="btn btn-secondary btn-sm btn-action mb-1" onclick="addToBoard({{$product->id_product}},this)" @if($product->restock) disabled @endif>Restock</button><br>
                        @endif
                        @endauth
                        @endif

                        {{-- <button class="btn btn-secondary btn-sm btn-action mb-1">Verify Quantity</button><br> --}}
                        
                        <button class="btn btn-secondary btn-sm btn-action mb-1" data-toggle="modal" data-target="#notifierModal" data-id="{{ $product->id_product }}" data-qty="{{$product->quantity}}"
                            @if($product->isObsoleted())
                                disabled
                            @endif
                            >Notifier</button><br>

                        {{--   Performing Unique Constraint Check
                                Check whether in the database such pid exist. 
                                If exist then not unique     --}}
                        @auth
                        @php 
                            $uniquity = DB::table('order_suggestions')->where('product_id', '=', $product->id_product)->count(); 
                        @endphp

                        @if ($uniquity == '1' || $product->isObsoleted())
                            <form class="form-inline" method="POST" action="{{route('order-suggestion.create', $product->id_product)}}" enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="POST">
                            
                            {{ csrf_field() }}

                            <button type="submit" class="btn btn-secondary btn-sm btn-action mb-1" disabled>Add to Order Suggestion</button><br>
                            </form>
                        
                        @elseif ($uniquity == '0')
                            <button class="btn btn-secondary btn-sm btn-action mb-1" onclick="addToOrderSuggestion({{$product->id_product}},this)">Add to Order Suggestion</button><br>
                        @else
                            <button class="btn btn-secondary btn-sm btn-action mb-1" disabled>Database Error on uniquity.</button><br>
                        @endif

                        @if(Auth::user()->hasAccess('shipment')->update)
                            @php 
                                $uniquity_op = DB::table('product_notes')->where('type_id', '=', '2')->where('isHandled', '0')->where('product_id', '=', $product->id_product)->count(); 
                                //dd($uniquity_op);
                            @endphp

                            @if($uniquity_op < 1)
                                {{-- Ordered button and form begin here --}}
                                <button class="btn btn-secondary btn-sm btn-action mb-1"
                                    data-toggle="modal"
                                    data-target="#purchaseModal"
                                    data-url="{{route('order-suggestion.purchase', $product->id_product )}}">Purchase and Hold
                                </button>
                            @else
                                <button class="btn btn-sm btn-action btn-secondary mb-1" style="background-color: #50C878; color: black;"
                                    data-toggle="modal"
                                    data-target="#purchaseModal"
                                    data-url="{{route('order-suggestion.purchase', $product->id_product )}}">Purchase and Hold
                                </button>                                
                            @endif



                            {{-- Purchase and Ship --}}
                            <button class="btn btn-secondary btn-sm btn-action mb-1"
                                data-toggle="modal"
                                data-target="#purchaseAndShipModal"

                                data-id="{{ $product->id_product }}" 
                                data-name="{{ $product->name }}"
                                data-vendor="{{$product->vendor}}"
                                data-remarks="{{$product->vendor_remarks}}"

                                data-url="{{route('order-suggestion.purchaseAndShip', $product->id_product)}}">Purchase and Ship
                            </button>
                        @endif
                        @endauth

                        @auth
                        <button class="btn btn-secondary btn-sm btn-action"
                        type="button"
                        data-toggle="modal"
                        data-target="#printModal"
                        data-id= "{{$product->id_product}}">Print Labels</button><br>
                        @endauth
                    </div>
                    </td>
                    <td class="categoryCol" ondblclick="hideRow(this)">
                        @if($product->online)
                            @if($product->getCategoryUrl($product->online->id_category_default && $product->online->id_category_default != 2))
                                @auth @if(Auth::user()->hasAccess('category')->read)<p>ID SET TO: {{$product->online->id_category_default}}</p>@endif @endauth
                                <a href="{{ $product->getCategoryUrl($product->online->id_category_default) }}" target="_blank">
                                    {{$product->getCategoryName($product->online->id_category_default)}}
                                </a>
                            @else
                                @auth
                                @if(Auth::user()->hasAccess('category')->update)
                                @include('layouts.category-update-form')
                                @endif
                                @endauth
                            @endif
                        @else
                            <p>This is a new item. Please upload items to change category.</p>
                        @endif
                    </td>
                </tr>
                @endforeach
            @endif
        </tbody>
    </table>

    @isset($products)
    @php 
        $entry_start = (($products->currentPage() - 1)* $products->perPage()) + 1;
        $entry_end = (($products->currentPage() - 1)* $products->perPage()) + $products->count();
    @endphp

    <p class="mt-2">Showing {{ $entry_start }} to <span id="page-limit">{{ $entry_end }}</span> of {{ $products->total() }} products <span id="page-adjusted"></span></p>
    @endisset

</div>

{{-- END OF DESKTOP TABLE --}}

{{-- Mobile View --}}
<div class="table_responsive mt-5 d-md-none">
    <table id="products-table" class="table table-striped " cellspacing="0" width="100%">
        @if(isset($products) && sizeof($products)>0)
        @foreach ($products as $product)
        <tr>
            <td><img class="lazy product-image" data-src="{{$product->image_url()}}" data-toggle="modal" data-target="#imageModal" width=150px height=150px></td>
            <td class="searchID">
                <b>PID: </b>
                <span>
                    <a href="{{config('custom.web_url')}}/product/{{$product->id_product}}.html" target="_blank">
                        {{$product->id_product}}
                    </a>
                </span>
                <hr>
                <p>{{$product->name}}</p>
                @auth
                @if($product->online)
                @if($product->online->isOnlineOnly())
                    <span class="badge badge-warning">This product is sold online only</span><br>
                @endif
                @endif
                @if($product->isObsoleted())
                    <span class="badge badge-danger">This product is obsoleted</span><br>
                @endif
                @if($product->isRestricted())
                    <span class="badge badge-danger">This product has restricted sale</span><br>
                @endif
                @if(Auth::user()->hasAccess('vendors')->read && $hold_vendor)
                    <hr>
                    <small>{{ $product->vendor }}</small>
                @endif
                @endauth
            </td>
        </tr>
        <tr>
            <td><span><b>Price: </b> 
                {{-- check if on sale --}}
            @if($product->onSale())
                @foreach($product->onlineSales as $sale)
                    @if($sale->onSale())
                        <span style="text-decoration: line-through;">
                            ${{$product->price}}
                        </span>
                        <span style="font-size:18px;color:red;">${{$sale->salePrice()}}</span>
                    @endif
                @endforeach
            @else
                <span>
                    ${{$product->price}}
                </span>
            @endif

            @if($product->online)
                @if($product->online->unity)
                    /{{$product->online->unity}}
                @endif
            @endif
            
            </span></td>  

            <td><span><b>Quantity: </b> {{$product->quantity}}</span></td>      
        </tr>
        <tr>
            <td><span class="cursor-pointer" onclick="displayShelfLoc('{{$product->shelf_loc}}')"><b>Shelf: </b> {{$product->shelf_loc}}</span></td>    
            <td><span><b>Storage: </b> {{$product->fraser_loc}}</span></td>      
        </tr>   
        <tr>
            <td class="categoryCol" colspan="2">
            <b>Category: </b> 
            @if($product->online)
                @if($product->getCategoryUrl($product->online->id_category_default && $product->online->id_category_default != 2))
                    <a href="{{ $product->getCategoryUrl($product->online->id_category_default) }}">
                        {{$product->getCategoryName($product->online->id_category_default)}}
                    </a>
                @else
                    No category, please update on desktop.
                @endif
            @else
                <p>This is a new item. Please upload items to change category.</p>
            @endif
            </td>
        </tr>
        <tr>
            <td colspan="2"><b>Actions:</b> 
                <span class="ml-2">
                    @auth
                    <a href="{{route('product-profile.show', $product->id_product)}}"><button class="btn btn-primary btn-sm">Profile</button></a>
                    @endauth
                    @if($product->quantity > 0)
                    @auth 
                    <button class="btn btn-primary btn-sm" onclick="addToBoard({{$product->id_product}},this)" @if($product->restock || $product->isObsoleted()) disabled @endif>Restock</button>
                    @endauth
                    @endif

                    @auth 
                        @php 
                            $uniquity = DB::table('order_suggestions')->where('product_id', '=', $product->id_product)->count(); 
                        @endphp

                        @if ($uniquity == '1' || $product->isObsoleted())
                            <form class="d-inline form-inline" method="POST" action="{{route('order-suggestion.create', $product->id_product)}}" enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="POST">
                            
                            {{ csrf_field() }}

                            <button type="submit" class="btn btn-primary btn-sm my-1" disabled>Suggest</button>
                            </form>
                        
                        @elseif ($uniquity == '0')
                            <form class="d-inline form-inline" method="POST" action="{{route('order-suggestion.create', $product->id_product)}}" enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="POST">
                            
                            {{ csrf_field() }}

                            <button type="submit" class="btn btn-primary btn-sm my-1">Suggest</button>
                            </form>

                        @else
                            <button class="btn btn-primary btn-sm my-1" disabled>Database Error on uniquity.</button>
                        @endif
                    @endauth

                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#notifierModal" data-id="{{ $product->id_product }}" data-qty="{{$product->quantity}}" @if($product->isObsoleted()) disabled @endif>Notifier</button>
                    
                    @auth
                    <button class="btn btn-primary btn-sm"
                    type="button"
                    data-toggle="modal"
                    data-target="#printModal"
                    data-id= "{{$product->id_product}}">Print</button>
                    @endauth
                </span>
            </td>    
        </tr>
        
        <tr><td></td><td></td></tr>
        
        @endforeach
        @else
            @if(isset($term))
                <p class="text-center">No results for <i>{{$term}}</i></p>
            @endif
        @endif
    
    </table>
</div>
{{-- END OF MOBILE TABLE --}}

{{-- Pagination Links --}}
@isset($products)
    <div class="d-flex justify-content-center">
        {{ $products->appends(request()->input())->render("pagination::bootstrap-4") }}
    </div>
@endisset
{{-- End of Links --}}

@include('layouts.notifier')
@include('layouts.shelf-location')
@include('layouts.image-modal')

{{-- Modals --}}

    {{-- Purchase Modal --}}
    <div class="modal fade" id="purchaseModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="POST">
                	{{ csrf_field() }}
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Input Order Information</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>

					<div class="modal-body">
						<label>Quantity:</label>
                    	<input class="form-control" type="text" name="qty" id="qty"><br/>

						<label>Unit:</label>
                        <select class="custom-select mr-sm-2 mb-2" name="unit" id="unit" required>
                            <option value="piece">Piece</option>
                            <option value="package">Package</option>
                            <option value="meter">Meter</option>
                            <option value="foot">Foot</option>
                            <option value="1/2lb">1/2LB</option>
                            <option value="box">Box</option>
                            <option value="roll">Roll</option>
                            <option value="winpos unit">Winpos Unit</option>
                        </select>

						<label>Comments:</label>
                    	<textarea class="form-control" type="text" name="comments" id="note-comments"></textarea><br/>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
						<a id="note-cancel" href="#">
						<button type="submit" class="btn btn-primary">Order Item</button>
						</a>
					</div>
        		</form>
            </div>
    	</div>
    </div>

    {{-- Purchase and Ship Modal --}}
    <div class="modal fade" id="purchaseAndShipModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="POST">
                	{{ csrf_field() }}
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Insert Order into Shipment</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>

					<div class="modal-body">
						<label>Product ID:</label>
                    	<input class="form-control" type="text" name="id" id="id" readonly><br/>

						<label>Product Name:</label>
                    	<input class="form-control" type="text" name="name" id="name" readonly><br/>

                        <label>Vendor:</label>
                        <input class="form-control" type="text" name="vendor" id="vendor" readonly><br/>

                        <label>Remark:</label>
                    	<input class="form-control" type="text" name="remarks" id="remarks" readonly><br/>

						<label>Ordered Quantity:</label>
                    	<input class="form-control" type="text" name="qty" id="qty"><br/>

                    	<label>Ordered Unit:</label>
                    	<select class="custom-select mr-sm-2 mb-2" name="unit" id="unit" required>
                            <option value="piece">Piece</option>
                            <option value="package">Package</option>
                            <option value="meter">Meter</option>
                            <option value="foot">Foot</option>
                            <option value="1/2lb">1/2LB</option>
                            <option value="box">Box</option>
                            <option value="roll">Roll</option>
                            <option value="winpos unit">Winpos Unit</option>
                        </select><br/>

                        @php $shipments = App\Shipment::all() @endphp
                        <label>Select Shipment:</label>
                        <select class="custom-select mr-sm-2 mb-2" name="shipment" id="shipment" required>
                            @foreach($shipments as $shipment)
                                @if ($shipment->isActive)
                                    <option value="{{$shipment->id}}">{{$shipment->id}} | [{{$shipment->status->name}}] | {{$shipment->vendor->name}} | 
                                        @if ($shipment->isLocal == '1')
                                            LOCAL
                                        @else
                                            NOT LOCAL
                                        @endif
                                        | {{$shipment->type->name}}
                                    </option>
                                @endif
                            @endforeach
                        </select>
                        <br/>
                        <p class='smallNote'> If no shipment is available, create one <a href="{{route('shipment.create')}}" target="_blank">here.</a>
                            <a href="javascript:window.location.href=window.location.href"> Refresh page.</a>
                        </p>

                    	<label>Invoice:</label>
                    	<input class="form-control" type="text" name="invoice" id="invoice"><br/>

						<label>Comments:</label>
                    	<textarea class="form-control" type="text" name="comments" id="comments"></textarea><br/>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>

						<button type="submit" class="btn btn-primary">Purchase and Ship</button>
					</div>
        		</form>
            </div>
    	</div>
    </div>

@endsection

@push('bottom-scripts')

<script type="text/javascript">
    $(document).ready(function () {
        $('#products-table').DataTable({

            paging:         false,
            info:     false,
            columnDefs: [ { orderable: false,targets: [
                @auth
                    @if(Auth::user()->hasAccess('vendors')->read && $hold_vendor)
                        7,8
                    @else
                        6,7
                    @endif
                @endauth

                @guest
                    6,7
                @endguest
            ] },{ width : "200px" , targets: [
                @auth
                    @if(Auth::user()->hasAccess('vendors')->read && $hold_vendor)
                        8,9
                    @else
                        6,7
                    @endif
                @endauth

                @guest
                    6,7
                @endguest    
            ]}, { type: 'any-number', targets : [2,3] } ],
            "displayLength": 25,
            "language": {
                "search": "Filter records:"
            }
        });
        $('.dataTables_length').addClass('bs-select');
    });
</script>

<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@10.19.0/dist/lazyload.min.js"></script>

<script>
    var myLazyLoad = new LazyLoad({
        elements_selector: ".lazy",
        load_delay: 300 
    });

    var input = document.getElementById('search_field');
    input.focus();
    input.select();

    function clearForm() {
        document.getElementById("search_field").value="";
        document.getElementById("search_field").select();
    }

    function keyPress(e) {
        var evtobj = window.event? event : e
        if (evtobj.keyCode == 27){
            clearForm();
        }
    }

    document.onkeydown = keyPress;

</script>

<script>
    function addToBoard(pid,btn) {
        btn.disabled = true;

        $.ajax({
            type: "get",
            url: "{{ route('restock.add') }}",
            data: {pid: pid},
            dataType: 'json',
            success: function( msg ) {
                console.log(msg)
                console.log(msg.status)
                if(msg.status == "success"){
                    alert("You have successfully added PID#" + pid + " into the restock board.");
                }else{
                    alert(msg.message);
                    btn.disabled = true;
                }
            },
            error: function (request, status, error) {
                alert(request.status + ':' + error + '. Please take picture and contact DevTeam.');
                btn.disabled = true;
            }
        });
    }
</script>

<script>
    function addToOrderSuggestion(pid,btn) {
        btn.disabled = true;

        $.ajax({
            type: "get",
            url: "{{ route('order-suggestion.add') }}",
            data: {pid: pid},
            dataType: 'json',
            success: function( msg ) {
                console.log(msg)
                console.log(msg.status)
                if(msg.status == "success"){
                    alert("You have successfully added PID#" + pid + " into the order suggestion.");
                }else{
                    alert(msg.message);
                    btn.disabled = true;
                }
            },
            error: function (request, status, error) {
                alert(request.status + ':' + error + '. Please take picture and contact DevTeam.');
                btn.disabled = true;
            }
        });
    }
</script>

<script>
    function hideRow(row){     
        row.parentNode.style.display = "none";
        var page_limit = $('span#page-limit').text();
        $('span#page-limit').text($('span#page-limit').text()-1);
        $('span#page-adjusted').text('(adjusted)');
    }
</script>

<script>
    $(document).ready(function () {
        $('#purchaseAndShipModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var id = button.data('id') // Extract info from data-* attributes
            var name = button.data('name') // Extract info from data-* attributes
            var vendor = button.data('vendor')
            var remarks = button.data('remarks')
            var url = button.data('url')

            var modal = $(this)
            modal.find('.modal-title').text('Insert Item: ' + name)
            modal.find('#id').val(id)
            modal.find('#name').val(name)
            modal.find('#vendor').val(vendor)
            modal.find('#remarks').val(remarks)
            modal.find('form').attr('action',url)
        });
    });
</script>

<script>
    $(document).ready(function () {
        $('#purchaseModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) 
            var url = button.data('url')
            var modal = $(this)
            modal.find('form').attr('action',url)
        });
    });
</script>

@include('functions.category-update-functions')

@endpush