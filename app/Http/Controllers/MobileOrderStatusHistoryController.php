<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MobileOrder;
use App\MobileOrderStatusHistory;
use Carbon\Carbon;

class MobileOrderStatusHistoryController extends Controller
{
    public function store(Request $request)
    {
        $this->validate(request(), [
            'order' => 'required',
            'employee' => 'required',
            'status' => 'required'
        ]);

        // create order status history
    
        $status_history = MobileOrderStatusHistory::create([
            'order_id' => request('order'),
            'employee_id' => request('employee'),
            'state_id' => request('status'),
            'date_stamp' => Carbon::now()->format('Y-m-d h:iA')
        ]);
    
        $status_history->save();

        // Once status history created, we want to change order status as well

        $order = MobileOrder::find($request->order);
        $order->current_state = $request->status; 

        $order->save();
            
        /*************************************************************************************
         * 
         * Customer Emails
         * 
         ***************************************************************************************/ 

        // Preperation in progress
        if($order->current_state == 1 && $order->customer()->email){
            \Mail::to($order->customer()->email)->send(new \App\Mail\MobileOrderPreperation($order));
        // Ready for pick up
        }elseif($order->current_state == 2 && $order->customer()->email){
            \Mail::to($order->customer()->email)->send(new \App\Mail\MobileOrderReady($order));
        // On hold / backorder
        }elseif($order->current_state == 6 && $order->customer()->email){
            \Mail::to($order->customer()->email)->send(new \App\Mail\MobileOrderOnHold($order));
        }
            
        session()->flash('message','You have successfully updated your orders status.');
        return back();
    }

    public function updateTracking(Request $request) {

        $this->validate(request(), [
            'order' => 'required',
            'employee' => 'required',
            'courier' => 'required',
            'tracking' => 'required'
        ]);

         // update tracking number

         $order = MobileOrder::find($request->order);
         $order->courier_id = $request->courier;
         $order->shipping_number = $request->tracking; 
 
         $order->save();
        
        // create order status history
    
        $status_history = MobileOrderStatusHistory::create([
            'order_id' => request('order'),
            'employee_id' => request('employee'),
            'state_id' => "5",
            'date_stamp' => Carbon::now()->format('Y-m-d h:iA')
        ]);

        $status_history->save();
        
        // Once status history created, we want to change order status as well
        $order->current_state = "5"; 
        $order->save();

        // Send email
        if($order->customer()->email){
            \Mail::to($order->customer()->email)->send(new \App\Mail\MobileOrderShipped($order));
        }
                    
        session()->flash('message','You have successfully updated the tracking number.');
        return back();
    }

}
