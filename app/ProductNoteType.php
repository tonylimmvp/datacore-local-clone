<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductNoteType extends Model
{
    protected $table='product_note_types';
    protected $connection='mysql';
}
