<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SidePanelOnlineNote extends Model
{
    protected $connection='mysql';
    protected $table='sidepanel_notes_online';
    protected $guarded=[];

    public function onlineOrder(){
        return $this->belongsTo(OnlineOrder::class);
    }

    public function employee(){
        return $this->belongsTo(Employee::class);
    }
}
