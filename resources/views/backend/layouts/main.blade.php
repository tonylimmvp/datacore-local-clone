@include('layouts.config')
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    {{-- CSRF Token --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }} | CoreUI @yield('title')</title>

    {{-- Bootstrap core CSS --}}
    <link href="{{ asset('css/compiled/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/compiled/custom.css')}}">
    <link href="{{asset('css/compiled/side-modal.css')}}" rel="stylesheet">


    {{-- Custom styles for this template --}}
    <link href="{{ asset('css/dashboard.css')}}" rel="stylesheet">
    @stack('css')

    {{-- Custom scripts --}}
    @stack('top-scripts')

  </head>

  <body>
      <div id="wrapper">
          <div id="backend-header">
            @include('backend.layouts.top-nav')
          </div>
        
          <div id="content">
        
            <div class="container-fluid">
              <div class="row">
                @include('backend.layouts.side-nav')
                
                @include('layouts.loader')
                @include('layouts.flash')
        
                <main role="main" class="backend-content col-12">
                    @yield('content')
                </main>
        
                {{-- Modals --}}
                @include('layouts.message-modal')
        
              </div>
            </div>
            
          </div>
        
          <footer id="main-footer">
              <p>Copyright &copy; {{Carbon\Carbon::now()->year}} Lee's Electronic Components Ltd (2005). All rights reserved.</p>
          </footer>
        </div>
    
    
    

     {{-- Bootstrap core JavaScript --}}
    {{-- ================================================== --> --}}
    {{-- Placed at the end of the document so the pages load faster --}}
    {{-- <script src="{{ asset('js/compiled/app.js') }}"></script> --}}

    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    {{-- Custom JS --}}
    <script src="{{asset('js/compiled/custom.js')}}"></script>

    {{-- Icons --}}
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
      feather.replace()
    </script>

    @stack('bottom-scripts')

    <script type="text/javascript">
    $("[data-toggle='tooltip']").tooltip({html:true,sanitize:false});
    </script>

    
  </body>
</html>
