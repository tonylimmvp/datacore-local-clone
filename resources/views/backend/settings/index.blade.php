@extends('backend.layouts.main')

@section('content')
    <div class="container d-flex justify-content-center" style="height:80vh">
        <div class="my-auto text-center">
            @if(Auth::user()->role->id <= 2 && Auth::user()->hasAccess("order_status")->read)
            <p><b>CONFIG</b></p>
            <a href="{{route('coreui.order-status')}}"><button class="btn btn-primary">Update Order Status</button></a>
            <hr>
            @endif
            <p><b>ACCOUNT</b></p>
                <a href="{{ route('coreui.profile') }}"><button class="btn btn-primary">Update Profile</button></a>
        </div>
    </div>
@endsection