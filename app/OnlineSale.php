<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class OnlineSale extends Model
{
    protected $connection = "leeselectronic";
    protected $table = "ps_specific_price";
    protected $primaryKey = 'id_specific_price';

    public $timestamps = false;
    public $incrementing = false;

    public function product(){
        return $this->belongsTo(Product::class, 'id_product');
    }

    public function online(){
        return $this->belongsTo(ProductOnline::class, 'id_product');
    }

    public function onSale(){
        $on_sale = false;
        $from = Carbon::parse($this->from);
        $to = Carbon::parse($this->to);
        $now = Carbon::now();
        if($now > $from && $now < $to){
            $on_sale = true;
        }
        return $on_sale;
    }

    public function salePrice(){
        if($this->reduction_type == "percentage"){
            $sale_price = $this->product->price * (1 - $this->reduction);
        }else if($this->reduction_type == "amount"){
            $sale_price = $this->product->price - $this->reduction;
        }else{
            $sale_price = $this->product->price;
        }
        
        return money_format('%i', $sale_price);
    }

}
