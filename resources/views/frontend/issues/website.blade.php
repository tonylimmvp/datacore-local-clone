@extends('frontend.layouts.main')

@push('css')
<style>
    .table{
        display:none;
    }
    #sidepanelModal .table{
        display:table;
    }
</style>
@endpush

@section('content')

    <h1>ISSUES</h1>

    {{------------------------------------------- 
    ----
    ----    Category Mismatch 
    ----
    ---------------------------------------------}}

    <h2>Category Mismatch</h2>
    @if(Auth::user()->hasAccess('category')->read || Auth::user()->hasAccess('category')->update )
    @php
        $categories = App\Issue::checkCategory();
    @endphp

    <button class="btn btn-sm btn-primary" onclick="toggle('category_mismatch')">Toggle</button>

    <span>
        {{sizeof($categories)}} Results found.
    </span>
    @if(Auth::user()->hasAccess('category')->read)
    <table id="category_mismatch" class="table table-bordered mt-2">
        <thead>
            <th>PID</th>
            <th>Name</th>
            <th>Default Category</th>
            <th>Categories</th>
            @if(Auth::user()->hasAccess('category')->update)
            <th>Action</th>
            @endif
        </thead>
        <tbody>
            @foreach($categories as $product)
                <tr>
                    <td><a href="{{config('custom.web_url')}}/product/{{$product->id_product}}.html" target="_blank">{{ $product->id_product }}</a></td>
                    <td>{{ $product->store["name"] }}</td>
                    <td>
                        {{ $product->id_category_default }} - {{ $product->getCategoryName($product->id_category_default)}}
                    </td>
                    <td>
                        @foreach($product->categories as $category)
                            <p class="m-0 p-0">{{$category->id_category}} - {{$category->name}}</p>
                        @endforeach
                    </td>
                    @if(Auth::user()->hasAccess('category')->update)
                    <td>
                        @include('layouts.category-update-form')
                    </td>
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>

    @endif

    @endif

    @if(!Auth::user()->hasAccess('category')->update)
        <div class="alert alert-info mt-2">
            <p>Note: You do not have permission to update category. Please contact someone if you need the categories to be updated.</p>
        </div>
    @endif

    {{------------------------------------------- 
    ----
    ----    Empty Category
    ----
    ---------------------------------------------}}

    <h2>Products Without Categories</h2>
    @if(Auth::user()->hasAccess('category')->read || Auth::user()->hasAccess('category')->update)
    @php
        $categories = App\Issue::checkEmptyCategory();
    @endphp

    <button class="btn btn-sm btn-primary" onclick="toggle('no_category')">Toggle</button>

    <span>
        {{sizeof($categories)}} Results found.
    </span>

    @if(Auth::user()->hasAccess('category')->read)
    <table id="no_category" class="table table-bordered mt-2">
        <thead>
            <th>PID</th>
            <th>Name</th>
            <th>FS Category</th>
            <th>Default Category</th>
            <th>Categories</th>
            @if(Auth::user()->hasAccess('category')->update)
            <th>Action</th>
            @endif
        </thead>
        <tbody>
            @foreach($categories as $product)
                <tr>
                    <td><a href="{{config('custom.web_url')}}/product/{{$product->id_product}}.html" target="_blank">{{ $product->id_product }}</a></td>
                    <td>{{ $product->store['name'] }}</td>
                    <td>{{ $product->store['cat_id']}} - {{$product->getCategoryName($product->store['cat_id'])}}</td>
                    <td>
                        {{ $product->id_category_default }} - {{ $product->getCategoryName($product->id_category_default)}}
                    </td>
                    <td>
                        @foreach($product->categories as $category)
                            <p class="m-0 p-0">{{$category->id_category}} - {{$category->name}}</p>
                        @endforeach
                    </td>
                    @if(Auth::user()->hasAccess('category')->update)
                    <td>   
                        @include('layouts.category-update-form')
                    </td>
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>

    @endif
    @endif

    @if(!Auth::user()->hasAccess('category')->update)
    <div class="alert alert-info mt-2">
        <p>Note: You do not have permission to update category. Please contact someone if you need the categories to be updated.</p>
    </div>
    @endif

    {{------------------------------------------- 
    ----
    ----    Product Weight
    ----
    ---------------------------------------------}}

    {{-- <h2>Product With No Weight</h2>
    @php
        $products = App\ProductOnline::where('weight','0')->where('id_product','>',16)->where('active',1)->get();
    @endphp

    <button class="btn btn-sm btn-primary" onclick="toggle('no_weight')">Toggle</button>

    <span>
        {{sizeof($products)}} Results found.
    </span>

    <table id="no_weight" class="table table-bordered mt-2">
        <thead>
            <th>PID</th>
            <th>Name</th>
            <th>Action</th>
        </thead>
        <tbody>
            @foreach($products as $product)
                <tr>
                    <td>{{ $product->id_product }}</td>
                    <td>
                        @if($product->store)
                            {{ $product->store->name }}
                        @else
                            NOT AVAILABLE IN STORE
                        @endif
                    </td>
                    <td>
                        <form class="weightForm" action="{{route('product.updateWeight',$product->id_product)}}" method="POST">
                            <div class="input-group">
                            {{ csrf_field() }}
                            <input class="form-control" name="weight" type="number" step="0.0001">
                            <div class="input-group-append">
                                <button class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table> --}}

    {{------------------------------------------- 
    ----
    ----    Product with no images
    ----
    ---------------------------------------------}}

    <h2>Products With No Images</h2>
    @php
        $products = App\Product::selectRaw('table_1.*,ps_image.id_image')->leftJoin('ps_image','table_1.id_product', '=', 'ps_image.id_product')->where('ps_image.id_image',NULL)->where('table_1.disabled','N')->where('table_1.id_product','>',16)->whereNotIn('table_1.id_product',[100])->get();
    @endphp

    <button class="btn btn-sm btn-primary" onclick="toggle('no_imgs')">Toggle</button>

    <span>
        {{sizeof($products)}} Results found.
    </span>

    <table id="no_imgs" class="table table-bordered mt-2">
        <thead>
            <th>PID</th>
            <th>Name</th>
            <th class="text-center">Action</th>
        </thead>
        <tbody>
            @foreach($products as $product)
                <tr>
                    <td>{{$product->id_product}}</td>
                    <td>{{$product->name}}</td>
                    <td class="text-center">
                        @if($product->online)
                            <a href="#">-- Upload --</a>
                        @else 
                            <p class="p-0 m-0">New Item</p>
                            <p class="p-0 m-0">Please Import First</p>
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

        {{------------------------------------------- 
    ----
    ----    Picture Retakes
    ----
    ---------------------------------------------}}

    <h2>Picture Retakes</h2>
    @php
        $notes = App\ProductNote::where('type_id',7)->where('isHandled',false)->get();
    @endphp

    <button class="btn btn-sm btn-primary" onclick="toggle('retake_img')">Toggle</button>

    <span>
        {{sizeof($notes)}} Results found.
    </span>

    <table id="retake_img" class="table table-bordered mt-2">
        <thead>
            <th>PID</th>
            <th>Name</th>
            <th>Image</th>
            @if(Auth::user()->hasAccess('product_note')->update)
                <th class="text-center">Action</th>
            @endif
        </thead>
        <tbody>
            @foreach($notes as $note)
                <tr>
                    <td>{{$note->product->id_product}}</td>
                    <td>{{$note->product->name}}</td>
                    <td>
                        <img src="{{$note->product->image_url()}}" width="100px" height="100px">
                    </td>
                    @if(Auth::user()->hasAccess('product_note')->update)
                        <td class="text-center">
                            <form  method="POST" action="{{route('notes.handle', $note->id)}}" enctype="multipart/form-data">
                                <input type="hidden" name="_method" value="POST">
                                {{ csrf_field() }}
                                @if ($note->isHandled)
                                    <button type="submit" class="btn btn-secondary btn-sm btn-primary mb-1" disabled>Handled</button><br>
                                @else
                                    <button type="submit" class="btn btn-primary btn-sm btn-primary mb-1">Handle</button><br>
                                @endif
                            </form>
                        </td>
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>

    {{------------------------------------------- 
    ----
    ----    Obsoleted Products
    ----
    ---------------------------------------------}}

    <h2>Obsoleted and Active Products</h2>
    @php
        $notes = App\ProductNote::where('type_id','3')->get();
    @endphp

    <button class="btn btn-sm btn-primary" onclick="toggle('obsolete_products')">Toggle</button>

    <span>
        {{sizeof($notes)}} Results found.
    </span>

    <table id="obsolete_products" class="table table-bordered mt-2">
        <thead>
            <th>PID</th>
            <th>Name</th>
        </thead>
        <tbody>
            @foreach($notes as $note)
                @if($note->product->disabled == "N")
                <tr>
                    <td> <a href="{{config('custom.web_url')}}/product/{{$note->product->id_product}}.html" target="_blank">{{$note->product->id_product}}</a></td>
                    <td>{{$note->product->name}}</td>
                </tr>
                @endif
            @endforeach
        </tbody>
    </table>

    {{------------------------------------------- 
    ----
    ----    Sales Restriction Products
    ----
    ---------------------------------------------}}

    <h2>Sales Restriction and Active Products</h2>
    @php
        $notes = App\ProductNote::where('type_id','8')->get();
    @endphp

    <button class="btn btn-sm btn-primary" onclick="toggle('restricted_products')">Toggle</button>

    <span>
        {{sizeof($notes)}} Results found.
    </span>

    <table id="restricted_products" class="table table-bordered mt-2">
        <thead>
            <th>PID</th>
            <th>Name</th>
        </thead>
        <tbody>
            @foreach($notes as $note)
                @if($note->product->isActive)
                <tr>
                    <td><a href="{{config('custom.web_url')}}/product/{{$note->product->id_product}}.html" target="_blank">{{$note->product->id_product}}</a></td>
                    <td>{{$note->product->name}}</td>
                </tr>
                @endif
            @endforeach
        </tbody>
    </table>

    {{------------------------------------------- 
    ----
    ----    Online Only Products
    ----
    ---------------------------------------------}}

    <h2>Online Only Products</h2>
    @php
        $products = App\ProductOnline::where('online_only','1')->where('active',1)->get();
    @endphp

    <button class="btn btn-sm btn-primary" onclick="toggle('online_only')">Toggle</button>

    <span>
        {{sizeof($products)}} Results found.
    </span>

    <table id="online_only" class="table table-bordered mt-2">
        <thead>
            <th>PID</th>
            <th>Name</th>
        </thead>
        <tbody>
            @foreach($products as $product)
                <tr>
                    <td><a href="{{config('custom.web_url')}}/product/{{$product->id_product}}.html" target="_blank">{{$product->id_product}}</a></td>
                    <td>@isset($product->store->name) {{$product->store->name}} @endisset</td>
                    <td>
                        <form class="onlineOnlyForm" action="{{ route('online-product.updateBO') }}" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="product_id" value="{{$product->id_product}}">
                            <input type="hidden" name="online" value="0">
                            <button type="submit" class="btn btn-primary">Remove from Online Only</button>
                        </form>                    
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    {{------------------------------------------- 
    ----
    ----    Backorder Available Products
    ----
    ---------------------------------------------}}

    <h2>Online Products with backorder</h2>
    @php
        $products = DB::connection("leeselectronic")
                    ->select("  SELECT ps_stock_available.id_product, table_1.name, ps_product.active
                                FROM `ps_stock_available` 
                                INNER JOIN `table_1` 
                                ON table_1.id_product = ps_stock_available.id_product
                                INNER JOIN `ps_product`
                                ON  ps_product.id_product = ps_stock_available.id_product
                                WHERE ps_stock_available.out_of_stock = 1
                                GROUP BY ps_stock_available.id_product");
    @endphp

    <button class="btn btn-sm btn-primary" onclick="toggle('allow_backorder')">Toggle</button>

    <span>
        {{sizeof($products)}} Results found.
    </span>

    <table id="allow_backorder" class="table table-bordered mt-2">
        <thead>
            <th>PID</th>
            <th>Name</th>
            <th>Active</th>
            <th>Action</th>
        </thead>
        <tbody>
            @foreach($products as $product)
                <tr>
                    <td><a href="{{config('custom.web_url')}}/product/{{$product->id_product}}.html" target="_blank">{{$product->id_product}}</a></td>
                    <td>@isset($product->name) {{$product->name}} @endisset</td>
                    <td> @if($product->active) Y @else N @endif </td>
                    <td>
                        <form class="onlineBackorderForm" action="{{ route('online-product.updateBO') }}" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="product_id" value="{{$product->id_product}}">
                            <input type="hidden" name="backorder_status" value="2">
                            <button type="submit" class="btn btn-primary">Remove from Backorder List</button>
                        </form>                    
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    {{------------------------------------------- 
    ----
    ----    Product Description Mismatch
    ----
    ---------------------------------------------}}

    <h2>Products Description Mismatch</h2>
    @php
        $products = DB::connection('leeselectronic')->select('SELECT * 
            FROM (SELECT id_product, count(*) as mismatched 
                    FROM (SELECT id_product 
                            FROM `ps_product_lang` 
                            WHERE id_lang != 2 
                            GROUP BY id_product, description) as custom 
                    GROUP BY id_product) as mismatch_table 
            WHERE mismatched != 1');
    @endphp

    <button class="btn btn-sm btn-primary" onclick="toggle('prod_mismatch')">Toggle</button>

    <span>
        {{sizeof($products)}} Results found.
    </span>

    <table id="prod_mismatch" class="table table-bordered mt-2">
        <thead>
            <th>PID</th>
        </thead>
        <tbody>
            @foreach($products as $product)
                <tr>
                    <td><a href="{{config('custom.web_url')}}/product/{{$product->id_product}}.html" target="_blank">{{$product->id_product}}</a></td>

                </tr>
            @endforeach
        </tbody>
    </table>

    {{-- <a href="{{ route("online-product.syncDescription") }}"><button class="btn btn-secondary btn-sm">Sync All Product Description</button>

    </a> --}}

        {{------------------------------------------- 
    ----
    ----    Product Name Mismatch
    ----
    ---------------------------------------------}}

    <h2>Products Name Mismatch</h2>
    @php
    $products = App\ProductOnline::join('ps_product_lang','ps_product.id_product', 'ps_product_lang.id_product')
                                    ->select('ps_product.id_product', 'name', 'description_short')
                                    ->whereRaw('description_short NOT LIKE concat("%", name, "%") && description_short != "" && id_lang = 1')
                                    ->get()
    @endphp

    <button class="btn btn-sm btn-primary" onclick="toggle('prod_name_mismatch')">Toggle</button>

    <span>
        {{sizeof($products)}} Results found.
    </span>

    <table id="prod_name_mismatch" class="table table-bordered mt-2">
        <thead>
            <th>PID</th>
            <th>Name</th>
            <th>Description</th>
        </thead>
        <tbody>
            @foreach($products as $product)
                <tr>
                    <td><a href="{{config('custom.web_url')}}/product/{{$product->id_product}}.html" target="_blank">{{$product->id_product}}</a></td>
                    <td>{{$product->name}}</td>
                    <td>{{$product->description_short}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    {{------------------------------------------- 
    ----
    ----    Unit
    ----
    ---------------------------------------------}}

    <h2>Product Unit</h2>
    @php
        $products = App\ProductOnline::join('ps_product_shop','ps_product.id_product','=','ps_product_shop.id_product')
                                        ->select(DB::raw("ps_product.id_product, ps_product.unity AS product_unity, ps_product_shop.unity AS shop_unity") )
                                        ->whereRaw(DB::raw('(ps_product.unity = "" OR ps_product_shop.unity = "" OR ps_product.unity = "unit" OR ps_product_shop.unity = "unit") AND ps_product.active = 1'))
                                        ->get();
    @endphp

    <button class="btn btn-sm btn-primary" onclick="toggle('unity')">Toggle</button>

    <span>
        {{sizeof($products)}} Results found.
    </span>

    <table id="unity" class="table table-bordered mt-2">
        <thead>
            <th>PID</th>
            <th>Image</th>
            <th>Name</th>
            <th>WinPOS Unit</th>
            <th>ps_product Unit</th>
            <th>ps_product_shop Unit</th>
            <th>Action</th>
        </thead>
        <tbody>
            @foreach($products as $product)
                <tr>
                    <td><a href="{{config('custom.web_url')}}/product/{{$product->id_product}}.html" target="_blank">{{$product->id_product}}</a></td>
                    <td>@if($product->store)<img class="lazy" data-src="{{$product->store->image_url()}}" width="80px"/>@else - No Image - @endif</td>
                    <td>@isset($product->store->name) {{$product->store->name}} @endisset</td>
                    <td>@isset($product->store->unit) {{$product->store->unit}} @endisset</td>
                    <td>
                        {{$product->product_unity}}          
                    </td>
                    <td>
                        {{$product->shop_unity}}          
                    </td>
                    <td>
                        <form class="unityForm" action="{{ route('product.updateUnity') }}" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="id_product" value="{{$product->id_product}}">
                            <div class="input-group">
                                <input class="form-control" type="text" name="unity">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </div>
                        </form>         
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <!-- items that are active on fast search but not active on prestashop -->
    <h2>Products with Unsync Disabled Status</h2>

    @php
        $products = App\ProductOnline::join('table_1','table_1.id_product','=','ps_product.id_product')
                                        ->select(DB::raw("table_1.id_product,disabled,active") )
                                        ->whereRaw(DB::raw('(ps_product.active = 1 AND table_1.disabled = "Y") OR (ps_product.active = 0 AND table_1.disabled = "N")'))
                                        ->get();
    @endphp
    <button class="btn btn-sm btn-primary" onclick="toggle('unsync-disabled')">Toggle</button>

    <span>
        {{sizeof($products)}} Results found.
    </span>

    <table id="unsync-disabled" class="table table-bordered mt-2">
        <thead>
            <th>PID</th>
            {{-- <th>Image</th> --}}
            <th>Name</th>
            <th>Quantity</th>
            <th>WinPOS Status</th>
            <th>Prestashop Status</th>
        </thead>
        <tbody>
            @foreach($products as $product)
                <tr>
                    <td>{{$product->id_product}}</td>
                    <td>{{$product->store->name}}</td>
                    <td>{{$product->store->quantity}}</td>
                    <td>{{$product->disabled}}</td>
                    @if($product->active == 0)
                        <td>Y</td>
                    @else
                        <td>N</td>
                    @endif
                    
                </tr>
            @endforeach
        </tbody>
    </table>


    @include('layouts.error-flash')
    
@endsection

@push('bottom-scripts')
<script>
    function toggle(table){
        $('#' + table).toggle();
    }

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>

<script>
    $("form.onlineBackorderForm").submit(function(e) {
        var form = $(this);
        var url = form.attr('action');
        $.ajax({
            type: "POST",
            url: "{{route('online-product.updateBO')}}",
            data: form.serialize(), // serializes the form's elements.
            dataType: 'json',
            success: function(data) {
                if(data.status == "success"){
                    form.html('<span style="color:green">Successfully Updated! ' + data.message + '</span>');
                }else{
                    form.html('<span style="color:red">Update Failed. ' + data.message + '</span>');
                }
            },
            error: function(request, status, error){

                var header = '<span data-feather="alert-circle" style="width:50px;height:50px;stroke:white;"></span>';
                $('#messageModal .modal-header').css("background-color","#e66262");

                $('#messageModal .modal-title').html(header);

                feather.replace()

                $('#messageModal .modal-body #message').html(error);
                $('#messageModal').modal('show');

                form.html('<span style="color:red">Update Failed. Contact DevTeam. </span>');
            }
        }); 

        e.preventDefault(); // avoid to execute the actual submit of the form.
    });

</script>

<script>
    $("form.onlineOnlyForm").submit(function(e) {
        var form = $(this);
        var url = form.attr('action');
        $.ajax({
            type: "POST",
            url: "{{route('online-product.updateOnlineOnly')}}",
            data: form.serialize(), // serializes the form's elements.
            dataType: 'json',
            success: function(data) {
                if(data.status == "success"){
                    form.html('<span style="color:green">Successfully Updated! ' + data.message + '</span>');
                }else{
                    form.html('<span style="color:red">Update Failed. ' + data.message + '</span>');
                }
            },
            error: function(request, status, error){

                var header = '<span data-feather="alert-circle" style="width:50px;height:50px;stroke:white;"></span>';
                $('#messageModal .modal-header').css("background-color","#e66262");

                $('#messageModal .modal-title').html(header);

                feather.replace()

                $('#messageModal .modal-body #message').html(error);
                $('#messageModal').modal('show');

                form.html('<span style="color:red">Update Failed. Contact DevTeam. </span>');
            }
        }); 

        e.preventDefault(); // avoid to execute the actual submit of the form.
    });

</script>

<script>
        $("form.weightForm").submit(function(e) {
            var form = $(this);
            var url = form.attr('action');
            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(), // serializes the form's elements.
                dataType: 'json',
                success: function(data) {
                    if(data.status == "success"){
                        form.html('<span style="color:green">Successfully Updated! ' + data.message + '</span>');
                    }else{
                        form.html('<span style="color:red">Update Failed. ' + data.message + '</span>');
                    }
                },
                error: function(request, status, error){
    
                    var header = '<span data-feather="alert-circle" style="width:50px;height:50px;stroke:white;"></span>';
                    $('#messageModal .modal-header').css("background-color","#e66262");
    
                    $('#messageModal .modal-title').html(header);
    
                    feather.replace()
    
                    $('#messageModal .modal-body #message').html(error);
                    $('#messageModal').modal('show');
    
                    form.html('<span style="color:red">Update Failed. Contact DevTeam. </span>');
                }
            }); 
    
            e.preventDefault(); // avoid to execute the actual submit of the form.
        });
    
    </script>

    <script>
        $("form.unityForm").submit(function(e) {
            var form = $(this);
            var url = form.attr('action');
            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(), // serializes the form's elements.
                dataType: 'json',
                success: function(data) {
                    if(data.status == "success"){
                        form.html('<span style="color:green">Successfully Updated! ' + data.message + '</span>');
                    }else{
                        form.html('<span style="color:red">Update Failed. ' + data.message + '</span>');
                    }
                },
                error: function(request, status, error){

                    var header = '<span data-feather="alert-circle" style="width:50px;height:50px;stroke:white;"></span>';
                    $('#messageModal .modal-header').css("background-color","#e66262");

                    $('#messageModal .modal-title').html(header);

                    feather.replace()

                    $('#messageModal .modal-body #message').html(error);
                    $('#messageModal').modal('show');

                    form.html('<span style="color:red">Update Failed. Contact DevTeam. </span>');
                }
            }); 

            e.preventDefault(); // avoid to execute the actual submit of the form.
        });

    </script>

@include('functions.category-update-functions')

@endpush