function showDetailedView(onlineOrderNumber,url,online_url){

    var html = "<button class='btn btn-primary btn-sm mb-2' onclick='dismissDetailedView()'>Back</button>";
	$.ajax({
		url: url,
		data: 'order='+onlineOrderNumber,
		type: 'GET',
		dataType: 'json',
		success: function(response){

            if(response.status == "success"){
                var order = response["result_data"];

                html += "<div class='card'><div class='card-body'>";

                html += "<b>Order ID: <a href='" + online_url + "'>"+ order.id_order + "</a> (" + order.reference + ")</b><br>" 
                html += "Carrier: " + order.carrier_name + "<hr>";

                html += "<b>" + order.firstname + " " + order.lastname + "</b>";
                if(order.company){
                    html += " - " + order.company;
                }

                html += "<br>" + order.address1;
                if(order.address2){
                    html += "<br/>" + order.address2;
                }
                html += "<br/>" + order.city + " " + order.state + " " + order.postcode;
                html += "<br/>" + order.country;
                html += "<br/>" + order.phone;
                
                html += "<hr> <table class='table table-bordered >";
                order.cart_items.forEach(function(item){
                    var imageBox = "<td><img style='width:75px;' src="+ item.img_url +" data-src='" + item.img_url +"' data-toggle='modal' data-target='#imageModal'></img></td>";
                    html += "<tr class='cell" + item.id_product +"'>" + imageBox + " <td><b>" + item.id_product + " x " + item.quantity + "</b><br />" +item.name +"<hr class='my-2'/><span><i>SHELF: <u>"+ item.shelf_loc +"</u></i></span>"+"<br /><span><i>LOC: <u>"+ item.fraser_loc +"</u></i></span><hr class='my-2'/>";
                    html += "   <div>\
                                <button class='btn btn-primary btn-sm' onclick='checkedItem(this)'>Checked</button> \
                                <button class='btn btn-primary btn-sm' \
                                type='button' \
                                data-toggle='modal' \
                                data-target='#printModal' \
                                data-id='" + item.id_product +"'>Print</button>\
                                </div>";
                    html += "</td></tr>";
                    
                });
                
                html += "</table></div>" ;
                html += "</div></div>";

                html += "<div class='mt-3 text-center'>\
                            <a href='" + online_url + "#signatureDiv'>\
                            <button class='btn btn-sm btn-primary'\
                            type='button' \
                            '>Order Pick Up</button></a></div>";

            }else{
                html += "Failed to find order. " + response.message;
            }

            $('#order-details').html(html);	

            $('#side-panel-main').toggle(false);
            $('#order-details').toggle(true);

		},
		error: function(e){
            console.log(e.responseText);
            $('#order-details').html("Error, please contact development team. " + e.responseText);	
            $('#side-panel-main').toggle(false);
            $('#order-details').toggle(true);
		}

    });

}

function showDetailedNote(sidePanelNoteID, url){
    var html = "<button class='btn btn-primary btn-sm mb-2' onclick='dismissDetailedView()'>Back</button>";
	$.ajax({
        url: url,
		data: 'sidePanelNote='+sidePanelNoteID,
		type: 'GET',
		dataType: 'json',
		success: function(response){

            if(response.status == "success"){
                var sidePanelNote = response["result_data"];
                var employeeName = response["employee_name"];

                html += "<div class='card'><div class='card-body'>";

                html += "<b>Order ID: "+ sidePanelNote.online_id + "</b><br>"
                html += employeeName + " - " + sidePanelNote.notes

            }else{
                html += "Failed to find order note. " + response.message;
            }

            $('#order-details').html(html);	

            $('#side-panel-main').toggle(false);
            $('#order-details').toggle(true);

		},
		error: function(e){
            console.log(e.responseText);
            $('#order-details').html("Error, please contact development team. " + e.responseText);	
            $('#side-panel-main').toggle(false);
            $('#order-details').toggle(true);
		}
    });
}

function showMobileOrder(mobileOrderNumber,url,mobile_url){

    var html = "<button class='btn btn-primary btn-sm mb-2' onclick='dismissDetailedView()'>Back</button>";
	$.ajax({
		url: url,
		data: 'order='+mobileOrderNumber,
		type: 'GET',
		dataType: 'json',
		success: function(response){
            
            if(response.status == "success"){
                var order = response["result_data"];

                html += "<div class='card'><div class='card-body'>";

                html += "<b>Order ID: <a href='"+ mobile_url +"'>"+ order.order_id +"</a> (" + order.reference + ")</b><br>" 
                html += "Type: " + order.type + "<hr>";

                html += "<b>" + order.name_first + " " + order.name_last + "</b>";

                if(order.address_billing_id != 0){
                    if(order.company){
                        html += " - " + order.company;
                    }

                    html += "<br>" + order.address1;
                    if(order.address2){
                        html += "<br/>" + order.address2;
                    }
                    html += "<br/>" + order.city + " " + order.state + " " + order.postcode;
                    html += "<br/>" + order.country;
                    html += "<br/>" + order.phone;
                }else{
                    html += "<br/>" + order.email;
                }
                
                html += "<hr> <table class='table table-bordered >";
                order.cart_items.forEach(function(item){
                    var imageBox = "<td><img style='width:75px;' src="+ item.img_url +" data-src='" + item.img_url +"' data-toggle='modal' data-target='#imageModal'></img></td>";
                    html += "<tr class='cell" + item.id_product +"'>" + imageBox + " <td><b>" + item.id_product + " x " + item.pivot.qty + "</b><br />" +item.name +"<hr class='my-2' /><span><i>SHELF: <u>"+ item.shelf_loc +"</u></i></span>" + "<br /><span><i>LOC: <u>"+ item.fraser_loc +"</u></i></span><hr class='my-2' />" ;
                    html += "   <div>\
                                <button class='btn btn-primary btn-sm' onclick='checkedItem(this)'>Checked</button> \
                                <button class='btn btn-primary btn-sm' \
                                type='button' \
                                data-toggle='modal' \
                                data-target='#printModal' \
                                data-id='" + item.id_product +"'>Print</button>\
                                </div>";
                    html += "</td></tr>";
                    
                });
                
                html += "</table></div>" ;
                html += "</div></div>";

                html += "<div class='mt-3 text-center'>\
                            <a href='" + mobile_url + "#signatureDiv'>\
                            <button class='btn btn-sm btn-primary'\
                            type='button' \
                            '>Order Pick Up</button></a></div>";

            }else{
                html += "Failed to find order. " + response.message;
            }

            $('#order-details').html(html);	

            $('#side-panel-main').toggle(false);
            $('#order-details').toggle(true);

		},
		error: function(e){
            console.log(e.responseText);
            $('#order-details').html("Error, please contact development team. " + e.responseText);	
            $('#side-panel-main').toggle(false);
            $('#order-details').toggle(true);
		}

    });

}

function showStoreOrder(order_id,url,detail_url){

    var html = "<button class='btn btn-primary btn-sm mb-2' onclick='dismissDetailedView()'>Back</button>";

	$.ajax({
		url: url,
		data: 'order='+order_id,
		type: 'GET',
		dataType: 'json',
		success: function(response){

            if(response.status == "success"){
                var order = response["result_data"];


                html += "<div class='card'><div class='card-body'>";

                html += "<b>Order ID: <a href="+ detail_url +">#"+ order.id + "</a> ("+ order.type +")</b>";
                if(order.invoice_id == '1234567'){
                    html += '<span class="badge badge-danger ml-2">UNPAID</span>';
                }
                html += "<br>";
                html += "Order Status: <b>" + order.status_name + "</b>";
                html += "<br>";
                html += "Tracking #:  " + order.tracking_id + "<br>";
                html += "Created By:  " + order.employee + "<hr>";

                html += "<b>" + order.name + "</b>";

                html += "<br>" + order.email;
                html += "<br/>" + order.phone;
                
                html += "<hr> <table class='table table-bordered >";
                order.cart_items.forEach(function(item){
                    var imageBox = "<td><img style='width:75px;' src="+ item.img_url +" data-src='" + item.img_url +"' data-toggle='modal' data-target='#imageModal'></img></td>";
                    html += "<tr class='cell" + item.id_product +"'>" + imageBox + " <td><b>" + item.id_product + " x " + item.pivot.quantity + "</b><br />" +item.pivot.name +"<hr class='my-2'/><span><i>SHELF: <u>"+ item.shelf_loc +"</u></i></span>"+"<br /><span><i>LOC: <u>"+ item.fraser_loc +"</u></i></span><hr class='my-2'/>";
                    html += "   <button class='btn btn-primary btn-sm' onclick='checkedItem(this)'>Checked</button> \
                                <button class='btn btn-primary btn-sm' \
                                type='button' \
                                data-toggle='modal' \
                                data-target='#printModal' \
                                data-id='" + item.id_product +"'>Print</button>";
                    html += "</td></tr>";
                    
                });
                
                html += "</table></div>" ;
                html += "</div></div>";

                html += "<div class='mt-3 text-center'>\
                            <a href='" + detail_url + "#signatureDiv'>\
                            <button class='btn btn-sm btn-primary'\
                            type='button' \
                            '>Order Pick Up</button></a></div>";

            }else{
                html += "Failed to find order. " + response.message;
            }

            $('#order-details').html(html);	

            $('#side-panel-main').toggle(false);
            $('#order-details').toggle(true);

		},
		error: function(e){
            console.log(e.responseText);
            $('#order-details').html("Error, please contact development team. " + e.responseText);	
            $('#side-panel-main').toggle(false);
            $('#order-details').toggle(true);
		}

    });

}


function dismissDetailedView(){
    $('#order-details').toggle(false);
    $('#side-panel-main').toggle(true);
}

function checkedItem(btn){
    if($(btn).html() == 'Checked'){
        $(btn).parent().parent().css('background','#28a745')
        $(btn).html('Uncheck') 
    }    
    else{
        $(btn).parent().parent().css('background','transparent')
        $(btn).html('Checked') 
    }
}


