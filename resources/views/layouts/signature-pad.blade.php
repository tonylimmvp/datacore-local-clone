{{-- Signature Pad Modal --}}

<div class="modal fade" id="signatureModal" tabindex="-1" role="dialog" aria-labelledby="signatureModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title" id="exampleModalLabel">Signature for Order#<span id="signatureOrderId">---</span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
              <form id="signatureForm" method="POST">
                {{csrf_field()}}
                <p>This order is for <span id="signatureName" style="font-weight: bold;">-- CUSTOMER NAME --</span>. By signing, you agree to our terms & condition that this item has been picked up.</p>
                <div class="input-group mb-3">
                    <label class="col-sm-3 col-lg-2" for="pick_up_name"> Pick Up Name</label>
                    <div class="col-sm-9 col-lg-10">
                      <input class="form-control form-control-sm" id="pick_up_name" name="signature_pickup" required>  
                    </div>
                </div>
                <div class="wrapper text-center">
                  <canvas id="signature-pad" 
                          class="signature-pad" 
                          style="border:#e2e2e2 1px solid;width:100%;height:100%"></canvas>
                  <hr>
                  <div id="signature-pad-footer mb-3">
                    <small>Please sign above</small>
                  </div>
                </div>
                <input type="hidden" id="signature_data" name="signature_data" class="form-control" value="">
                <input type="hidden" id="signature_type" name="signature_type" class="form-control" value="">
                <input type="hidden" id="signature_order_id" name="signature_order_id" class="form-control" value="">
                <div id="signatureError" class="alert alert-danger text-center"></div>
                <div class="text-center mt-2">
                  <button class="btn btn-sm btn-primary" id="saveSignatureButton">Save</button>
                  <button class="btn btn-sm btn-primary" id="clearSignatureButton">Clear</button>
                </div>
              </form>
            </div>
        </div>
    </div>
</div>

{{-- End Signature Pad  Modal --}}

@push('bottom-scripts')

<script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
@include('functions.signature-functions')
<script>
  var form = $('#signatureForm');
  var canvas = document.getElementById('signature-pad');
  $('#signatureError').toggle(false)

  var signaturePad = new SignaturePad(canvas, {
    penColor: 'rgb(0, 0, 0)'
  });
  var saveButton = document.getElementById('save');
  var cancelButton = document.getElementById('clear');

  form.submit(function( event ) {
    event.preventDefault(); // avoid to execute the actual submit of the form.

    $('#saveSignatureButton').attr('disabled', true);

    // Check if signature pad is empty
    if( signaturePad.isEmpty()){
      $('#signatureError').text("Signature must be signed.")
      $('#signatureError').toggle(true);
      $('#saveSignatureButton').attr('disabled', false);
      return false;            
    }
    // Updating signature data
    var data = trimCanvas(canvas);
    document.querySelector('[name=signature_data]').value = data;

    //Submitting form
    $.ajax({
        url: form.attr( 'action' ),
        data: form.serialize(),
        type: 'POST',
        dataType: 'JSON',
        success: function(results) {
          if(results.status == "success"){
            if(results.url == "refresh"){
              window.location.reload()
            }else{
              window.location.href = results.url;
            }
            signaturePad.clear();
          }else{
            $('#signatureError').text(results.message)
            $('#signatureError').toggle(true)
            $('#saveSignatureButton').attr('disabled', false);
          }
        },
        error: function(xhr, status, error){
          var errorMessage = xhr.status + ': ' + xhr.statusText
          $('#signatureError').text(errorMessage + " Please contact DevTeam")
          $('#signatureError').toggle(true)
          $('#saveSignatureButton').attr('disabled', false);
        }
    });

  });

  $('#clearSignatureButton').click(function (event) {
    event.preventDefault(); // avoid to execute the actual submit of the form.
    resizeCanvas(canvas)
    signaturePad.clear();
  });

</script>

<script>
    $(document).ready(function () {
        $('#signatureModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var id = button.data('id') // Extract info from data-* attributes
            var name = button.data('name') // Extract info from data-* attributes
            var type = button.data('type') // Extract info from data-* attributes
            
            var modal = $(this)
            modal.find('#signatureOrderId').text(id)
            modal.find('#signatureName').text(name)
            modal.find('#signature_order_id').val(id)
            modal.find('#signature_type').val(type)
            // modal.find('#pick_up_name').attr('placeholder', "-- " + name + " --")

            //Change action url according to type of order
            if(type == "online"){
              var url = "{{ route('signature.saveOnline') }}";
            }
            else if(type == "order"){
              var url = "{{ route('signature.saveOrder') }}";
            }
            else if(type == "mobile"){
              var url = "{{ route('signature.saveMobile') }}";
            }

            modal.find('form').attr('action',url)

        });

        $('#signatureModal').on('shown.bs.modal', function (event) {
          canvas.width = $('#signature-pad').parent().outerWidth()
          canvas.height = 250;
        })

        $('#signatureModal').on('hidden.bs.modal', function (event) {
          $('#pick_up_name').val('')
          signaturePad.clear()
          $('#signatureError').toggle(false)
        })
        
    });
</script>
@endpush

