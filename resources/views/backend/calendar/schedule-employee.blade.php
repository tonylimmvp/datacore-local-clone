{{-- Modal --}}

{{-- create modal --}}
<!-- Modal -->
@if(Auth::user()->hasAccess('events')->create)
<div class="modal fade" id="createEventModal" tabindex="-1" role="dialog" aria-labelledby="createEventModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form id="createEventForm" method="POST" action="{{ route('coreui.event.create') }}">
                <div class="modal-header">
                    <h5 class="modal-title" id="createEventModalTitle">Create Event</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{csrf_field()}}

                    <div class="form-group">
                        <select class="form-control" name="type">
                            @foreach(App\EventType::whereNotIn('id',[2,3,7])->get() as $type)
                                <option value="{{$type->id}}"> {{ $type->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <input class="form-control" name="title" placeholder="Title">
                    </div>
                    
                    <div class="form-row">
                        <div class="form-group col-6">
                            <input id="event_start" class="form-control" name="start" placeholder="Start Date" autocomplete="off" required>
                        </div>
                        <div class="form-group col-6 event-time">
                            <input id="event_start_time" class="form-control" name="start_time" placeholder="Start Time (09:00 or 10:00)">
                        </div>
                        <div class="form-group col-6">
                            <input id="event_end" class="form-control" name="end" placeholder="End Date" autocomplete="off" required>
                        </div>
                        <div class="form-group col-6 event-time">
                            <input id="event_end_time" class="form-control" name="end_time" placeholder="End Time (17:00 or 18:00)">
                        </div>
                    </div>
    
                    <div class="form-group">
                        <select class="form-control" disabled>
                            <option value="{{ $employee->id }}">{{ $employee->name }}</option>
                        </select>
                    </div>
                    
                    <input type="hidden" name="employee" value="{{$employee->id}}">
                    <input type="hidden" name="all_day" value="0">
                    <div id="create_error" class="alert alert-danger modal-error d-none mt-2"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Create Event</button>
                </div>
            </form>
        </div>
    </div>
    </div>
    @endif
    
    {{-- edit modal --}}
    @if(Auth::user()->hasAccess('events')->update)
    <div class="modal fade" id="editEventModal" tabindex="-1" role="dialog" aria-labelledby="editEventModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form id="editEventForm" method="POST" action="{{ route('coreui.event.update') }}">
            <div class="modal-header">
                <h5 class="modal-title" id="editEventModalTitle">Edit Event</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <input type="hidden" name="event_id">

                <div class="form-group">
                    <select class="form-control" name="type">
                        @foreach(App\EventType::whereNotIn('id',[2,3,7])->get() as $type)
                        <option value="{{$type->id}}"> {{ $type->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <input class="form-control" name="title" placeholder="title" required>
                </div>

                <div class="form-row">
                    <div class="form-group col-6">
                        <input id="edit_event_start" class="form-control" name="start" placeholder="Start Date" autocomplete="off" required>
                    </div>
                    <div class="form-group col-6 event-time">
                        <input id="edit_event_start_time" class="form-control" name="start_time" placeholder="Start Time (09:00 or 10:00)">
                    </div>
                    <div class="form-group col-6">
                        <input id="edit_event_end" class="form-control" name="end" placeholder="End Date" autocomplete="off" required>
                    </div>
                    <div class="form-group col-6 event-time">
                        <input id="edit_event_end_time" class="form-control" name="end_time" placeholder="End Time (17:00 or 18:00)">
                    </div>
                </div>
    
                <select class="form-control" disabled>
                    <option value="{{ $employee->id }}">{{ $employee->name }}</option>
                </select>
                <a id="employee_url" class="btn btn-info btn-sm mt-2">View Employee's Schedule</a>
    
                <input type="hidden" name="employee" value="{{$employee->id}}">
                <input type="hidden" name="all_day" value="0">
                <div id="update_error" class="alert alert-danger modal-error d-none mt-2"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Update Event</button>
                @if(Auth::user()->hasAccess('events')->delete)
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteEventModal">Delete Event</button>
                @endif
            </div>
        </form>
        </div>
    </div>
    </div>
    @endif

    {{-- delete confirmation --}}
    @if(Auth::user()->hasAccess('events')->delete)
    <div class="modal fade" id="deleteEventModal" tabindex="-1" role="dialog" aria-labelledby="deleteEventModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form id="deleteEventForm" method="POST" action="{{ route('coreui.event.delete') }}">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteEventModalTitle">Delete Event</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{ csrf_field() }}
                    <input type="hidden" name="event_id">
                    <p>Are you sure you want to delete this event?</p>
                    <div id="delete_error" class="alert alert-danger modal-error d-none mt-2"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Delete Event</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    @endif
    
    @push("bottom-scripts")
    
    <script>
        $(document).ready(function(){
            // Datepicker
            $('#event_start').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'yyyy-mm-dd'
            });
            $('#event_end').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'yyyy-mm-dd'
            });
            $('#edit_event_start').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'yyyy-mm-dd'
            });
            $('#edit_event_end').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'yyyy-mm-dd'
            });
    
            // Timepicker
            $('#event_start_time, #edit_event_start_time').timepicker({
                timeFormat: 'HH:mm',
                interval: 60,
                minTime: '9',
                maxTime: '6:00pm',
                defaultTime: '9:00',
                startTime: '9:00am',
                dynamic: false,
                dropdown: true,
                scrollbar: true
            });
    
            $('#event_end_time, #edit_event_end_time').timepicker({
                timeFormat: 'HH:mm',
                interval: 60,
                minTime: '9',
                maxTime: '6:00pm',
                defaultTime: '17:00',
                startTime: '9:00am',
                dynamic: false,
                dropdown: true,
                scrollbar: true
            });
        });
    </script>
    
    <script>
        
        $('#createEventModal,#editEventModal').on('show.bs.modal', function (event) {
            var type = $(this).find('[name="type"]').val()
            if(type == "1" || type == "4" || type == "5" || type == "6"){
                $('input[name="title"]').hide()
            }else{
                $('input[name="title"]').show()
            }
            if(type == "3" || type == "4" || type == "6"){
                $(this).find('.event-time').hide()
                $(this).find('input[name="all_day"]').val("1")
            }else{
                $(this).find('.event-time').show()
                $(this).find('input[name="all_day"]').val("0")
            }
            
        })
    
        $( "#createEventModal, #editEventModal" ).change(function() {
            var type = $(this).find('[name="type"]').val()
            if(type == "1" || type == "4" || type == "5" || type == "6"){
                $('input[name="title"]').hide()
            }else{
                $('input[name="title"]').show()
            }
            if(type == "3" || type == "4" || type == "6"){
                $(this).find('.event-time').hide()
                $(this).find('input[name="all_day"]').val("1")
            }else{
                $(this).find('.event-time').show()
                $(this).find('input[name="all_day"]').val("0")
            }
        });

        $('#createEventModal,#editEventModal,#deleteEventModal').on('hide.bs.modal', function (event) {
            $(this).find('.modal-error').addClass('d-none')
        })
    </script>
    
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var Calendar = FullCalendar.Calendar;
            var calendarEl = document.getElementById('calendar');
        
            var calendar = new Calendar(calendarEl, {
                plugins: [ 'interaction','dayGrid','timeGrid','list' ],
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
                },
                defaultView: 'dayGridMonth',
                views: {
                    dayGrid: {
                        eventTimeFormat: {
                            hour: 'numeric',
                            minute: '2-digit',
                            meridiem: 'short'
                        },
                    },
                    timeGrid: {
                        eventTimeFormat: {
                            hour: 'numeric',
                            minute: '2-digit',
                        },
                    },
                },
                displayEventEnd: true, 
                hiddenDays: [0],
                businessHours: [
                    {
                        daysOfWeek: [ 1, 2, 3, 4, 5 ],
                        startTime: '09:00', 
                        endTime: '18:00' 
                    },
                    {
                        daysOfWeek: [ 6 ], 
                        startTime: '09:00', 
                        endTime: '17:00' 
                    }
                ],
                eventSources: [
                    {
                        events: [
                            @foreach($events as $event)
                            {
                                id: '{{ $event->id }}',
                                type: '{{ $event->type_id }}',
                                @if($event->type_id == 4 || $event->type_id == 6)
                                title : '{{ $event->type->name}} - {{ $event->employee->name}}',
                                @elseif($event->type_id == 2 || $event->type_id == 7 || $event->type_id == 8)
                                title : '{{ $event->title}}',
                                @else
                                title : '{{ $event->employee->name}}',
                                @endif
                                start : '{{ $event->start }}',
                                @if($event->end)
                                    end: '{{ $event->end }}',
                                @endif
                                allDay: !!parseInt('{{$event->all_day}}'),
                                @if(\Carbon\Carbon::parse($event->start)->format('H:i') != '09:00' && $event->type_id == 1)
                                    backgroundColor: 'saddlebrown', 
                                    borderColor: 'saddlebrown',                               
                                @else
                                    backgroundColor: '{{ $event->type->bg_color }}', 
                                    borderColor: '{{ $event->type->bg_color }}',
                                @endif
                                textColor: '{{ $event->type->text_color }}',
                                employeeId: '{{$event->employee_id}}',
                                employeeName: '{{$event->employee->name}}',
                            },
                            @endforeach
                        ],
                    },
                    {
                        events: [
                            @foreach($holidays as $event)
                            {
                                id: '{{ $event->id }}',
                                type: '{{ $event->type_id }}',
                                title : '{{ $event->title}}',
                                start : '{{ $event->start }}',
                                @if($event->end)
                                    end: '{{ $event->end }}',
                                @else
                                    allDay: true,
                                @endif
                                backgroundColor: '{{ $event->type->bg_color }}', 
                                borderColor: '{{ $event->type->bg_color }}',
                                textColor: '{{ $event->type->text_color }}',
                                employeeId: '{{$event->employee_id}}'
                            },
                            @endforeach
                        ],
                    },
                ],
                dayRender: function (arg) {
                    var current_date = moment(arg.date).valueOf()
                    @foreach($holidays as $event)
                        var holiday = moment("{{$event->start}}").valueOf()
                        if (current_date == holiday) {
                            arg.el.bgColor = 'lightcyan'
                        }
                    @endforeach
                },
                eventClick: function(arg){
                    if(arg.event.extendedProps.type != 3){
                        var modal = $('#editEventModal')

                        modal.find("input[name='start']").val(moment(arg.event.start).format('YYYY-MM-DD'))
                        modal.find("input[name='start_time']").val(moment(arg.event.start).format('HH:mm'))
                        modal.find("input[name='end_time']").val(moment(arg.event.end).format('HH:mm'))
                        if(arg.event.allDay){
                            modal.find("input[name='end']").val(moment(arg.event.end).subtract(1, 'days').format('YYYY-MM-DD'))
                        }else{
                            modal.find("input[name='end']").val(moment(arg.event.end).format('YYYY-MM-DD'))
                        }

                        modal.find("input[name='title']").val(arg.event.title)
                        modal.find("input[name='event_id']").val(arg.event.id)
                        modal.find("select[name='type'] option[value='"+arg.event.extendedProps.type+"']").prop('selected',true)
                        modal.find("select[name='employee'] option[value='"+arg.event.extendedProps.employeeId+"']").prop('selected',true)

                        // URL
                        var employee_url = ("{{route('coreui.schedule-employee','')}}/"+ arg.event.extendedProps.employeeId)
                        modal.find("#employee_url").attr('href',employee_url)
                        modal.find("#employee_url").text('View ' + arg.event.extendedProps.employeeName + '\'s Schedule')

                        @if(Auth::user()->hasAccess('events')->delete)
                        $('#deleteEventModal').find("input[name='event_id']").val(arg.event.id)
                        @endif

                        modal.modal('show')
                    }
                },
                
            });
    
            calendar.render();
    
            // Form submission
            @if(Auth::user()->hasAccess('events')->create)
            $("form#createEventForm").submit(function(e) {
                e.preventDefault(); // avoid to execute the actual submit of the form.
                var form = $(this);
                var url = form.attr('action');

                $.ajax({
                    type: "POST",
                    url: url,
                    data: form.serialize(), // serializes the form's elements.
                    dataType: 'json',
                    success: function(data){
                        if(data.status == "success"){
                            calendar.addEvent({ 
                                id: data.event.id,
                                type: data.event.type_id,
                                title : data.event.title,
                                start : data.event.start,
                                end: data.event.end,
                                allDay: !!parseInt(data.event.all_day),
                                backgroundColor: data.event.backgroundColor, 
                                borderColor: data.event.backgroundColor,
                                textColor: data.event.textColor,
                                employeeId: data.event.employee_id,
                                employeeName: data.event.name
                            })
                            $('#createEventModal').modal('hide')
                        }else{
                            $('#create_error').html('<p>'+ data.message +'</p>')
                        }
                        
                    },
                    error: function(request, status, error){
                        $('#create_error').html('<p>'+ status +': ' + error + '</p>')
                    }
                });
        
            });
            @endif

            @if(Auth::user()->hasAccess('events')->update)
            $("#editEventForm").submit(function(e) {
                e.preventDefault(); // avoid to execute the actual submit of the form.
                var form = $(this);
                var url = form.attr('action');

                $.ajax({
                    type: "POST",
                    url: url,
                    data: form.serialize(), // serializes the form's elements.
                    dataType: 'json',
                    success: function(data){
                        if(data.status == "success"){
                            var event = calendar.getEventById(form.find('input[name="event_id"]').val())
                            if(data.event.type_id == 2 || data.event.type_id == 7 || data.event.type_id == 8)
                                event.setProp('title', data.event.title)
                            else if(data.event.type_id == 1){
                                event.setProp('title', data.event.name)
                            }else if(data.event.type_id == 4){
                                event.setProp('title', 'Vacation - ' + data.event.name)
                            }else if(data.event.type_id == 6){
                                event.setProp('title', 'Day Off - ' + data.event.name)
                            }
                            event.setStart(data.event.start)
                            event.setEnd(data.event.end)
                            event.setAllDay(!!parseInt(data.event.all_day))
                            event.setExtendedProp('employeeId',data.event.employee_id)
                            event.setExtendedProp('employeeName',data.event.name)
                            event.setExtendedProp('type',data.event.type_id)
                            event.setProp('backgroundColor',data.event.backgroundColor)
                            event.setProp('borderColor',data.event.backgroundColor)
                            event.setProp('textColor',data.event.textColor)

                            // URL
                            var employee_url = ("{{route('coreui.schedule-employee','')}}/"+ data.event.extendedProps.employeeId)
                            form.find("#employee_url").attr('href',employee_url)
                            form.find("#employee_url").text('View ' + data.event.extendedProps.employeeName + '\'s Schedule')

                            $('#editEventModal').modal('hide')
                            calendar.render(); 
                        }else{
                            $('#update_error').html('<p>'+ data.message +'</p>')
                        }
                    },
                    error: function(request, status, error){
                        $('#update_error').html('<p>'+ status +': ' + error + '</p>')
                    }
                });
        
            });
            @endif

            @if(Auth::user()->hasAccess('events')->delete)
            $("#deleteEventForm").submit(function(e) {
                e.preventDefault(); // avoid to execute the actual submit of the form.
                var form = $(this);
                var url = form.attr('action');
    
                $.ajax({
                    type: "POST",
                    url: url,
                    data: form.serialize(), // serializes the form's elements.
                    dataType: 'json',
                    success: function(data){
                        if(data.status == "success"){
                            var event = calendar.getEventById($('#deleteEventModal input[name="event_id"]').val())
                            event.remove()
                            $('#deleteEventModal').modal('toggle')
                            $('#editEventModal').modal('toggle')
                        }else{
                            $('#delete_error').html('<p>'+ data.message +'</p>')
                        }
                    },
                    error: function(request, status, error){
                        $('#delete_error').html('<p>'+ status +': ' + error + '</p>')
                    }
                });
        
            });
            @endif
        });
    </script>
    
    @endpush
    