@component('mail::message')
#**Hello There!**

@if(strpos(strtolower($notifier->customer_email),'leeselectronic.com') !== false)
This is a notification that an item requested from a customer ({{$notifier->name}}) has arrived. Please note that these items are **unpaid** for and is subjected to a **first come first serve** basis. The customer has requested to be called at <a href="tel:{{$notifier->phone}}">{{$notifier->phone}}</a>.

Please double check the quantity the customer is interested in.
@else
This is a notification to inform you that an item you were interested in is now available. Please note that this item is **not reserved** and is subjected to a **first come first serve** basis.

It is highly recommended to call in to confirm the stock of the item due to the **first come first serve** policy. 
@endif

<hr>

<h1 style="text-align:center">PID: #{{$notifier->product->id_product}}</h1>

<p style="text-align:center">{{$notifier->product->name}}</p>

<div style="text-align:center">
    <img src="{{$notifier->product->image_url()}}">
</div>

@component('mail::button', ['url' => config('custom.web_url').'/product/'.$notifier->product->id_product.'.html'])
Shop Now
@endcomponent

Regards,<br>
Lee's Electronic Components
@endcomponent
