@extends('frontend.layouts.main')

@section('content')

<h1>Braintree Transaction Report</h1>

@include('layouts.errors')

@if(sizeof($transactions)>0)

<div class="table-responsive">
<table class="table">
    <tr>
        <th> TID </th>
        <th> Customer Name </th>
        {{-- <th> Order # </th> --}}
        <th> Card </th>
        <th> Type </th>
        <th> Amount </th>
        <th> Response </th>
        <th> Status History </th>
        <th> Date Created</th>
    </tr>
    
    @foreach($transactions as $transaction)
    
    {{-- <tr><td>{{var_dump($transaction)}}</td></tr> --}}

    <tr @if($transaction->type != "sale")style="background:#ff000021" @endif>
        <td> {{$transaction->id}} </td>
        
        <td> 
            @if($transaction->paymentInstrumentType == "credit_card")
                {{$transaction->customer['firstName']}} {{$transaction->customer['lastName']}} <br>
                {{$transaction->customer['email']}}

            @elseif($transaction->paymentInstrumentType == "paypal_account")
                {{$transaction->paypalDetails->payerFirstName}} {{$transaction->paypalDetails->payerLastName}} <br>
                {{$transaction->paypalDetails->payerEmail}}

            @endif
            @if($transaction->customer["phone"])<br> {{$transaction->customer["phone"]}} @endif
        </td>

        {{-- <td> {{$transaction->orderId}} </td> --}}

        <td> 
            @if($transaction->paymentInstrumentType == "credit_card")
                @if($transaction->creditCard["cardType"] == "Visa")
                VI
                @elseif($transaction->creditCard["cardType"] == "MasterCard")
                MC
                @endif
                ****{{$transaction->creditCard["last4"]}}
            @elseif($transaction->paymentInstrumentType == "paypal_account")
                Paypal
            @endif
        </td>

        <td> {{$transaction->type}}</td>

        <td> ${{$transaction->amount}} </td>

        <td> {{$transaction->status}} </td>

        <td>
            @foreach($transaction->statusHistory as $status)
                
                <span class="badge badge-secondary">
                    {{Carbon\Carbon::parse($status->timestamp->format('Y-m-d H:i:s'))->format('Y-m-d g:iA')}} 
                    - 
                    {{$status->status}}
                </span>
                <br>
            @endforeach
        </td>

        <td> {{Carbon\Carbon::parse($transaction->createdAt->format('Y-m-d H:i:s'))->format('Y-m-d g:iA')}} </td>

    </tr>
    @endforeach
    
</table>
</div>
@else
No results found
@endif

@endsection