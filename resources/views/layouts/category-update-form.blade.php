<form class="categoryForm" action="{{ route('categories.add') }}" method="POST">
    {{ csrf_field() }}
    <input type="hidden" value="{{$product->id_product}}" name="pid"/>
    <div class="input-group pt-4">
        <input class="form-control form-control-sm category-input" name="category"  autocomplete="off">
        <div class="input-group-append">
            <button class="btn btn-sm btn-primary">Update</button>
        </div>
    </div>
    <p class="text-center pt-3 category-name"></p>
</form>