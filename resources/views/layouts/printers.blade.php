{{-- Print Modal --}}
{{-- Add Product Modal --}}

<div class="modal fade" id="printModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title" id="exampleModalLabel">Print PID#<span id="printProduct">---</span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                @php $printers_upstairs = App\LabelPrinter::where('location', 'UPSTAIRS')->get(); @endphp
                {{-- Reverse the printer list, requested by Raymond --}}
                @php $printers = $printers_upstairs->reverse(); @endphp
                <h5 class="text-center">UPSTAIRS</h5>
                @foreach($printers as $printer)
                    @if($printer->location == "UPSTAIRS")
                        <div class="text-center">
                            <img class="img-fluid" src="{{asset('img/'.$printer->type.'.png')}}" style="height:100px">
                            <div class="btn-group">
                                <button class="btn btn-outline-secondary" onclick="printLabel({{$printer->id}},'{{route('print.index')}}')">Normal</button>
                                <button class="btn btn-outline-secondary" onclick="printQuantityLabel({{$printer->id}},'{{route('print.quantity')}}')">Quantity</button>
                                <button class="btn btn-outline-secondary" onclick="printNoPriceLabel({{$printer->id}},'{{route('print.noprice')}}')">No Price</button>
                            </div>
                        </div>
                    @endif
                @endforeach

                {{-- Postek --}}
                <div class="text-center">
                    <img class="img-fluid" src="{{asset('img/postek.png')}}" style="height:100px">
                    <div class="btn-group">
                        <a id="postek-price" class="btn btn-outline-secondary" onclick="printPostekPrice()" target="_blank" href="#">Normal</a>
                        <a id="postek-quantity" class="btn btn-outline-secondary" onclick="printPostekQuantity()" target="_blank" href="#">Quantity</button>
                        <a id="postek-no-price" class="btn btn-outline-secondary" onclick="printPostekNoPrice()" target="_blank" href="#">Location</a>
                    </div>
                </div>
                <hr>

                <h5 class="text-center">DOWNSTAIRS</h5>
                @php $printers_downstairs = App\LabelPrinter::where('location', 'DOWNSTAIRS')->get(); @endphp
                @foreach($printers_downstairs as $printer)
                    @if($printer->location == "DOWNSTAIRS")
                        <div class="text-center">
                            <img class="img-fluid" src="{{asset('img/'.$printer->type.'.png')}}" >
                            <div class="btn-group">
                                <button class="btn btn-outline-secondary" onclick="printLabel({{$printer->id}},'{{route('print.index')}}')">Normal</button>
                                <button class="btn btn-outline-secondary" onclick="printQuantityLabel({{$printer->id}},'{{route('print.quantity')}}')">Quantity</button>
                                <button class="btn btn-outline-secondary" onclick="printNoPriceLabel({{$printer->id}},'{{route('print.noprice')}}')">
                                    @if($printer->id == "3")
                                    Location
                                    @else
                                    No Price
                                    @endif
                                </button>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</div>

@include('functions.printers.printer-functions')

{{-- End Print Modal --}}

@push('bottom-scripts')
<script>
    $(document).ready(function () {
        $('#printModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var id = button.data('id') // Extract info from data-* attributes

            var modal = $(this)
            modal.find('#printProduct').text(id)
        });
    });
</script>
@endpush