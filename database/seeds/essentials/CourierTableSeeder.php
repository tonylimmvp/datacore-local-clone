<?php

use Illuminate\Database\Seeder;

class CourierTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('couriers')->insert([
            'name' => 'Yet Unknown',
        ]);

        DB::table('couriers')->insert([
            'name' => 'Canada Post',
            'url' => 'https://www.canadapost.ca/trackweb/en#/search?searchFor=',
        ]);

        DB::table('couriers')->insert([
            'name' => 'FedEx',
            'url' => 'https://www.fedex.com/apps/fedextrack/?action=track&tracknumbers=',
        ]);

        DB::table('couriers')->insert([
            'name' => 'USPS',
            'url' => 'https://tools.usps.com/go/TrackConfirmAction?tLabels=',
        ]);

        DB::table('couriers')->insert([
            'name' => 'DHL',
            'url' => 'https://www.logistics.dhl/ca-en/home/tracking.html?tracking-id=',
        ]);

        DB::table('couriers')->insert([
            'name' => 'Purolator',
            'url' => 'https://www.purolator.com/purolator/ship-track/tracking-details.page?pin=',
        ]);

        DB::table('couriers')->insert([
            'name' => 'UPS',
            'url' => 'https://wwwapps.ups.com/tracking/tracking.cgi?tracknum=',
        ]);

        DB::table('couriers')->insert([
            'name' => 'Active International',
        ]);
    }
}
