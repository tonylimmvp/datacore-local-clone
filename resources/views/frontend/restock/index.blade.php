@extends('frontend.layouts.main')

@include('layouts.datatables')

@section('content')

<h1> Restock Board </h1>

<form method="post" action="{{ route('restock.store') }}">
    {{ csrf_field() }}
    <div class="input-group col-sm-6 col-lg-3">
    <input id="product-id" class="form-control" name="pid" type="number" placeholder="Enter PID...">
    <div class="input-group-append">   
    <button class="btn btn-primary">Add to List</button>
    </div>
</div>
<p id="product-name" class="ml-4 mt-2"> --- </p>
</form>

@include('layouts.errors')
<div class="table-responsive">
<table id="restock-board" class="table table-bordered mt-2" width="100%">
<thead>
    <tr>
        <th>PID</th>
        <th>Name</th>
        <th>Price</th>
        <th>Quantity</th>
        <th>Shelf Location</th>
        <th>Storage Location</th>
        <th>Image</th>
        <th class="text-center">Action</th>
        <th>Requested Date</th>
        <th></th>
    </tr>
</thead>
<tbody>
    @foreach($restockList as $product)
    <tr 
    @if($product->info->quantity < 10)
        style="background-color:#fff3e4"
    @endif
    >
        <td>{{$product->info->id_product}}</td>
        <td>{{$product->info->name}}</td>
        <td>${{$product->info->price}}</td>
        <td>{{$product->info->quantity}}</td>
        <td><span class="cursor-pointer" onclick="displayShelfLoc('{{$product->info->shelf_loc}}')">{{$product->info->shelf_loc}}</span></td>
        <td class="text-center">
            {{$product->info->locations()}} 
        </td>
        <td class="text-center" width="100px">
            @if($product->info->image_url())
            <img class="lazy product-image" data-src="{{ $product->info->image_url()}}" width="100px" data-toggle="modal" data-target="#imageModal">
            @else
            <a href="#">Upload</a>
            @endif
        </td>
        <td class="text-center">
            <button class="btn btn-primary btn-sm btn-action mb-1" data-toggle="modal" data-target="#printModal" data-id="{{$product->info->id_product}}">Print Button</button>
            
            @php 
                $uniquity = DB::table('order_suggestions')->where('product_id', '=', $product->info->id_product)->count(); 
            @endphp

            @if ($uniquity == '1')
                <form class="form-inline" method="POST" action="{{route('order-suggestion.create', $product->info->id_product)}}" enctype="multipart/form-data">
                <input type="hidden" name="_method" value="POST">
                
                {{ csrf_field() }}

                <button type="submit" class="btn btn-secondary btn-sm btn-action mb-1" disabled>Add to Order Suggestion</button><br>
                </form>
            
            @elseif ($uniquity == '0')
                <form class="form-inline" method="POST" action="{{route('order-suggestion.create', $product->info->id_product)}}" enctype="multipart/form-data">
                <input type="hidden" name="_method" value="POST">
                
                {{ csrf_field() }}

                <button type="submit" class="btn btn-primary btn-sm btn-action mb-1">Add to Order Suggestion</button><br>
                </form>
            
            @else
                <button class="btn btn-primary btn-sm btn-action mb-1" disabled>Database Error on uniquity.</button><br>
            @endif

        </td>
        <td>{{$product->created_at->format('Y-m-d h:iA')}}</td>
        <td>
            <form action="{{route('restock.delete',$product->product_id)}}">
                <button class="btn btn-danger btn-sm" type="submit"><span data-feather="x"></span></button>
            </form> 
        </td>
    </tr>
    @endforeach
</tbody>
</table>
</div>

@include('layouts.image-modal')
@include('layouts.shelf-location')

@endsection

@push('bottom-scripts')

<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@10.19.0/dist/lazyload.min.js"></script>

<script>
    var myLazyLoad = new LazyLoad({
    elements_selector: ".lazy",
    load_delay: 300 
});
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#restock-board').DataTable({
            "paging":         false,
            "order": [[ 8, "asc" ]],
            columnDefs: [ { orderable: false, targets: [
                6,7,9
            ]}],
            "language": {
                "search": "Filter records:"
            }
        });
    });
</script>

<script>
    var typingTimer;
    var doneTypingInterval = 10;
    var finaldoneTypingInterval = 500;
    
    $('input#product-id').keydown(function () {
        clearTimeout(typingTimer);
                
    
        typingTimer = setTimeout(function () {
            $('#product-name').html('Typing...');
        }, doneTypingInterval);
        
    });
    
    $('input#product-id').keyup(function () {
        clearTimeout(typingTimer);
        var pid = $(this).val();
        
        typingTimer = setTimeout(function () {
            $.ajax({
                url:"{{route('product.autofill')}}",
                data:'_token={{csrf_token()}}&pid='+pid,
                type: 'GET',
                dataType: 'json',
                success: function(response){
                    
                    if(response.status=="success"){			
                        $('#product-name').html( pid + " - " + response.name);					
                    }else if(response.status=="failed"){
                        $('#product-name').html("PID#" + pid + " does not exist.");
                    }else{
                        $('#product-name').html("Error! Please contact developer.");		
                    }
                },
                error: function(e){
                    $('#product-name').html("Product Search Failed...");
                }
            });
        }, finaldoneTypingInterval);
        
    });
</script>

@endpush