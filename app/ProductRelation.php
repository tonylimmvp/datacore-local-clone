<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductRelation extends Model
{
    protected $table = 'product_relations';
    protected $connection='mysql';
    protected $guarded = [];

    public $timestamps = false;

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function relatedProduct()
    {
        return $this->belongsTo(Product::class, 'related_product_id');
    }
}
