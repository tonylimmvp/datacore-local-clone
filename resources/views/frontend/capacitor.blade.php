@extends('frontend.layouts.main')

@include('layouts.datatables')

@section('content')

{{-- Cap Filters  --}}
<div class="card mb-5">
    <div class="card-body ml-5 mr-5">
        <form action="{{route('capacitor')}}" method="GET">
            <div class="row">
                <div class="input-group col-md-6 mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text">Volt: </label>
                    </div>

                    <select class="form-control" name="volt_sign" multiple>
                        <option value="="  @if(request()->get('volt_sign') == "=") selected @endif>equal</option>
                        <option value=">=" @if(request()->get('volt_sign') == ">=") selected @endif>greater than</option>
                        <option value="<=" @if(request()->get('volt_sign') == "<=") selected @endif>less than</option>
                    </select>

                    <select class="form-control" name="voltage" multiple>
                        @foreach(App\Capacitor::groupBy('voltage')->get() as $capacitor)
                            <option value="{{$capacitor->voltage}}" 
                                @if(request()->get('voltage') == $capacitor->voltage) selected @endif>
                                {{$capacitor->voltage}}V
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="input-group col-md-6 mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text">Value: </label>
                    </div>

                    <select class="form-control" name="cap_sign" multiple>
                        <option value="="  @if(request()->get('cap_sign') == "=") selected @endif>equal</option>
                        <option value=">=" @if(request()->get('cap_sign') == ">=") selected @endif>greater than</option>
                        <option value="<=" @if(request()->get('cap_sign') == "<=") selected @endif>less than</option>
                    </select>

                    <select class="form-control" name="capacitance" multiple>
                        @foreach(App\Capacitor::groupBy('capacitance')->get() as $capacitor)
                            <option value="{{$capacitor->capacitance}}"
                                @if(request()->get('capacitance') == $capacitor->voltage) selected @endif>
                                @if($capacitor->capacitance < 0.0001){{sprintf('%f', $capacitor->capacitance)}} uF @else{{$capacitor->capacitance}} uF @endif
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
            
            <button class="btn btn-primary btn-sm">Submit</button>
            <a href={{url()->current()}}><button type="button" class="btn btn-primary btn-sm">Clear Filter</button></a>
        </form>
    </div>
</div>

@if((isset($_GET['voltage']) && isset($_GET['volt_sign'])) || (isset($_GET['capacitance']) && isset($_GET['cap_sign'])))
<div class="alert alert-info text-center">
    @if(isset($_GET['voltage']) && isset($_GET['volt_sign']))
    <span>
        Voltage {{$_GET['volt_sign']}} {{$_GET['voltage']}}V
    </span>
    @endif
    @if((isset($_GET['voltage']) && isset($_GET['volt_sign'])) && (isset($_GET['capacitance']) && isset($_GET['cap_sign'])))
        <span>|</span>
    @endif
    @if(isset($_GET['capacitance']) && isset($_GET['cap_sign'])) 
    <span>
        Capacitance {{$_GET['cap_sign']}} @if($_GET['capacitance'] < 0.0001){{sprintf('%f', $_GET['capacitance'])}} uF @else{{$_GET['capacitance']}} uF @endif
    </span>
    @endif
</div>
@endif

{{-- Cap table --}}
<table id="capacitor-table" class="table table table-bordered table-hover" width="100%">
    <thead>
        <th>PID</th>
        <th>Name</th>
        <th class="d-none d-md-table-cell">Voltage (V)</th>
        <th class="d-none d-md-table-cell">Capacitance (uF)</th>
        <th>Price</th>
        <th>Quantity</th>
        <th>Location</th>
        {{-- <th>Temperature</th> --}}
    </thead>
    <tbody>
        @foreach($capacitors as $capacitor)
        <tr>
            <td>{{$capacitor->product_id}}</td>
            <td>{{$capacitor->product->name}}</td>
            <td class="d-none d-md-table-cell">{{$capacitor->voltage}}V</td>
            <td class="d-none d-md-table-cell">@if($capacitor->capacitance < 0.0001){{sprintf('%f', $capacitor->capacitance)}} uF @else{{$capacitor->capacitance}} uF @endif</td>
            <td>${{$capacitor->product->price}}</td>
            <td>{{$capacitor->product->quantity}}</td>
            <td class="text-center">
                {{$capacitor->product->locations()}}
            </td>
            {{-- <td>{{$capacitor->temperature}}</td> --}}
        </tr>
        @endforeach
    </tbody>
    
</table>

<div class="text-center">
    <a href="{{ route('capacitor.update') }}"><button class="btn btn-primary" onclick="openLoader()">Update Capacitor Table</button></a>
</div>

@endsection

@push('bottom-scripts')
<script>
    
    $('#capacitor-table').DataTable(
        {
            scrollY:        "60vh",
            scrollX:        true,
            scrollCollapse: true,
            paging:         false,
            displayLength: 25,
            language: {
                "search": "Filter records:"
            },
            columnDefs:      [
                { type: 'any-number', targets : [2,3,4,5] } 
            ]
        }
    );
</script>
@endpush