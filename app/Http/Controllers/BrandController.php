<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\ProductOnline;

class BrandController extends Controller
{
    public function search() {
        return view('backend.integrations.brands.search');
    }

    public function searchDetail(Request $request) {
        $term = $request->term;
        $products = Product::search("$term")->get();
        return view('backend.integrations.brands.search',compact('products','term'));
    }

    public function update(ProductOnline $product, Request $request) {
        $this->validate(request(), [
            'brand' => 'required|exists:leeselectronic.ps_manufacturer,id_manufacturer',
        ]);

        $product->id_manufacturer = $request->brand;
        $product->save();
        session()->flash('message','Updated Category Successfully');
        return back();
    }
}
