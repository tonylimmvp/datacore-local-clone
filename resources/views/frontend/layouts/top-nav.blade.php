    {{-- Top Navigation Bar begins --}}
    <nav class="navbar navbar-expand navbar-light bg-light">
            <a href="{{ route('home') }}">
                <img src="{{ asset('/img/lees_logo_large.png') }}" width="100" height="40">
            </a>

            <button class="navbar-toggler mb-2" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
                <ul class="navbar-nav">

                    {{-- Search bar on top navigation --}}
                    <li>
                        <form class="form-inline active-cyan-4" method="get" action="{{route('search.detail')}}">
                        <div class="input-group">
                            <input class="form-control form-control-sm" name="term" @if(isset($term)) value="{{$term}}" @endif required placeholder="Search" aria-label="Search">
                        
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-sm btn-secondary">Search</button>
                            </div>
                        </div>
                        </form>
                    </li>
                    
                    @if (Auth::guest())
                        <li class="nav-item"><a href="{{ route('login') }}" class="nav-link">Login</a></li>
                    @else
                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                               <span data-feather="user"></span> {{ Auth::user()->name }} <span data-feather="chevron-right"></span> 
                                {{Auth::user()->role->name}}
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">

                                <a href="{{ route('coreui.dashboard') }}" class="dropdown-item">
                                    <span data-feather="database"></span> CoreUI
                                </a>

                                {{-- show schedule link if NOT terminal account --}}
                                @if(Auth::user()->role->id != 5)
                                <a href="{{ route('coreui.schedule') }}" class="dropdown-item">
                                    <span data-feather="calendar"></span> My Schedule
                                </a>

                                    {{-- show admin schedule link if admin --}}
                                    @if(Auth::user()->hasAccess('events')->read)
                                    <a href="{{ route('coreui.schedule-admin') }}" class="dropdown-item">
                                        <span data-feather="calendar"></span> All Schedule
                                    </a>
                                    @endif

                                @endif

                                <a href="{{ route('coreui.settings') }}" class="dropdown-item">
                                   <span data-feather="settings"></span> Settings
                                </a>

                                <a href="{{ route('logout') }}" class="dropdown-item">
                                   <span data-feather="log-out"></span> Logout
                                </a>

                            </div>
                        </li>                      
                    @endif
                </ul>
            </div>
    </nav>
    {{-- Top Navigation Bar Ends --}}

    {{-- Bottom Navigation Bar Starts --}}
    <nav class="navbar navbar-expand navbar-dark bg-dark">
        <div class="container d-flex flex-column flex-md-row justify-content-between">

            <a href="{{route('search')}}" class="nav-link">
                <span data-feather="search"></span> SEARCH
            </a>

            {{-- Sales Dropdown --}}
            @auth
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" id="navbarDropdownOrders" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        <span data-feather="pocket"></span> SALES
                    </a>
                    {{-- Sales --}}
                    <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdownOrders">
                        {{-- Orders --}}
                        @if(Auth::user()->hasAccess('order')->read || Auth::user()->hasAccess('order')->create)
                        @php $order_count = App\Order::orderCount(); @endphp
                        @if(Auth::user()->hasAccess('order')->read)
                        <a href="{{ route('orders') }}" class="dropdown-item">
                            <span data-feather="package"></span> ORDERS
                            @if($order_count->all_orders>0)
                            <span class="badge badge-danger">{{$order_count->all_orders}}</span>
                            @endif
                        </a>
                        
                        {{-- <a href="{{ route('orders.special') }}" class="dropdown-item">
                            <span class="nav-indent"><span data-feather="corner-down-right"></span> SPECIAL ORDERS</span>
                            @if(sizeof($order_count->special)>0)
                            <span class="badge badge-danger">{{$order_count->special}}</span>
                            @endif
                        </a>

                        <a href="{{ route('orders.backorder') }}" class="dropdown-item">
                            <span class="nav-indent"><span data-feather="corner-down-right"></span> BACK ORDERS</span>
                            @if(sizeof($order_count->backorders)>0)
                            <span class="badge badge-danger">{{$order_count->backorders}}</span>
                            @endif
                        </a> --}}

                        @elseif(Auth::user()->hasAccess('order')->create)
                        <div class="dropdown-item">
                            <span data-feather="package"></span> ORDERS
                        </div>
                        @endif

                        @if(Auth::user()->hasAccess('order')->create)
                        <a href="{{ route('orders.create') }}" class="dropdown-item">
                            <span class="nav-indent"><span data-feather="corner-down-right"></span> CREATE ORDER</span>
                        </a>
                        @endif
                        
                        @endif

                         {{-- Bambora Payment --}}
                         @auth
                         @if(Auth::user()->hasAccess('payment')->read || Auth::user()->hasAccess('payment')->create)
                         
                         <hr>
                         
                         @if(Auth::user()->hasAccess('payment')->read)
                         
                         <a href="{{ route('payments') }}" class="dropdown-item" onclick="openLoader()" >
                             <span data-feather="file-text"></span> BAMBORA PAYMENTS
                         </a>
 
                         @elseif(Auth::user()->hasAccess('payment')->create)
                             <div class="dropdown-item">
                                 <span data-feather="file-text"></span> BAMBORA PAYMENTS
                             </div>
                         @endif
 
                         @if(Auth::user()->hasAccess('payment')->create)
                         <a href="{{ route('checkout') }}" class="dropdown-item">
                             <span class="nav-indent" onclick="openLoader()"><span data-feather="corner-down-right"></span> NEW PAYMENT </span>
                         </a>     
                         @endif
 
                         @endif
                         
                         @endauth

                         {{-- Braintree Payment --}}
                         @auth
                         @if(Auth::user()->hasAccess('payment')->read)
                         
                         <hr>
                         
                         @if(Auth::user()->hasAccess('payment')->read)
                         
                         <a href="{{ route('payments.braintree') }}" class="dropdown-item" onclick="openLoader()" >
                             <span data-feather="file-text"></span> BRAINTREE PAYMENTS (APP)
                         </a>

                         @endif

                         @endif
                         
                         @endauth

                        @if(Auth::user()->hasAccess('quotation')->read || Auth::user()->hasAccess('quotation')->create)
                        
                        <hr>

                        {{-- Quotations --}}
                        @if(Auth::user()->hasAccess('quotation')->read)
                        <a href="{{ route('quotations') }}?expired=0" class="dropdown-item">
                            <span data-feather="dollar-sign"></span> QUOTATIONS
                        </a>
                        @elseif(Auth::user()->hasAccess('quotation')->create)
                        <div class="dropdown-item">
                            <span data-feather="dollar-sign"></span> QUOTATIONS
                        </div>
                        @endif

                        @if(Auth::user()->hasAccess('quotation')->create)
                        <a href="{{ route('quotation.create') }}" class="dropdown-item">
                            <span class="nav-indent" ><span data-feather="corner-down-right"></span>  CREATE QUOTATION</span>
                        </a>
                        @endif

                        @endif

                        @if(Auth::user()->hasAccess('rma')->read || Auth::user()->hasAccess('rma')->create)
                        <hr>

                        {{-- RMA --}}
                        
                        @if(Auth::user()->hasAccess('rma')->read)
                        <a href="{{ route('rma') }}" class="dropdown-item">
                            <span data-feather="headphones"></span> RMA
                        </a>
                        @elseif(Auth::user()->hasAccess('rma')->create)
                            <div class="dropdown-item">
                                <span data-feather="headphones"></span> RMA
                            </div>
                        @endif
                        
                        @if(Auth::user()->hasAccess('rma')->create)
                        <a href="{{ route('rma.create') }}" class="dropdown-item">
                            <span class="nav-indent"><span data-feather="corner-down-right"></span>  CREATE RMA</span>
                        </a>
                        @endif

                        @endif
                        
                        @auth
                        @if(Auth::user()->hasAccess('notifier')->read || Auth::user()->hasAccess('notifier')->update)
                        
                        <hr>

                        {{-- Notifier --}}
                        
                        @if(Auth::user()->hasAccess('notifier')->read)

                        
                        <a href="{{ route('product.notifier') }}" class="dropdown-item">
                            <span data-feather="bell"></span> NOTIFIERS
                            @php $notifiers = App\Notifier::groupBy('id_product')->get(); @endphp
                            @if(sizeof($notifiers)>0)
                            <span class="badge badge-danger">{{sizeof($notifiers)}}</span>
                            @endif
                        </a>

                        @elseif(Auth::user()->hasAccess('notifier')->update)
                            <div class="dropdown-item">
                                <span data-feather="bell"></span> NOTIFIERS
                                @php $notifiers = App\Notifier::groupBy('id_product')->get(); @endphp
                                @if(sizeof($notifiers)>0)
                                <span class="badge badge-danger">{{sizeof($notifiers)}}</span>
                                @endif
                            </div>
                        @endif

                        @if(Auth::user()->hasAccess('notifier')->update)
                        <a href="{{ route('notifier.send') }}" class="dropdown-item">
                            <span class="nav-indent" onclick="openLoader()"><span data-feather="corner-down-right"></span> SEND OUT NOTIFIERS</span>
                        </a>     
                        @endif

                        @endif
                        @endauth

                    </div>
                </li>
            </ul>

            @endauth

            @auth

            <a href="{{route('restock')}}" class="nav-link">
                <span data-feather="list"></span> RESTOCK
                @php $restock_count = App\RestockProduct::all(); @endphp
                @if(sizeof($restock_count)>0)
                <span class="badge badge-danger">{{sizeof($restock_count)}}</span>
                @endif
            </a>

            @endauth

            @auth
                @if(Auth::user()->hasAccess('order_suggestion')->read || Auth::user()->hasAccess('shipment')->read)
                {{-- PURCHASING --}}
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" id="navbarDropdownOrders" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <span data-feather="shopping-cart"></span> PURCHASING
                        </a>

                        <div class="dropdown-menu dropdown-menu-center" aria-labelledby="navbarDropdownOrders">

                            @if(Auth::user()->hasAccess('order_suggestion')->read)
                                <a href="{{ route('order-suggestion') }}" class="dropdown-item">
                                    <span data-feather="shopping-bag"></span> ORDER SUGGESTIONS
                                </a>
                            @endif

                            @if(Auth::user()->hasAccess('shipment')->read)
                                <a href="{{ route('ordered-products') }}" class="dropdown-item">
                                    <span data-feather="credit-card"></span> ORDERED PRODUCTS
                                </a>

                                <a href="{{ route('shipment.show') }}" class="dropdown-item">
                                    <span data-feather="truck"></span> SHIPMENT LIST
                                </a>
                            @endif

                            @if(Auth::user()->hasAccess('vendors')->read)
                            <a href="{{route('coreui.vendors')}}" class="dropdown-item">
                                <span data-feather="shopping-bag"></span> VENDORS
                            </a>
                            @endif

                            @if(Auth::user()->hasAccess('shipment')->read)
                                <a href="{{route('store-supplies')}}" class="dropdown-item">
                                    <span data-feather="box"></span> STORE SUPPLIES
                                </a>
                            @endif
                        </div>
                    </li>
                </ul>
                @endif
            @endauth
            
            @auth
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" id="navbarDropdownOrders" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        <span data-feather="alert-triangle"></span> ISSUES/TOOLS
                    </a>

                    <div class="dropdown-menu dropdown-menu-center" aria-labelledby="navbarDropdownOrders">

                            <a href="{{route('issues.store')}}" class="dropdown-item" onclick="openLoader()">
                                <span data-feather="corner-down-right"></span> STORE ISSUES
                            </a>

                            <a href="{{route('issues.website')}}" class="dropdown-item" onclick="openLoader()">
                                <span data-feather="corner-down-right"></span> WEBSITE ISSUES
                            </a>

                            <hr>

                            <a href="{{route('store-ips')}}" class="dropdown-item" onclick="openLoader()">
                                <span data-feather="list"></span> STORE IP ADDRESS
                            </a>
                            
                            <a href="{{route('phonebook')}}" class="dropdown-item" onclick="openLoader()">
                                <span data-feather="book-open"></span> PHONEBOOK
                            </a>
                            
                            <a href="{{route('resistor')}}" class="dropdown-item" onclick="openLoader()">
                                <span data-feather="search"></span> RESISTOR SEARCH
                            </a>
                        
                            <a href="{{route('capacitor')}}" class="dropdown-item" onclick="openLoader()">
                                <span data-feather="search"></span> CAPACITOR SEARCH
                            </a>

                            <a href="{{config('custom.web_url')}}/resistor-reader" class="dropdown-item" target="_blank">
                                <span data-feather="activity"></span> RESISTOR READER
                            </a>

                            <a href="{{config('custom.web_url')}}/capacitor-reader" class="dropdown-item" target="_blank">
                                <span data-feather="database"></span> CAPACITOR READER
                            </a>

                    </div>
                </li>
            </ul>
            @endauth

            @auth
            {{-- Links Dropdown --}}
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    
                    <a href="#" class="nav-link dropdown-toggle" id="navbarDropdownLinks" data-toggle="dropdown" 
                        aria-haspopup="true" aria-expanded="false">
                        <span data-feather="globe"></span> LINKS
                    </a>

                    <div class="dropdown-menu" aria-labelledby="navbarDropdownLinks">
                        <a href="{{config('custom.web_url')}}/fastsearch_lite/index.php" class="dropdown-item" target="_blank">
                            <span data-feather="loader"></span> FASTSEARCH
                        </a>

                        <a href="{{config('custom.web_url')}}" class="dropdown-item" target="_blank">
                            <span data-feather="cloud"></span> WEBSITE
                        </a>

                        <a href="{{ route('coreui.dashboard') }}" class="dropdown-item" target="_blank">
                            <span data-feather="database"></span> COREUI
                        </a>

                        <a href="{{config('custom.web_url')}}/webui/index.php" class="dropdown-item" target="_blank">
                            <span data-feather="book"></span> WEBUI
                        </a>

                        <a href="{{config('custom.web_url')}}/leesapp_webui/index.php" class="dropdown-item" target="_blank">
                            <span data-feather="smartphone"></span> APP WEBUI
                        </a>
                    </div>
                </li>
            </ul>
            @endauth
        </div>
    </nav>
    {{-- Bottom Navbar Ends --}}

    @push('bottom-scripts')

    <script>
    function openLoader(){
        $('#loader').toggle();
    }
    </script>

    @endpush