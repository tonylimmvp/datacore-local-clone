<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Blade;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        // example of how to place static info on layout pages 
        view()->composer('frontend.layouts.orders', function ($view){
            $view->with('statusFilters', \App\OrderStatus::orderBy('position','asc')->get());
            $view->with('orderCount', \App\Order::orderCount());
            $view->with('old_backorders', \App\Order::oldBO());
            $view->with('old_special_orders', \App\Order::oldSPO());
        });

        Blade::directive('formatMoney', function ($money) {
            return "<?php echo number_format($money, 2); ?>";
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
