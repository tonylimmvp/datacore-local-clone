<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RMAStatusHistory extends Model
{
    protected $table = 'rma_status_history';

    protected $fillable = [
        'rma_id','status_id','employee_id','comment'
    ];

    protected $primaryKey = 'id';

    public function rmas(){
        return $this->belongsTo(RMA::class,'rma_id');
    }
    public function status(){
        return $this->belongsTo(RMA_Status::class,'status_id');
    }
    public function sales(){
        return $this->belongsTo(Employee::class,'employee_id');
    }
}
