
{{-- Top Navigation Bar begins --}}
<nav class="navbar navbar-expand navbar-light bg-light">
        <a href="{{ route('home') }}">
            <img src="{{ asset('/img/lees_logo_large.png') }}" width="100" height="40">
        </a>

        <button class="navbar-toggler mb-2" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

    <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
        <ul class="navbar-nav">

            {{-- Search bar on top navigation --}}
            <li>
                <form class="form-inline active-cyan-4" method="get" action="{{route('search.detail')}}">
                <div class="input-group">
                    <input class="form-control form-control-sm" name="term" @if(isset($term)) value="{{$term}}" @endif required placeholder="Search" aria-label="Search">
                
                    <div class="input-group-append">
                        <button type="submit" class="btn btn-sm btn-secondary">Search</button>
                    </div>
                </div>
                </form>
            </li>
            
            @if (Auth::guest())
                <li class="nav-item"><a href="{{ route('login') }}" class="nav-link">Login</a></li>
            @else
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        <span data-feather="user"></span> {{ Auth::user()->name }} <span data-feather="chevron-right"></span> 
                        {{Auth::user()->role->name}}
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                        
                        <a href="{{ route('coreui.dashboard') }}" class="dropdown-item">
                            <span data-feather="home"></span> Dashboard
                        </a>

                        {{-- show schedule link if NOT terminal account --}}
                        @if(Auth::user()->role->id != 5)
                        <a href="{{ route('coreui.schedule') }}" class="dropdown-item">
                            <span data-feather="calendar"></span> My Schedule
                        </a>

                            {{-- show admin schedule link if admin --}}
                            @if(Auth::user()->hasAccess('events')->read)
                            <a href="{{ route('coreui.schedule-admin') }}" class="dropdown-item">
                                <span data-feather="calendar"></span> All Schedule
                            </a>
                            @endif

                        @endif
                        
                        <a href="{{ route('coreui.settings') }}" class="dropdown-item">
                            <span data-feather="settings"></span> Settings
                        </a>

                        <a href="{{ route('logout') }}" class="dropdown-item">
                            <span data-feather="log-out"></span> Logout
                        </a>

                    </div>
                </li>                      
            @endif
        </ul>
    </div>
</nav>
 {{-- Top Navigation Bar Ends --}}