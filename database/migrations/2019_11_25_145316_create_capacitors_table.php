<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCapacitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('capacitors', function (Blueprint $table) {
            $table->unsignedInteger('product_id');
            $table->double('voltage');
            $table->double('capacitance');
            $table->integer('temperature')->nullable();
            $table->integer('tolerance')->nullable();

            $table->primary('product_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('capacitors');
    }
}
