<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MobileOrderStatus extends Model
{
    protected $connection = 'leeselectronic';
    protected $table = 'la_order_state';
    protected $primaryKey = 'order_state_id';

    public $incrementing = false;
    public $timestamps = false;

}
