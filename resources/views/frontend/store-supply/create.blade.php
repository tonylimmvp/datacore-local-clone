@extends('frontend.layouts.main')

@section('content')

<div class="container">

    <h1 class="mt-2">Create Shipment</h1>
    @php $shipmentTypes = App\ShipmentType::all() @endphp
    <form method="POST" action="{{route('shipment.store')}}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="shipment_type">Shipment Type:</label>

            <select class="custom-select mr-sm-2 mb-2" name="type_id" required>
                @foreach($shipmentTypes as $type)
                    <option value='{{$type->id}}'>{{$type->name}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="isLocal">Local:</label>
            <select class="custom-select mr-sm-2 mb-2" name="isLocal" required>
                <option value="0">NO</option>
                <option value="1">YES</option>
            </select>
        </div>

        <div class="form-group">
            <label for="vendor">Vendor</label>

            {{-- @php $query = DB::connection('leeselectronic')->table("table_1")->select("vendor")->distinct()->get(); @endphp
            <select class="custom-select mr-sm-2 mb-2" name="vendor" required>
                @foreach($query as $q)
                    <option value="{{$q->vendor}}">{{$q->vendor}}</option>
                @endforeach
            </select> --}}

            @php $vendors = App\Vendor::where('isDisabled', '0')->get(); @endphp
            <select class="custom-select mr-sm-2 mb-2" name="vendor" required>
                @foreach($vendors as $vendor)
                    <option value="{{$vendor->id}}">{{$vendor->reference}}</option>
                @endforeach
            </select>

        </div>

        <div class="form-group">
            <input type="hidden" name="employee" value="{{Auth::user()->id}}">
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>

        @include('layouts.errors')

    </form>
</div>
@endsection