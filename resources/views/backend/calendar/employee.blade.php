@extends('backend.layouts.main')

@include('layouts.fullcalendar')
@include('layouts.datepicker')

@section('content')
    <div id="calendar-tools" class="card card-body mb-4">
        <div class="row">
            <div class="col-3">
                <a href="{{ route('coreui.schedule-admin')}}">
                    <button class="btn btn-primary" type="button">    
                        View All
                    </button>
                </a>
            </div>
        <div class="text-center col-6">
            <h2>{{ $employee->name }}'s Schedule</h2>
        </div>
        <div class="text-right col-3">
            @if(Auth::user()->hasAccess('events')->create)
            <button class="btn btn-primary" data-toggle="modal" data-target="#createEventModal">Create Shift</button>
            @endif
        </div>
        </div>
    </div>
    <div id="calendar"></div>

    @include('backend.calendar.schedule-employee')

@endsection