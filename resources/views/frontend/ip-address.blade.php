@extends('frontend.layouts.main')

@include('layouts.datatables')

@section('content')

{{-- IP Address table --}}
<table id="ip-table" class="table table table-bordered table-hover" width="100%">
    <thead>
        <th>ID</th>
        <th>IP Address</th>
        <th>Date Created</th>
        @auth
        @if(Auth::user()->hasAccess('store_ips')->delete)
        <th>Action</th>
        @endif
        @endauth
    </thead>
    <tbody>
        @foreach($ips as $ip)
        <tr>
            <td>{{$ip->id}}</td>
            <td>{{$ip->ip_address}}</td>
            <td>{{$ip->created_at}}</td>
            @auth
            @if(Auth::user()->hasAccess('store_ips')->delete)
            <td class="text-center">
                <button class="btn btn-danger"
                        data-id="{{ $ip->ip_address }}"
                        data-toggle="modal"
                        data-target="#deleteModal"
                        data-url="{{ route('store-ips.delete',$ip->ip_address) }}">
                    <span data-feather="x"></span>
                </button>
            </td>
            @endif
            @endauth
        </tr>
        @endforeach
    </tbody>
</table>

<div class="pt-2">
<h2>Your current IP is: <span>{!!Request::ip()!!}</span></h2>
@auth
@if(Auth::user()->hasAccess('store_ips')->create)
    @if(App\StoreIP::hasIP(Request::ip()))
    <p>This IP is already on the list.</p>
    @else
    <form method="POST" action="{{route('store-ips.store')}}">
        {{ csrf_field() }}
        <div class="input-group">
            <input class="form-control" name="ip" placeholder="{!! Request::ip() !!}" value="{!! Request::ip() !!}" required>
            <div class="input-group-append">
                <button class="btn btn-primary">Add IP</button>
            </div>
        </div>
    </form>
    @endif
@endif
@endauth
@include('layouts.errors')
</div>

@auth
@if(Auth::user()->hasAccess('store_ips')->delete)
{{-- Delete Modal --}}
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Remove IP Address?</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Are you sure you want to remove <span id="ip-address">---</span> from the list?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <form method="POST">
            {{csrf_field()}}
            <button class="btn btn-danger">Remove IP Address</button>
        </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endif
@endauth

@endsection

@push('bottom-scripts')
<script>
    $('#ip-table').DataTable(
        {
            scrollY:        "60vh",
            scrollX:        true,
            scrollCollapse: true,
            paging:         false,
            displayLength: 25,
        }
    );
</script>

<script>
    $(document).ready(function () {
        $('#deleteModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) 
            var id = button.data('id')
            var delete_url = button.data('url')
            var modal = $(this)
            modal.find('#ip-address').text(id)
            modal.find('form').attr('action',delete_url)
        });
    });
</script>
@endpush