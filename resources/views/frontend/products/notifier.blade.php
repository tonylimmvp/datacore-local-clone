@extends('frontend.layouts.main')

@include('layouts.datatables')

@push('css')
<style>
@media (min-width: 576px){
    .modal-dialog {
        max-width: 80%;
    }
}

@media (min-width: 1200px){
    .modal-dialog {
        max-width: 50%;
    }
}

@media (min-width: 1920px){
    .modal-dialog {
        max-width: 30%;
    }
}

</style>
@endpush
@section('content')
<h1>Notifiers</h1>

<table id="notifierTable" class="table table-bordered" width="100%">
    <thead>
        <tr>
            <th>PID</th>
            <th>Name</th>
            <th>Total Interested</th>
            <th>Total Amount</th>
            <th>Current Amount</th>
            <th>Last Requested</th>
        </tr>
    </thead>
    <tbody>
        @php $notifiers = App\Notifier::selectRaw('*, COUNT(id_product) as interest_num, SUM(value) as interest_value')->groupBy('id_product')->orderBy('created_at','desc')->get(); @endphp
        @foreach($notifiers as $notifier)
        <tr class=" table-sm @if($notifier->interest_value < $notifier->product->quantity) table-warning @endif">
            <td> {{ $notifier->product->id_product}} </td>
            <td> {{ $notifier->product->name}} </td>
            <td onclick="toggleNotifierModal({{$notifier->product->id_product}})"> {{ $notifier->interest_num }}</td>
            <td onclick="toggleNotifierModal({{$notifier->product->id_product}})"> {{ $notifier->interest_value }}</td>
            <td onclick="toggleNotifierModal({{$notifier->product->id_product}})"> {{ $notifier->product->quantity }}</td>
            <td> {{ Carbon\Carbon::parse($notifier->created_at)->format('Y-m-d')}} </td>
        </tr>
        @endforeach
    </tbody>
</table>

{{-- Notifier Modal --}}
<div class="modal fade" id="notifierModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title" id="exampleModalLabel">Requests for PID#<span id="requestedProduct">---</span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                
                <div class="modal-body">
                    {{-- desktop --}}
                    <div id="notifier-desktop" class="table-responsive d-none d-md-block">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Customer</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Amount</th>
                                <th>Type</th>
                                <th>Requested At</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    </div>

                    {{-- mobile --}}
                    <div id="notifier-mobile" class="d-md-none">
                        
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection

@push('bottom-scripts')

<script type="text/javascript">
    $(document).ready(function () {
        $('#notifierTable').DataTable({
            "scrollY":        "60vh",
            "scrollCollapse": true,
            "paging":         false,
            "order":          [[5,'desc']],
            "language": {
                "search": "Filter records:"
            }
        });
    });

function toggleNotifierModal(pid) {
    $('#requestedProduct').html(pid);
    $.ajax({
        type: "get",
        url: "{{ route('notifier.fetch') }}",
        data: {pid: pid},
        dataType: 'json',
        success: function( result ) {
            if(result.status == 'success') {
                var notifiers = result.result_data;
                var notifiers_mobile = result.result_data;
                
                {{-- Desktop --}}
                var desktop = '';

                notifiers.forEach(function(notifier){
                    desktop += '<tr><td>';

                    if(notifier.customer_name == ''){
                        desktop += notifier.name;
                    }else{
                        desktop += notifier.customer_name;
                    }

                    desktop += '</td>\
                            <td>'+notifier.customer_email+'</td>\
                            <td>'+notifier.phone+'</td>\
                            <td>'+notifier.value+'</td>\
                            <td>'+notifier.type+'</td>\
                            <td>'+notifier.created_at+'</td>\
                            </tr>';
                })

                $('#notifier-desktop tbody').html(desktop);

                {{-- Mobile --}}

                var mobile = '';

                notifiers.forEach(function(notifier){
                    mobile += '<div class="card mb-2">\
                            <div class="card-body">\
                                <p><b>Customer Name:</b>';

                    if(notifier.customer_name == ''){
                        mobile += notifier.name;
                    }else{
                        mobile += notifier.customer_name;
                    }
                                    
                    mobile +=   '</p>\
                                <p><b>Email:</b> '+notifier.customer_email+'</p>\
                                <p><b>Phone:</b> '+ notifier.phone +'</p>\
                                <p><b>Type:</b>'+ notifier.type +'</p>\
                                <p><b>Created at:</b>'+ notifier.created_at +'</p>\
                            </div>\
                        </div>';
                })

                $('#notifier-mobile').html(mobile);

                $('#notifierModal').modal(true);
                
            }else{
                alert(result.message);
            }
        },
        error: function (request, status, error) {
            alert(request.status + ':' + error + '. Please take picture and contact DevTeam.');
        }
    });
}

</script>

@endpush