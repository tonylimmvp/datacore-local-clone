<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Product;

class ProductPriceChange extends Model
{
    protected $table='product_price_change';
    protected $connection='mysql';
    protected $guarded = [];

    public static function getLastPrice($product_id) {
        //Get the last entry of the db for that specific product_id
        $lastID = DB::table('product_price_change')->where('product_id', $product_id)->latest()->first()->id;
        $lastPrice = DB::table('product_price_change')->find($lastID)->new_price;

        return (string) $lastPrice;
    }
}