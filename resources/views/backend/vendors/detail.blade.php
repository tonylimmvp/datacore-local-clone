@extends('backend.layouts.main')

@include('layouts.datatables')

@section('content')
@include('layouts.errors')

<h1>{{$vendor->name}}'s Products</h1>
<p class="smallNote" style="text-align:left; font-size: 18px;">Products with shopping cart icon beside it are suggested for purchase.</p>

<table id="product_table" class="table table-bordered">
    <thead>
    <tr>
        <th>PID</th>
        <th>Image</th>
        <th>Name</th>
        <th>Price</th>
        <th>Quantity</th>
        @if(Auth::user()->hasAccess('cost')->read)<th>Recent Cost</th>@endif
        <th style="min-width:180px">Remarks</th>
        <th>Related Products | Purchased?</th>
        <th>Other Vendors</th>
        <th>Purchasing</th>
    </tr>
    </thead>
    <tbody>
        @php  @endphp
        @foreach($vendor->products->where('disabled','N') as $product)
        <tr>
            <td class="searchID">
                {{-- <a href="{{config('custom.web_url')}}/product/{{$product->id_product}}.html" target="_blank"> --}}
                <a href="{{route('product-profile.show', $product->id_product)}}" target="_blank">
                    {{$product->id_product}}
                    {{-- Show shopping cart icon beside product id if order suggestion exists. --}}
                    @if($product->orderSuggestion)
                        <span title="Order Suggestion Exists" data-feather="shopping-cart"></span>
                    @endif
                </a>
            </td>
            <td style="width:100px"><img class="img-fluid" src="{{$product->image_url()}}" width="100px"/></td>
            <td>
                {{$product->name}}
                    @auth
                    @include('layouts.product-name-icons')

                    {{-- Orders for product --}}
                    @if($product->orders)
                        @foreach($product->orders as $order)
                            @if($order->currentStatus->name != "Done")
                                <br>
                                <a href="{{route('orders.detail',$order->id)}}">
                                    <button class="btn btn-sm btn-secondary m-1">
                                        Order #{{$order->id}} - {{$order->name}}
                                    </button>
                                </a>
                            @endif
                        @endforeach
                    @endif

                    {{-- show quotations if exit --}}
                    @if($product->quotations)
                        @foreach($product->quotations as $quote)
                            @if(!$quote->isExpired)
                                <br>
                                <a href="{{route('quotations')}}">
                                    <button class="btn btn-sm btn-info m-1">
                                        Quote #{{$quote->id}} - {{$quote->name}}
                                    </button>
                                </a>
                            @endif
                        @endforeach
                    @endif

                    {{--    Show applicable notes. 
                            If a note for this particular product exist. --}}
                    @if((sizeof($product->notes) > 0))
                        @auth
                        {{-- Only show note if your role is staff or above. --}}
                        @if(Auth::user()->role->id <= 2)
                            {{-- Check if there is a non handled note, if true then show table headers. --}}
                            @if(($product->notes->where("isHandled", "=", "0")->count()) > 0)
                                <table id="notes_table" class="table table-hover table-light table-bordered">
                                    <thead>
                                        <tr>
                                            <th scope="col">Type</th>
                                            <th scope="col">Note</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {{-- Iterate Each Note --}}
                                        @foreach($product->notes as $note)
                                            @if (!($note->isHandled))
                                            {{-- If note type is purchased, Show qty and unit instead of comments.  --}}
                                                @if(($note->type_id) == 2)
                                                    <tr>
                                                        <td><font color="#{{$note->type->color}}">{{$note->type->name}}</font></td>
                                                        <td class="lightTable">
                                                            [{{$note->qty}} {{$note->unit}} by {{$note->employee->name}}] on [{{$note->created_at->format('Y-m-d')}}]

                                                            @if ($note->comments == NULL)
                                                            @else
                                                                Comments: {{$note->comments}}
                                                            @endif
                                                        </td>
                                                    </tr>
                                                {{-- Obsolete Note --}}
                                                @elseif (($note->type_id) == 3)
                                                    <tr>
                                                        <td><font color="#{{$note->type->color}}">{{$note->type->name}}</font></td>
                                                        <td class="lightTable">
                                                            Obsoleted by {{$note->employee->name}} -- [{{$note->updated_at}}]
                                                        </td>
                                                    </tr>
                                                {{-- Shipment Found Note  --}}
                                                @elseif((($note->type_id) == 9))
                                                    <tr>
                                                        <td><font color="#{{$note->type->color}}">{{$note->type->name}}</font></td>
                                                        <td class="lightTable">
                                                            @if ($note->product->cart)
                                                                @if($note->product->cart->shipment)
                                                                    <a href="{{route('shipment.view', $note->product->cart->shipment->id)}}">Found in Shipment: #{{$note->product->cart->shipment->id}}</a>
                                                                @endif
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @else
                                                    <tr>
                                                        <td><font color="#{{$note->type->color}}">{{$note->type->name}}</font></td>
                                                        <td class="lightTable">{{$note->comments}}</td>
                                                    </tr>
                                                @endif
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            @endif
                        @endif
                        @endauth
                    @endif
                @endauth

            </td>
            <td>${{$product->price}}</td>
            <td>{{$product->quantity}}</td>
            @if(Auth::user()->hasAccess('cost')->read)<td>${{$product->pivot->recent_cost}}</td>@endif
            <td>
                <small id="currentRemark{{$product->id_product}}" class="mb-2">Current: {{$product->pivot->remark}}</small>
                <form class="remarkForm" method="POST" action="{{route('vendors.update-remark')}}">
                    {{csrf_field()}}
                    <input type="hidden" value="{{$product->id_product}}" name="pid"/>
                    <input type="hidden" value="{{$vendor->id}}" name="vendor_id"/>
                    <div class="input-group">

                        <input class="form-control form-control-sm" value="{{$product->pivot->remark}}" name="remark"/>

                        <div class="input-group-append">
                            <button class="btn btn-primary btn-sm" type="submit">Update</button>
                        </div>

                    </div>   
                </form>
            </td>
            <td>
                @php $relations = App\ProductRelation::where('product_id', $product->id_product)->get() @endphp
                @foreach($relations as $relation)

                    #{{$relation->related_product_id}} | 

                    @php $shipmentNotes = App\ProductNote::where('product_id', $relation->related_product_id)->where('type_id', '9')->where('isHandled', '0')->get() @endphp
                    @php $purchaseNotes = App\ProductNote::where('product_id', $relation->related_product_id)->where('type_id', '2')->where('isHandled', '0')->get() @endphp

                    @if(($purchaseNotes->count() > 0) || ($shipmentNotes->count() > 0))
                        <span class="nameIcons" data-feather="check"></span>
                    @else
                        <span class="nameIcons" data-feather="x"></span>
                    @endif
                    <br/>
                @endforeach
            </td>
            <td>
                @foreach($product->vendors as $alt)
                    @if($vendor->name != $alt->name)
                        <a href="{{ route('coreui.vendors.products',$alt->id) }}">{{$alt->name}}</a>
                        @if($alt->pivot->remark)
                            - {{$alt->pivot->remark}}
                        @endif
                        @if(Auth::user()->hasAccess('cost')->read)
                        (${{$alt->pivot->recent_cost}})
                        @endif
                        <br>
                    @endif
                @endforeach
            </td>
            <td>
                <div class="container text-center position-ref">

                    <button class="btn btn-secondary btn-sm btn-action mb-1"
                        data-toggle="modal"
                        data-target="#purchaseAndShipModal"

                        data-id="{{ $product->id_product }}" 
                        data-name="{{ $product->name }}"
                        data-vendor="{{$product->vendor}}"
                        data-remarks="{{$product->vendor_remarks}}"

                        data-url="{{route('order-suggestion.purchaseAndShip', $product->id_product)}}">Purchase and Ship
                    </button>

                    {{-- Ordered button and form begin here --}}
                    <button class="btn btn-secondary btn-sm btn-action mb-1"
                        data-toggle="modal"
                        data-target="#purchaseModal"
                        data-url="{{route('order-suggestion.purchase', $product->id_product )}}">Purchase and Hold
                    </button>

                    <button id='obsoleteBtn{{$product->id_product}}' 
                        type="button" 
                        class="btn btn-secondary btn-sm btn-action mb-1" 
                        onclick="obsolete('{{route('product.obsolete', $product->id_product)}}', '{{$product->id_product}}')" 
                        @if($product->isObsoleted()) disabled @endif>Obsoleting
                    </button><br>

                    <button class="btn btn-secondary btn-sm btn-action mb-1"
                        data-toggle="modal"
                        data-target="#restrictSalesModal"
                        data-id="{{$product->id_product}}"
                        data-name="{{$product->name}}"
                        data-url="{{route('notes.restrictSale', $product->id_product )}}">Restrict Sales
                    </button>

                    @if($product->orderSuggestion)
                        <button class="btn btn-secondary btn-sm btn-action mb-1" onclick="ignoreSuggestion({{$product->orderSuggestion->id}}, {{$product->orderSuggestion->product_id}},this)">Remove Order Suggestion</button>
                    @endif

                    <button class="btn btn-secondary btn-sm btn-action"
                        type="button"
                        data-toggle="modal"
                        data-target="#printModal"
                        data-id= "{{$product->id_product}}">Print Labels
                    </button>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

    {{-- Purchase Modal --}}
    <div class="modal fade" id="purchaseModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="POST">
                	{{ csrf_field() }}
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Input Order Information</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>

					<div class="modal-body">
						<label>Quantity:</label>
                    	<input class="form-control" type="text" name="qty" id="qty"><br/>

						<label>Unit:</label>
                        <select class="custom-select mr-sm-2 mb-2" name="unit" id="unit" required>
                            <option value="piece">Piece</option>
                            <option value="package">Package</option>
                            <option value="meter">Meter</option>
                            <option value="foot">Foot</option>
                            <option value="1/2lb">1/2LB</option>
                            <option value="box">Box</option>
                            <option value="roll">Roll</option>
                            <option value="winpos unit">Winpos Unit</option>
                        </select>

						<label>Comments:</label>
                    	<textarea class="form-control" type="text" name="comments" id="note-comments"></textarea><br/>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
						<a id="note-cancel" href="#">
						<button type="submit" class="btn btn-primary">Order Item</button>
						</a>
					</div>
        		</form>
            </div>
    	</div>
    </div>

    {{-- Purchase and Ship Modal --}}
    <div class="modal fade" id="purchaseAndShipModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="POST">
                	{{ csrf_field() }}
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Insert Order into Shipment</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>

					<div class="modal-body">
						<label>Product ID:</label>
                    	<input class="form-control" type="text" name="id" id="id" readonly><br/>

						<label>Product Name:</label>
                    	<input class="form-control" type="text" name="name" id="name" readonly><br/>

						<label>Ordered Quantity:</label>
                    	<input class="form-control" type="text" name="qty" id="qty"><br/>

                        <label>Vendor:</label>
                        <input class="form-control" type="text" name="vendor" id="vendor" readonly><br/>

                        <label>Remark:</label>
                    	<input class="form-control" type="text" name="remarks" id="remarks" readonly><br/>

                    	<label>Ordered Unit:</label>
                    	<select class="custom-select mr-sm-2 mb-2" name="unit" id="unit" required>
                            <option value="piece">Piece</option>
                            <option value="package">Package</option>
                            <option value="meter">Meter</option>
                            <option value="foot">Foot</option>
                            <option value="1/2lb">1/2LB</option>
                            <option value="box">Box</option>
                            <option value="roll">Roll</option>
                            <option value="winpos unit">Winpos Unit</option>
                        </select><br/>

                        @php $shipments = App\Shipment::all() @endphp
                        <label>Select Shipment:</label>
                        <select class="custom-select mr-sm-2 mb-2" name="shipment" id="shipment" required>
                            @foreach($shipments as $shipment)
                                @if ($shipment->isActive)
                                    <option value="{{$shipment->id}}">{{$shipment->id}} | [{{$shipment->status->name}}] | {{$shipment->vendor->name}} | 
                                        @if ($shipment->isLocal == '1')
                                            LOCAL
                                        @else
                                            NOT LOCAL
                                        @endif
                                        | {{$shipment->type->name}}
                                    </option>
                                @endif
                            @endforeach
                        </select>
                        <br/>
                        <p class='smallNote'> If no shipment is available, create one <a href="{{route('shipment.create')}}" target="_blank">here.</a>
                            <a href="javascript:window.location.href=window.location.href"> Refresh page.</a>
                        </p>

                    	<label>Invoice:</label>
                    	<input class="form-control" type="text" name="invoice" id="invoice"><br/>

						<label>Comments:</label>
                    	<textarea class="form-control" type="text" name="comments" id="comments"></textarea><br/>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>

						<button type="submit" class="btn btn-primary">Purchase Suggestion</button>
					</div>
        		</form>
            </div>
    	</div>
    </div>

    {{-- Restrict Sales Modal --}}
    <div class="modal fade" id="restrictSalesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="POST">
                	{{ csrf_field() }}
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Input Information</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>

					<div class="modal-body">
                        <label>Product ID:</label>
                    	<input class="form-control" type="text" name="id" id="id" readonly><br/>

						<label>Product Name:</label>
                    	<input class="form-control" type="text" name="name" id="name" readonly><br/>

						<label>Comments:</label>
                    	<textarea class="form-control" type="text" name="comments" id="note-comments"></textarea><br/>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
						<a id="note-cancel" href="#">
						<button type="submit" class="btn btn-primary">Restrict</button>
						</a>
					</div>
        		</form>
            </div>
    	</div>
    </div>

    @include('layouts.printers')

@endsection

@push('bottom-scripts')
<script type="text/javascript">
    $(document).ready(function () {
        $('#product_table').DataTable({
            scrollX:    true,
            paging:     false,
            language: {
                "search": "Filter records:"
            }
        });
    });

    $("form.remarkForm").submit(function(e) {
            var form = $(this);
            var url = form.attr('action');
        
            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(), // serializes the form's elements.
                dataType: 'json',
                success: function(data){
                    // header
                    if(data.status == "success"){
                        var header = '<span data-feather="check-circle" style="width:50px;height:50px;stroke:white;"></span>';
                        $('#messageModal .modal-header').css("background-color","#6ba984");
                        $('#currentRemark' + form.find('[name="pid"]').val()).html("Current: " + form.find('[name="remark"]').val());
                    }else{
                        var header = '<span data-feather="alert-circle" style="width:50px;height:50px;stroke:white;"></span>';
                        $('#messageModal .modal-header').css("background-color","#e66262");
                    }
                    
                    $('#messageModal .modal-title').html(header);

                    feather.replace()

                    // body
                    $('#messageModal .modal-body #message').html(data.message);
                    $('#messageModal').modal('show');

                },
                error: function(request, status, error){
                    $('#notifierModal').modal('hide');

                    var header = '<span data-feather="alert-circle" style="width:50px;height:50px;stroke:white;"></span>';
                    $('#messageModal .modal-header').css("background-color","#e66262");

                    $('#messageModal .modal-title').html(header);

                    feather.replace()

                    $('#messageModal .modal-body #message').html(error);
                    $('#messageModal').modal('show');
                }
            });
        
            e.preventDefault(); // avoid to execute the actual submit of the form.
        });
</script>

   <script>
    $(document).ready(function () {
        $('#purchaseModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) 
            var url = button.data('url')
            var modal = $(this)
            modal.find('form').attr('action',url)
        });
    });
    </script>

    <script>
    $(document).ready(function () {
        $('#purchaseAndShipModal').on('show.bs.modal', function (event) {

            var button = $(event.relatedTarget) // Button that triggered the modal
            var id = button.data('id') // Extract info from data-* attributes
            var name = button.data('name') // Extract info from data-* attributes
            var vendor = button.data('vendor')
            var remarks = button.data('remarks')
            var url = button.data('url')

            var modal = $(this)
            modal.find('.modal-title').text('Insert Item: ' + name)
            modal.find('#id').val(id)
            modal.find('#name').val(name)
            modal.find('#vendor').val(vendor)
            modal.find('#remarks').val(remarks)
            modal.find('form').attr('action',url)
        });
    });
    </script>

    <script>
    $(document).ready(function () {
        $('#restrictSalesModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var id = button.data('id') // Extract info from data-* attributes
            var name = button.data('name') // Extract info from data-* attributes
            var url = button.data('url')

            var modal = $(this)
            modal.find('.modal-title').text('Restrict Sale: ' + name)
            modal.find('#id').val(id)
            modal.find('#name').val(name)
            modal.find('form').attr('action',url)
        });
    });
    </script>

    <script>
        function obsolete(url, pid){
            $.ajax({
                type: "post",
                url: url,
                data: '_token={{csrf_token()}}', 
                dataType: 'json',
                success: function( msg ) {
                    document.getElementById('obsoleteBtn' + pid).disabled = true;
                    alert("Obsoleting Successful.");
                },
                error: function (request, status, error) {
                    alert(request.status + ':' + error + '. Obsoleting failed. Please contact DevTeam.');
                }
            });
        }
    </script>

    <script>
        function ignoreSuggestion(suggestion_id,product_id,btn) {
            btn.disabled = true;

            $.ajax({
                type: "get",
                url: "{{ route('order-suggestion.ignore') }}",
                data: {suggestion_id: suggestion_id, product_id: product_id},
                dataType: 'json',
                success: function( msg ) {
                    console.log(msg)
                    console.log(msg.status)
                    if(msg.status == "success"){
                        alert("Succesfully removed product ID: #" + product_id + " from suggestion list. Changes will be updated upon page refresh.");
                    }else{
                        alert(msg.message);
                        btn.disabled = true;
                    }
                },
                error: function (request, status, error) {
                    alert(request.status + ':' + error + '. Please take picture and contact DevTeam.');
                    btn.disabled = true;
                }
            });
        }
    </script>

@endpush