<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleSuggestion extends Model
{
    protected $table='sale_suggestions';
    protected $connection='mysql';
    protected $guarded = [];

    public function product()
    {
        return $this->belongsTo(Product::class,'product_id');
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }

    public function vendor(){
        return $this->belongsTo(Vendor::class, 'vendor_id');
    }
}