<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrder extends Model
{
    protected $connection = "mysql";
    protected $primaryKey = "po_id";
    protected $guarded = [];

    public $timestamps = false;
    public $incrementing = false; 

    public function products() {
        return $this->hasMany(PurchaseOrderProduct::class,'po_id');
    }

    public function vendor() {
        return $this->belongsTo(Vendor::class);
    }

    /******************************************************************
     * 
     *      QUERY SCOPES     
     * 
     ******************************************************************/

    public function scopeActive($query) {
        return $query->where('pr_id','!=','-3');
    }

    public function scopeOpen($query) {
        return $query->where('pr_id','=','-1');
    }

    public function scopeFilter($query, $filters){
        if(isset($filters['date'])){
            $date = $filters['date'];
            $query->whereYear('created_at',$date);
        }
        if(isset($filters['opened'])){
            $opened = $filters['opened'];
            if($opened == "true"){
                $query->open();
            }
        }
    }
}
