<?php

namespace App\Http\Controllers;

use App\RMA;
use App\RMA_Status;
use App\RMAStatusHistory;
use App\Employee;
use App\EmployeeRole;
use App\OrderSuggestion;
use App\Vendor;
use Auth;
use DB;
use Table;
use Illuminate\Http\Request;

class RMAController extends Controller
{
    public function index(Request $request){
        if ($request->showAllState == 1){
            $rmas = RMA::all();
            $buttonText = 'Show only active RMAs';
            $showAllState = 1;
            $title = 'All RMAs';
        }
        else {
            $rmas = RMA::all()->where('isActive', 1);
            $buttonText = 'Show all RMAs (including inactive)';
            $showAllState = 0;
            $title = 'Active RMAs List';
        }

        return view('frontend.rma.index', compact('rmas', 'buttonText', 'showAllState', 'title'));
    }

    public function show(RMA $rma){
        $status_history = $rma->statuses;
        $statuses = RMA_Status::all();

        return view('frontend.rma.show',compact('rma','status_history','statuses'));
    }

    public function create(){
        $rmas = RMA::all();
        $statuses = RMA_Status::all();

        return view('frontend.rma.create',compact('statuses', 'rmas'));
    }

    public function store(Request $request)
    {
        $vendor_ref = request('product_vendor');
        $vend = DB::table('vendors')->where('reference', $vendor_ref)->first();

        // Validate form
        $this->validate(request(), [
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'id_product' => 'required|integer|exists:leeselectronic.table_1,id_product',
            'product_name' => 'required',
            'qty' => 'required|integer',
            'invoice_num' => 'required|string|min:7|max:8|regex:/^[\w-]*$/',
        ]);

        $phone = preg_replace('/\D+/', '', $request->phone);
        
        $rma = RMA::create([
            'name' => request('name'),
            'phone' => $phone,
            'phone_ext' => request('phone_ext'),
            'email' => request('email'),
            'id_product' => request('id_product'),
            'product_name' => request('product_name'),
            'qty' => request('qty'),
            'vendor_id' => $vend->id,
            'employee_id' => Auth::user()->id,
            'invoice_num' => request('invoice_num'),
            'invoice_date' => request('invoice_date'),
            'comments' => request('comments')
        ]);
        
        $rma->save();

        $rma_history = RMAStatusHistory::create([
            'rma_id' => $rma->id,
            'status_id' => '1',
            'employee_id' => Auth::user()->id,
            'comment' => request('comments'),
        ]);

        $rma_history->save();

        return redirect()->route('rma');
    }

    public function deactivate(Request $request, RMA $rma)
    {
        $rmas = RMA::all();
        $rma -> isActive = '0';

        $rma->save();

        return redirect()->route('rma');
    }

    public function printLabel(RMA $rma){
        return view('frontend.rma.printLabel', compact('rma'));
    }
}
