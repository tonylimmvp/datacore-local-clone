<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\ServiceType;

use App\Company;

class ServiceTypesController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function filter(Request $request){

        if (count($request->all())==1) { //if no request, only csrf
            $companies = App\Company::latest()->get();
            $filters = App\ServiceType::all(); // display filters on index
            return view('frontend.phonebook.index', compact("companies"), compact('filters'));
        }
        //if filter(s) on
        $filter_arry=[];
        $count=0;
        foreach($request->all() as $filter){
            if($count == 0){
                $count++;
            }
            elseif(ServiceType::find($filter) == null){
                return back();
            }
            else{
                array_push($filter_arry,$filter);
            }
        }
        $companies = Company::with('service_types')->whereHas('service_types', function ($query) use ($filter_arry ){
            $query->whereIn('id', $filter_arry);
        })->get();
        $filters = App\ServiceType::all(); // display filters on index
        return view('frontend.phonebook.index', compact("companies"), compact('filters'));
    }
    public function create(){
        return view('frontend.phonebook.create-filter');
    }

    public function store(Request $request){
        $this->validate(request(),[
            'name' => 'required',
        ]);

        $name = $request->input('name');

        $filter = new ServiceType();
        $filter->name = $name;

        $serviceTypes = ServiceType::latest()->get();
        
        //need to check if already exists
        foreach($serviceTypes as $serviceType){
            if($serviceType->name == $filter->name){
                session()->flash("error", "ERROR: this filter already exists");
                return back();
            }
        }

        $filter->save();
        
        session()->flash("message", "Successfully created filter!");

        return redirect( route('phonebook'));
    }

    public function manage(){
        $filters = App\ServiceType::all();
        return view('frontend.phonebook.manageFilter',compact('filters'));
    }

    public function update(Request $request){
        // get all current filters and set to all not active
        $filters = App\Servicetype::all();
        foreach($filters as $filter){
            $filter->isActive= false;
            $filter->save();
        }
        // Then set the appropirate active
        $count = 0;
        foreach($request-> all() as $filter){
            if($count < 2){
                $count++;
            }
            else{
                $theFilter= ServiceType::find($filter);
                $theFilter->isActive= true;
                $theFilter->save();
            }

        }
        session()->flash("message", "Successfully updated!");
        return redirect( route('phonebook'));
    }
}
