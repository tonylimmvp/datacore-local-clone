<?php

use Illuminate\Database\Seeder;
use App\StoreIP;
use App\LabelPrinter;

class StorePrinterIPSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ip = new StoreIP();
        $ip->ip_address = "69.50.177.18";
        $ip->created_at = "2017-05-06 13:13:38";
        $ip->updated_at = NOW();
        $ip->save();

        $ip = new StoreIP();
        $ip->ip_address = "69.51.139.55";
        $ip->created_at = "2017-05-06 13:24:16";
        $ip->updated_at = NOW();
        $ip->save();

        $ip = new StoreIP();
        $ip->ip_address = "172.103.241.234";
        $ip->created_at = "2017-11-22 15:39:28";
        $ip->updated_at = NOW();
        $ip->save();

        $ip = new StoreIP();
        $ip->ip_address = "209.205.90.194";
        $ip->created_at = "2018-03-02 09:19:53";
        $ip->updated_at = NOW();
        $ip->save();

        $ip = new StoreIP();
        $ip->ip_address = "208.110.116.114";
        $ip->created_at = "2018-03-02 09:21:59";
        $ip->updated_at = NOW();
        $ip->save();

        $ip = new StoreIP();
        $ip->ip_address = "172.16.2.29";
        $ip->created_at = "2018-03-02 12:06:18";
        $ip->updated_at = NOW();
        $ip->save();

        $ip = new StoreIP();
        $ip->ip_address = "207.81.180.69";
        $ip->created_at = "2018-03-22 14:53:46";
        $ip->updated_at = NOW();
        $ip->save();

        $ip = new StoreIP();
        $ip->ip_address = "208.110.116.114";
        $ip->created_at = "2018-05-01 10:34:53";
        $ip->updated_at = NOW();
        $ip->save();

        $printer = new LabelPrinter();
        $printer->name = 'GoDex Upstairs';
        $printer->port = '9100';
        $printer->location = "UPSTAIRS";
        $printer->type = "godex";
        $printer->size = "small";
        $printer->isActive = true;
        $printer->save();

        $printer = new LabelPrinter();
        $printer->name = 'GoDex Downstairs';
        $printer->port = '9101';
        $printer->location = "DOWNSTAIRS";
        $printer->type = "godex";
        $printer->size = "small";
        $printer->isActive = true;
        $printer->save();

        $printer = new LabelPrinter();
        $printer->name = 'ZEBRA Downstairs';
        $printer->port = '9102';
        $printer->location = "DOWNSTAIRS";
        $printer->type = "zebra";
        $printer->size = "large";
        $printer->isActive = true;
        $printer->save();

        $printer = new LabelPrinter();
        $printer->name = 'ZEBRA Upstairs';
        $printer->port = '9103';
        $printer->location = "UPSTAIRS";
        $printer->type = "zebra";
        $printer->size = "small";
        $printer->isActive = true;
        $printer->save();

    }
}
