<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLabelPrintersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('label_printers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('ip_address')->nullable();
            $table->integer('port');
            $table->string('location');
            $table->string('type');
            $table->string('size');
            $table->boolean('isActive')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('label_printers');
    }
}
