<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

    <style>
        body {
            margin: 0;
            padding: 0;
            background-color: #FAFAFA;

        }
        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }
        .page {
            width: 8.2cm;
            height: 8.3cm;
            margin: 0cm auto;
            border: 1px #D3D3D3 solid;
            border-radius: 0px;
            background: white;
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
            line-height: 4px;
        }

        @page {
            size: 5.5cm 6.3cm;
            margin: 0;
        }

        @media print {
            .page {
                margin: 0;
                border: initial;
                border-radius: initial;
                width: 6.3cm;
                min-height: 5.5cm;
                box-shadow: initial;
                background: initial;
                page-break-after: always;
            }
        }

        #container_style1{
            width: 301px;
            text-align: center;
            line-height: 0px;
        }

        h1 {
            text-align: center;
            font: 32pt "Nunito";
            font-weight: bold;
        }

        h2 {
            text-align: center;
            font: 18pt "Nunito";
        }

        h3 {
            text-align: center;
            font: 14pt "Nunito";
        }

    </style>
</head>

<body>
    <div id='container_style1' class='page'>
        <h1>RMA #{{$rma->id}}</h1>
        <h2>{{$rma->product_name}} (#{{$rma->id_product}})</h2>
        <h3>{{$rma->name}} ({{$rma->phone}})</h3>
        <h3>Invoice #: {{$rma->invoice_num}}</h3>
    </div>
</body>