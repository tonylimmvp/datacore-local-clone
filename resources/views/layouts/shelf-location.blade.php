<div id="shelfLocationModal" class="modal"> 
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="max-height:90vh; background-color:rgb(255, 255, 255)" version="1.1" viewBox="-0.5 -0.5 836 1088">
				<defs/>
				<g>
					<rect id="COMPUTER" width="210" height="270" x="310" y="815" fill="#ffffff" stroke="#000000" pointer-events="none"/>

					<rect width="210" height="80" x="310" y="675" fill="#ffffff" stroke="#000000" pointer-events="none" rx="12" ry="12"/>
					<rect id="FD" width="87.5" height="80" x="371.25" y="675" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(406.5,708.5)">
						<switch>
							<foreignObject width="16" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:17px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										FD
									</div>
								</div>
							</foreignObject>
							<text x="8" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								FD
							</text>
						</switch>
					</g>
					
					<rect id="BEAM" width="80" height="70" x="395.5" y="415" fill="#ffffff" stroke="#000000" pointer-events="none" rx="10.5" ry="10.5"/>
					<g transform="translate(418.5,443.5)">
						<switch>
							<foreignObject width="34" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:36px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										BEAM
									</div>
								</div>
							</foreignObject>
							<text x="17" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								BEAM
							</text>
						</switch>
					</g>

					{{-- KIT RACKS --}}
					
					<rect id="KIT1" width="104" height="20" x="132.5" y="525" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(176.5,528.5)">
						<switch>
							<foreignObject width="16" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:18px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										KIT1
									</div>
								</div>
							</foreignObject>
							<text x="8" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								KIT1
							</text>
						</switch>
					</g>

					<rect id="KIT2" width="104" height="20" x="611.5" y="982.5" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(650.5,986.5)">
						<switch>
							<foreignObject width="26" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:26px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										KIT2
									</div>
								</div>
							</foreignObject>
							<text x="13" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								KIT2
							</text>
						</switch>
					</g>
					{{-- END OF KIT RACK --}}

					<rect id="RR" width="45" height="30" x="585" y="1050" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(598.5,1058.5)">
						<switch>
							<foreignObject width="17" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:19px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										RR
									</div>
								</div>
							</foreignObject>
							<text x="9" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								RR
							</text>
						</switch>
					</g>
					
					<rect id="DIR" width="90" height="45" x="640" y="1035" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(674.5,1051.5)">
						<switch>
							<foreignObject width="20" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:22px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										DIR
									</div>
								</div>
							</foreignObject>
							<text x="10" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								DIR
							</text>
						</switch>
					</g>

					<rect id="TR" width="40" height="257" x="785" y="749" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(796.5,871.5)">
						<switch>
							<foreignObject width="16" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:17px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										TR
										<br/>
									</div>
								</div>
							</foreignObject>
							<text x="8" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								TR&lt;br&gt;
							</text>
						</switch>
					</g>

					<rect id="TB" width="38" height="32" x="564.5" y="43" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(575.5,52.5)">
						<switch>
							<foreignObject width="16" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:16px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										TB
									</div>
								</div>
							</foreignObject>
							<text x="8" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								TB
							</text>
						</switch>
					</g>
					
					<rect id="BOXRACK" width="28.5" height="90" x="451.5" y="310" fill="#ffffff" stroke="#000000" pointer-events="none"/>

					<rect id="3DFIL" width="28.5" height="90" x="480" y="310" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(512.5,348.5)">
						<switch>
							<foreignObject width="32" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:34px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										3DFIL
									</div>
								</div>
							</foreignObject>
							<text x="16" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								3DFIL
							</text>
						</switch>
					</g>

					 {{-- DISPLAY RACK --}}

					<rect id="DR1" width="50" height="275" x="0" y="195" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(12.5,326.5)">
						<switch>
							<foreignObject width="24" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:26px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										DR1
									</div>
								</div>
							</foreignObject>
							<text x="12" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								DR1
							</text>
						</switch>
					</g>

					<rect id="DR2" width="50" height="90" x="0" y="105" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(12.5,143.5)">
						<switch>
							<foreignObject width="24" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:26px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										DR2
									</div>
								</div>
							</foreignObject>
							<text x="12" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								DR2
							</text>
						</switch>
					</g>
					
					<rect id="DR3" width="50" height="70" x="0" y="35" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(12.5,63.5)">
						<switch>
							<foreignObject width="24" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:26px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										DR3
									</div>
								</div>
							</foreignObject>
							<text x="12" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								DR3
							</text>
						</switch>
					</g>

					<rect id="DR4" width="50" height="280" x="785" y="275" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(797.5,408.5)">
						<switch>
							<foreignObject width="24" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:26px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										DR4
									</div>
								</div>
							</foreignObject>
							<text x="12" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								DR4
							</text>
						</switch>
					</g>
					
					<rect id="DR5" width="50" height="90" x="785" y="555" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(797.5,593.5)">
						<switch>
							<foreignObject width="24" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:26px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										DR5
									</div>
								</div>
							</foreignObject>
							<text x="12" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								DR5
							</text>
						</switch>
					</g>
					
					<rect id="DR6" width="50" height="90" x="785" y="645" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(797.5,683.5)">
						<switch>
							<foreignObject width="24" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:26px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										DR6
									</div>
								</div>
							</foreignObject>
							<text x="12" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								DR6
							</text>
						</switch>
					</g>
					{{-- END OF DISPLAY RACK --}}

					{{-- HEATSHRINK BOX --}}
					<rect id="HB1" width="61" height="35" x="344.5" y="375" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(363.5,386.5)">
						<switch>
							<foreignObject width="23" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:25px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										HB1
									</div>
								</div>
							</foreignObject>
							<text x="12" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								HB1
							</text>
						</switch>
					</g>

					<rect id="HB2" width="40" height="60" x="344.5" y="415" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(352.5,438.5)">
						<switch>
							<foreignObject width="24" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:24px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										HB2
									</div>
								</div>
							</foreignObject>
							<text x="12" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								HB2
							</text>
						</switch>
					</g>
					{{-- END OF HEATSHRINK BOX --}}

					{{-- CABLE RACKS --}}

					<rect id="CR1" width="210" height="45" x="81" y="1027.5" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(173.5,1043.5)">
						<switch>
							<foreignObject width="24" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:26px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										CR1
									</div>
								</div>
							</foreignObject>
							<text x="12" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								CR1
							</text>
						</switch>
					</g>

					<rect id="CR2" width="40" height="60" x="10" y="987" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(17.5,1010.5)">
						<switch>
							<foreignObject width="24" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:26px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										CR2
									</div>
								</div>
							</foreignObject>
							<text x="12" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								CR2
							</text>
						</switch>
					</g>

					<rect id="CR3" width="104" height="20" x="133.5" y="949" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(172.5,952.5)">
						<switch>
							<foreignObject width="26" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:26px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										CR3
									</div>
								</div>
							</foreignObject>
							<text x="13" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								CR3
							</text>
						</switch>
					</g>

					{{-- END OF CABLE RACKS --}}

					{{-- MOBILE RACK --}}
					<ellipse id="MR1" cx="473" cy="529" fill="#ffffff" stroke="#000000" pointer-events="none" rx="36" ry="36"/>
					<g transform="translate(459.5,522.5)">
						<switch>
							<foreignObject width="26" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:26px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										MR1
									</div>
								</div>
							</foreignObject>
							<text x="13" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								MR1
							</text>
						</switch>
					</g>

					<path id="MR2" fill="#ffffff" stroke="#000000" stroke-miterlimit="10" d="M 530 1025 L 570 1050 L 530 1075 Z" pointer-events="none"/>
					<g transform="translate(536.5,1043.5)">
						<switch>
							<foreignObject width="26" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:26px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										MR2
									</div>
								</div>
							</foreignObject>
							<text x="13" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								MR2
							</text>
						</switch>
					</g>

					<rect id="MR3" width="100" height="32" x="720" y="43" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(756.5,52.5)">
						<switch>
							<foreignObject width="26" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:26px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										MR3
									</div>
								</div>
							</foreignObject>
							<text x="13" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								MR3
							</text>
						</switch>
					</g>

					<rect id="MR4" width="35" height="75" x="486.5" y="415" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(491.5,446.5)">
						<switch>
							<foreignObject width="15" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:27px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										MR4
									</div>
								</div>
							</foreignObject>
							<text x="8" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								MR4
							</text>
						</switch>
					</g>

					<rect id="MR5" xmlns="http://www.w3.org/2000/svg" x="58" y="35" width="72" height="30" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g xmlns="http://www.w3.org/2000/svg" transform="translate(80.5,43.5)">
						<switch>
							<foreignObject style="overflow:visible;" pointer-events="all" width="26" height="12" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display: inline-block; font-size: 12px; font-family: Helvetica; color: rgb(0, 0, 0); line-height: 1.2; vertical-align: top; width: 26px; white-space: nowrap; overflow-wrap: normal; text-align: center;">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit;">MR5
									</div>
								</div>
							</foreignObject>
							<text x="13" y="12" fill="#000000" text-anchor="middle" font-size="12px" font-family="Helvetica">MR5</text>
						</switch>
					</g>

					{{-- END OF MOBILE RACK --}}

					{{-- WINDOWS --}}
					<rect id="WIN1" width="160" height="15" x="0" y="0" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(64.5,1.5)">
						<switch>
							<foreignObject width="30" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:31px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										WIN1
									</div>
								</div>
							</foreignObject>
							<text x="15" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								WIN1
							</text>
						</switch>
					</g>
					<rect id="WIN2" width="130" height="15" x="160" y="0" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(209.5,1.5)">
						<switch>
							<foreignObject width="30" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:31px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										WIN2
									</div>
								</div>
							</foreignObject>
							<text x="15" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								WIN2
							</text>
						</switch>
					</g>
					<rect id="WIN3" width="170" height="15" x="479.5" y="0" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(549.5,1.5)">
						<switch>
							<foreignObject width="30" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:31px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										WIN3
									</div>
								</div>
							</foreignObject>
							<text x="15" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								WIN3
							</text>
						</switch>
					</g>
					<rect id="WIN4" width="170" height="15" x="650" y="0" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(719.5,1.5)">
						<switch>
							<foreignObject width="30" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:31px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										WIN4
									</div>
								</div>
							</foreignObject>
							<text x="15" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								WIN4
							</text>
						</switch>
					</g>

					{{-- END OF WINDOWS --}}

					{{-- SHELF --}}
					<rect id="S1" width="15.08" height="383" x="169.67" y="552.5" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<rect id="FL1" width="32.17" height="383" x="137" y="552.5" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(106.5,737.5)">
						<switch>
							<foreignObject width="6" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:8px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										1
									</div>
								</div>
							</foreignObject>
							<text x="3" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								1
							</text>
						</switch>
					</g>

					<rect id="S2" width="15.08" height="383" x="185.75" y="552.5" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<rect id="FL2" width="32.17" height="383" x="200.83" y="552.5" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(249.5,737.5)">
						<switch>
							<foreignObject width="6" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:8px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										2
									</div>
								</div>
							</foreignObject>
							<text x="3" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								2
							</text>
						</switch>
					</g>

					<rect id="S3" width="15" height="430" x="170" y="35" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<rect id="FL3" width="32" height="430" x="137.5" y="35" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(113.5,243.5)">
						<switch>
							<foreignObject width="6" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:8px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										3
									</div>
								</div>
							</foreignObject>
							<text x="3" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								3
							</text>
						</switch>
					</g>

					<rect id="S4" width="15" height="430" x="186" y="35" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<rect id="FL4" width="32" height="430" x="201" y="35" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(249.5,243.5)">
						<switch>
							<foreignObject width="6" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:8px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										4
									</div>
								</div>
							</foreignObject>
							<text x="3" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								4
							</text>
						</switch>
					</g>
					
					<rect id="FL5" width="32.17" height="260" x="434" y="35" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<rect id="S5" width="15.08" height="260" x="466.67" y="35" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(410.5,158.5)">
						<switch>
							<foreignObject width="6" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:8px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										5
									</div>
								</div>
							</foreignObject>
							<text x="3" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								5
							</text>
						</switch>
					</g>

					<rect id="S6" width="15.08" height="260" x="482.75" y="35" fill="#fff" stroke="#000000" pointer-events="none"/>
					<rect id="FL6" width="32.17" height="260" x="497.83" y="35" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(546.5,158.5)">
						<switch>
							<foreignObject width="6" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:8px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										6
									</div>
								</div>
							</foreignObject>
							<text x="3" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								6
							</text>
						</switch>
					</g>

					<rect id="FL7" width="32.17" height="181" x="614" y="44" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<rect id="S7" width="15.08" height="181" x="646.67" y="44" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(588.5,128.5)">
						<switch>
							<foreignObject width="6" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:8px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										7
									</div>
								</div>
							</foreignObject>
							<text x="3" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								7
							</text>
						</switch>
					</g>

					<rect id="S8" width="15.08" height="181" x="662.75" y="44" fill="#fff" stroke="#000000" pointer-events="none"/>
					<rect id="FL8" width="32.17" height="181" x="677.83" y="44" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(726.5,128.5)">
						<switch>
							<foreignObject width="6" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:8px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										8
									</div>
								</div>
							</foreignObject>
							<text x="3" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								8
							</text>
						</switch>
					</g>

					<rect id="FL9" width="32.17" height="383" x="614.5" y="252.5" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<rect id="S9" width="15.08" height="383" x="647.17" y="252.5" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(591.5,438.5)">
						<switch>
							<foreignObject width="6" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:8px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										9
									</div>
								</div>
							</foreignObject>
							<text x="3" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								9
							</text>
						</switch>
					</g>

					<rect id="S10" width="15.08" height="383" x="663.25" y="252.5" fill="#fff" stroke="#000000" pointer-events="none"/>
					<rect id="FL10" width="32.17" height="383" x="678.33" y="252.5" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(724.5,437.5)">
						<switch>
							<foreignObject width="14" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:14px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										10
									</div>
								</div>
							</foreignObject>
							<text x="7" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								10
							</text>
						</switch>
					</g>

					<rect id="FL11" width="32.17" height="301" x="615" y="675" fill="#fff" stroke="#000000" pointer-events="none"/>
					<rect id="S11" width="15.08" height="301" x="647.67" y="675" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(588.5,819.5)">
						<switch>
							<foreignObject width="12" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:14px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										11
									</div>
								</div>
							</foreignObject>
							<text x="6" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								11
							</text>
						</switch>
					</g>

					<rect id="S12" width="15.08" height="301" x="663.75" y="675" fill="#fff" stroke="#000000" pointer-events="none"/>
					<rect id="FL12" width="32.17" height="301" x="678.83" y="675" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g transform="translate(724.5,819.5)">
						<switch>
							<foreignObject width="14" height="12" pointer-events="all" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility" style="overflow:visible">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;font-size:12px;font-family:Helvetica;color:rgb(0, 0, 0);line-height:1.2;vertical-align:top;width:14px;white-space:nowrap;overflow-wrap:normal;text-align:center">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit">
										12
									</div>
								</div>
							</foreignObject>
							<text x="7" y="12" fill="#000000" font-family="Helvetica" font-size="12px" text-anchor="middle">
								12
							</text>
						</switch>
					</g>

					{{-- END OF SHELF --}}

					{{-- WALL --}}

					<rect id="WALL1" width="10" height="500" x="0" y="485" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g xmlns="http://www.w3.org/2000/svg" transform="translate(10.5,728.5)rotate(-90,19,6)">
						<switch>
							<foreignObject style="overflow:visible;" pointer-events="all" width="38" height="12" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display: inline-block; font-size: 12px; font-family: Helvetica; color: rgb(0, 0, 0); line-height: 1.2; vertical-align: top; width: 40px; white-space: nowrap; overflow-wrap: normal; text-align: center;">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit;">
										WALL1
									</div>
								</div>
							</foreignObject>
							<text x="19" y="12" fill="#000000" text-anchor="middle" font-size="12px" font-family="Helvetica">
								WALL1
							</text>
						</switch>
					</g>

					<rect id="WALL2" width="10" height="265" x="825" y="0" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g xmlns="http://www.w3.org/2000/svg" transform="translate(795.5,128.5)rotate(-90,19,6)">
						<switch>
							<foreignObject style="overflow:visible;" pointer-events="all" width="38" height="12" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display: inline-block; font-size: 12px; font-family: Helvetica; color: rgb(0, 0, 0); line-height: 1.2; vertical-align: top; width: 40px; white-space: nowrap; overflow-wrap: normal; text-align: center;">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit;">
										WALL2
									</div>
								</div>
							</foreignObject>
							<text x="19" y="12" fill="#000000" text-anchor="middle" font-size="12px" font-family="Helvetica">
								WALL2
							</text>
						</switch>
					</g>

					<path id="WALL3" fill="#ffffff" stroke="#000000" stroke-miterlimit="10" d="M 785 740 L 835 740 L 835 748 L 795 748 L 795 1085 L 785 1085 Z" pointer-events="none" transform="translate(810,0)scale(-1,1)translate(-810,0)"/>
					<g xmlns="http://www.w3.org/2000/svg" transform="translate(795.5,1038.5)rotate(-90,19,6)">
						<switch>
							<foreignObject style="overflow:visible;" pointer-events="all" width="38" height="12" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display: inline-block; font-size: 12px; font-family: Helvetica; color: rgb(0, 0, 0); line-height: 1.2; vertical-align: top; width: 40px; white-space: nowrap; overflow-wrap: normal; text-align: center;">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit;">
										WALL3
									</div>
								</div>
							</foreignObject>
							<text x="19" y="12" fill="#000000" text-anchor="middle" font-size="12px" font-family="Helvetica">
								WALL3
							</text>
						</switch>
					</g>

					<rect id="WALL4" width="10" height="269" x="295" y="816" fill="#fff" stroke="#000000" pointer-events="none"/>
					<g xmlns="http://www.w3.org/2000/svg" transform="translate(270.5,943.5)rotate(-90,19,6)">
						<switch>
							<foreignObject style="overflow:visible;" pointer-events="all" width="38" height="12" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display: inline-block; font-size: 12px; font-family: Helvetica; color: rgb(0, 0, 0); line-height: 1.2; vertical-align: top; width: 40px; white-space: nowrap; overflow-wrap: normal; text-align: center;">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit;">
										WALL4
									</div>
								</div>
							</foreignObject>
							<text x="19" y="12" fill="#000000" text-anchor="middle" font-size="12px" font-family="Helvetica">
								WALL4
							</text>
						</switch>
					</g>

					<rect id="WALL5" width="10" height="208" x="182" y="978" fill="#ffffff" stroke="#000000" pointer-events="none" transform="rotate(90,187,1082)"/>
					<g xmlns="http://www.w3.org/2000/svg" transform="translate(35.5,1073.5)">
						<switch>
							<foreignObject style="overflow:visible;" pointer-events="all" width="38" height="12" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display: inline-block; font-size: 12px; font-family: Helvetica; color: rgb(0, 0, 0); line-height: 1.2; vertical-align: top; width: 40px; white-space: nowrap; overflow-wrap: normal; text-align: center;">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit;">
										WALL5
									</div>
								</div>
							</foreignObject>
							<text x="19" y="12" fill="#000000" text-anchor="middle" font-size="12px" font-family="Helvetica">
								WALL5
							</text>
						</switch>
					</g>

					<rect id="WALL6" width="10" height="60" x="0" y="987" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<g xmlns="http://www.w3.org/2000/svg" transform="translate(0.5,1051.5)">
						<switch>
							<foreignObject style="overflow:visible;" pointer-events="all" width="38" height="12" requiredFeatures="http://www.w3.org/TR/SVG11/feature#Extensibility">
								<div xmlns="http://www.w3.org/1999/xhtml" style="display: inline-block; font-size: 12px; font-family: Helvetica; color: rgb(0, 0, 0); line-height: 1.2; vertical-align: top; width: 40px; white-space: nowrap; overflow-wrap: normal; text-align: center;">
									<div xmlns="http://www.w3.org/1999/xhtml" style="display:inline-block;text-align:inherit;text-decoration:inherit;">
										WALL6
									</div>
								</div>
							</foreignObject>
							<text x="19" y="12" fill="#000000" text-anchor="middle" font-size="12px" font-family="Helvetica">
								WALL6
							</text>
						</switch>
					</g>

					{{-- END OF WALL --}}
					
					{{-- DOORS --}}

					<rect width="103" height="5" x="307" y="5" fill="#ffffff" stroke="#000000" pointer-events="none"/>
					<path fill="none" stroke="#000000" stroke-miterlimit="10" d="M 307 10 C 307 66.89 353.11 113 410 113 L 410 10" pointer-events="none"/>
					
					<rect width="50" height="5" x="514" y="759" fill="#ffffff" stroke="#000000" pointer-events="none" transform="rotate(-90,539,785.5)"/>
					<path fill="none" stroke="#000000" stroke-miterlimit="10" d="M 514 764 C 514 791.61 536.39 814 564 814 L 564 764" pointer-events="none" transform="rotate(-90,539,785.5)"/>
					
					<rect width="50" height="5" x="265" y="759" fill="#ffffff" stroke="#000000" pointer-events="none" transform="translate(0,785.5)scale(1,-1)translate(0,-785.5)rotate(90,290,785.5)"/>
					<path fill="none" stroke="#000000" stroke-miterlimit="10" d="M 265 764 C 265 791.61 287.39 814 315 814 L 315 764" pointer-events="none" transform="translate(0,785.5)scale(1,-1)translate(0,-785.5)rotate(90,290,785.5)"/>

					{{-- END OF DOORS --}}

				</g>
			</svg>

			
		</div>
	</div>
</div>

@push('bottom-scripts')
<script>
    function displayShelfLoc( x ){
        var spanText = x;
        var split =spanText.trim(); 
        var split = split.split("-");

        var modal = document.getElementById('shelfLocationModal');
    
        if( document.getElementById(split[0])){
            $('#shelfLocationModal').modal('show');
            if(split[1] == undefined){
                document.getElementById(split[0]).style.fill="green";
            }
            else{
                document.getElementById(split[0]).style.fill="green";
            }

            window.onclick = function(event) {
                    if (event.target == modal ) {
                            document.getElementById(split[0]).style.fill="white";
                            $('#shelfLocationModal').modal('hide');
                    }
            }
        }else{
            alert(split[0] + " does not exist.");
        }
    }
  </script>
@endpush