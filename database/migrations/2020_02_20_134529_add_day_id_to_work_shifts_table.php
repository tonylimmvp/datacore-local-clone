<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\WorkShift;

class AddDayIdToWorkShiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('work_shifts', function (Blueprint $table) {
            $table->integer('day_id')->after('day');
        });

        foreach(WorkShift::all() as $shift){
            if($shift->day == "MONDAY")
                $shift->day_id = 1;
            elseif($shift->day == "TUESDAY") 
                $shift->day_id = 2; 
            elseif($shift->day == "WEDNESDAY") 
                $shift->day_id = 3; 
            elseif($shift->day == "THURSDAY") 
                $shift->day_id = 4;
            elseif($shift->day == "FRIDAY") 
                $shift->day_id = 5;
            elseif($shift->day == "SATURDAY") 
                $shift->day_id = 6;   
            $shift->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('work_shifts', function (Blueprint $table) {
            $table->dropColumn('day_id');
        });
    }
}
