<?php

use Illuminate\Database\Seeder;
use App\Employee;
use App\Permission;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = Permission::all();


        $user = new Employee();
        $user->name = "SYSTEM";
        $user->username = "root";
        $user->email = "info@leeselectronic.com";
        $user->password = Hash::make("admin163028");
        $user->employee_role_id = 1;
        $user->save();
        foreach($permissions as $permission){
            $user->permissions()->attach($permission->id, [
                'create' => 1,
                'read' => 1,
                'update' => 1,
                'delete' => 1,
                ]);
        }
        
        $user = new Employee();
        $user->name = "Leon Cao";
        $user->username = "leoncao";
        $user->email = "leon@leeselectronic.com";
        $user->password = Hash::make("163028");
        $user->employee_role_id = 1;
        $user->save();
        foreach($permissions as $permission){
            $user->permissions()->attach($permission->id, [
                'create' => 1,
                'read' => 1,
                'update' => 1,
                'delete' => 1,
                ]);
        }
        

        $user = new Employee();
        $user->name = "Tony Lim";
        $user->username = "tonylim";
        $user->email = "tony@leeselectronic.com";
        $user->password = Hash::make("163028");
        $user->employee_role_id = 1;
        $user->save();
        foreach($permissions as $permission){
            $user->permissions()->attach($permission->id, [
                'create' => 1,
                'read' => 1,
                'update' => 1,
                'delete' => 1,
                ]);
        }

        $user = new Employee();
        $user->name = "Raymond Wong";
        $user->username = "raymond";
        $user->email = "raymondwong@leeselectronic.com";
        $user->password = Hash::make("163028");
        $user->employee_role_id = 1;
        $user->save();
        foreach($permissions as $permission){
            $user->permissions()->attach($permission->id, [
                'create' => 1,
                'read' => 1,
                'update' => 1,
                'delete' => 1,
                ]);
        }

        $user = new Employee();
        $user->name = "Theresa Wong";
        $user->username = "twong";
        $user->email = "theresawong@leeselectronic.com";
        $user->password = Hash::make("163028");
        $user->employee_role_id = 1;
        $user->save();
        foreach($permissions as $permission){
            $user->permissions()->attach($permission->id, [
                'create' => 1,
                'read' => 1,
                'update' => 1,
                'delete' => 1,
                ]);
        }

        $user = new Employee();
        $user->name = "Sales";
        $user->username = "sales";
        $user->email = "support@leeselectronic.com";
        $user->password = Hash::make("lees4131");
        $user->employee_role_id = 5;
        $user->save();
        foreach($permissions as $permission){
            $user->permissions()->attach($permission->id, [
                'create' => 0,
                'read' => 1,
                'update' => 0,
                'delete' => 0,
                ]);
        }

        // factory(Employee::class, 50)->create();
    }
}
