{{-- Notifier Modal --}}

<div class="modal fade" id="notifierModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title" id="exampleModalLabel">Notifier for #<span id="notifyProduct">---</span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
    
                <div class="modal-body">
                    <form id="notifierForm" action="{{route('notifier.add')}}" method="POST">
                        {{csrf_field()}}
                        <label> Name </label>
                        <input class="form-control mb-2" name="name" required/>
                        <label> Email </label>
                        <input class="form-control mb-2" type="email" name="email" required/>
                        <label> Phone </label>
                        <input class="form-control mb-2" name="phone" required/>
                        <label> Quantity </label>
                        <input class="form-control mb-2" type="number" name="quantity" required/>

                        <input type="hidden" name="pid"/>

                        <button class="float-right btn btn-primary" type="submit">Submit</button>

                   </form>
                </div>
            </div>
        </div>
    </div>
    
    @include('functions.printers.printer-functions')
    
    {{-- End Notifier Modal --}}
    
    @push('bottom-scripts')
    <script>

        $(document).ready(function () {
            $('#notifierModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var pid = button.data('id') // Extract info from data-* attributes
                var qty = button.data('qty') // Extract info from data-* attributes

                var modal = $(this)
                $('#notifierForm input[name="pid"]').val(pid);
                modal.find('#notifyProduct').text(pid);

                $('#notifierForm input[name="quantity"]').val(qty+1);
            });
        });
        // this is the id of the form
        $("#notifierForm").submit(function(e) {
            var form = $(this);
            var url = form.attr('action');
        
            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(), // serializes the form's elements.
                dataType: 'json',
                success: function(data){
                    $('#notifierModal').modal('hide');

                    // header
                    if(data.status == "success"){
                        var header = '<span data-feather="check-circle" style="width:50px;height:50px;stroke:white;"></span>';
                        $('#messageModal .modal-header').css("background-color","#6ba984");
                        $('#notifierForm').trigger('reset');
                    }else{
                        var header = '<span data-feather="alert-circle" style="width:50px;height:50px;stroke:white;"></span>';
                        $('#messageModal .modal-header').css("background-color","#e66262");
                    }
                    
                    $('#messageModal .modal-title').html(header);

                    feather.replace()

                    // body
                    $('#messageModal .modal-body #message').html(data.message);
                    $('#messageModal').modal('show');

                },
                error: function(request, status, error){
                    $('#notifierModal').modal('hide');

                    var header = '<span data-feather="alert-circle" style="width:50px;height:50px;stroke:white;"></span>';
                    $('#messageModal .modal-header').css("background-color","#e66262");

                    $('#messageModal .modal-title').html(header);

                    feather.replace()

                    $('#messageModal .modal-body #message').html(error);
                    $('#messageModal').modal('show');
                }
            });
        
            e.preventDefault(); // avoid to execute the actual submit of the form.
        });
    </script>
    @endpush