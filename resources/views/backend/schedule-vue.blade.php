@extends('backend.layouts.main')

@section('content')
    <div id="app">
        <calendar></calendar>
    </div>
@endsection

@push('bottom-scripts')
    <script src="{{ asset('/js/compiled/app.js') }}"></script>
@endpush
