<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone',30);
            $table->string('name',30);
            $table->string('email',60)->nullable();
            $table->string('invoice_id',30)->default('')->nullable();
            $table->string('tracking_id',30)->default('')->nullable();
            $table->text('comments')->nullable();
            $table->unsignedInteger('employee_id')->default(1);
            $table->string('type',30);
            $table->timestamps();

            // foreign keys
            $table->foreign('employee_id')->references('id')->on('employees');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
