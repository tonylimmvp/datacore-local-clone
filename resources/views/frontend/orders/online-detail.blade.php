@extends('frontend.layouts.main')

@section('content')
    <div class="container">
        <div class="text-center pb-3">
            <h1>Online Order #{{$order->id_order}}</h1>
            <h3>({{$order->reference}})</h3>
            <p><span class="badge" style="background-color:{{$order->statusColor()}}">{{$order->statusName()}}</span></p>
            <p>Ordered on: {{$order->date_add}}</p>
            <button class="btn btn-primary btn-sm d-lg-none no-print" style="position: absolute; right:5vw; top:100px"
                    type="button"
                    data-toggle="modal"
                    data-target="#printOnlineQRModal"
                    data-id= "{{$order->id_order}}">Print QR</button>
            <button class="btn btn-primary btn-sm d-none d-lg-block no-print" style="position: absolute; right:25vw; top:100px" 
                    type="button"
                    data-toggle="modal"
                    data-target="#printOnlineQRModal"
                    data-id= "{{$order->id_order}}">Print QR</a>    
        </div>

        {{-- Address Information --}}
        <div class="row">
            <div class="address-box col-sm-6 mt-2">
                <div class="card">
                    <div class="card-header">
                        <b>Billing Address</b>
                    </div>
                    <div class="card-body">
                        <p>{{$order->addressBilling()->firstname}} {{$order->addressBilling()->lastname}}</p>
                        @if(isset($order->addressBilling()->company))<p>{{$order->addressBilling()->company}}</p>@endif
                        <p>{{$order->addressBilling()->address1}}</p>
                        <p>{{$order->addressBilling()->city}} {{App\OnlineOrder::stateToISO($order->addressBilling()->id_state)}} {{$order->addressBilling()->postcode}}</p>
                        <p>{{$order->addressBilling()->phone}}</p>
                    </div>
                </div>
            </div>
            <div class="address-box col-sm-6 mt-2">
                <div class="card">
                    <div class="card-header">
                        <b>Shipping Address</b>
                    </div>
                    <div class="card-body">
                        <p>{{$order->addressShipping()->firstname}} {{$order->addressShipping()->lastname}}</p>
                        @if(isset($order->addressShipping()->company))<p>{{$order->addressShipping()->company}}</p>@endif
                        <p>{{$order->addressShipping()->address1}}</p>
                        <p>{{$order->addressShipping()->city}} {{App\OnlineOrder::stateToISO($order->addressShipping()->id_state)}} {{$order->addressShipping()->postcode}}</p>
                        <p>{{$order->addressShipping()->phone}}</p>
                    </div>
                </div>
            </div>
        </div>

        {{-- Private Message --}}
        @if($order->getMessages()->count() > 0)
        <h2 class="mt-3">Messages:</h2>
        <div class="card">
            <div class="card-body">
                @foreach($order->getMessages() as $message)
                    <p>
                        <b>
                        [{{$message->date_add}}]
                        @if($message->id_employee == 0)
                        Customer:
                        @else
                        Staff:
                        @endif
                        </b>
                        {{$message->message}}
                    </p>
                @endforeach
            </div>
        </div>
        @endif

        {{-- Cart --}}
        <h2 class="mt-3">Cart</h2>
        <div class="table-responsive">
            <table class="table table-bordered">
                @foreach($cart as $product)
                <tr>
                    <td>{{$product->id_product}}</td>
                    <td width="70px"><img src="{{ App\Product::find($product->id_product)->image_url() }}" width="70px"></td>
                    <td>{{$product->name}}</td>
                    <td>${{$product->price}}</td>
                    <td>{{$product->quantity}}</td>
                    {{-- <td>${{number_format($product->price * $product->quantity,2)}}</td> --}}
                    <td class="no-print text-center">
                        <button class="btn btn-sm btn-secondary"
                        type="button"
                        data-toggle="modal"
                        data-target="#printModal"
                        data-id= "{{$product->id_product}}"><span data-feather="printer"></span></button>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
        <div class="row">
            <div class="col-sm-8 mt-2">
                <div class="alert border">
                    <p>Carrier: {{$order->carrier()->name}}</p>
                    <p>Payment by: {{$order->payment}} [{{$order->module}}]</p>
                </div>
            </div>
            <div class="col-sm-4 offset-sm-0 col-8 offset-4 mt-2">

                    <table class="table table-bordered">
                        <tr>
                            <td>Total Products</td>
                            <td>${{number_format($order->total_products, 2)}}</td>
                        </tr>
                        <tr>
                            <td>Delivery</td>
                            @if($order->total_shipping == 0)
                            <td>Pickup</td>
                            @else
                            <td>${{number_format($order->total_shipping, 2)}}</td>
                            @endif
                            
                        </tr>
                        <tr>
                            <td>Total Tax</td>
                            <td>${{number_format($order->total_paid_tax_incl - $order->total_paid_tax_excl,2)}}</td>
                        </tr>
                        <tr>
                            <td><b>Total Paid</b></td>
                            <td><b>${{number_format($order->total_paid,2)}}</b></td>
                        </tr>
                    </table>

            </div>
        </div>

        {{-- Notes and Comments --}}
        <br/>
        <div id="orderNotes" class="no-print">
            @php $notes = $order->note; @endphp
            <h5>NOTES AND COMMENTS:</h5>
            <div style="text-align:right">
				<button class="btn btn-primary mb-1"
                        title="Create New Note"
						data-toggle="modal"
						data-target="#createNoteModal"><span data-feather="file-plus"></i>
				</button>
			</div>
            @include('frontend.note.view', ['notes' => $notes], ['order' => $order])
        </div>

        {{-- Signature --}}
        <div id="signatureDiv" class="mt-3">
            <h5 @if(!$order->isSigned()) class="no-print" @endif>CUSTOMER SIGNATURE:</h5>
            @if($order->isSigned())
                <img class="signature-img pb-3" src="{{ url('storage/invoices/online'. $order->id_order . '.jpg') }}" style="border-bottom: #c7c7c7 1px solid">
                <p class="mt-2" style="color:#8a8a8a">Picked up by: {{$order->picked_up_by}} on {{$order->lastSigned()}}</p>
            @else
                <button id="signature-btn" class='btn btn-sm btn-primary no-print'
                    type='button' 
                    data-toggle='modal' 
                    data-target='#signatureModal' 
                    data-id='{{$order->id_order}}'
                    data-name='{{$order->addressBilling()->firstname}} {{$order->addressBilling()->lastname}}'
                    data-type='online'
                    >Order Pick Up</button>
            @endif
        </div>

        {{-- Error Message --}}
        @include('layouts.errors')

        {{-- QR Modal --}}
        @include('layouts.printers-online-qr')

        {{-- Footer --}}
        <div class="footer text-center pt-5">
            <img class="img-fluid" src="{{asset('img/lees_logo_large.png')}}" width="300px;">
            <h2>Thank you!</h2>

            <p>We will contact you when the order is ready. Please keep this as your receipt!</p>

            <p>Please note, we only keep orders for three (3) months after the first initial call. We do not hold liability for discarding orders older than 3 months.</p>

            <p>4131 Fraser St. Vancouver BC V5V 4E9 Canada | 604 - 875 - 1993 | www.leeselectronic.com </p>
        </div>
    </div>

    {{-- Order Note Modal --}}
    <div class="modal fade" id="createNoteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{ route('onlineOrder-notes.store', $order->id_order)}}" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create New Note</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <label>Order ID: </label>
                        <input class="form-control" type="integer" name="orderid" id="note-orderid" value="{{$order->id_order}}" readonly>
                        <hr/>

                        <br/>

                        <label>Comments:</label>
                        <textarea class="form-control" type="text" name="comments" id="note-comments"></textarea>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <a id="note-cancel" href="#">
                        <button type="submit" class="btn btn-primary">Create Note</button>
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('css')
<style>
    .address-box p{
        margin: 0;
    }
</style>
@endpush

@push('bottom-scripts')
<script>
   $(document).ready(function() { 
    if ( document.location.href.indexOf('#signatureDiv') > -1 ) {
        setTimeout(
            function() {
                $('#signature-btn').click()
            },
            60);
    }
    });
</script>
@endpush