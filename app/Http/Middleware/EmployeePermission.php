<?php

namespace App\Http\Middleware;
// namespace App\Http\Controllers\Auth;

use Closure;

class EmployeePermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ... $args)
    {

        if (!\Auth::check()) // I included this check because you have it, but it really should be part of your 'auth' middleware, most likely added as part of a route group.
        return redirect('login');
        
        $user = \Auth::user();
        
        foreach($args as $arg) {

            // dd($arg);
            $parsedData = explode(".", $arg);

            //TODO CHECK VALUES TO MAKE SURE THEY ARE CORRECT 

            $table = $parsedData[0];
            $permission = $parsedData[1];

            if($user->hasPermission($table,$permission))
                return $next($request);
        }
        // TODO: Make unauthorize page 
        return redirect()->route('restricted');
    }
}
