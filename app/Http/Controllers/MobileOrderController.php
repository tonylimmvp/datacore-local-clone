<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\MobileOrder;
use DB;

class MobileOrderController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth',['except' => ['index','backorders','specialorders','show']]);
        $this->middleware('auth');
    }

    public function fetchOrder(Request $request) {

        // dd($request->all());
        if($request->order){
            $id_order = $request->order;
        }else{
            return json_encode(array("status"=>"failed","message"=>"There was no order specified."));
        }
        
        $sql = "SELECT `la_order`.`order_id`,
                        `la_order`.`address_billing_id`, 
                        `la_order`.`address_shipping_id`, 
                        `la_order`.`type`, 
                        `la_order`.`reference`, 
                        `la_order`.`current_state`, 
                        `la_order`.`created_at`, 
                        `la_order`.`total_paid`, 
                        `la_order`.`total_paid_tax`, 
                        `la_order`.`total_shipping`, 
                        `la_order`.`total_shipping_tax`, 
                        `la_user`.`name_first`, 
                        `la_user`.`name_last`, 
                        `la_user`.`email`, 
                        `la_address`.`company`, 
                        `la_address`.`firstname` AS `delivery_firstname`, 
                        `la_address`.`lastname` AS `delivery_lastname`, 
                        `la_address`.`address1`, 
                        `la_address`.`address2`, 
                        `la_address`.`postcode`, 
                        `la_address`.`city`, 
                        `la_address`.`phone`, 
                        `la_address`.`phone_mobile`, 
                        `la_state`.`name` AS `state`, 
                        `la_country_lang`.`name` AS `country`
                FROM    `la_order`, 
                        `la_user`, 
                        `la_address`, 
                        `la_country_lang`, 
                        `la_state`
                WHERE `la_order`.`user_id` = `la_user`.`user_id`";
                
        $order = MobileOrder::find($id_order);
        if($order){
            if($order->address_billing_id != 0){
                $sql .= " AND `la_order`.`address_billing_id` = `la_address`.`address_id` ";

            }
        }else{
            return json_encode(array("status"=>"failed","message"=>"Mobile order does not exist."));
        }

        $sql .= " AND `la_address`.`country_id` = `la_country_lang`.`country_id`
                AND `la_address`.`state_id` = `la_state`.`state_id`
                AND `la_country_lang`.`lang_id` = 1
                AND `la_order`.`order_id` = '$id_order'
                ORDER BY `la_order`.`order_id` DESC
                LIMIT 15";

        $result = DB::connection('leeselectronic')->select(DB::raw($sql));

        if($result){
            $result = $result[0];
        }else{
            return json_encode(array('status' => 'failed', 'message' => 'Order could not be found.'));
        }

        $cart_array = [];
        
        foreach(MobileOrder::find($id_order)->products as $product){
            $id_product = $product->id_product;
            $product->img_url = Product::find($id_product)->image_url(); 
            array_push($cart_array, $product);
        }

        $result->cart_items = $cart_array;
        
        $last_array = array('status' => 'success', 'result_data' => $result);
        return json_encode($last_array);
        
    }

    /********
     * 
     * Detail View of mobile order
     * 
     */

    public function detail(MobileOrder $order){
        return view('frontend.orders.mobile-detail',compact('order'));
    }
}
