<?php

use Illuminate\Database\Seeder;

class ProductRelationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_relations')->insert([
            'product_id' => '2183',
            'related_product_id' => '28601'
        ]);

        DB::table('product_relations')->insert([
            'product_id' => '28601',
            'related_product_id' => '2183'
        ]);

        DB::table('product_relations')->insert([
            'product_id' => '4146',
            'related_product_id' => '4147'
        ]);

        DB::table('product_relations')->insert([
            'product_id' => '4146',
            'related_product_id' => '4148'
        ]);

        DB::table('product_relations')->insert([
            'product_id' => '4147',
            'related_product_id' => '4146'
        ]);

        DB::table('product_relations')->insert([
            'product_id' => '4148',
            'related_product_id' => '4146'
        ]);    
    }
}
