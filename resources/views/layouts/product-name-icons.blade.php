{{-- Check to see if sales restriction note exist. --}}
@if ($product->isRestricted())
    <a href="{{route('product-profile.show', $product->id_product)}}">
        <span class="nameIcons" data-feather="alert-circle" data-toggle="tooltip" title="Sales Restriction Exist"></span>
    </a>
@endif

{{-- Set Related Product Icon --}}
@if ($product->relatedProduct->count() > 0)
    <a href="{{route('product-profile.show', $product->id_product)}}">
        <span class="nameIcons" data-feather="layers" data-toggle="tooltip" title="Related Product(s) Exist"></span>     
    </a>
@endif