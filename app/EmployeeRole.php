<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Employee;

class EmployeeRole extends Model
{   protected $table = "employee_roles";

    public function employees(){
        return $this->hasMany(Employee::class);
    }

    public function permissions(){
        return $this->belongsToMany(Permission::class,'employee_role_permission')->withPivot('create','read','update','delete');
    }
}
