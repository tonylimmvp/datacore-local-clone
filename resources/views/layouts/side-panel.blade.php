@php 
    $new_online = sizeof(App\OnlineOrder::newOrders()); 
    $new_mobile = sizeof(App\MobileOrder::newOrders());
    $new_orders = $new_online + $new_mobile; 
    $online_orders = App\OnlineOrder::whereNotIn('current_state',[4,5,6,7,17,20])->orderBy('id_order','desc')->get();
    $mobile_orders = App\MobileOrder::whereNotIn('current_state',[3,4,5,7])->orderBy('order_id','desc')->get();
    $store_orders = App\Order::where('status_id','!=','4')->orderBy('id','desc')->get();
@endphp


<button class="btn btn-secondary btn-side-modal no-print" data-toggle="modal" data-target="#sidepanelModal">
    <span data-feather="menu"></span>
    @if($new_online  > 0)
    <span class="badge badge-danger">{{$new_online}}</span>
    @endif
    @if($new_mobile  > 0)
    <span class="badge badge-warning">{{$new_mobile}}</span>
    @endif
</button>


{{-- Modal --}}
<div class="modal right fade" id="sidepanelModal" tabindex="-1" role="dialog" aria-labelledby="sidepanelModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body">
                    <div id="side-panel-main">
                        <ul class="nav nav-tabs mb-3" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">
                                    Online/Mobile
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="store-order-tab" data-toggle="tab" href="#store-order" role="tab" aria-controls="store" aria-selected="false">
                                    Store Orders
                                </a>
                            </li>

                        </ul>

                        <div class="tab-content mb-3" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                {{-- Online Orders --}}
                                <h6>Online Orders  @if($new_online  > 0) <span class="badge badge-danger">{{$new_online}}</span>@endif</h6><br>
                                <small>Ctrl+F to Find Order via Reference or Name</small>
                                <div id="online-orders" class="card mb-3">
                                    <div class="card-body">
                                        @if(sizeof($online_orders) > 0)
                                        <table class="table table-sm">
                                        @foreach($online_orders as $order)
                                            <tr>
                                                <td><button class="btn btn-primary btn-sm" onclick="showDetailedView({{$order->id_order}},'{{ route('online-order.fetch') }}','{{ route('orders.online',$order->id_order)}}')">{{$order->id_order}}</button></td>
                                                <td class="pb-2">
                                                    <div class="pb-2">
                                                    <span class="badge badge-secondary">{{$order->reference}}</span><br>
                                                    @if($order->addressBilling())
                                                    <span>{{$order->addressBilling()->firstname}} {{$order->addressBilling()->lastname}}</span>
                                                    @if($company = $order->addressBilling()->company) <br> <span><i>{{$company}}</i></> @endif
                                                    </div>
                                                    <span class="order-status" style="color:black;background-color:{{$order->statusColor()}}">
                                                        {{$order->statusName()}}
                                                    </span>
                                                    @endif
                                                </td>
                                                <td>{{Carbon\Carbon::parse($order->date_add)->format('M d')}}</td>
                                    
                                                @if(!(is_null($order->note->first())))
                                                    <td style="text-align: center" class="notes" name="notes">
                                                        <div style="text-align: center">
                                                        <a href="{{route('orders.online',$order->id_order)}}#orderNotes">
                                                            <button class="btn btn-secondary mb-1" title="Order Note Exists"><span data-feather="monitor"></i></button>
                                                        </a>
                                                        </div>
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                        </table>
                                        @else
                                        <div class="text-center alert alert-info">
                                            <span>All online orders are completed.</span>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                {{-- Mobile App Orders --}}
                                <h6>Mobile App Orders  @if($new_mobile  > 0) <span class="badge badge-info">{{$new_mobile}}</span>@endif</h6>
                                <div id="mobile-orders" class="card mt-2">
                                    <div class="card-body">
                                        @if(sizeof($mobile_orders) > 0)
                                        <table class="table table-sm">
                                        @foreach($mobile_orders as $order)
                                        <tr>
                                        <td><button class="btn btn-primary btn-sm" onclick="showMobileOrder({{$order->order_id}},'{{ route('mobile-order.fetch') }}','{{ route('orders.mobile',$order->order_id) }}')">{{$order->order_id}}</button></td>
                                            <td class="pb-2">
                                            <div class="pb-2">
                                                <span class="badge badge-secondary">{{$order->reference}}</span><br>
                                                @if($order->addressBilling())
                                                <span>{{$order->addressBilling()->firstname}} {{$order->addressBilling()->lastname}}</span>
                                                @if($company = $order->addressBilling()->company) <br> <span><i>{{$company}}</i></span> @endif
                                                
                                                @else
                                                <span>{{$order->customer()->firstname}} {{$order->customer()->lastname}}</span>
                                                @if($company = $order->customer()->email) <br> <span><i>{{$order->customer()->email}}</i></span> @endif
                                                
                                                @endif
                                            </div>
                                            <span class="order-status" style="color:black;background-color:{{$order->status->color}}">
                                                {{$order->status->name}}
                                            </span>
                                            </td>
                                            <td>{{Carbon\Carbon::parse($order->created_at)->format('M d')}}</td>
                                                @if(!(is_null($order->note->first())))
                                                    <td style="text-align: center" class="notes" name="notes">
                                                        <div style="text-align: center">
                                                        <a href="{{route('orders.online',$order->order_id)}}#orderNotes">
                                                            <button class="btn btn-secondary mb-1" title="Order Note Exists"><span data-feather="monitor"></i></button>
                                                        </a>
                                                        </div>
                                                    </td>
                                                @endif

                                        </tr>
                                        @endforeach
                                        </table>
                                        @else
                                        <div class="text-center alert alert-info">
                                            <span>All store orders are completed.</span>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                             </div>
                            
                            <div class="tab-pane fade" id="store-order" role="tabpanel" aria-labelledby="store-tab">
                                {{-- Store Orders --}}
                                <h6>Store Orders</h6>
                                <div id="store-orders" class="card mt-2">
                                    <div class="card-body">
                                        @if(sizeof($store_orders) > 0)
                                        <table class="table table-sm">
                                        @foreach($store_orders as $order)
                                        <tr>
                                            <td><button class="btn btn-primary btn-sm" onclick="showStoreOrder({{$order->id}},'{{ route('order.fetch') }}','{{route('orders.detail',$order->id)}}')">{{$order->id}}</button></td>
                                            <td class="pb-2">
                                                <div class="pb-2">
                                                @if($order->type == "backorder") 
                                                    <span class="badge badge-info">BACKORDER</span>
                                                @elseif($order->type == "special")
                                                    <span class="badge badge-warning">SPECIAL ORDER</span>
                                                @elseif($order->type == "phone")
                                                    <span class="badge badge-dark">PHONE ORDER</span>
                                                @endif
                                                @if($order->invoice_id == "1234567")
                                                    <span class="badge badge-danger">UNPAID</span>
                                                @endif
                                                <br>{{$order->name}} 
                                                <br>{{$order->phone}}
                                                </div>
                                                <span class="order-status" style="background:{{$order->currentStatus->color}}">{{$order->currentStatus->name}}</span>
                                            </td>

                                            <td>{{Carbon\Carbon::parse($order->created_at)->format('M d')}}</td>

                                            @if(!(is_null($order->note->first())))
                                                <td style="text-align: center" class="notes" name="notes">
                                                    <div style="text-align: center">
                                                    <a href="{{ route('orders.detail',$order->id)}}#orderNotes">
                                                        <button class="btn btn-secondary mb-1" title="Order Note Exists"><span data-feather="monitor"></i></button>
                                                    </a>
                                                    </div>
                                                </td>
                                            @endif
                                        </tr>
                                        @endforeach
                                        </table>
                                        @else
                                        <div class="text-center alert alert-info">
                                            <span>All store orders are completed.</span>
                                        </div>
                                        @endif
                                    </div>
                                </div> 
                            </div>
                        </div>   
                    </div>
                    <div id="order-details"></div>
				</div>

			</div>{{-- modal-content --}}
		</div>{{-- modal-dialog --}}
    </div>{{-- modal --}}

    {{-- Edit Order Modal --}}
    <div class="modal fade" id="editOrderNoteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="POST">
                <input type="hidden" name="_method" value="patch">
                	{{ csrf_field() }}
                    {{ method_field('PATCH') }}
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Edit Online Order Note</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>

					<div class="modal-body">
                    	<label>Order ID:</label>
                    	<input class="form-control" type="text" name="id" id="id" readonly><br/>

                        <label>Notes:</label>
                    	<textarea class="form-control" type="text" name="notes" id="notes"></textarea><br/>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>

						<button type="submit" class="btn btn-primary">Edit Order</button>
					</div>
        		</form>
            </div>
    	</div>
    </div>

    @push('bottom-scripts')
        <script src="{{ asset('js/online-side-modal.js') }}"></script>

        <script>
            $(document).ready(function () {
                $('#editOrderNoteModal').on('show.bs.modal', function (event) {
                    var button = $(event.relatedTarget) 
                    var id = button.data('id') 
                    var notes = button.data('notes') 
                    var url = button.data('url')

                    var modal = $(this)
                    modal.find('.modal-title').text('Edit Order Notes')
                    modal.find('#id').val(id)
                    modal.find('#notes').val(notes)
                    modal.find('form').attr('action',url)
                });
            });
        </script>

        <script>
            $(document).ready(function () {
                $('#createOrderNoteModal').on('show.bs.modal', function (event) {
                    var button = $(event.relatedTarget) 
                    var id = button.data('id') 
                    var url = button.data('url')

                    var modal = $(this)
                    modal.find('.modal-title').text('Create Order Notes')
                    modal.find('#id').val(id)
                    modal.find('form').attr('action',url)
                });
            });
        </script>
    @endpush