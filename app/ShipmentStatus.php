<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipmentStatus extends Model
{
    protected $table='shipment_statuses';   

    public function shipment(){
        return $this->belongsTo(Shipment::class);
    }

    public function history(){
        return $this->belongsTo(ShipmentHistory::class);
    }
}
