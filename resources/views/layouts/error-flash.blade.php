@if($error = session('error'))
    <div id="error-flash-message" class="alert alert-danger no-print" role="alert">
        {{ $error }}
    </div>
@elseif(count($errors))
    <div id="error-flash-message" class="alert alert-danger no-print" role="alert">
        @foreach ($errors->all() as $error)

        <li>{{ $error }}</li>

        @endforeach
        
    </div>
@endif