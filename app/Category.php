<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $connection = "leeselectronic";
    protected $table = "ps_category_lang";
    protected $primaryKey = "id_category";

    public function products(){
        return $this->belongsToMany(Product::class, "ps_category_product",'id_category','id_product')->withPivot('position');
    }

    //Functions

    public static function getName($id){
        $query = static::where('id_category',$id)->first();
        
        if($query){
            return  $query->name;
        } 
        
        return "No Name";
    }
}
