<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shipment;
use App\Employees;
use App\EmployeesRole;
use App\ShipmentCart;
use App\ShipmentType;
use App\ShipmentHistory;
use App\ShipmentStatus;
use App\Product;
use App\ProductNote;
use Auth;

class ShipmentController extends Controller
{
    public function show(Request $request)
    {   
        if ($request->showAllState == 1){
            $shipments = Shipment::all();
            $shipment_carts = ShipmentCart::all();
            $buttonText = 'Show only active shipment';
            $showAllState = 1;
            $title = 'All Shipment List';
        }
        else {
            $shipments = Shipment::all()->where('isActive', 1);
            $shipment_carts = ShipmentCart::all();
            $buttonText = 'Show all shipments (including inactive)';
            $showAllState = 0;
            $title = 'Active Shipment List';
        }

        return view('frontend.shipment.index', compact('shipments', 'shipment_carts', 'buttonText', 'showAllState', 'title'));
    }

    public function create(Request $request)
    {
        $shipments = Shipment::all();
        $shipment_carts = ShipmentCart::all();
        $shipment_types = ShipmentType::all();

        return view('frontend.shipment.create', compact('shipments', 'shipment_carts', 'shipment_types', 'title'));
    }

    public function store(Request $request){

        // Validate form
        $this->validate(request(), [
            'type_id' => 'required|integer',
            'isLocal' => 'required',
            'vendor' => 'required',
        ]);

        $shipment = Shipment::create([
            'type_id' => request('type_id'),
            'isLocal' => request('isLocal'),
            'vendor_id' => request('vendor'),
            'employee_id' => request('employee'),
            'isShipped' => '0',
            'isActive' => '1',
        ]);

      
        $shipment->save();
        
        $title = 'Active Shipment List';
        $showAllState = 0;
        $buttonText = 'Show all shipments (including inactive)';
        
        $shipments = Shipment::all()->where('isActive', 1);
        return view('frontend.shipment.index', compact('shipments', 'title', 'showAllState', 'buttonText'));
    }

    public function storeHistory(Shipment $shipment, Request $request)
    {
        $this->validate(request(), [
            'shipment' => 'required',
            'employee' => 'required',
            'status' => 'required'
        ]);

        // create order status history
    
        $status_history = ShipmentHistory::create([
            'shipment_id' => request('shipment'),
            'employee_id' => request('employee'),
            'status_id' => request('status'),
            'comment' => request('comment')            
        ]);
    
        $status_history->save();

        // Once status history created, we want to change order status as well

        $shipment = Shipment::find($request->shipment);
        $shipment->status_id = $request->status; 

        $shipment->save();

        if($shipment->status_id == 5){
            $shipment->isShipped = true;
            $shipment->save();
        }

        if($shipment->status_id == 3 || $shipment->status_id == 6){
            $shipment->isShipped = true;
            $shipment->save();

            if($shipment->status_id == 6){
                // Set note of this particular shipment to handled.
                // Then set all isHandled to 1.
                $carts = $shipment->cart;
  
                foreach($carts as $cart){

                    if($cart->notes){
                        foreach($cart->notes as $note){
                            $note->isHandled = '1';
                            $note->handledBy = Auth::user()->id;
                            $note->handledDate = NOW();
                            // dd($cart->notes);
                            $note->save();
                        }
                    }
                }

                $shipment->isActive = false;
                $shipment->save();
            }
        }

        session()->flash('message','You have successfully updated your Shipment status.');
        return back();
    }

    public function view(Shipment $shipment){
        $shipment_histories = $shipment->history;
        $statuses = ShipmentStatus::all();

        // Estimate ETA automatically based on departure date
        if (($shipment->isLocal) && ($shipment->type_id == 1) && ($shipment->departure_date != NULL)){
            $shipment->estimated_arrival_date = date("Y-m-d", strtotime("+7 days", strtotime($shipment->departure_date)));
        }
        elseif (($shipment->isLocal) && ($shipment->type_id == 2) && ($shipment->departure_date != NULL)){
            $shipment->estimated_arrival_date = date("Y-m-d", strtotime("+14 days", strtotime($shipment->departure_date)));
        }
        elseif ((!$shipment->isLocal) && ($shipment->type_id == 1) && ($shipment->departure_date != NULL)){
            $shipment->estimated_arrival_date = date("Y-m-d", strtotime("+21 days", strtotime($shipment->departure_date)));
        }
        elseif ((!$shipment->isLocal) && ($shipment->type_id == 3) && ($shipment->departure_date != NULL)){
            $shipment->estimated_arrival_date = date("Y-m-d", strtotime("+90 days", strtotime($shipment->departure_date)));
        }

        $shipment->save();

        return view('frontend.shipment.view',compact('shipment', 'shipment_histories', 'statuses'));
    }

    public function saveInformation(Shipment $shipment, Request $request)
    {
        $shipment->purchase_order = $request->shipment_po;
        $shipment->departure_date = $request->shipment_departure_date;
        $shipment->estimated_arrival_date = $request->shipment_estimated_date;
        $shipment->actual_arrival_date = $request->shipment_actual_date;
        $shipment->courier_id = $request->courier;
        $shipment->tracking = $request->shipment_tracking;

        $shipment->save();

        return back();
    }

    public function delete(ProductNote $note)
    {
        $ActiveNote = ProductNote::find($note->id)->delete();
        $ActiveNote = ProductNote::all();
        return redirect()->route('ordered-products',compact('note'));
    }
}
