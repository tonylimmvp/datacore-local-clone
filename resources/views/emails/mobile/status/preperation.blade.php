@component('mail::message')
# Status of Order {{$order->reference}}: Preperation in Progress
This message is to inform you that we are currently preparing your order. 
Please wait for a second email before heading out for pick up, as we are double checking our stock.

To review your order, you can log in into the mobile application and view your order under "Orders".

If you have any questions or additional instructions regarding your order, please contact us at <a href="tel:6048751993">604-875-1993</a>.

Regards,<br>
Lee's Electronic Components
@endcomponent
