<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPhoneExtension extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('phone_ext')->after('phone')->nullable();
        });

        Schema::table('quotations', function (Blueprint $table) {
            $table->string('phone_ext')->after('phone')->nullable();
        });

        Schema::table('rmas', function (Blueprint $table) {
            $table->string('phone_ext')->after('phone')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rmas', function (Blueprint $table) {
            $table->dropColumn(['phone_ext']);
        });

        Schema::table('quotations', function (Blueprint $table) {
            $table->dropColumn(['phone_ext']);
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn(['phone_ext']);
        });
    }
}
