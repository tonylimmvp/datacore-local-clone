<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestockProduct extends Model
{
    protected $connection = 'mysql';
    protected $primaryKey = 'product_id';
    protected $guarded = [];

    public function info(){
        return $this->belongsTo(Product::class,'product_id','id_product');
    }
}
