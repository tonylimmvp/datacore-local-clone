<?php

use Illuminate\Database\Seeder;
use App\OrderSuggestion;

class OrderSuggestionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $order_suggestion = new OrderSuggestion();
        $order_suggestion->product_id = 10997;
        $order_suggestion->product_name = "Arduino Uno";
        $order_suggestion->store_qty = 0;
        $order_suggestion->comments = "Created by Seed";
        $order_suggestion->recent_cost = 99.25;
        $order_suggestion->vendor = "CCLLCC";
        $order_suggestion->remarks = "Vendor remark is here.";
        $order_suggestion->employee_id = 2;
        $order_suggestion->created_at = "2010-01-01 12:00:00";
        $order_suggestion->updated_at = "2012-01-01 12:00:00";

        $order_suggestion->save();
    }
}