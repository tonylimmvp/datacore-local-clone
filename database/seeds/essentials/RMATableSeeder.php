<?php

use Illuminate\Database\Seeder;

class RMATableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Seeding rma_status table begins
        DB::table('rma_status')->insert([
            'name' => 'Pending Approval',
            'color' => 'B7D9E5',
            'position' => '1',
        ]);

        DB::table('rma_status')->insert([
            'name' => 'Processing',
            'color' => 'FFFF00',
            'position' => '2',
        ]);

        DB::table('rma_status')->insert([
            'name' => 'Item Exchanged',
            'color' => '66FF66',
            'position' => '3',
        ]);

        DB::table('rma_status')->insert([
            'name' => 'Refunded',
            'color' => '66FF66',
            'position' => '4',
        ]);

        DB::table('rma_status')->insert([
            'name' => 'Denied',
            'color' => 'FF0000',
            'position' => '5',
        ]);

        DB::table('rma_status')->insert([
            'name' => 'Request Cancelled',
            'color' => 'FF8080',
            'position' => '6',
        ]);        
    }
}