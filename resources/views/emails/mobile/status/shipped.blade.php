@component('mail::message')
# Status of Order {{$order->reference}}: Shipped
Your order has been shipped with {{$order->courier->name}}.

Your Tracking Number is:

<a href="{{$order->getTrackingURL()}}">
    {{$order->shipping_number}}
</a> 

To review your order, you can log in into the mobile application and view your order under "Orders".

If you have any questions or additional instructions regarding your order, please contact us at <a href="tel:6048751993">604-875-1993</a>.

Regards,<br>
Lee's Electronic Components
@endcomponent
