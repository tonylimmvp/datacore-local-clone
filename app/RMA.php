<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RMA extends Model
{
    /**
     * The table associated with the model.
    */
    protected $table='rmas';
    protected $guarded=[];

    public function status()
    {
        return $this->belongsTo(RMA_Status::class, 'status_id');
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }

    public function sales(){
        return $this->belongsTo(Employee::class,'employee_id');
    }

    public function currentStatus(){
        return $this->belongsTo(RMA_Status::class,'status_id');
    }

    public function statuses(){
        return $this->hasMany(RMAStatusHistory::class, 'rma_id');
    }

    public function product(){
        return $this->belongsTo(Product::class, 'id_product');
    }

    public function vendor(){
        return $this->belongsTo(Vendor::class, 'vendor_id');
    }
}