<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductNote extends Model
{
    protected $table='product_notes';
    protected $connection='mysql';
    protected $guarded = [];
    
    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }

    public function type()
    {
        return $this->belongsTo(ProductNoteType::class,'type_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class,'product_id');
    }

    public function handlingEmployee()
    {
        return $this->belongsTo(Employee::class, 'handledBy');
    }

    public function cart(){
        return $this->hasOne(ShipmentCart::class, 'note_id');
    }
}
