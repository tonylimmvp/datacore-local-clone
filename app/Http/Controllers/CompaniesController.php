<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App;
use App\Company;
use App\ServiceType;

class CompaniesController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $companies = App\Company::orderBy('name')->get();
        $filters = App\ServiceType::all();
        return view('frontend.phonebook.index', compact("companies"), compact('filters'));
        // /
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // /create page
        $filters = App\ServiceType::all(); //display all filters on form
        return view('frontend.phonebook.create',compact('filters'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // /POST /companies

        //Create a company using the request data

        // save it to the database
        
        //then redirect
        $this->validate(request(),[
            'name' => 'required',
            'description' => 'required',
            'site'=>'nullable',
            'address'=> 'nullable',
            'telephone'=> 'nullable',
            'maps'=> 'nullable',
        ]);
        

        $company = Company::create(request(['name','address','telephone','site','maps','description'])); //create new var and push to database so we can get the id

        session()->flash("message", "Successfully created!");

        $count = 0; //request data count to use to skip to filters so can insert to pivot table
        foreach($request->all() as $filter){
            if($count < 7){
                $count++;
            }
            else{
                $company->service_types()->attach($filter);
            }
        }
        return redirect( route('phonebook'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        // GET/ companies/id/edit
        $filters = App\ServiceType::all();
        return view('frontend.phonebook.edit', compact("company"), compact('filters'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        // /PATCH /companies/id
        $this->validate(request(),[
            'name' => 'required',
            'description' => 'required',
            'site'=>'nullable',
            'address'=> 'nullable',
            'telephone'=> 'nullable',
            'maps'=> 'nullable',
        ]   );
        
        $company->update(request(['name','address','telephone','site','maps','description']));

        session()->flash("message", " Successfully updated! ");

        $company->service_types()->detach();

        $count = 0; //request data count to use to skip to filter section so we can insert/remove from pivot table
        foreach($request->all() as $filter){
            if($count < 8){
                $count++;
            }
            else{
                $company->service_types()->attach($filter);
                
            }
        }

        return redirect( route('phonebook'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // DELETE /companies/id
        $company = Company::find($id);
        $company->delete();

        session()->flash("message", "Successfully deleted");
        return redirect( route("phonebook"));
    }
}
