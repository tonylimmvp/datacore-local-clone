@if($flash = session('message'))
    <div id="flash-message" class="alert alert-success no-print" role="alert">
        {{ $flash }}
    </div>
@elseif($flash = session('error-message'))
    <div id="flash-message" class="alert alert-danger no-print" role="alert">
        {{ $flash }}
    </div>
@endif