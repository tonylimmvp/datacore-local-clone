@extends('frontend.layouts.main')

@section('content')

<div class="container">

<div class="d-flex">
        <h1 class="p-2 w-100 ">QUOTATIONS</h1>
        @if(Auth::user()->hasAccess('quotation')->create)
        <div class="p-3 flex-shrink-1">
            <a href="{{route('quotation.create')}}">
                <button class="btn btn-lg btn-primary">Create Quotation</button>
            </a>
        </div>
        @endif
</div>

{{-- Quotation Search --}}
{{-- <form>
    <div class="form-group pt-2">
        <div class="input-group mr-sm-2 mb-2">

            <input class="form-control">

            <div class="input-group-append">
                <button class="btn btn-outline-secondary"> Reset Form</button>
            </div>

            <div class="input-group-append">
                <button class="btn btn-primary" type="submit">Search</button>
            </div>

        </div>
    </div>
</form> --}}
{{-- End of quotation search --}}

{{-- Filters --}}

<a href="?expired=0">
    <button class="btn btn-primary m-1 mb-4">
        Active
    </button>
</a>

<a href="?expired=1">
    <button class="btn btn-primary m-1 mb-4">
        Expired
    </button>
</a>

<a href="?">
    <button class="btn btn-primary m-1 mb-4">
        All
    </button>
</a>

{{-- End of Filter --}}

@if(sizeof($quotations)==0)
    <p class="alert alert-info">There are no quotations in this section at the moment. </p>
@endif

@foreach($quotations as $quotation)
<div class="card mt-4">
        <div class="card-header">
            <h3 class="text-center">QUOTE #{{$quotation->id}}</h3>
        </div>
    <div class="card-body">
        {{-- Quotation Info --}}
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <th>Quote ID</th>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Sales</th>
                    <th>Active</th>
                    <th>Date Created</th>
                    <th>Date Expires</th>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ $quotation->id }}</td>
                        <td>{{ $quotation->name }}</td>
                        <td>{{ $quotation->phone }} @if($quotation->phone_ext) <span style="color:gray">Ext.{{$quotation->phone_ext}}</span> @endif</td>
                        <td>{{ $quotation->email }}</td>
                        <td>{{ $quotation->employee->name}}</td>
                        <td>@if($quotation->isExpired) <span style="color:red">Expired</span> @else <span style="color:green">Active</span> @endif</td>
                        <td>{{ $quotation->created_at->toFormattedDateString() }}</td>
                        <td>{{ $quotation->created_at->addDays(7)->toFormattedDateString() }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        {{-- End of Quotation Info --}}

        {{-- Quotation Cart --}}
        <h5>CART</h5>
        
        <div class="table-responsive">
            <table id="quotation-cart-{{$quotation->id}}" class="table table-bordered">
                <thead>
                    <th style="min-width:120px">PID</th>
                    <th style="min-width:400px">Name</th>
                    <th style="min-width:120px">Price</th>
                    <th style="min-width:100px">Quantity</th>
                    @if(Auth::user()->hasAccess('vendors')->read || Auth::user()->hasAccess('quotation')->update)
                    <th style="min-width:100px">Vendor</th>
                    <th style="min-width:100px">Remark</th>
                    @endif
                    @if(Auth::user()->hasAccess('quotation')->update || Auth::user()->hasAccess('quotation')->delete)
                    <th class="text-center" width="200px">Actions</th>
                    @endif
                </thead>
                <tbody>
                    @foreach($quotation->products as $product)
                    
                    <tr>
                        @if(Auth::user()->hasAccess('quotation')->update)
                        <form action="{{ route('quotation-product.update',$product->pivot->id) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('PATCH') }}
                        <td><input class="form-control form-control-sm" type="number" name="pid" value="{{ $product->id_product }}" required></td>
                        <td><input class="form-control form-control-sm" type="text" name="name" value="{{ $product->pivot->name }}" required></td>
                        <td>
                            <div class="input-group input-group-sm mr-sm-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <input class="form-control form-control-sm" name="price" type="number" value="{{ $product->pivot->price }}" step="0.01" required>
                            </div>
                        </td>
                        <td><input class="form-control form-control-sm" type="number" name="quantity" value="{{ $product->pivot->quantity }}" required></td>
                        <td><input class="form-control form-control-sm" type="text" name="vendor" value="{{ $product->pivot->vendor }}"></td>
                        <td><input class="form-control form-control-sm" type="text" name="vendor_remark" value="{{ $product->pivot->vendor_remark }}"></td>
                        <td class="text-center">
                            <button class="btn btn-sm btn-secondary mb-2">Update</button>
                            </form>
                            @else
                            <td>{{ $product->id_product }}</td>
                            <td>{{ $product->pivot->name }}</td>
                            <td>${{ $product->pivot->price }} </td>
                            <td>{{ $product->pivot->quantity }} </td>
                            @if(Auth::user()->hasAccess('vendors')->read)
                            <td>{{ $product->pivot->vendor }} </td>
                            <td>{{ $product->pivot->vendor_remark }} </td>
                            @endif
                            @if(Auth::user()->hasAccess('quotation')->delete)<td class="text-center">@endif
                            @endif
                            @if(Auth::user()->hasAccess('quotation')->delete)
                            <button class="btn btn-sm btn-secondary mb-2"
                                    type="button"
                                    data-id="{{ $quotation->id }}"
                                    data-name="{{ $product->pivot->name }}"
                                    data-toggle="modal"
                                    data-target="#deleteModal"
                                    data-url="{{ route('quotation-product.delete',$product->pivot->id) }}">
                                Delete
                            </button>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @if(Auth::user()->hasAccess('quotation')->update)
        <button class="btn btn-secondary mb-2" 
                data-id="{{ $quotation->id }}"
                data-toggle="modal"
                data-target="#addModal"
                data-url="{{ route('quotation-product.add',$quotation->id) }}">Add Item</button>
        @endif

        {{-- End of Quotation Cart --}}
        <hr>

        {{-- Comment Section --}}
        <form class="pb-4" action="{{ route('quotation.update.lego',$quotation->id) }}" method="POST">
            {{ csrf_field() }}
            {{ method_field('patch') }}
            <h5> Internal Comments </h5>
        
            <div class="form-group pt-2">
                <div class="input-group mr-sm-2 mb-2">
                    <textarea class="form-control" name="internal" rows="3" style="resize:none">@if($quotation->lego){{ Crypt::decryptString($quotation->lego) }}@endif</textarea>

                    <div class="input-group-append">
                        <button type="submit" class="btn btn-outline-secondary">Update Comment</button>
                    </div>
                </div>
                
            </div>
        </form>

        <form class="pb-4" action="{{ route('quotation.update.comment',$quotation->id) }}" method="POST">
            {{ csrf_field() }}
            {{ method_field('patch') }}
            <h5> Comments </h5>

            
            <div class="form-group pt-2">
                <div class="input-group mr-sm-2 mb-2">
                        <textarea class="form-control" name="comments" rows="3" style="resize:none"  @if(!Auth::user()->hasAccess('quotation')->update) disabled @endif>@if($quotation->comments){{ Crypt::decryptString($quotation->comments) }}@endif</textarea>            

                    <div class="input-group-append">
                        <button type="submit" class="btn btn-outline-secondary" @if(!Auth::user()->hasAccess('quotation')->update) disabled @endif>Update Comment</button>       
                    </div>
                </div>
                
            </div>
        </form>

        {{-- End of comments --}}

    </div>
    @if(Auth::user()->hasAccess('order')->create)
    <div class="card-footer text-center">
        {{-- Convert quotation to order --}}
        <button class="btn btn-primary"
                data-toggle="modal" 
                data-target="#convertModal"
                data-quote_id="{{$quotation->id}}"
                data-url="{{ route("orders.convert-quote",$quotation->id) }}" >Convert to Order</button>
    
            {{-- End of conversion --}}
    </div>
    @endif
</div>
@endforeach

{{-- Add Product Modal --}}

<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Add Product to Quote #<span id="quote_id"> --- </span> to Order</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
        <div class="modal-body">
            <label>Product ID</label>
            <div class="input-group mr-sm-2 mb-2">
                    <input class="form-control" type="number" name="pid" required>
                <div class="input-group-append">
                    <button type="button" class="btn btn-outline-secondary autofill">
                        <span data-feather="align-left"></span>
                    </button>
                </div>
            </div>
            
            <label>Name</label>
            <input class="form-control" type="text" name="name" required>
            <label>Price</label>
            <input class="form-control" type="number" name="price" step="0.01" required>
            <label>Quantity</label>
            <input class="form-control" type="number" name="quantity" required>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Add Product</button>
        </div>
        </form>
        </div>
    </div>
</div>

{{-- Convert Modal --}}

<div class="modal fade" id="convertModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Convert Quote # <span id="quote_id"> --- </span> to Order</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form id="quote-to-order-form" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
        <div class="modal-body">
            <div>
                <label>Invoice #</label>
                <input class="form-control" type="text" name="invoice" required>
                <input type="hidden" name="employee" value="{{ Auth::user()->id }}">
                @auth @if(Auth::user()->hasAccess('payment')->create) <small>Customer haven't paid yet? <a href="{{route('checkout')}}" target="_blank"> Pay with Bambora! </a></small> @endif @endauth
            </div>
            <div>
                <label>Bambora Payment ID #</label>
                <input class="form-control" type="text" name="payment_id">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button id="quoteToOrderButton" type="submit" class="btn btn-primary">Create Order</button>
        </div>
        </form>
        </div>
    </div>
</div>
      
{{-- Delete Modal --}}
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Remove Item From Quote?</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              Are you sure you want to remove <span id="product_name">---</span> from Quote #<span id="quote_id">---</span>?
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <form method="POST">
                {{csrf_field()}}
                <button class="btn btn-danger">Remove Item</button>
            </form>
            </div>
          </div>
        </div>
      </div>

</div>

@include('layouts.error-flash')

@endsection

@push('bottom-scripts')

<script>
    $(document).ready(function(){
            $( "#addModal" ).on("click", ".autofill", function() {
                var inputs = $("#addModal").find('input');
                var pid = $("#addModal").find('input')[1].value;
                console.log(pid);
                $.ajax({
                    type: "get",
                    url: "{{ route('product.autofill') }}",
                    data: {pid: pid},
                    success: function( msg ) {
                        if(msg.status == "success"){
                            inputs[1].value = pid
                            inputs[2].value = msg.name
                            inputs[3].value = msg.price
                        }else{
                            alert(msg.message);
                        }
                    },
                    error: function (request, status, error) {
                        alert(request.status + ':' + error + '.\n\n Please take picture and contact DevTeam.');
                    }
                });
            });
        });
</script>

    <script>
        $(document).ready(function () {
            $('#convertModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) 
                var id = button.data('quote_id')
                var url = button.data('url')
                
                var modal = $(this)
                modal.find('#quote_id').text(id)
                modal.find('form').attr('action',url)
            });
        });
    </script>

    <script>
        $(document).ready(function () {
            $('#addModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) 
                var id = button.data('id')
                var add_url = button.data('url')
                var modal = $(this)
                modal.find('#quote_id').text(id)
                modal.find('form').attr('action',add_url)
            });
        });
    </script>

    <script>
        $(document).ready(function () {
            $('#deleteModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) 
                var id = button.data('id')
                var name = button.data('name') 
                var delete_url = button.data('url')
                var modal = $(this)
                modal.find('#product_name').text(name)
                modal.find('#quote_id').text(id)
                modal.find('form').attr('action',delete_url)
            });
        });
    </script>

    <script>
        $('form#quote-to-order-form').on('submit',function(){
            $('#quoteToOrderButton').attr("disabled",true);
            $('#loader').toggle();
        });
    </script>

@endpush