<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Order;

class OrderCreateCustomer extends Mailable
{
    use Queueable, SerializesModels;

    public $order;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.customers.order-create')
                    ->from('no_reply@leeselectronic.com','Lee\'s Electronic Components')
                    ->replyTo('support@leeselectronic.com')
                    ->subject("[Lee's Electronic Components] Order #".$this->order->id." Confirmation (".$this->order->type.")");
    }
}
