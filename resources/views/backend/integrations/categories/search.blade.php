@extends('backend.layouts.main')

@include('layouts.datatables')

{{-- Automated Checks --}}
@php 
    App\Quotation::checkExpired();
@endphp

@push('css')
<style>
    button:disabled {
        background-color: grey;
        pointer-events: none;
        cursor: not-allowed;
    }

    #notes_table td {
        margin: 0;
        padding: 0;
    }

    #notes_table th {
        margin: 0;
        padding: 0;
    }

</style>
@endpush

@section('content')

<div class="row">
    <div class="col-12">
        <form method="post" action="{{route('category.search.detail')}}">
            {{ csrf_field() }}
            <div class="form-group row text-center">
                <input type="text" class="form-control col-7 col-md-6 offset-1 offset-md-2" name="term" @if(isset($term)) value="{{$term}}" @endif required>
                <button type="submit" class="btn btn-primary col-3 col-md-3 col-lg-1" >Search</button>
            </div>
        </form>
    </div>
</div>

{{-- Desktop View --}}
<div class="table_responsive mt-5 d-none d-md-block">
<table id="products-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th class="th-sm">ID</th>
            <th class="th-sm">Name</th>
            @auth @if(Auth::user()->hasAccess('vendors')->read)<th class="th-sm">Vendor</th>@endif @endauth
            <th class="th-sm">Image</th>
            <th class="th-sm">Category</th>
            @if(Auth::user()->hasAccess('category')->update)
            <th class="th-sm">Force Cat</th>
            @endif
        </tr>
    </thead>
    <tbody>
        @if(isset($products))
            @foreach ($products as $product)
            <tr>
                <td>{{$product->id_product}}</td>
                <td>{{$product->name}}
                @auth @if(Auth::user()->hasAccess('vendors')->read)<td>{{$product->vendor}}</td>@endif @endauth
                <td width="80px">
                    @if($product->online)
                        <img class="lazy" data-src="{{$product->image_url()}}" width=80px>
                    @else
                        <p>This is a new item.</p>
                    @endif
                </td>
                
                <td style="max-width:200px"> 
                    @if($product->online)
                        <p><b>Current Category: {{$product->online->id_category_default}} -  {{$product->getCategoryName($product->online->id_category_default)}}</b></p>

                        @foreach($product->categories as $category)
                            @if($category->id_category != $product->online->id_category_default)
                            <form>
                                {{ csrf_field() }}
                                <input type="hidden" name="pid" value="{{ $product->id_product }}">
                                <input type="hidden" name="category" value="{{ $category->id_category }}">
                                <input type="hidden" name="category_name" value="{{ $category->name }}">
                                <button type="submit" class="btn sub-category-submit">{{$category->id_category}} - {{$category->name}}</button> <br>
                            </form>
                            @endif
                        @endforeach
                    @else
                        <p>This is a new item. Please upload items to change category.</p>
                    @endif   
                </td>
                @if(Auth::user()->hasAccess('category')->update)
                <td>
                    @if($product->online)
                        @include('layouts.category-update-form')
                    @else
                        <p>This is a new item. Please upload items to change category.</p>
                    @endif
                </td>
                @endif
            </tr>
            @endforeach
        @endif
    </tbody>
</table>
</div>
{{-- END OF DESKTOP TABLE --}}

{{-- Mobile View --}}
<div class="table_responsive mt-5 d-md-none">
    <table id="products-table" class="table table-striped " cellspacing="0" width="100%">
        @if(isset($products) && sizeof($products)>0)
        @foreach ($products as $product)
        <tr>
            <td><img class="lazy" data-src="{{$product->image_url()}}" width=150px></td>
            <td>
                <b>PID: </b><span>{{ $product->id_product }}</span>
                <hr>
                <span>{{$product->name}}</span>
                @auth
                @if(Auth::user()->hasAccess('vendors')->read)
                    <hr>
                    <small>{{ $product->vendor }}</small>
                @endif
                @endauth
            </td>
        </tr>
        <tr>
            <td><span><b>Price: $</b> {{$product->price}}</span></td>    
            <td><span><b>Quantity: </b> {{$product->quantity}}</span></td>      
        </tr>
        <tr>
            <td><span><b>Shelf: </b> {{$product->shelf_loc}}</span></td>    
            <td><span><b>Storage: </b> {{$product->fraser_loc}}</span></td>      
        </tr>   
        <tr><td></td><td></td></tr>
        
        @endforeach
        @else
            @if(isset($term))
                <p class="text-center">No results for <i>{{$term}}</i></p>
            @endif
        @endif
    
    </table>
</div>
{{-- END OF MOBILE TABLE --}}

@include('layouts.error-flash')

@endsection

@push('bottom-scripts')

<script type="text/javascript">
    $(document).ready(function () {
        $('#products-table').DataTable({
            scrollY:        "60vh",
            scrollX:        true,
            scrollCollapse: true,
            paging:         false,
            columnDefs: [ { orderable: false, targets: [
                @auth
                    @if(Auth::user()->role->id<3)
                        3
                    @else
                        2
                    @endif
                @endauth
            ] } ],
            "displayLength": 25,
            "language": {
                "search": "Filter records:"
            }
        });
        $('.dataTables_length').addClass('bs-select');
    });
</script>

<script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@10.19.0/dist/lazyload.min.js"></script>

<script>
    var myLazyLoad = new LazyLoad({
    elements_selector: ".lazy",
    load_delay: 300 
});
</script>

<script>
    $('.sub-category-submit').on('click',function(event){
        event.preventDefault();
        var category = $(this)
        var form = $(this).parent()
        var formData = form.serializeArray()

        if(confirm("Continue to delete category #"+ formData[2].value +" - "+formData[3].value +" from PID#"+ formData[1].value +"?")){
            $.ajax({
                url: "{{route('categories.delete')}}",
                data: form.serialize(),
                type: 'POST',
                dataType: 'json',
                success: function(response){
                    // header
                    if(response.status == "success"){
                        var header = '<span data-feather="check-circle" style="width:50px;height:50px;stroke:white;"></span>';
                        $('#messageModal .modal-header').css("background-color","#6ba984");
                        category.hide()
                    }else{
                        var header = '<span data-feather="alert-circle" style="width:50px;height:50px;stroke:white;"></span>';
                        $('#messageModal .modal-header').css("background-color","#e66262");
                    }
                    
                    $('#messageModal .modal-title').html(header);

                    feather.replace()

                    // body
                    $('#messageModal .modal-body #message').html(response.message);
                    $('#messageModal').modal('show');
                    
                },
                error: function(request, status, error){

                    var header = '<span data-feather="alert-circle" style="width:50px;height:50px;stroke:white;"></span>';
                    $('#messageModal .modal-header').css("background-color","#e66262");

                    $('#messageModal .modal-title').html(header);

                    feather.replace()

                    $('#messageModal .modal-body #message').html(error);
                    $('#messageModal').modal('show');
                }
            });
        }
        
        
    });
</script>

@include('functions.category-update-functions')

@endpush
