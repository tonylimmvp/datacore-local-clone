<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipmentType extends Model
{
    protected $table='shipment_types';

    public function shipment(){
        return $this->belongsTo(Shipment::class);
    }

}
