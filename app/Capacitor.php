<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Capacitor extends Model
{
    protected $connection = 'mysql';
    protected $guarded = [];
    protected $primaryKey = 'product_id';


    public $timestamps = false;

    public function product(){
        return $this->belongsTo(Product::class,'product_id','id_product');
    }

    // Filters
    public function scopeFilter($query, $filters){
        if(isset($filters['voltage']) && $filters['volt_sign']){
            $voltage = $filters['voltage'];
            $volt_sign = $filters['volt_sign'];
            $query->where('voltage',$volt_sign,$voltage);
        }
        if(isset($filters['capacitance']) && $filters['cap_sign']){
            $capacitance = $filters['capacitance'];
            $cap_sign = $filters['cap_sign'];
            $query->where('capacitance',$cap_sign,$capacitance);
        }
    }
}
