@extends('frontend.layouts.main')

@include('layouts.datatables')

@push('top-scripts')
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
@endpush

@push('css')
    <style>
        h1 {
            text-align: center;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
        }

        h2 {
            font-family: 'Nunito', sans-serif;
            font-weight: 200; 
            text-align: center;
        }

        .position-ref {
            position: relative;
            justify-content: center;
            align-items: center;
        }

        .product-image-thumbnail {
            display: block;
            width: 100px;
            height: auto;
        }

        td img {
            display: block;
            margin-left: auto;
            margin-right: auto;
        }
    </style>
@endpush

@section('content')
    @include('layouts.errors')

    <h1>Order Suggestion List</h1>
    <hr/>

    @php 
       $products = \App\OrderProduct::join("orders","orders.id","=","order_product.order_id")
                    ->select(DB::raw('product_id, order_product.id as order_product_id ,quantity, order_product.name, SUM(quantity) as total , COUNT(*) as count'))
                    ->whereNotIn('status_id',[4,6,3,8,2,9,10,11])
                    ->groupBy('product_id','order_product.name')
                    ->get();
    @endphp

    {{-- <h2>Products from Orders</h2>
    <div class="table-responsive">
        <table id="product_order_table" class="table table-hover table-dark table-bordered" width="100%">
            <thead>
                <tr>
                    <th>PID</th>
                    <th>Name</th>
                    <th>Quantity</th>
                    <th width="100px">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($products as $product)
                    @if($product->product_id > 10)
                        <tr>
                            <td>{{$product->product_id}}</td>
                            <td style="font-weight: bold; font-size: large;">
                                {{$product->name}} <br>
                                @foreach($product->orders() as $order)
                                    <a target="_blank" href="{{route('orders.detail',$order->id)}}">
                                        <button class="btn btn-sm btn-secondary mb-2" style="background-color:{{\App\OrderStatus::getStatusColor($order->status_id)}}">Order #{{$order->id}} - {{$order->name}}</button>
                                    </a>
                                @endforeach
                            </td>
                            <td>{{$product->total}}</td>
                            <td class="text-center">
                                
                                <button class="btn btn-secondary btn-sm btn-action mb-1"
                                    data-toggle="modal"
                                    data-target="#purchaseModal"
                                    data-name="{{ $product->name }}"
                                    data-url="{{route('order-product.purchase', $product->order_product_id )}}">Purchase and Hold
                                </button>

                                <button class="btn btn-secondary btn-sm btn-action mb-1"
                                    data-toggle="modal"
                                    data-target="#purchaseAndShipModal"
                                    data-id="{{ $product->product_id }}" 
                                    data-name="{{ $product->name }}"
                                    data-qty="{{ $product->quantity }}"
                                    data-url="{{route('order-product.purchaseAndShip', $product->order_product_id)}}"
                                    disabled>Purchase and Ship
                                </button>
                            </td>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
    </div> --}}

    <h2>Suggestion List</h2>
    <p class='mediumNote'>This list will include products on notifiers list</p>
    <div class="table-responsive">
        <table id="suggestionListTable" class="table table-hover table-dark table-bordered" width="100%">
            <thead>
                <tr>
                    <th scope="col"><span data-feather="hash"></span> Product ID</th>
                    <th scope="col"><span data-feather="align-justify"></span> Product Name</th>
                    <th scope="col"><span data-feather="list"></span> Quantity upon Suggestion</th>
                    <th scope="col"><span data-feather="list"></span> Current Quantity</th>
                    <th scope="col"><span data-feather="image"></span> Product Image</th>
                    <th scope="col"><span data-feather="message-square"></span> Comments</th>
                    @auth 
                        @if(Auth::user()->role->id == 1)
                            @if(Auth::user()->hasAccess('cost')->read)<th scope="col"><span data-feather="dollar-sign"></span> Recent Cost</th>@endif
                            @if(Auth::user()->hasAccess('vendors')->read)<th scope="col"><span data-feather="shopping-cart"></span> Vendor</th>
                            <th scope="col"><span data-feather="shopping-cart"></span> Remarks</th>@endif
                            <th scope="col"><span data-feather="more-horizontal"></span> Related Products | Purchased?</th>
                        @endif
                    @endauth
                    <th scope="col"><span data-feather="user-check"></span> Staff Name</th>
                    <th scope="col"><span data-feather="calendar"></span> Suggested Date</th>
                    <th scope="col"><span data-feather="calendar"></span> Ordered Date</th>
                    @if(Auth::user()->hasAccess('order_suggestion')->delete || Auth::user()->hasAccess('shipment')->update || Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Supervisor'))
                    <th scope="col"><span data-feather="activity"></span> Activities</th>
                    @endif
                    
                </tr>
            </thead>
            <tbody>
            @foreach ($order_suggestions as $order_suggestion)
                <tr>
                    <td scope="row" style="font-weight:bold"><a href='{{url('https://leeselectronic.com/en/product/'. $order_suggestion->product->id_product. '.html')}}' target='_blank'>{{$order_suggestion->product_id}}</a></td>
                    <td>{{ $order_suggestion->product_name }}
                        @php $product = $order_suggestion->product; @endphp
                        @include('layouts.product-name-icons')
                    </td>
                    <td>{{ $order_suggestion->store_qty }}</td>
                    <td>
                        @php $product = App\Product::find($order_suggestion->product->id_product); @endphp
                        {{$product->quantity}}
                    </td>
                    <td><a href="{{$order_suggestion->product->image_url()}}"><img class="lazy product-image-thumbnail img-thumbnail" src="{{ $order_suggestion->product->image_url() }}"></a></td>
                    <td>{{ $order_suggestion->comments }}</td>
                    @auth 
                        @if(Auth::user()->role->id == 1)
                            @if(Auth::user()->hasAccess('cost')->read)<td>{{ $order_suggestion->recent_cost }}</td>@endif
                            @if(Auth::user()->hasAccess('vendors')->read)<td>{{ $order_suggestion->vendor}}</td>
                                <td>{{ $order_suggestion->remarks}}</td>
                            @endif

                            <td>
                                @php $relations = App\ProductRelation::where('product_id', $order_suggestion->product_id)->get() @endphp
                                @foreach($relations as $relation)
 
                                    #{{$relation->related_product_id}} | 
    
                                    @php $shipmentNotes = App\ProductNote::where('product_id', $relation->related_product_id)->where('type_id', '9')->where('isHandled', '0')->get() @endphp
                                    @php $purchaseNotes = App\ProductNote::where('product_id', $relation->related_product_id)->where('type_id', '2')->where('isHandled', '0')->get() @endphp

                                    @if(($purchaseNotes->count() > 0) || ($shipmentNotes->count() > 0))
                                        <span class="nameIcons" data-feather="check"></span>
                                    @else
                                        <span class="nameIcons" data-feather="x"></span>
                                    @endif
                                    <br/>
                                @endforeach
                            </td>

                        @endif
                    @endauth
                    <td>{{ $order_suggestion->employee->name}}</td>
                    <td>{{ $order_suggestion->created_at->diffForHumans()}}</td>
                    <td>{{ $order_suggestion->updated_at->diffForHumans()}}</td>
                    @if(Auth::user()->hasAccess('order_suggestion')->delete || Auth::user()->hasAccess('shipment')->update || Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Supervisor'))
                        <td>
                            <div class="container text-center position-ref">
                                {{-- Ordered button and form begin here --}}
                                <button class="btn btn-secondary btn-sm btn-action mb-1"
                                    data-toggle="modal"
                                    data-target="#purchaseModal"
                                    data-name="{{ $order_suggestion->product->name }}"
                                    data-url="{{route('order-suggestion.purchase', $order_suggestion->product_id )}}">Purchase and Hold
                                </button>
                                
                                <button class="btn btn-secondary btn-sm btn-action mb-1"
                                    data-toggle="modal"
                                    data-target="#purchaseAndShipModal"

                                    data-id="{{ $order_suggestion->product_id }}" 
                                    data-name="{{ $order_suggestion->product->name }}"
                                    data-qty="{{ $order_suggestion->qty }}"
                                    data-unit="{{ $order_suggestion->unit }}"

                                    data-url="{{route('order-suggestion.purchaseAndShip', $order_suggestion->product_id)}}">Purchase and Ship
                                </button>

                                <form class="form-inline" method="POST" action="{{route('order-suggestion.obsolete', $order_suggestion->product_id)}}" enctype="multipart/form-data">
                                    <input type="hidden" name="_method" value="POST">
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-secondary btn-sm btn-action mb-1">Obsoleting</button><br>
                                </form>

                                {{-- Ignore request button in action column begins  --}}
                                <button class="btn btn-secondary btn-sm btn-action mb-1" onclick="ignoreSuggestion({{$order_suggestion->id}}, {{$order_suggestion->product_id}},this)">Ignore Request</button><br>
                            </div>
                        </td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <hr/>

    @if(Auth::user()->hasRole('SuperAdmin'))
        <div style="text-align: center">
            <h3>Order Suggestion Maintenance Remove By:</h3>
            <p class='mediumNote'>Hover over the button for information tooltip</p>
            <div class="btn-group mr-2">
                <a href="{{route('order-suggestion.removeByQty')}}"><button class="btn btn-xs btn-secondary" title="Remove suggestions when current store quantity > quantity upon suggestion">Current Qty > Suggested Qty</button></a>
            </div>
            <div class="btn-group mr-2">
                <a href="{{route('order-suggestion.removeThreeMonthsOlder')}}"><button class="btn btn-xs btn-secondary" title="Remove suggestion when it has been suggested longer than 90 days">> 90 Days</button></a>
            </div>
        </div>
    @endif

    {{-- Purchase Modal --}}
    <div class="modal fade" id="purchaseModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="POST">
                	{{ csrf_field() }}
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Input Order Information</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>

					<div class="modal-body">
                        <input type="hidden" name="name" id="name">
                        <label>Quantity:</label>
                    	<input class="form-control" type="text" name="qty" id="qty"><br/>

						<label>Unit:</label>
                        <select class="custom-select mr-sm-2 mb-2" name="unit" id="unit" required>
                            <option value="piece">Piece</option>
                            <option value="package">Package</option>
                            <option value="meter">Meter</option>
                            <option value="foot">Foot</option>
                            <option value="1/2lb">1/2LB</option>
                            <option value="box">Box</option>
                            <option value="roll">Roll</option>
                            <option value="winpos unit">Winpos Unit</option>
                        </select>

						<label>Comments:</label>
                    	<textarea class="form-control" type="text" name="comments" id="note-comments"></textarea><br/>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
						<a id="note-cancel" href="#">
						<button type="submit" class="btn btn-primary">Order Item</button>
						</a>
					</div>
        		</form>
            </div>
    	</div>
    </div>

    {{-- Purchase and Ship Modal --}}
    <div class="modal fade" id="purchaseAndShipModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="POST">
                	{{ csrf_field() }}
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Insert Order into Shipment</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>

					<div class="modal-body">
						<label>Product ID:</label>
                    	<input class="form-control" type="text" name="id" id="id" readonly><br/>

						<label>Product Name:</label>
                    	<input class="form-control" type="text" name="name" id="name" readonly><br/>

						<label>Ordered Quantity:</label>
                    	<input class="form-control" type="text" name="qty" id="qty"><br/>

                    	<label>Ordered Unit:</label>
                    	<select class="custom-select mr-sm-2 mb-2" name="unit" id="unit" required>
                            <option value="piece">Piece</option>
                            <option value="package">Package</option>
                            <option value="meter">Meter</option>
                            <option value="foot">Foot</option>
                            <option value="1/2lb">1/2LB</option>
                            <option value="box">Box</option>
                            <option value="roll">Roll</option>
                            <option value="winpos unit">Winpos Unit</option>
                        </select><br/>

                        @php $shipments = App\Shipment::all() @endphp
                        <label>Select Shipment:</label>
                        <select class="custom-select mr-sm-2 mb-2" name="shipment" id="shipment" required>
                            @foreach($shipments as $shipment)
                                @if ($shipment->isActive)
                                    <option value="{{$shipment->id}}">{{$shipment->id}} | [{{$shipment->status->name}}] | {{$shipment->vendor->name}} | 
                                        @if ($shipment->isLocal == '1')
                                            LOCAL
                                        @else
                                            NOT LOCAL
                                        @endif
                                        | {{$shipment->type->name}}
                                    </option>
                                @endif
                            @endforeach
                        </select>
                        <br/>
                        <p class='smallNote'> If no shipment is available, create one <a href="{{route('shipment.create')}}" target="_blank">here.</a>
                            <a href="javascript:window.location.href=window.location.href"> Refresh page.</a>
                        </p>

                    	<label>Invoice:</label>
                    	<input class="form-control" type="text" name="invoice" id="invoice"><br/>

						<label>Comments:</label>
                    	<textarea class="form-control" type="text" name="comments" id="comments"></textarea><br/>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
						<button type="submit" class="btn btn-primary">Purchase Suggestion</button>
					</div>
        		</form>
            </div>
    	</div>
    </div>

@endsection

@push('bottom-scripts')
    <script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@10.19.0/dist/lazyload.min.js"></script>
    <script>
        var myLazyLoad = new LazyLoad({
            elements_selector: ".lazy",
            load_delay: 300 
        });
    </script>

    <script>
        $(document).ready(function () {
            $('#purchaseModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) 
                var url = button.data('url')
                var name = button.data('name') // Extract info from data-* attributes
                var modal = $(this)
                modal.find('#name').val(name)
                modal.find('form').attr('action',url)
            });
        });
    </script>

    <script>
        $(document).ready(function () {
            $('#purchaseAndShipModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var id = button.data('id') // Extract info from data-* attributes
                var name = button.data('name') // Extract info from data-* attributes
                var url = button.data('url')

                var modal = $(this)
                modal.find('.modal-title').text('Insert Item: ' + name)
                modal.find('#id').val(id)
                modal.find('#name').val(name)
                modal.find('form').attr('action',url)
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#product_order_table').DataTable({
                "scrollY":        "85vh",
                "scrollCollapse": true,
                "paging":         true,
                "pageLength":     8,
                "language": {
                    "search": "Filter records:"
                }
            });
        });

        $(document).ready(function () {
            $('#suggestionListTable').DataTable({
                "scrollY":        "100vh",
                "scrollCollapse": true,
                "paging":         true,
                "pageLength":     20,
                columnDefs: [ { orderable: false, targets: [
                    3, 4, 8, 12
                ]}],
                "language": {
                    "search": "Filter records:"
                }
            });
        });
    </script>

    <script>
        function ignoreSuggestion(suggestion_id,product_id,btn) {
            btn.disabled = true;

            $.ajax({
                type: "get",
                url: "{{ route('order-suggestion.ignore') }}",
                data: {suggestion_id: suggestion_id, product_id: product_id},
                dataType: 'json',
                success: function( msg ) {
                    console.log(msg)
                    console.log(msg.status)
                    if(msg.status == "success"){
                        alert("Succesfully removed product ID: #" + product_id + " from suggestion list. Changes will be updated upon page refresh.");
                    }else{
                        alert(msg.message);
                        btn.disabled = true;
                    }
                },
                error: function (request, status, error) {
                    alert(request.status + ':' + error + '. Please take picture and contact DevTeam.');
                    btn.disabled = true;
                }
            });
        }
    </script>
@endpush