<?php

namespace App\Http\Controllers;

use Auth;
use App\Notifier;
use Illuminate\Http\Request;
use App\Product;
use App\OrderSuggestion;
use App\Mail\SendNotifier;
use Mail;

class NotifierController extends Controller
{
    
    public function index()
    {
        //
    }
   
    public function store(Request $request)
    {
        //
    }

    public function show(Notifier $notifier)
    {
        //
    }

   
    public function edit(Notifier $notifier)
    {
        //
    }

  
    public function update(Request $request, Notifier $notifier)
    {
        //
    }

    public function destroy(Notifier $notifier)
    {
        //
    }

    /*******************************************************************
     * 
     *      API
     * 
     *******************************************************************/

    public function fetchInterested(Request $request){
        // validate pid
        if(!$request->pid){
            return json_encode(['status' => 'failed','message' => 'Product not specified.']);
        }

        $check = Product::find($request->pid);

        if(!$check){
            return json_encode(['status' => 'failed','message' => 'Product does not exist.']);
        }

        $notifiers = Notifier::where('id_product',$request->pid)->get();

        if(!$notifiers){
            return json_encode(['status' => 'failed','message' => 'No notifiers found for product.']);
        }

        foreach($notifiers as $notifier){
            if($notifier->customer()){
                $notifier->customer_name = $notifier->customer()->firstname . " " . $notifier->customer()->lastname;
            }else{
                $notifier->customer_name = '';
            }
        }

        return json_encode(['status' => 'success', 'message' => 'success', 'result_data' => $notifiers]);

        // return json with customer name, email, phone, amount, type and request date

    }

    public function sendNotifier(){

        $counter = 0;

        $notifiers = Notifier::all();

        foreach($notifiers as $notifier) {
            if($notifier->value < $notifier->product->quantity) {
                $counter++;
                Mail::to($notifier->customer_email)->send(new SendNotifier($notifier));
                $notifier->delete();
            }
        }
        
        session()->flash('message','You have successfully sent out ' . $counter . ' notifiers.');

        return back();

    }

    public function addNotifier(Request $request){

        // check name
        if(!$request->name){
            return json_encode(["status" => "failed", "message" => "Name not specified."]);
        }

        // check phone
        if(!$request->phone){
            return json_encode(["status" => "failed", "message" => "Phone not specified."]);
        }

        // check pid
        if(!$request->pid){
            return json_encode(["status" => "failed", "message" => "Product not specified."]);
        }

        $product = Product::where('id_product', $request->pid)->first();

        if(!$product){
            return json_encode(["status" => "failed", "message" => "Product not found."]);
        }

        // check email
        if(!$request->email){
            return json_encode(["status" => "failed", "message" => "Email not specified."]);
        }else if(!filter_var($request->email, FILTER_VALIDATE_EMAIL)){
            return json_encode(["status" => "failed", "message" => "Email Invalid."]);
        }

        // check quantity
        if($product->quantity > $request->quantity){
            return json_encode(["status" => "failed", "message" => "Product available, please check stock."]);
        }

        // check unique
        $check = Notifier::where('id_customer','0')
                            ->where('customer_email',$request->email)
                            ->where('id_product',$request->pid)
                            ->where('id_product_attribute','0')
                            ->where('id_shop','1')
                            ->first();

        if($check){
            return json_encode(["status" => "failed", "message" => "Notifier Duplicated. Creation Failed."]);
        }

        //Also create order suggestion (ignore if product already in order suggestion list)
        $suggestion = OrderSuggestion::firstOrCreate(
            ['product_id' => $request->pid],
            ['product_name' => $product->name, 
            'store_qty' => $product->quantity, 
            'recent_cost' => $product->recent_cost, 
            'vendor' => $product->vendor, 
            'remarks' => $product->vendor_remarks,
            'employee_id' => Auth::user()->id]
        );

        $notifier = Notifier::create([
            'id_customer' => '0',
            'customer_email' => $request->email,
            'id_product' => $request->pid,
            'id_product_attribute' => '0',
            'id_shop' => '1',
            'id_lang' => '1',
            'name' => $request->name,
            'phone' => $request->phone,
            'value' => $request->quantity,
            'type' => 'store',
            'created_at' => NOW()
            ]);
        
        // return json_encode($request->all());
        if($notifier){
            return json_encode(["status" => "success", "message" => "You have added item into notifier."]);
        }else{
            return json_encode(["status" => "failed", "message" => "Fail to create notification"]);
        }
    }
}