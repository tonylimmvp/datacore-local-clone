@extends('frontend.layouts.main')

@section('content')
@include('functions.bambora-functions')

<h1>Bambora Transaction Report</h1>

@include('layouts.errors')

@if($result->records)

<div class="table-responsive">
<table class="table">
    <tr>
        <th> TID </th>
        <th> Customer Name </th>
        <th> Order # </th>
        <th> Card </th>
        <th> Type </th>
        <th> Amount </th>
        <th> Message </th>
        <th> AVS </th>
        {{-- <th> Void </th> --}}
        <th> Response </th>
        <th> Date </th>
        <th> Action </th>
    </tr>
    
    @foreach($result->records as $transaction)
    {{-- <tr>
        <td>{{json_encode($transaction, JSON_PRETTY_PRINT)}}</td>
    </tr> --}}

    <tr @if($transaction->trn_type == "R")style="background:#ff000021" @endif>
        <td> {{$transaction->trn_id}} </td>

        <td> {{$transaction->trn_card_owner}} 
            @if($transaction->b_email)<br> {{$transaction->b_email}} @endif 
            @if($transaction->b_phone)<br> {{$transaction->b_phone}} @endif
        </td>

        <td> {{$transaction->trn_order_number}} </td>

        <td> {{$transaction->trn_card_type}}****{{$transaction->trn_masked_card}}</td>

        <td> {{$transaction->trn_type}}</td>

        <td> ${{$transaction->trn_amount}} </td>

        <td> {{$transaction->message_text}} </td>

        <td> <span class="cursor-default" data-toggle="tooltip" data-placement="bottom" title="{{ avsMessage($transaction->trn_avs_result) }}"> {{$transaction->trn_avs_result}} </span> </td>
        {{-- <td> <span> {{$transaction->trn_voided}} </span> </td> --}}
        <td> {!! trnResponse($transaction->trn_response,$transaction->trn_avs_result) !!} </td>

        <td> {{Carbon\Carbon::parse($transaction->trn_date_time)}} </td>

        <td>
            @if(strpos($transaction->trn_order_number, '-') !== false)
                <div class="text-center">
                    <span class="badge badge-warning">Online Order</span>
                </div>
            @elseif(($transaction->message_text == 'Approved' || $transaction->message_text == 'APPROVAL') && $transaction->trn_type == "P")
                @php $orders = App\Order::where("payment_id",$transaction->trn_id)->get() @endphp
                @if(sizeof($orders)>0)
                @foreach($orders as $order)
                <div class="text-center p-2">            
                    <a href="{{ route('orders.detail',$order->id) }}"><button class="btn btn-info btn-sm"> Order #{{$order->id}}</button></a>
                </div>
                @endforeach
                @else
            
                @php
                    $payment = [
                        'tid' => $transaction->trn_id,
                        'name' => $transaction->trn_card_owner,
                        'email' => $transaction->b_email,
                        'phone' => $transaction->b_phone
                    ];
                @endphp

                <div class="text-center p-2">
                    <a href="{{ route('orders.create', $payment) }}"><button class="btn btn-primary btn-sm">Create new order</button></a>
                </div>

                <div class="text-center p-2">            
                    <span> -- OR -- </span>
                </div>

                <form class="p-2" method="POST" action="{{ route('orders.update-payment') }}">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                    <div class="input-group">
                        <input class="form-control form-control-sm" name="order_id" required> 
                        <input type="hidden" name="payment_id" value="{{$transaction->trn_id}}"> 
                        <div class="input-group-append">
                            <button class="btn btn-primary btn-sm">Link Order</button>
                        </div>
                    </div>
                </form>
                
                @endif
            @endif
        </td>

    </tr>
    @endforeach
</table>
</div>
@elseif($result)
{{$result->error}}
@else
No results found
@endif

@endsection