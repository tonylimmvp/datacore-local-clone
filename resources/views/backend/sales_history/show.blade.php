@extends('backend.layouts.main')

@section('content')

<h1>Sales History for PID #{{$product->id_product}}</h1>

@include('layouts.sales-history')

@endsection