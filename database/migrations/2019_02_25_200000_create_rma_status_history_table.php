<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRmaStatusHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rma_status_history', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('rma_id');
            $table->unsignedInteger('status_id')->default('1');
            $table->unsignedInteger('employee_id');
            $table->text('comment')->nullable();
        
            $table->timestamps();

            $table->foreign('rma_id')->references('id')->on('rmas')->onDelete('cascade');
            $table->foreign('status_id')->references('id')->on('rma_status');
            $table->foreign('employee_id')->references('id')->on('employees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rma_status_history');
    }
}
