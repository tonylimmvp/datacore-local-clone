<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalePriceBreak extends Model
{
    protected $table = 'sale_price_breaks';
    protected $guarded=[];

}
