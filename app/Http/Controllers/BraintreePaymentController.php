<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Mail;

class BraintreePaymentController extends Controller
{
    protected $environment;
    protected $merchantId;
    protected $publicKey;
    protected $privateKey;

    public function __construct() { 
        $this->merchantId = env('BRAINTREE_MERCHANT_ID'); 
        $this->publicKey = env('BRAINTREE_PUBLIC_KEY'); 
        $this->privateKey = env('BRAINTREE_PRIVATE_KEY'); 
        $this->environment = env('BRAINTREE_ENV'); 
    }

    public function index(){
        //Create Braintree Gateway
        $gateway = new \Braintree\Gateway([
            'environment' => "$this->environment",
            'merchantId' => "$this->merchantId",
            'publicKey' => "$this->publicKey",
            'privateKey' => "$this->privateKey"
        ]);

        $now = Carbon::now();
        $yesterday = Carbon::yesterday();
        $lastWeek = $now->subWeeks('1');
        $lastMonth = $now->subMonths('3');

        // dd($lastWeek);

        $transactions = $gateway->transaction()->search([
            \Braintree\TransactionSearch::createdAt()->greaterThanOrEqualTo($lastMonth)
        ]);

        // dd($transactions);

        return view('frontend.payment-braintree',compact('transactions'));
    }

}
