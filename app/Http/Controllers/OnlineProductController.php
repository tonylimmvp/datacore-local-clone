<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Product;
use App\ProductOnline;

class OnlineProductController extends Controller
{
    public function updateBackorderStatus(Request $request) {
        if(!$request->product_id){
            return json_encode(array("status"=>"failed","message"=>"There is no product id specified."));
        }
        if(!isset($request->backorder_status)){
            return json_encode(array("status"=>"failed","message"=>"There is no backorder status."));
        }
        if($request->backorder_status < 0 || $request->backorder_status >2){
            return json_encode(array("status"=>"failed","message"=>"Backorder Status Invalid."));
        }

        $products = DB::connection('leeselectronic')
                        ->select("  SELECT *
                                    FROM ps_stock_available
                                    WHERE id_product = $request->product_id");
        if($products){
            $update = DB::connection('leeselectronic')
                        ->table('ps_stock_available')
                        ->where('id_product',$request->product_id)
                        ->update(['out_of_stock' => $request->backorder_status]);
        }else{
            return json_encode(array("status"=>"failed","message"=>"Nothing to update."));
        }

        return json_encode(array("status"=>"success","message"=>"Updated $update rows."));
    }

    public function updateOnlineOnly(Request $request) {
        if(!$request->product_id){
            return json_encode(array("status"=>"failed","message"=>"There is no product id specified."));
        }
        if(!isset($request->online)){
            return json_encode(array("status"=>"failed","message"=>"There is no online order status."));
        }
        if($request->online < 0 || $request->online > 1){
            return json_encode(array("status"=>"failed","message"=>"Online order status not valid."));
        }

        // Update on ps_product
        $products = DB::connection('leeselectronic')
                        ->select("  SELECT *
                                    FROM ps_product
                                    WHERE id_product = $request->product_id");
        if($products){
            $update1 = DB::connection('leeselectronic')
                        ->table('ps_product')
                        ->where('id_product',$request->product_id)
                        ->update(['online_only' => $request->online]);
        }else{
            return json_encode(array("status"=>"failed","message"=>"Nothing to update in."));
        }

        // Update on ps_product
        $products = DB::connection('leeselectronic')
                        ->select("  SELECT *
                                    FROM ps_product_shop
                                    WHERE id_product = $request->product_id");
        if($products){
            $update2= DB::connection('leeselectronic')
                        ->table('ps_product_shop')
                        ->where('id_product',$request->product_id)
                        ->update(['online_only' => $request->online]);
        }else{
            return json_encode(array("status"=>"failed","message"=>"Nothing to update."));
        }

        return json_encode(array("status"=>"success","message"=>"Updated $update1 rows in ps_product. Updated $update2 rows in ps_product_shop"));
    }

    public function syncDescription(Request $request) {

        $products = DB::connection('leeselectronic')->select('SELECT * FROM (SELECT id_product, count(*) as mismatched FROM (SELECT id_product FROM `ps_product_lang` WHERE id_lang != 2 GROUP BY id_product, description) AS custom GROUP BY id_product) AS mismatch_table where mismatched != 1');
        foreach($products as $product){
            $prod_descriptions = DB::connection('leeselectronic')->select("SELECT description FROM ps_product_lang WHERE id_product = $product->id_product && id_lang = 1");
            $description = $prod_descriptions[0]->description;
            DB::connection('leeselectronic')->table('ps_product_lang')->where('id_product', $product->id_product)->update(['description'=>$description]);
        }
        
        session()->flash('message','You have successfully updated product\'s description.');

        return back();
    }

    public function syncShortDescription(Request $request) {

        $products = DB::connection('leeselectronic')->select('SELECT * FROM (SELECT id_product, count(*) as mismatched FROM (SELECT id_product FROM `ps_product_lang` WHERE id_lang != 2 GROUP BY id_product, description_short) AS custom GROUP BY id_product) AS mismatch_table where mismatched != 1');
        foreach($products as $product){
            $prod_descriptions = DB::connection('leeselectronic')->select("SELECT description_short FROM ps_product_lang WHERE id_product = $product->id_product && id_lang = 1");
            $description = $prod_descriptions[0]->description_short;
            DB::connection('leeselectronic')->table('ps_product_lang')->where('id_product', $product->id_product)->update(['description_short'=>$description]);
        }
        
        session()->flash('message','You have successfully updated product\'s short description.');

        return back();
    }

    public function updateUnity(Request $request) {

        if(!(isset($request->id_product) && isset($request->unity))){
            return json_encode(array("status"=>"failed","message"=>"Missing Arguement"));
        }

        $product = ProductOnline::find($request->id_product);

        if(!$product){
            return json_encode(array("status"=>"failed","message"=>"Product Does Not Exist"));
        }

        $product->unity = $request->unity;
        $product->save();

        $shop_product = DB::connection('leeselectronic')
                            ->table('ps_product_shop')
                            ->where('id_product','=',$request->id_product)
                            ->update(['unity'=>$request->unity]);

        return json_encode(array("status"=>"success","message"=>"Updated unity successfully."));
    }
}
