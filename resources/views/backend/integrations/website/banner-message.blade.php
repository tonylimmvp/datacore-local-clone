@extends('backend.layouts.main')

@section('content')

    <h1>Banner Message</h1>

    @php
    // read file to get text.
    $file_name = url("/storage/banner_message.txt");
    $file = fopen($file_name, "r");
    if (!$file) {
        echo "<p>Unable to open remote file.\n";
        exit;
    }
    while (!feof ($file)) {
        $line = fgets ($file, 1024);
        $banner_message = $line;
    }
    fclose($file);
    @endphp

    <div class="card col-md-6 offset-md-3 col-xl-4 offset-xl-4 text-center p-0">
        <div class="card-header">
            <h5>Current Message</h5>
        </div>
        <div class="card-body">
                @if(isset($banner_message))
                    {{$banner_message}}
                @endif
        </div>
            
    </div>
    

    <form class="offset-md-2 col-md-8 col-xl-6 offset-xl-3 text-center p-0" action="{{ route('banner-message.update') }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
                <label class="pt-4" for="message_body"><h5>Banner Message</h5></label>
                <small class="form-text text-muted text-left">Example: STORE HOURS: M-F 9-6 PST, SAT 9-5 PST, SUN CLOSED</small>
                <input class="form-control  " type="text" placeholder="ENTER BANNER MESSAGE" id="message_body" name="message_body">
                
                <small class="form-text text-muted alert alert-warning mt-2">Please do not enter single or double quotes.</small>
              </div>

        <input class="btn btn-primary" type="submit" value="Set Banner Message">
    </form>			

@endsection
