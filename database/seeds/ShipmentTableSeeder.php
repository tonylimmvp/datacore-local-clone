<?php

use Illuminate\Database\Seeder;

class ShipmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shipments')->insert([
            'departure_date' => '2019-01-01',
            'estimated_arrival_date' => '2019-01-08',
            'actual_arrival_date' => '2019-01-17',
            'employee_id' => '3',
            'type_id' => '1',
            'isLocal' => '0',
            'isShipped' => '1',
            'isActive' => '1',
            'courier_id' => '2',
            'status_id' => '3',
            'tracking' => '4008110836760143',
            'vendor_id' => '1009',
            'created_at' => NOW(),
        ]);

        DB::table('shipments')->insert([
            'departure_date' => '2019-01-02',
            'estimated_arrival_date' => '2019-01-15',
            'actual_arrival_date' => '2019-01-15',
            'employee_id' => '2',
            'type_id' => '2',
            'isLocal' => '1',
            'isShipped' => '0',
            'isActive' => '1',
            'courier_id' => '3',
            'status_id' => '1',
            'tracking' => '773929578959',
            'vendor_id' => '1140',
            'created_at' => '2019-01-01 12:00:00',
        ]);

        DB::table('shipments')->insert([
            'departure_date' => '2019-01-03',
            'estimated_arrival_date' => '2019-01-12',
            'actual_arrival_date' => '2019-01-15',
            'employee_id' => '1',
            'type_id' => '3',
            'isLocal' => '0',
            'isShipped' => '0',
            'isActive' => '0',
            'courier_id' => '4',
            'status_id' => '5',
            'vendor_id' => '1000',
            'created_at' => '2019-01-31 00:00:00',
        ]);
    }
}
