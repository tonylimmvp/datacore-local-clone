{{-- Modal --}}

{{-- create modal --}}
<!-- Modal -->
<div class="modal fade" id="createEventModal" tabindex="-1" role="dialog" aria-labelledby="createEventModalTitle" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <form id="createEventForm" method="POST" action="{{ route('coreui.event.create') }}">
            <div class="modal-header">
                <h5 class="modal-title" id="createEventModalTitle">Create Event</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{csrf_field()}}
                <div class="form-group">
                    <input type="text" class="form-control" name="title" placeholder="Title">
                </div>
                <div class="form-row">
                    <div class="form-group col-6">
                        <input id="event_start" class="form-control" name="start" placeholder="Start Date" autocomplete="off" required>
                    </div>
                    <div class="form-group col-6 event-time">
                        <input id="event_start_time" class="form-control" name="start_time" placeholder="Start Time (09:00 or 10:00)">
                    </div>
                    <div class="form-group col-6">
                        <input id="event_end" class="form-control" name="end" placeholder="End Date" autocomplete="off" required>
                    </div>
                    <div class="form-group col-6 event-time">
                        <input id="event_end_time" class="form-control" name="end_time" placeholder="End Time (17:00 or 18:00)">
                    </div>
                </div>
                <select class="form-control" disabled>
                    <option value="{{Auth::user()->id}}"> {{Auth::user()->name}} </option>
                </select>
                <input type="hidden" name="employee" value="{{ Auth::user()->id }}">
                <input type="hidden" name="type" value="2">
                <input type="hidden" name="all_day" value="0">

                <div id="create_error" class="alert alert-danger modal-error d-none mt-2"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Create Event</button>
            </div>
        </form>
    </div>
</div>
</div>

{{-- edit modal --}}

<div class="modal fade" id="editEventModal" tabindex="-1" role="dialog" aria-labelledby="editEventModalTitle" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <form id="editEventForm" method="POST" action="{{ route('coreui.event.update') }}">
        <div class="modal-header">
            <h5 class="modal-title" id="editEventModalTitle">Edit Event</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <input type="hidden" name="event_id">
            <div class="form-group">
                <input type="text" class="form-control" name="title" placeholder="title">
            </div>
            
            <div class="form-row">
                <div class="form-group col-6">
                    <input id="edit_event_start" class="form-control" name="start" placeholder="Start Date" autocomplete="off" required>
                </div>
                <div class="form-group col-6 event-time">
                    <input id="edit_event_start_time" class="form-control" name="start_time" placeholder="Start Time (09:00 or 10:00)">
                </div>
                <div class="form-group col-6">
                    <input id="edit_event_end" class="form-control" name="end" placeholder="End Date" autocomplete="off" required>
                </div>
                <div class="form-group col-6 event-time">
                    <input id="edit_event_end_time" class="form-control" name="end_time" placeholder="End Time (17:00 or 18:00)">
                </div>
            </div>

            <select class="form-control" disabled>
                <option value="{{Auth::user()->id}}" disabled> {{Auth::user()->name}} </option>
            </select>
            <input type="hidden" name="employee" value="{{ Auth::user()->id }}">
            <input type="hidden" name="type" value="2">
            <input type="hidden" name="all_day" value="0">
            <div id="update_error" class="alert alert-danger modal-error d-none mt-2"></div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Update Event</button>
            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteEventModal">Delete Event</button>
        </div>
    </form>
    </div>
</div>
</div>

{{-- delete confirmation --}}

<div class="modal fade" id="deleteEventModal" tabindex="-1" role="dialog" aria-labelledby="deleteEventModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form id="deleteEventForm" method="POST" action="{{ route('coreui.event.delete') }}">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteEventModalTitle">Delete Event</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{ csrf_field() }}
                <input type="hidden" name="event_id">
                <p>Are you sure you want to delete this event?</p>
                <div id="delete_error" class="alert alert-danger modal-error d-none mt-2"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger">Delete Event</button>
            </div>
            </form>
        </div>
    </div>
</div>

@push("bottom-scripts")

<script>
    $('#event_start').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd'
    });
    $('#event_end').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd'
    });
    $('#edit_event_start').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd'
    });
    $('#edit_event_end').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd'
    });

    // Timepicker
    $('#event_start_time, #edit_event_start_time').timepicker({
        timeFormat: 'HH:mm',
        interval: 60,
        minTime: '9',
        maxTime: '6:00pm',
        defaultTime: '9:00',
        startTime: '9:00am',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });

    $('#event_end_time, #edit_event_end_time').timepicker({
        timeFormat: 'HH:mm',
        interval: 60,
        minTime: '9',
        maxTime: '6:00pm',
        defaultTime: '10:00',
        startTime: '9:00am',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });
</script>

<script>
    $('#createEventModal,#editEventModal,#deleteEventModal').on('hide.bs.modal', function (event) {
        $(this).find('.modal-error').addClass('d-none')
    })
</script>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        var Calendar = FullCalendar.Calendar;
        var calendarEl = document.getElementById('calendar');
    
        var calendar = new Calendar(calendarEl, {
            plugins: [ 'interaction','dayGrid','timeGrid','list' ],
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
            },
            defaultView: 'dayGridMonth',
            views: {
                dayGrid: {
                    eventTimeFormat: {
                        hour: 'numeric',
                        minute: '2-digit',
                        meridiem: 'short'
                    },
                },
                timeGrid: {
                    eventTimeFormat: {
                        hour: 'numeric',
                        minute: '2-digit',
                    },
                },
            },
            displayEventEnd: true, 
            hiddenDays: [0],
            businessHours: [
                {
                    daysOfWeek: [ 1, 2, 3, 4, 5 ],
                    startTime: '09:00', 
                    endTime: '18:00' 
                },
                {
                    daysOfWeek: [ 6 ], 
                    startTime: '09:00', 
                    endTime: '17:00' 
                }
            ],
            eventSources: [
                {
                    events: [
                        @foreach($events as $event)
                        {
                            id: '{{ $event->id }}',
                            type: '{{ $event->type_id }}',
                            @if($event->type_id == 4 || $event->type_id == 6)
                            title : '{{ $event->type->name}} - {{ $event->employee->name}}',
                            @elseif($event->type_id == 2 || $event->type_id == 7 || $event->type_id == 8)
                            title : '{{ $event->title}}',
                            @else
                            title : '{{ $event->employee->name}}',
                            @endif
                            start : '{{ $event->start }}',
                            @if($event->end)
                                end: '{{ $event->end }}',
                            @endif
                            allDay: !!parseInt('{{$event->all_day}}'),
                            @if(\Carbon\Carbon::parse($event->start)->format('H:i') != '09:00' && $event->type_id == 1)
                                backgroundColor: 'saddlebrown', 
                                borderColor: 'saddlebrown',                               
                            @else
                                backgroundColor: '{{ $event->type->bg_color }}', 
                                borderColor: '{{ $event->type->bg_color }}',
                            @endif
                            textColor: '{{ $event->type->text_color }}',
                            employeeId: '{{$event->employee_id}}',
                            employeeName: '{{$event->employee->name}}',
                        },
                        @endforeach
                    ],
                },
                {
                    events: [
                        @foreach($holidays as $event)
                        {
                            id: '{{ $event->id }}',
                            type: '{{ $event->type_id }}',
                            title : '{{ $event->title}}',
                            start : '{{ $event->start }}',
                            @if($event->end)
                                end: '{{ $event->end }}',
                            @else
                                allDay: true,
                            @endif
                            backgroundColor: '{{ $event->type->bg_color }}', 
                            borderColor: '{{ $event->type->bg_color }}',
                            textColor: '{{ $event->type->text_color }}',
                            employeeId: '{{$event->employee_id}}'
                        },
                        @endforeach
                    ],
                },
            ],
            dayRender: function (arg) {
                var current_date = moment(arg.date).valueOf()
                @foreach($holidays as $event)
                    var holiday = moment("{{$event->start}}").valueOf()
                    if (current_date == holiday) {
                        arg.el.bgColor = 'lightcyan'
                    }
                @endforeach
            },
            eventClick: function(arg){
                if(arg.event.extendedProps.type == 2){
                    var modal = $('#editEventModal')
                    modal.find("input[name='event_id']").val(arg.event.id)
                    modal.find("input[name='type']").val(arg.event.extendedProps.type)
                    modal.find("input[name='title']").val(arg.event.title)
                    modal.find("input[name='start']").val(moment(arg.event.start).format('YYYY-MM-DD'))
                    modal.find("input[name='end']").val(moment(arg.event.end).format('YYYY-MM-DD'))
                    modal.find("input[name='start_time']").val(moment(arg.event.start).format('HH:mm'))
                    modal.find("input[name='end_time']").val(moment(arg.event.end).format('HH:mm'))
                    modal.find("option[value='"+arg.event.extendedProps.employeeId+"']").prop('selected',true)
                    $('#deleteEventModal').find("input[name='event_id']").val(arg.event.id)
                    $('#editEventModal').modal('show')
                }
            },
        });

        calendar.render();

        // Form submission
        $("form#createEventForm").submit(function(e) {
            e.preventDefault(); // avoid to execute the actual submit of the form.
            var form = $(this);
            var url = form.attr('action');

            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(), // serializes the form's elements.
                dataType: 'json',
                success: function(data){
                    if(data.status == "success"){
                        calendar.addEvent({ 
                            id: data.event.id,
                            type: data.event.type_id,
                            title : data.event.title,
                            start : data.event.start,
                            end: data.event.end,
                            backgroundColor: data.event.backgroundColor, 
                            borderColor: data.event.backgroundColor,
                            textColor: data.event.textColor,
                            employeeId: data.event.employee_id
                        })
                        $('#createEventModal').modal('hide')
                    }else{
                        $('#create_error').html('<p>'+ data.message +'</p>')
                    }
                    
                },
                error: function(request, status, error){
                    $('#create_error').html('<p>'+ status +': ' + error + '</p>')
                }
            });
   
        });

        $("#editEventForm").submit(function(e) {
            e.preventDefault(); // avoid to execute the actual submit of the form.
            var form = $(this);
            var url = form.attr('action');

            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(), // serializes the form's elements.
                dataType: 'json',
                success: function(data){
                    if(data.status == "success"){
                        var event = calendar.getEventById($('#editEventModal input[name="event_id"]').val())
                        event.setProp('title',data.event.title)
                        event.setStart(data.event.start)
                        event.setEnd(data.event.end)
                        event.setExtendedProp('employeeId',data.event.employee_id)
                        $('#editEventModal').modal('hide')
                    }else{
                        $('#update_error').html('<p>'+ data.message +'</p>')
                    }
                },
                error: function(request, status, error){
                    $('#update_error').html('<p>'+ status +': ' + error + '</p>')
                }
            });
    
        });
        $("#deleteEventForm").submit(function(e) {
            e.preventDefault(); // avoid to execute the actual submit of the form.
            var form = $(this);
            var url = form.attr('action');

            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(), // serializes the form's elements.
                dataType: 'json',
                success: function(data){
                    if(data.status == "success"){
                        var event = calendar.getEventById($('#deleteEventModal input[name="event_id"]').val())
                        event.remove()
                        $('#deleteEventModal').modal('hide')
                        $('#editEventModal').modal('hide')
                    }else{
                        $('#delete_error').html('<p>'+ data.message +'</p>')
                    }
                },
                error: function(request, status, error){
                    $('#delete_error').html('<p>'+ status +': ' + error + '</p>')
                }
            });
    
        });
    });
</script>

@endpush
