<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $this->call(PermissionTableSeeder::class);
        $this->call(EmployeeRolesTableSeeder::class);
        $this->call(EmployeesTableSeeder::class);
        $this->call(OrderStatusesTableSeeder::class);
        // $this->call(OrdersTableSeeder::class);
        // $this->call(OrderProductTableSeeder::class);
        $this->call(RMATableSeeder::class);
        // $this->call(OrderSuggestionTableSeeder::class);
        $this->call(ProductNoteTypeTableSeeder::class);
        // $this->call(ProductNoteTableSeeder::class);
        $this->call(CourierTableSeeder::class);
        // $this->call(ShipmentTableSeeder::class);
        $this->call(ShipmentTypeTableSeeder::class);
        $this->call(ShipmentStatusesSeeder::class);
        // $this->call(ShipmentHistoriesSeeder::class);
        $this->call(StorePrinterIPSeeder::class);
        // $this->call(ProductRelationTableSeeder::class);
        // $this->call(SaleSuggestionsTableSeeder::class);

    }
}
