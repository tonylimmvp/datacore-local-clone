@component('mail::message')
# Status of Order {{$order->reference}}: On Hold
This message is to inform you that your order has been put on hold. If we have not contact you already, please contact us at <a href="tel:6048751993">604-875-1993</a>.

There may be many reasons for an hold order. The most common is that an item you have ordered is currently out of stock. If that is the case, we can provide you with multiple options.

1. A full refund of missing product 
2. A backorder for the missing product

To review your order, you can log in into the mobile application and view your order under "Orders".

If you have any questions or additional instructions regarding your order, please contact us at <a href="tel:6048751993">604-875-1993</a>.

Regards,<br>
Lee's Electronic Components
@endcomponent
