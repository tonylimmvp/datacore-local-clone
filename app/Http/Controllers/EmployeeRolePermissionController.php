<?php

namespace App\Http\Controllers;
use App\EmployeeRolePermission;
use App\EmployeeRole;
use App\Permission;

use Illuminate\Http\Request;

class EmployeeRolePermissionController extends Controller
{
    public function edit(EmployeeRole $employee_role)
    {

        return view('backend.employee_roles.permissions.index',compact('employee_role'));
    }

    public function update(Request $request, EmployeeRole $employee_role)
    {

        $permissions = Permission::all();

        foreach($permissions as $i => $permission){
            $create = 0;
            if($request->create){
                foreach($request->create as $name){
                    if("$permission->id" == "$name"){
                        $create = 1;
                    }
                }
            }

            $read = 0;
            if($request->read){
                foreach($request->read as $name){
                    if("$permission->id" == "$name"){
                        $read = 1;
                    }
                }
            }

            $update = 0;
            if($request->update){
                foreach($request->update as $name){
                    if("$permission->id" == "$name"){
                        $update = 1;
                    }
                }
            }

            $delete = 0;
            if($request->delete){
                foreach($request->delete as $name){
                    if("$permission->id" == "$name"){
                        $delete = 1;
                    }
                }
            }
            
            $employee_role->permissions()->updateExistingPivot($permission->id,['create'=>$create , 'read'=>$read, 'update'=>$update, 'delete'=>$delete]);


        }

        session()->flash('message','You have successfully updated ' . $employee_role->name . ' permissions.');

        return redirect()->route('coreui.employee-role-permission.edit',$employee_role->id);

    }

    public function destroy(EmployeePermission $employeePermission)
    {
        
    }
}
