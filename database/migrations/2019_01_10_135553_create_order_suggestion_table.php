<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderSuggestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_suggestions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unique();
            $table->string('product_name')->unique();
            $table->integer('store_qty');
            $table->text('comments')->nullable();
            $table->decimal('recent_cost',6,2)->nullable();
            $table->string('vendor')->nullable();
            $table->string('remarks')->nullable();
            $table->text('purchase_ref')->nullable();
            $table->integer('employee_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_suggestions');
    }
}
