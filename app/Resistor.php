<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resistor extends Model
{
    protected $connection = 'mysql';
    protected $guarded = [];
    protected $primaryKey = 'product_id';

    public $timestamps = false;

    public function product(){
        return $this->belongsTo(Product::class,'product_id','id_product');
    }

    // Filters
    public function scopeFilter($query, $filters){
        if(isset($filters['wattage']) && $filters['watt_sign']){
            $wattage = $filters['wattage'];
            $watt_sign = $filters['watt_sign'];
            $query->where('wattage',$watt_sign,$wattage);
        }
        if(isset($filters['resistance']) && $filters['ohm_sign']){
            $resistance = $filters['resistance'];
            $ohm_sign = $filters['ohm_sign'];
            $query->where('resistance',$ohm_sign,$resistance);
        }
    }
}
