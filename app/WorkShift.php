<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkShift extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function employees(){
        return $this->belongsToMany(Employee::class);
    }

    public function scopeAvailableShift($query, $employee_id){

        return $query->with('employees')
        ->whereDoesntHave('employees', 
            function ($query) use($employee_id){ 
            $query->where('id', $employee_id);
        });
                    
    }
}
