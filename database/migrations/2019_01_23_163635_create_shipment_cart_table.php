<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShipmentCartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipment_carts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shipment_id')->nullable(false);
            $table->integer('product_id')->nullable(false);
            $table->string('item_name');
            $table->integer('qty');
            $table->string('unit')->nullable();
            $table->text('comments')->nullable();
            $table->string('invoice')->nullable();
            $table->unsignedInteger('employee_id');
            $table->unsignedInteger('note_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipment_carts');
    }
}
