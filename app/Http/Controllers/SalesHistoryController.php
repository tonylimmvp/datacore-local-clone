<?php

namespace App\Http\Controllers;

use App\SalesHistory;
use App\Product;
use Illuminate\Http\Request;

class SalesHistoryController extends Controller
{

    public function __construct(){
        return $this->middleware('auth');
    }

    public function index()
    {
        return view('backend.sales_history.index');
    }

    public function show(Product $product)
    {
        return view('backend.sales_history.show',compact('product'));
    }

    public function upload()
    {
        return view('backend.sales_history.upload');
    }

    public function store(Request $request)
    {   

        $this->validate(request(), [
            'csv_file' => 'required|file|mimes:txt,csv',
            'year' => 'required',
            'month' => 'required'
        ]);

        $year = $request->year;
        
        $month = $request->month;

        $V1_columns = array("Code",
                            "Description",
                            "$year\\1","Qty",
                            "$year\\2","Qty",
                            "$year\\3","Qty",
                            "$year\\4","Qty",
                            "$year\\5","Qty",
                            "$year\\6","Qty",
                            "$year\\7","Qty",
                            "$year\\8","Qty",
                            "$year\\9","Qty",
                            "$year\\10","Qty",
                            "$year\\11","Qty",
                            "$year\\12","Qty");

        $path = $request->file('csv_file')->getRealPath();

        $handle = fopen($path, "r");

        $line_num = 0;
        $header = false;
        
        while ( (($data = fgetcsv($handle,0, "\t")) !== FALSE)) {
            if($data[0]==$V1_columns[0]){
                $header = true;
            }
            if($header){
                // Validate columns
                if($line_num == 0){

                    for($index=0 ; $index < count($V1_columns); $index++){

                        $data_token = $data[$index];
                        $header_token = $V1_columns[$index];
							
						if(strcmp($data[$index], $V1_columns[$index]) != 0){
                            session()->flash('error','Header did not match, please consult the development team.');
                            return back();
                        }
                    }
                    $deletedRows = SalesHistory::where('year', $year)->where('month',$month)->delete();
                }else{
                    $id_product = $data[0];

                    $sales = array(
                        "total_1" => $data[2],
                        "total_2" => $data[4],
                        "total_3" => $data[6],
                        "total_4" => $data[8],
                        "total_5" => $data[10],
                        "total_6" => $data[12],
                        "total_7" => $data[14],
                        "total_8" => $data[16],
                        "total_9" => $data[18],
                        "total_10" => $data[20],
                        "total_11" => $data[22],
                        "total_12" => $data[24]
                    );
    
                    $quantity = array(
                        "qty_1" => $data[3],
                        "qty_2" => $data[5],
                        "qty_3" => $data[7],
                        "qty_4" => $data[9],
                        "qty_5" => $data[11],
                        "qty_6" => $data[13],
                        "qty_7" => $data[15],
                        "qty_8" => $data[17],
                        "qty_9" => $data[19],
                        "qty_10" => $data[21],
                        "qty_11" => $data[23],
                        "qty_12" => $data[25]
                    );
                    
                    $month_total = $sales['total_'.$month];
                    $month_qty = $quantity['qty_'.$month];

                    // Ignore products with symbols
                    if (!preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $id_product))
                    {
                        $sales_history = new SalesHistory();
                        $sales_history->product_id = $id_product;
                        $sales_history->month = $month;
                        $sales_history->year = $year;
                        $sales_history->qty = $month_qty;
                        $sales_history->total = $month_total;
                        $sales_history->save();
                    }
                    
                }
                $line_num++;
            }
        }

        if(!$header){
            session()->flash('error','Could not find header, please check the file you uploaded.');
            return back();
        }

        session()->flash('message',"You have successfully imported the sales history for " . $year);

        return back();
    }
}
