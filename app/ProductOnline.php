<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class ProductOnline extends Model
{
    protected $connection = 'leeselectronic';
    protected $table = 'ps_product';
    protected $primaryKey = 'id_product';

    public $timestamps = false;

    public function categories(){
        return $this->belongsToMany(Category::class, 'ps_category_product','id_product','id_category')->withPivot('position')->where('id_lang','1');
    }

    public function store(){
        return $this->belongsTo(Product::class,'id_product');
    }

    public function brand(){
        return $this->hasOne(Brand::class,'id_manufacturer','id_manufacturer');
    }

    public function detail(){
        return $this->belongsToMany(OnlineOrder::class,'ps_order_detail','product_id','id_order')->withPivot('product_quantity')->whereIn('current_state',[2,3,9]);
    }

    public static function getCategoryUrl($category_id){
        $category = Category::find($category_id);
        $url = config('custom.web_url') . "/category/" . $category_id . "-" . $category->link_rewrite;
        return $url;
    }

    public static function getCategoryName($category_id){
        return Category::getName($category_id);
    }

    /********************************************
    *
    *           Online Product Setting
    *
    ********************************************/

    public function isOnlineOnly(){
        $online_only = false;

        $online_only = $this->online_only;

        $products = DB::connection('leeselectronic')
                        ->select('  SELECT *
                                    FROM ps_product_shop
                                    WHERE id_product = ' . $this->id_product);

        if($products){
            foreach($products as $product){
                if($product->online_only == 1){
                    $online_only = 1;
                }
            }
        }

        
        return $online_only;
    }

    public function allowBO(){
        $allow = false;
        $products = DB::connection('leeselectronic')
                        ->select('  SELECT *
                                    FROM ps_stock_available
                                    WHERE id_product = ' . $this->id_product);

        if($products){
            foreach($products as $product){
                if($product->out_of_stock == 1){
                    $allow = 1;
                }
            }
        }
        
        return $allow;
    }
    
}
