@extends('backend.layouts.main')

@section('content')

<h1>Purchase Order #{{$purchase->po_id}}</h1>

<table id="po_table" class="table table-bordered">
    <thead>
        <th>PID</th>
        <th>Image</th>
        <th>Name</th>
        <th>Qunatity Purchased</th>
        <th>Quanatity Received</th>
        @if(Auth::user()->hasAccess('cost')->read)
        <th>Unit Cost</th>
        <th>Total Cost</th>
        @endif
    </thead>
    <tbody>
        @foreach($purchase->products as $product)
            <tr>
                <td>{{$product->product_id}}</td>
                <td style="width:100px">
                    @if($product->detail && $product->detail->image_url())
                        @if(get_headers($product->detail->image_url()) && get_headers($product->detail->image_url())[0] != 'HTTP/1.1 404 Not Found')
                            <img class="img-fluid" src="{{$product->detail->image_url()}}" width="100px">
                        @else
                            <img class="img-fluid" src="{{ asset('img/no-image.png')}}" width="100px">
                        @endif
                    @else
                    <img class="img-fluid" src="{{ asset('img/no-image.png')}}" width="100px">
                    @endif
                </td>
                <td>{{$product->name}}</td>
                <td>{{$product->po_qty}}</td>
                <td>{{$product->rec_qty}}</td>
                @if(Auth::user()->hasAccess('cost')->read)
                <td>${{$product->unit_cost}}</td>
                <td>${{$product->total_cost}}</td>
                @endif
            </tr>
        @endforeach
    </tbody>
</table>

@endsection