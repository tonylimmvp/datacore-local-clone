<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vendor;

class VendorController extends Controller
{
    public function __construct(){
        return $this->middleware('auth');
    }

    public function index()
    {
        $vendors = Vendor::all();

        return view('backend.vendors.index',compact('vendors'));
    }

    public function products(Vendor $vendor) {
        return view('backend.vendors.detail',compact('vendor'));
    }

    public function upload()
    {
        return view('backend.vendors.upload');
    }

    public function store(Request $request)
    {   
       
        $this->validate(request(), [
            'csv_file' => 'required|file|mimes:txt,csv,xlsx',
        ]);

        $V1_columns = array("VENDOR_ID",
                            "FULL_NAME",
                            "CONTACT",
                            "ADDRESS",
                            "CITY",
                            "PROVINCE",
                            "POSTAL_CODE",
                            "COUNTRY",
                            "BUS_NO",
                            "EXT",
                            "FAX_NO",
                            "CELL_NO",
                            "PAGER_NO",
                            "PAGER_PIN",
                            "EMAIL_ADDRESS",
                            "COMMENT",
                            "VIA",
                            "FOB",
                            "TERRITORY",
                            "GROUP",
                            "DISCOUNT_TYPE",
                            "PAYMENT_TERM",
                            "PAYB_DISC",
                            "PAYB_DAY",
                            "INTEREST",
                            "SUSPEND",
                            "AMOUNT_OWE",
                            "CREDIT_LIMIT",
                            "SINCE_DATE",
                            "LAST_PAYMENT"
                            );

        $path = $request->file('csv_file')->getRealPath();

        $handle = fopen($path, "r");

        $line_num = 0;
        $header = false;
        
        while ( (($data = fgetcsv($handle,0, "\t","'")) !== FALSE)) {

            if($data[0]==$V1_columns[0]){
                $header = true;
            }
            if($header){
                // Validate columns
                if($line_num == 0){

                    for($index=0 ; $index < count($V1_columns); $index++){

                        $data_token = $data[$index];
                        $header_token = $V1_columns[$index];
							
						if(strcmp($data[$index], $V1_columns[$index]) != 0){
                            session()->flash('error','Header did not match, please consult the development team.');
                            return back();
                        }
                    }
                }
                else{   
                    if($data[25]){
                        $vendor = Vendor::where('reference',$data[0])->first();
                        if($vendor){
                            $vendor->isDisabled = true;
                            $vendor->save();
                        }
                    }else{
                        $vendor = Vendor::updateOrCreate(
                            ['reference' => $data[0]],
                            ['name' => $data[1],
                            'contact' => $data[2],
                            'address' => $data[3],
                            'city' => $data[4],
                            'province' => $data[5],
                            'postal' => $data[6],
                            'country' => $data[7],
                            'phone' => $data[8],
                            'ext' => $data[9],
                            'fax' => $data[10],
                            'cell' => $data[11],
                            'email' => $data[14],
                            'comment' => $data[15],
                            'isDisabled' => $data[25],
                            'created_at' => $data[28],
                            ]
                        );
                    }
                }

                $line_num++;
            }
        }

        if(!$header){
            session()->flash('error','Could not find header, please check the file you uploaded.');
            return back();
        }

        session()->flash('message',"You have successfully imported/updated vendors");

        return back();
    }

    public function updateURL(Request $request) {

        if(!$request->vendor_id){
            return json_encode(['status'=>'failed','message'=>'Missing Parameter.']);
        }
        
        $vendor = Vendor::find($request->vendor_id);

        if($vendor){
            $vendor->url = $request->url;
            $vendor->save();

            return json_encode(['status'=>'success','message'=>'You have successfully updated the remark.']);
        }else{
            return json_encode(['status'=>'failed','message'=>'Vendor does not exist.']);
        }
        
    }

}