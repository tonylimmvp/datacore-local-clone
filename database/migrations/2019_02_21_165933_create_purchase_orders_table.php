<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_orders', function (Blueprint $table) {
            $table->string('po_id');
            $table->string('pr_id')->nullable();
            $table->unsignedInteger('vendor_id');
            $table->string('invoice')->nullable();
            $table->string('user');
            $table->decimal('total_excl_tax',10,2);
            $table->decimal('total_incl_tax',10,2);
            $table->decimal('discount',8,2)->nullable();
            $table->decimal('pst',8,2)->nullable();
            $table->decimal('gst',8,2)->nullable();
            $table->string('currency')->nullable();
            $table->date('created_at')->nullable();
            $table->date('received_at')->nullable();

            $table->primary('po_id');
            $table->foreign('vendor_id')->references('id')->on('vendors');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_orders');
    }
}
