<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Product;
use App\OnlineOrder;

class OnlineOrderController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth',['except' => ['index','backorders','specialorders','show']]);
        $this->middleware('auth');
    }
    
    public function fetchOrder(Request $request) {

        // dd($request->all());
        if($request->order){
            $id_order = $request->order;
        }else{
            return json_encode(array("status"=>"failed","message"=>"There was no order specified."));
        }
        
        $sql = "SELECT `ps_orders`.`id_order`,
                        `ps_orders`.`id_cart`, 
                        `ps_orders`.`id_carrier`, 
                        `ps_orders`.`reference`, 
                        `ps_orders`.`current_state`, 
                        `ps_order_state_lang`.`name` AS `current_state_string`, 
                        `ps_orders`.`total_paid_tax_incl`, 
                        `ps_orders`.`total_shipping_tax_incl`, 
                        `ps_order_invoice`.`note` AS `invoice_note`, 
                        `ps_customer`.`note` AS `private_note`, 
                        `ps_orders`.`valid`, `ps_orders`.`date_add`, 
                        `ps_customer`.`firstname`, 
                        `ps_customer`.`lastname`, 
                        `ps_customer`.`email`, 
                        `ps_address`.`company`, 
                        `ps_address`.`firstname` AS `delivery_firstname`, 
                        `ps_address`.`lastname` AS `delivery_lastname`, 
                        `ps_address`.`address1`, 
                        `ps_address`.`address2`, 
                        `ps_address`.`postcode`, 
                        `ps_address`.`city`, 
                        `ps_address`.`phone`, 
                        `ps_address`.`phone_mobile`, 
                        `ps_state`.`name` AS `state`, 
                        `ps_country_lang`.`name` AS `country`,
                        `ps_carrier`.id_carrier,
                        `ps_carrier`.name AS `carrier_name`
                FROM    `ps_orders`, 
                        `ps_customer`, 
                        `ps_address`, 
                        `ps_country_lang`, 
                        `ps_state`,
                        `ps_order_state_lang`, 
                        `ps_order_invoice`,
                        `ps_carrier`
                WHERE `ps_orders`.`id_customer` = `ps_customer`.`id_customer` 
                AND `ps_order_state_lang`.`id_lang` = 1
                AND `ps_order_state_lang`.`id_order_state` = `ps_orders`.`current_state`
                AND `ps_orders`.`id_address_delivery` = `ps_address`.`id_address`
                AND `ps_address`.`id_country` = `ps_country_lang`.`id_country`
                AND `ps_address`.`id_state` = `ps_state`.`id_state`
                AND `ps_order_invoice`.`id_order` = `ps_orders`.`id_order`
                
                AND `ps_country_lang`.`id_lang` = 1
                
                AND `ps_orders`.`id_order` = '$id_order'
                AND `ps_carrier`.`id_carrier` = `ps_orders`.`id_carrier`
                ORDER BY `ps_orders`.`id_order` DESC
                LIMIT 15";

        $result = DB::connection('leeselectronic')->select(DB::raw($sql));

        if($result){
            $result = $result[0];
        }else{
            return json_encode(array('status' => 'failed', 'message' => 'Order could not be found.'));
        }
    
        $sql_cart = "SELECT `ps_order_detail`.`product_reference` as id_product, 
                            `ps_order_detail`.`product_quantity` as quantity, 
                            `ps_order_detail`.`product_name` as `name`, 
                            `table_1`.`price`, 
                            `table_1`.`unit`, 
                            `table_1`.`fraser_quantity`, 
                            `table_1`.`main_qty`, 
                            `table_1`.`fraser_loc`, 
                            `table_1`.`main_loc`,
                            `table_1`.`shelf_loc`
                    FROM `ps_order_detail`
                    INNER JOIN `table_1` 
                    ON `ps_order_detail`.`product_reference`= `table_1`.`id_product`
                    WHERE `ps_order_detail`.`id_order` = '$id_order'";

        
        $cart_result = DB::connection('leeselectronic')->select(DB::raw($sql_cart));

        if(!$cart_result){
            return json_encode(array('status' => 'failed', 'message' => 'Order cart missing.'));
        }

        $cart_array = array();
        
        foreach($cart_result as $product){
            $id_product = $product->id_product;
            $product->img_url = Product::find($id_product)->image_url(); 
            array_push($cart_array, $product);
        }

        $result->cart_items = $cart_array;
        
        $last_array = array('status' => 'success', 'result_data' => $result);
        return json_encode($last_array);
        
    }

    /********
     * 
     * Detail View of online order
     * 
     */

    public function detail(OnlineOrder $order){
        $sql_cart = "SELECT `ps_order_detail`.`product_reference` as id_product, 
        `ps_order_detail`.`product_quantity` as quantity, 
        `ps_order_detail`.`product_name` as `name`, 
        ROUND(`ps_order_detail`.`product_price`,2) as 'price', 
        `table_1`.`unit`, 
        `table_1`.`fraser_quantity`, 
        `table_1`.`main_qty`, 
        `table_1`.`fraser_loc`, 
        `table_1`.`main_loc`,
        `table_1`.`shelf_loc`
        FROM `ps_order_detail`
        INNER JOIN `table_1` 
        ON `ps_order_detail`.`product_reference`= `table_1`.`id_product`
        WHERE `ps_order_detail`.`id_order` = '$order->id_order'";

        $cart = DB::connection('leeselectronic')->select(DB::raw($sql_cart));
        return view('frontend.orders.online-detail',compact('order','cart'));
    }
    
}
