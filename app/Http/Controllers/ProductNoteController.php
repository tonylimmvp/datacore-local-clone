<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\ProductNote;
use App\ProductNoteType;
use Auth;
use Employee;
use EmployeeRole;
use DB;

class ProductNoteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request, Product $product)
    {
        $this->validate(request(), [
            'type_id' => 'required|integer',
        ]);

        $note = new ProductNote();
        $note->type_id = $request->type_id;
        $note->product_id = $request->id_product;
        $note->comments = $request->comments;
        $note->employee_id = Auth::user()->id;
        $note->created_at = NOW();

        $note->save();

        return redirect()->route('product-profile.show', $note->product_id);
    }

    public function convertMissingNotesFromFastsearch()
    {
        $search = "missing";
        $fsNotes = DB::connection('leeselectronic')->table('fs_product_notes')->where('note', 'LIKE', '%'.$search.'%')->where('handled', '0')->get();

        foreach ($fsNotes as $fsNote){
            $relation = ProductNote::updateOrCreate([
                'type_id' => '4',
                'product_id' => $fsNote->id_product,
                'comments' => $fsNote->note,
                'employee_id' => '1',
            ]);
        }

        $relation->save();

        return back();
    }

    public function convertRelationNotes()
    {
        $search = "sold separately";
        $fsNotes = DB::connection('leeselectronic')->table('fs_product_notes')->where('note', 'LIKE', '%'.$search.'%')->where('handled', '0')->get();

        foreach ($fsNotes as $fsNote){
            $relation = ProductNote::updateOrCreate([
                'type_id' => '1',
                'product_id' => $fsNote->id_product,
                'comments' => $fsNote->note,
                'employee_id' => '1',
            ]);
        }

        $relation->save();

        return back();
    }

    public function convertObsoleteNotes()
    {
        $fsNotes = DB::connection('leeselectronic')->table('fs_purchasing_board')->where('order_type', '=', 'obsolete')->where('done', '0')->get();
        foreach ($fsNotes as $fsNote){
            $note = ProductNote::updateOrCreate([
                'type_id' => '3',
                'product_id' => $fsNote->id_product,
                'comments' => 'Obtained from fastsearch',
                'employee_id' => '1',
                'created_at' => NOW(),
            ]);
        }

        $note->save();

        return back();
    }

    public function restrictSale(Product $product, Request $request)
    {
        $note = ProductNote::updateOrCreate([
            'type_id' => '8',
            'product_id' => $product->id_product,
            'comments' => $request->comments,
            'employee_id' => Auth::user()->id,
            'created_at' => NOW(),
        ]);

        return redirect()->back();
    }

    public function deleteNote(ProductNote $note)
    {
        $activeNote = ProductNote::find($note->id)->delete();
        return redirect()->back();
    }
}
