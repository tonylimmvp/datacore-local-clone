<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MobileOrderStatusHistory extends Model
{
    protected $connection = 'leeselectronic';
    protected $table = 'la_order_state_history';
    protected $primaryKey = 'order_state_history_id';
    protected $guarded = [];

    public $incrementing = false;
    public $timestamps = false;

    public function orders(){
        return $this->belongsTo(MobileOrder::class,'order_id');
    }
    public function status(){
        return $this->belongsTo(MobileOrderStatus::class,'state_id','order_state_id');
    }
    public function sales(){
        return $this->belongsTo(Employee::class,'employee_id');
    }

}
