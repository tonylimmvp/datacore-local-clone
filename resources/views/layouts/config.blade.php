<?php
// Start the session
session_start();
date_default_timezone_set('Canada/Pacific');
$INTERNAL_DATE_TIMESTAMP = date("d-m-Y");

function pc_validate($user,$pass) {
    /* replace with appropriate username and password checking,
       such as checking a database */
    $users = array(config('custom.user1','lees')=> config('custom.pass1','lees'));

    if (isset($users[$user]) && ($users[$user] == $pass)) {
        return true;
    } else {
        return false;
    }
}
if(isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])){
	if (!pc_validate($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW'])) {
	    header('WWW-Authenticate: Basic realm="Fast Search Lite"');
	    header('HTTP/1.0 401 Unauthorized');
	    echo "You need to enter a valid username and password.";
	    exit;
	}
}else{
    header('WWW-Authenticate: Basic realm="Fast Search Lite"');
    header('HTTP/1.0 401 Unauthorized');
    echo "You need to enter a valid username and password.";
    exit;
}

?>