@php

function avsMessage($status){
    if($status == "0"){
        return "Address Verification not performed for this transaction.";
    }elseif($status == "5"){
        return "Invalid AVS Response.";
    }elseif($status == "9"){
        return "Address Verification Data contains edit error..";
    }elseif($status == "A"){
        return "Street address matches, Postal/ZIP does not match.";
    }elseif($status == "B"){
        return "Street address matches, Postal/ZIP not verified.";
    }elseif($status == "C"){
        return "Street address and Postal/ZIP not verified.";
    }elseif($status == "D"){
        return "Street address and Postal/ZIP match.";
    }elseif($status == "E"){
        return "Transaction ineligible.";
    }elseif($status == "G"){
        return "Non AVS participant. Information not verified.";
    }elseif($status == "I"){
        return "Address information not verified for international transaction.";
    }elseif($status == "M"){
        return "Street address and Postal/ZIP match.";
    }elseif($status == "N"){
        return "Street address and Postal/ZIP do not match.";
    }elseif($status == "P"){
        return "Postal/ZIP matches. Street address not verified.";
    }elseif($status == "R"){
        return "System unavailable or timeout.";
    }elseif($status == "S"){
        return "AVS not supported at this time.";
    }elseif($status == "U"){
        return "Address information is unavailable.";
    }elseif($status == "W"){
        return "Postal/ZIP matches, street address does not match.";
    }elseif($status == "X"){
        return "Street address and Postal/ZIP match.";
    }elseif($status == "Y"){
        return "Street address and Postal/ZIP match.";
    }elseif($status == "Z"){
        return "Postal/ZIP matches, street address does not match.";
    }
}

function trnResponse($response,$avs="N"){
    if($response == 2){
        return "<i data-feather='x' style='color:red'></i>";
    }elseif($response == 1 && ($avs == "D" || $avs == "M" || $avs == "X" || $avs == "Y" || $avs == "" ||  $avs == " "  || $avs == null)){
        return "<i data-feather='check' style='color:green'></i>";
    }else{
        return "<i data-feather='alert-triangle' style='color:darkgoldenrod'></i>";
    }
}

@endphp
