<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipmentHistory extends Model
{
    protected $table='shipment_histories';   
    protected $fillable = ['shipment_id', 'status_id', 'employee_id', 'comment', 'created_at'];

    public function shipment(){
        return $this->belongsTo(Shipment::class);
    }

    public function employee(){
        return $this->belongsTo(Employee::class);
    }

    public function status(){
        return $this->belongsTo(ShipmentStatus::class);
    }
}
