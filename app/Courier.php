<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Courier extends Model
{
    protected $connection = "mysql";

    public function shipment(){
        return $this->belongsTo(Shipment::class);
    }
}
