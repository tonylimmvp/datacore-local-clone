let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/assets/sass/app.scss', 'public/css/compiled')
    .js('resources/assets/js/app.js', 'public/js/compiled')
    .js('resources/assets/js/custom.js', 'public/js/compiled')
    .sass('resources/assets/sass/custom.scss', 'public/css/compiled')
    .sass('resources/assets/sass/login.scss', 'public/css/compiled')
    .sass('resources/assets/sass/frontend_navtop.scss', 'public/css/compiled')
    .sass('resources/assets/sass/side-modal.scss', 'public/css/compiled');