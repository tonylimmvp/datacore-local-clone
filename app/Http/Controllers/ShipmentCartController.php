<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ShipmentCart;
use App\ProductNote;
use App\PurchaseOrderProduct;
use App\Shipment;
use Auth;
use DB;

class ShipmentCartController extends Controller
{
    public function insertToShipment(Request $request, ProductNote $note)
    {
        $this->validate(request(), [
            'qty' => 'required|integer|min:1',
        ]);

        $cart = new ShipmentCart();
        $cart->shipment_id = $request->shipment;
        $cart->product_id = $request->id;
        $cart->item_name = $request->name;
        $cart->qty = $request->qty;
        $cart->unit = $request->unit;
        $cart->comments = $request->comments;
        $cart->invoice = $request->invoice;
        $cart->employee_id = Auth::user()->id;
        $cart->created_at = NOW();

        $cart->save();

        $note->isHandled = '1';
        $note->updated_at = NOW();
        $note->handledBy = Auth::user()->id;
        $note->handledDate = NOW();
        $note->save();

        return redirect()->route('shipment.show');
    }

    public function editCart(Request $request, ShipmentCart $cart)
    {
        $this->validate(request(), [
            'qty' => 'required|integer|min:1',
        ]);
    
        $cart->shipment_id = $request->shipment;
        $cart->product_id = $request->id;
        $cart->item_name = $request->name;
        $cart->qty = $request->qty;
        $cart->unit = $request->unit;
        $cart->comments = $request->comments;
        $cart->vendor_remarks = $request->remarks;
        $cart->invoice = $request->invoice;
        $cart->employee_id = Auth::user()->id;
        $cart->updated_at = NOW();
        $cart->save();

        return back();
    }

    public function delete(ShipmentCart $cart)
    {
        $ActiveCart = ShipmentCart::find($cart->id)->delete();
        $ActiveCart = ShipmentCart::all();
        return back();
    }

    public function create(Request $request)
    {
        $shipmentNote = new ProductNote();
        $shipmentNote->type_id = 9;
        $shipmentNote->product_id = $request->id;
        $shipmentNote->qty = $request->qty;
        $shipmentNote->unit = $request->unit;
        
        $shipmentNote->employee_id = Auth::user()->id;
        $shipmentNote->created_at = NOW();
        $shipmentNote->save();

        $cart = new ShipmentCart();
        $cart->shipment_id = $request->shipment;
        $cart->product_id = $request->id;
        $cart->item_name = $request->name;
        $cart->qty = $request->qty;
        $cart->unit = $request->unit;
        $cart->comments = $request->comments;
        $cart->invoice = $request->invoice;
        $cart->employee_id = Auth::user()->id;
        $cart->created_at = NOW();
        $cart->note_id = $shipmentNote->id;

        $cart->save();

        return back();
    }

    public function importPO(Request $request)
    {
        // Validate form
        $this->validate(request(), [
            'po' => 'required|integer|exists:purchase_orders,po_id',
        ]);

        $products = PurchaseOrderProduct::where('po_id', $request->po)->get();

        foreach($products as $product){
            $shipmentNote = new ProductNote();
            $shipmentNote->type_id = 9;
            $shipmentNote->product_id = $product->product_id;
            $shipmentNote->qty = $product->po_qty;
            $shipmentNote->unit = 'winpos unit';
            $shipmentNote->employee_id = Auth::user()->id;
            $shipmentNote->created_at = NOW();
            $shipmentNote->save();


            $cart = new ShipmentCart();
            $cart->shipment_id = $request->shipment;
            $cart->product_id = $product->product_id;
            $cart->item_name = $product->name;
            $cart->qty = $product->po_qty;
            $cart->unit = 'winpos unit';
            $cart->comments = 'Imported from PO';
            $cart->employee_id = Auth::user()->id;
            $cart->created_at = NOW();
            $cart->note_id = $shipmentNote->id;
    
            $cart->save();
        }

        return back();
    }

    public function clear(Shipment $shipment)
    {
        //Converting created notes into isHandled = 1
        $carts = ShipmentCart::where('shipment_id', $shipment->id)->get();
        foreach ($carts as $c){
            $notes = $c->notes;
            foreach($notes as $note){
                $note->isHandled = '1';
                $note->handledBy = '1';
                $note->handledDate = NOW();
                $note->save();
            }
        }

        //Deleting all contents of shipment carts.
        $cart = ShipmentCart::where('shipment_id', $shipment->id)->delete();

        return redirect()->route('shipment.view', $shipment->id);
    }
}