<script>
function printLabel(printer_id,url){
    var product_id = $('#printProduct').text();
    var prompt_msg = "Number of labels to print (MAX 20 for SMALL| MAX 5 for BIG) for PID: " + product_id;
    var num_labels = window.prompt(prompt_msg,"1");
    var printer_selected = printer_id;
    if(num_labels != null){
        console.log(prompt_msg);
        console.log("printNumberOfLabels value: " + num_labels);
    }

    if(num_labels){

        $.ajax({
            type: "get",
            url: url,
            data: {pid: product_id, printer: printer_id,num: num_labels,date:"yes"},
            success: function( msg ) {
                $.ajax({
                    type: "get",
                    url: "{{ route('restock.remove') }}",
                    data: {pid: product_id},
                    dataType: 'json',
                    success: function( msg ) {
                        // console.log(msg)
                        // console.log(msg.status)
                        if(msg.status == "failed"){
                            alert(msg.message);
                        }
                    },
                    error: function (request, status, error) {
                        alert(request.status + ':' + error + '. Please take picture and contact DevTeam.');
                    }
                });
            },
            error: function (request, status, error) {
                alert(request.status + ':' + error + '. Please take picture and contact DevTeam.');
            }
        });

        $('#printModal').modal('hide');
    }

}

function printNoPriceLabel(printer_id,url){
    var product_id = $('#printProduct').text();
   
    var prompt_msg = "Number of no-price labels to print (MAX 20 for SMALL| MAX 5 for BIG) for PID: " + product_id;
    var num_labels = window.prompt(prompt_msg,"1");
    var printer_selected = printer_id;
    if(num_labels != null){
        console.log(prompt_msg);
        console.log("printNumberOfLabels value: " + num_labels);
    }
    
    if(num_labels){

        $.ajax({
            type: "get",
            url: url,
            data: {pid: product_id, printer: printer_id,num: num_labels,date:"yes"},
            success: function( msg ) {
                $.ajax({
                    type: "get",
                    url: "{{ route('restock.remove') }}",
                    data: {pid: product_id},
                    dataType: 'json',
                    success: function( msg ) {
                        // console.log(msg)
                        // console.log(msg.status)
                        if(msg.status == "failed"){
                            alert(msg.message);
                        }
                    },
                    error: function (request, status, error) {
                        alert(request.status + ':' + error + '. Please take picture and contact DevTeam.');
                    }
                });
            },
            error: function (request, status, error) {
                alert(request.status + ':' + error + '. Please take picture and contact DevTeam.');
            }
        });

        $('#printModal').modal('hide');
    }
    
    
}

function printQuantityLabel(printer_id,url){
    var product_id = $('#printProduct').text();
    var prompt_msg = "Enter number of quantity for PID #" + product_id;
    var quantity = window.prompt(prompt_msg,"1");
    var printer_selected = printer_id;
    if(quantity != null){
        console.log(prompt_msg);
    }
    if(quantity){
        $.ajax({
            type: "get",
            url: url,
            data: {pid: product_id, printer: printer_id,count: quantity,date:"yes"},
            success: function( msg ) {
                
            },
            error: function (request, status, error) {
                alert(request.status + ':' + error + '. Please take picture and contact DevTeam.');
            }
        });

        $('#printModal').modal('hide');
    }

}

function printPostekPrice(){
    var product_id = $('#printProduct').text();
    $('#postek-price').attr('href',"{{url('/api/printers/postek')}}/" + product_id);
}

function printPostekNoPrice(){
    var product_id = $('#printProduct').text();
    $('#postek-no-price').attr('href',"{{url('/api/printers/postek-noprice')}}/" + product_id);
}

function printPostekQuantity(){
    var product_id = $('#printProduct').text();
    var prompt_msg = "Enter number of quantity for PID #" + product_id;
    var quantity = window.prompt(prompt_msg,"1");
    if(quantity != null){
        console.log(prompt_msg);
    }
    if(quantity){
        $('#postek-quantity').attr('href',"{{url('/api/printers/postek-quantity')}}/" + product_id +"?quantity=" +quantity); 
    }
}
</script>