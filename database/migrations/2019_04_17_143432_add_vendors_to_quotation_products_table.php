<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVendorsToQuotationProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quotation_product', function (Blueprint $table) {
            $table->string('vendor')->nullable();
            $table->string('vendor_remark')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quotation_product', function (Blueprint $table) {
            $table->dropColumn('vendor_remark');
            $table->dropColumn('vendor');
        });
    }
}
