@extends('backend.layouts.main')

@include('layouts.datatables')

@section('content')

<h1 class="h1">Dashboard</h1>

<div>
    <div class="card-deck mb-3 text-center">
        <div class="card mb-4 shadow-sm">
            <div class="card-header">
            <h5 class="my-0 font-weight-normal">Orders</h5>
            </div>
            <div class="card-body">
            <h1 class="card-title pricing-card-title">
                @if(Auth::user()->hasAccess('order')->read)
                    <a href="{{route('orders')}}">{{ sizeof($orders) }}</a>
                @else
                {{ sizeof($orders) }}
                @endif
            </h1>
            </div>
        </div>
        <div class="card mb-4 shadow-sm">
            <div class="card-header">
            <h5 class="my-0 font-weight-normal">New Items</h5>
            </div>
            <div class="card-body">
            <h1 class="card-title pricing-card-title">{{ sizeof($products) }}</h1>
            </div>
        </div>
        <div class="card mb-4 shadow-sm">
            <div class="card-header">
            <h5 class="my-0 font-weight-normal">Restock</h5>
            </div>
            <div class="card-body">
            <h1 class="card-title pricing-card-title"><a href="{{route('restock')}}">{{ sizeof(App\RestockProduct::all()) }}</a></h1>
            </div>
        </div>
    </div>

    {{-- <canvas class="my-4 w-100" id="myChart" width="900" height="300"></canvas> --}}

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        
            <h2>Recent Orders</h2>
            @if(Auth::user()->hasAccess('order')->read)
            <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group mr-2">
                <a href="{{ route('orders') }}">
                    <button class="btn btn-sm btn-outline-secondary">View All</button>
                </a>
            </div>
            </div>
            @endif
        </div>
    
    <div class="table-responsive">
        <table class="table table-striped table-sm" id="recent-orders" width="100%">
        <thead>
            <tr>
                <th width="100px" style="width:100px" class="text-center">Order #</th>
                <th width="100px" style="width:100px"></th>
                <th>Customer Name</th>
                <th>Customer Email</th>
                <th>Customer Phone</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            @foreach($orders as $order)
            <tr>
                <td class="text-center">
                    @if(Auth::user()->hasAccess('order')->read)
                    <a href="{{route('orders.detail',$order->id)}}">
                    @endif
                    <span> {{ $order->id }} </span>

                    @if(Auth::user()->hasAccess('order')->read)
                    </a>
                    @endif
                </td>
                <td class="text-center"> @if($order->invoice_id == "1234567")
                        <span class="badge badge-danger">UNPAID</span>
                    @else
                        <span class="badge badge-success">INVOICED</span>
                    @endif
                </td>
                <td>{{ $order->name }}</td>
                <td>{{ $order->email }}</td>
                <td>{{ $order->phone}}</td>
                <td class="text-center"><span class="order-status" style="background:{{$order->currentStatus->color}}">{{ $order->currentStatus->name }}</span></td>
            </tr>
            @endforeach
        </tbody>
        </table>
    </div>

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        
        <h2>New Items</h2>
        @if(Auth::user()->hasAccess('product')->update)
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="btn-group btn-group-sm mr-2">
                <a class="btn btn-sm btn-outline-secondary" href="{{ config('custom.web_url') }}/webui/index.php?controller=AdminImport" target="_blank">
                    Import
                </a>
                <a class="btn btn-sm btn-outline-secondary" href="{{ route('product.export-new') }}">
                    Export
                </a>
            </div>
        </div>
        @endif
    </div>

    <div class="table-responsive">
        <table class="table table-striped table-sm"  id="new-products" width="100%">
            <thead>
            <tr>
                <th>PID</th>
                <th>Name</th>
                <th>Price</th>
                <th>Quantity</th>
                @if(Auth::user()->hasAccess('vendors')->read)
                    <th>Vendor</th>
                @endif
            </tr>
            </thead>
            <tbody>
                @foreach($products as $product)
            <tr>
                <td>{{$product->id_product}}</td>
                <td>{{$product->name}}</td>
                <td>${{$product->price}}</td>
                <td>{{$product->quantity}}</td>
                @if(Auth::user()->hasAccess('vendors')->read)
                    <td>{{$product->vendor}}</td>
                @endif
            </tr>

            @endforeach
            </tbody>
        </table>
    </div>

    @if(!Auth::user()->hasAccess('product')->update)
        <div class="alert alert-info mt-2">
            <p>Note: To upload new items, please contact dev team or supervisors.</p>
        </div>
    @endif
@endsection

@push('bottom-scripts')
{{-- Graphs --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
{{-- <script>
    var ctx = document.getElementById("myChart");
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            datasets: [{
                data: [15339, 21345, 18483, 24003, 23489, 24092, 12034],
                lineTension: 0,
                backgroundColor: 'transparent',
                borderColor: '#007bff',
                borderWidth: 4,
                pointBackgroundColor: '#007bff'
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: false
                    }
                }]
            },
            legend: {
                display: false,
            }
        }
    });
</script> --}}

<script type="text/javascript">
    $(document).ready(function () {
        $('#recent-orders').DataTable({
            "scrollY":        "200px",
            scrollX:        true,
            "scrollCollapse": true,
            "paging":         false,
            "order":          [[0,"desc"]],
            "language": {
                "search": "Filter records:"
            }
        });
        $('#new-products').DataTable({
            "scrollY":        "200px",
            scrollX:           true,
            "scrollCollapse": true,
            "paging":         false,
            "language": {
            "search": "Filter records:"
        }
        });
    });
</script>

@endpush