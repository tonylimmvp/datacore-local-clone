<?php

use Illuminate\Database\Seeder;

class ShipmentHistoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Seeding rma_status table begins
        DB::table('shipment_histories')->insert([
            'shipment_id' => '1',
            'status_id' => '3',
            'employee_id' => '3',
            'comment' => 'db:seed',
            'created_at' => NOW()
        ]);

        DB::table('shipment_histories')->insert([
            'shipment_id' => '2',
            'status_id' => '2',
            'employee_id' => '5',
            'comment' => 'db:seed',
            'created_at' => NOW()
        ]);
        
    }
}
