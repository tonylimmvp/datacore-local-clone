@extends('backend.layouts.main')

@include('layouts.datatables')

@section('content')

<h1>Purchase Orders</h1>

<div class="card-deck mb-3 text-center">
    <div class="card mb-4 shadow-sm">
        <div class="card-header">
        <h5 class="my-0 font-weight-normal">Opened PO</h5>
        </div>
        <div class="card-body">
        <h1 class="card-title pricing-card-title">
            <a href="{{ route('coreui.po') . "?opened=true" }}">{{ sizeof(App\PurchaseOrder::open()->get() ) }}</a>
        </h1>
        </div>
    </div>
    <div class="card mb-4 shadow-sm">
        <div class="card-header">
        <h5 class="my-0 font-weight-normal">Date</h5>
        </div>
        <div class="card-body">
            @for($i = 2014 ; $i <= Carbon\Carbon::now()->year ; $i++)
            <a href="{{ route('coreui.po') . "?date=" . $i }}">
                <button class="btn btn-primary mb-1">
                    {{$i}}
                </button>
            </a>
            @endfor
        </div>
    </div>
    <div class="card mb-4 shadow-sm">
        <div class="card-header">
        <h5 class="my-0 font-weight-normal">All PO</h5>
        </div>
        <div class="card-body">
        <h1 class="card-title pricing-card-title">
            <a href="{{ route('coreui.po') }}">{{ sizeof(App\PurchaseOrder::active()->get() ) }}</a>
        </h1>
        </div>
    </div>
</div>

<div class="table-responsive">
    <table id="po_table" class="table table-bordered" width=100%>
        <thead>
            <th>PO #</th>
            <th>PR #</th>
            <th>Vendor</th>
            <th>Invoice</th>
            <th>Created At</th>
            <th>Received At</th>
        </thead>
        <tbody>
        @foreach($purchases as $purchase)
            @php $products = App\PurchaseOrderProduct::where('po_id',$purchase->po_id)->get(); @endphp
            <tr>
            <td><a href="{{ route('coreui.po.products',$purchase->po_id) }}" data-toggle="tooltip" data-placement="bottom" title="# of items: {{sizeof($products)}}">{{$purchase->po_id}}</a></td>
                <td>{{$purchase->pr_id}}</td>
                <td>{{$purchase->vendor->reference}}</td>
                <td>{{$purchase->invoice}}</td>
                <td>{{$purchase->created_at}}</td>
                <td>{{$purchase->received_at}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

@isset($purchases)
@php 
    $entry_start = (($purchases->currentPage() - 1)* $purchases->perPage()) + 1;
    $entry_end = (($purchases->currentPage() - 1)* $purchases->perPage()) + $purchases->count();
@endphp

<p class="mt-2">Showing {{ $entry_start }} to {{ $entry_end }} of {{ $purchases->total() }} purchase orders</p>


{{-- Pagination Links --}}

<div class="d-flex flex-wrap justify-content-center">
    {{ $purchases->appends(request()->input())->render("pagination::bootstrap-4") }}
</div>
@endisset
{{-- End of Links --}}

@endsection

@push('bottom-scripts')

<script type="text/javascript">
    $(document).ready(function () {
        $('#po_table').DataTable({
            scrollY:        "60vh",
            scrollX:        true,
            scrollCollapse: true,
            paging:         false,
            info:     false,
            order: [[ 4, "desc" ]],
            "language": {
                "search": "Filter records:"
            }
        });
    });
</script>
@endpush