<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use DB;

class AnalyticsController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    /***************************************************************************
     * 
     *  Views
     * 
     ***************************************************************************/

    public function purchaseAnalytics(){
        return view('backend.stats.purchase_analytics.index');
    }

    public function salesAnalytics(){
        return view('backend.stats.sale_analytics.index');
    }

    public function productAnalytics(){
        return view('backend.stats.product_analytics.index');
    }

    public function productSales(){
        return view('backend.stats.product_analytics.sales');
    }

    /***************************************************************************
     * 
     *  Functions
     * 
     ***************************************************************************/

    //Static function will allow view page to call these functions
    public static function getMonthlySalesRevenue(Product $pid, int $month, int $year){
        $monthlyRevenue = 0;

        //Perform a check whether the PID exist. If exist, grab sales revenue.
        if (Product::find($pid)){
            $saleHistories = DB::table('sales_histories')->where('product_id', $pid)->where('year', $year)->where('month', $month)->get();
            foreach($saleHistories as $saleHistory){
                $monthlyRevenue = $saleHistory->total;
            }
        }

        return $monthlyRevenue;
    }

    public static function getYearlySalesRevenue(Product $pid, int $year){
        $month = 1;
        $endMonth = 12;
        $yearlyRevenue = 0; //Initiate default amount for yearlyRevenue.

        //Perform a check whether the PID exist. If exist, grab sales revenue.
        if (Product::find($pid)){
            $saleHistories = DB::table('sales_histories')->where('product_id', $pid)->where('year', $year)->get();
            foreach($saleHistories as $saleHistory){
                $yearlyRevenue += $saleHistory->total;
            }
        }    
        
        return $yearlyRevenue;
    }

    public static function getTotalSalesRevenue(Product $pid){
        $month = 1;
        $endMonth = 12;
        $totalRevenue = 0;

        //Perform a check whether the PID exist. If exist, grab sales revenue.
        if (Product::find($pid)){
            $saleHistories = DB::table('sales_histories')->where('product_id', $pid)->get();
            foreach($saleHistories as $saleHistory){
                $totalRevenue += $saleHistory->total;
            }
        }

        return $totalRevenue;
    }

    public static function getCompanyYearlyRevenue(int $year){
        $totalRevenue = 0;

        $saleHistories = DB::table('sales_histories')->where('year', $year)->get();
        foreach($saleHistories as $saleHistory){
            $totalRevenue += $saleHistory->total;
        }

        return $totalRevenue;
    }

    public static function getCompanyMonthlyRevenue(int $year, int $month){
        $totalRevenue = 0;

        $saleHistories = DB::table('sales_histories')->where('year', $year)->where('month', $month)->get();
        foreach($saleHistories as $saleHistory){
            $totalRevenue += $saleHistory->total;
        }

        return $totalRevenue;
    }

    public static function y_ySaleRevenue(){
        $revenueCurrentYear = 0;
        $revenueLastYear = 0;
        $revenueChange = 0;
        $y_yChange = 0;
        $currentYear = date('Y') + 0; //Use the +0 to convert into integer, instead of getting string.
        $lastYear = date('Y') - 1;

        $saleHistoriesCurrentYear = DB::table('sales_histories')->where('year', $currentYear)->get();
        $saleHistoriesLastYear = DB::table('sales_histories')->where('year', $lastYear)->get();

        foreach($saleHistoriesCurrentYear as $saleHistoryCurrentYear){
            $revenueCurrentYear += $saleHistoryCurrentYear->total;
        }

        foreach($saleHistoriesLastYear as $saleHistoryLastYear){
            $revenueLastYear += $saleHistoryLastYear->total;
        }

        $revenueChange = $revenueCurrentYear - $revenueLastYear;
        
        if($revenueCurrentYear < $revenueLastYear){
            $y_yChange = -(1 - ($revenueCurrentYear / $revenueLastYear)) * 100;
        }
        else{
            $y_yChange = (1 - ($revenueCurrentYear / $revenueLastYear)) * 100;
        }

        return $revenueChange;
    }

    public static function m_mSaleRevenue(){
        $revenueCurrentMonth = 0;
        $revenueLastMonth = 0;
        $revenueChange = 0;
        $m_mChange = 0;
        $currentYear = date('Y') + 0;
        $currentMonth = date('m') + 0;
        
        if ($currentMonth >= 2){
            $lastMonth = date('m') - 1;
            $saleHistoriesLastMonth = DB::table('sales_histories')->where('year', $currentYear)->where('month', $lastMonth)->get();
        }
        elseif ($currentMonth = 1){
            $lastMonth = 12;
            $lastYear = $currentYear - 1;
            $saleHistoriesLastMonth = DB::table('sales_histories')->where('year', $lastYear)->where('month', $lastMonth)->get();
        }

        $saleHistoriesCurrentMonth = DB::table('sales_histories')->where('year', $currentYear)->where('month', $currentMonth)->get();

        foreach($saleHistoriesCurrentMonth as $saleHistoryCurrentMonth){
            $revenueCurrentMonth += $saleHistoryCurrentMonth->total;
        }

        foreach($saleHistoriesLastMonth as $saleHistoryLastMonth){
            $revenueLastMonth += $saleHistoryLastMonth->total;
        }

        $revenueChange = $revenueCurrentMonth - $revenueLastMonth;

        if($revenueCurrentMonth < $revenueLastMonth){
            $m_mChange = -(1 - ($revenueCurrentMonth / $revenueLastMonth)) * 100;
        }
        else{
            $m_mChange = (1 - ($revenueCurrentMonth / $revenueLastMonth)) * 100;
        }

        return $revenueChange;
    }

    //Get Company's average item revenue for all sales within the year.
    public static function getAverageProductRevenue(int $year){
        $avgProductRevenue = 0;
        $totalItemsSold = 0;
        $totalRevenue = 0;
        
        $saleHistories = DB::table('sales_histories')->where('year', $year)->get();

        foreach($saleHistories as $saleHistory){
            $totalItemsSold += $saleHistory->qty;
            $totalRevenue += $saleHistory->total;
        }

        if($totalItemsSold == 0){
            $avgProductRevenue = 0;
        }
        else{
            $avgProductRevenue =  $totalRevenue / $totalItemsSold;
        }

        return $avgProductRevenue;
    }

    public function getMonthlyRevenue(Request $request){
        $monthly_revenues = [];

        $saleHistories = DB::select("SELECT * , SUM(total) as total_rev FROM `sales_histories` where year = '$request->year' group by month");

        foreach($saleHistories as $saleHistory){
            array_push($monthly_revenues,$saleHistory->total_rev);
        }

        return json_encode(["status"=>"success","monthly_revenues"=>$monthly_revenues]);
    }

    public static function getCompanyYearlyCost(int $year){
        $totalCost = 0;

        $costHistories = DB::select("SELECT year(received_at) as year, SUM(total_excl_tax) as annual_total_excl_tax, SUM(total_incl_tax) as annual_total_incl_tax FROM `purchase_orders`where pr_id > 0 AND year(received_at) = '$year' group by year(received_at)");

        foreach ($costHistories as $costHistory){
            $totalCost += $costHistory->annual_total_incl_tax;
        }

        return $totalCost;
    }

}