@extends('frontend.layouts.main')

@push('css')
<link rel="stylesheet" href="{{asset("css/phonebook.css")}}">   
@endpush
@section('content')
<div class="title m-b-md">
    Phonebook
</div>
@auth
        @if(Auth::user()->hasAccess('companies')->create)
        <div class="row d-flex justify-content-center">
            <a class ="create-button btn btn-primary btn-lg mx-3" href="{{route('phonebook-create')}}">Create New Company</a>
        </div>
        <br>
        <div class="row d-flex justify-content-center">
            <a class ="create-button btn btn-primary btn-lg mb-1 mx-3" href="{{route('phonebook-create-filter')}}">Create New Filter</a>
            <a class ="create-button btn btn-primary btn-lg mb-1 mx-3" href="{{route('phonebook-manage-filter')}}">Manage Filters</a>
        </div>
        @endif
        @endauth
        <hr>
    <form class ="row d-flex justify-content-center" method='post' action= {{route('phonebook-filter')}}>
        {{csrf_field()}}
        @foreach ($filters as $filter)
            @if($filter->isActive)
                <div class= "input-group-txt col-md-1 row">
                    <input type="checkbox" id="{{$filter->name}}"name= "{{$filter->name}}" class= "col-md-4 mt-1" value = "{{$filter->id}}"
                    <?php 
                        if(isset($_POST[$filter->name])) echo "checked='true'"; 
                        ?>>
                    <label for="{{$filter->name}}" class= "col-md-8">{{$filter->name}}</label>
                </div>
            @endif
        @endforeach
        <div class= "row d-flex justify-content-center">
            <div class= 'col-md-1 col-sm-6'>
                <input class="btn btn-primary m-1" value ='Reset' type='reset'>
            </div>
            <div class = "col-md-1 col-sm-6">
                <input class="btn btn-primary m-1" value ='Filter' type='submit'>
            </div>
        </div>
    </form>
        <div class="row">
            <div class="company-list">
                <h2>List of Companies</h2>
                @foreach ($companies as $company)
                <ul class="company">
                    <h3>{{$company ->name}}</h3>
                    @if ($company->service_types->first())
                            <li>
                                @foreach ($company->service_types as $service)
                                    @if($service->isActive)
                                        <span class ="badge badge-primary">{{$service->name}}</span>
                                    @endif
                                @endforeach
                            </li>
                    @endif

                    @if($company ->address)
                   
                        <li><strong>Address: </strong>{{$company ->address}}</li>

                    @endif

                    @if($company ->telephone)

                        <li><strong>Telephone #:</strong> {{$company ->telephone}}</li>

                    @endif

                    <li><strong>Description:</strong> {{$company ->description}}</li>

                    @if($company ->site)

                        <a class="btn btn-primary btn-md " href="{{$company ->site}}" target="_blank">Site</a>

                    @endif

                    @if($company ->maps)

                        <a class="btn btn-primary btn-md " href="{{$company ->maps}}" target="_blank">Maps</a>

                    @endif
                    @auth
                    @if(Auth::user()->hasAccess('companies')->update)
                    <a class="btn btn-primary btn-md " href ="{{route('phonebook-edit', $company->id, $filters)}}"> Modify </a>
                    @endif
                    
                    @if(Auth::user()->hasAccess('companies')->delete)
                    <form class="delete-form" action="{{route('phonebook-delete',$company->id)}}" method = "post">
                        
                        <button class="btn btn-primary btn-md" type="submit" >Remove</button>
                        {{ method_field('DELETE') }}
                        {{csrf_field()}}
                    </form>
                    @endif
                    @endauth
                </ul>

                @endforeach
            </div>
        </div>
    </div>
    
    
 @endsection

 @push("bottom-scripts")
 <script type="text/javascript" src = "{{asset('js/phonebook.js')}}"></script>
 @endpush