<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use DB;

class Order extends Model
{
    protected $connection = "mysql";
    protected $guarded = [];

    protected $searchable = [
        'orders.id',
        'orders.name',
        'phone',
        'email',
        'invoice_id',
        'tracking_id',
        'order_product.product_id',
        'order_product.name',
        'order_product.vendor'
    ];

    ////////////////////////////////////////////
    //
    //      Eloquent Relatioons
    //
    //////////////////////////////////////////

    public function products(){
        $database = $this->getConnection()->getDatabaseName();
        return $this->belongsToMany(Product::class, "$database.order_product",'order_id','product_id')->withPivot('id','name','price','quantity','vendor','vendor_remark','inCart','isPurchased');
    }

    public function sales(){
        return $this->belongsTo(Employee::class,'employee_id');
    }

    public function currentStatus(){
        return $this->belongsTo(OrderStatus::class,'status_id');
    }

    public function statuses(){
        return $this->hasMany(OrderStatusHistory::class);
    }

    public function note(){
        return $this->belongsToMany('App\Note', 'order_notes');
    }

    ////////////////////////////////////////////
    //
    // Functions
    //
    //////////////////////////////////////////

    public function scopeFilter($query, $filters){
        if(isset($filters['status'])){
            $status = $filters['status'];
            $query->where('status_id',$status);
        }
        if(isset($filters['order'])){
            $order = $filters['order'];
            if($order == "unpaid"){
                $query->unpaid();
            }
        }
    }

    public function scopeUnpaid($query) {
        $query->where('invoice_id','=','1234567')->where('status_id','!=','4');
    }

    public function scopeBackorder($query) {
        $query->where('type','=','backorder');
    }

    public function scopeSpecial($query) {
        $query->where('type','=','special');
    }

    public function scopePhoneorder($query) {
        $query->where('type','=','phone');
    }

    public function scopeCompleted($query){
        
        $query->where('status_id','!=',4);
        
    }

    public static function orderCount(){
        $count = Order::selectRaw('count(*) as all_orders, (SELECT count(*) from orders where status_id != 4 && type = "backorder") as backorders,(SELECT count(*) from orders where status_id != 4 && type = "special") as special, (SELECT count(*) from orders where status_id != 4 && type = "phone") as phone');
        $count->where('status_id', '!=', '4');
        return $count->first();
    }

    public static function oldBO(){
        $orders = DB::connection('leeselectronic')->table('backorders')->where('status','!=','3')->get();

        foreach($orders as $order){
            $products = array();
            
            $order_products = DB::connection('leeselectronic')->table('backorder_items')->where('backorder_id',$order->backorder_id)->get();
            foreach($order_products as $product){
                array_push($products,$product);
            }
            $order->products = $products;
        }
        
        return $orders;
    }

    public static function oldSPO(){
        $orders = DB::connection('leeselectronic')->table('orders')->where('status','!=','7')->get();

        foreach($orders as $order){
            $products = array();
            
            $order_products = DB::connection('leeselectronic')->table('cart_items')->where('order_id',$order->order_id)->get();
            foreach($order_products as $product){
                array_push($products,$product);
            }
            $order->products = $products;
        }

        return $orders;
    }

    public static function relatedPhone($order){

        $query = static::where('phone',$order->phone)->where('id','!=',$order->id);

        return $query->get();
    }

    public static function incompleteRelatedPhone($order){

        $query = static::where('phone',$order->phone)->where('id','!=',$order->id)->where('status_id','!=','4');

        return $query->get();
    }

    public static function relatedInvoice($order){
    
        $query = static::where('invoice_id',$order->invoice_id)->where('id','!=',$order->id)->where('invoice_id','!=','1234567');

        return $query->get();
    }

    public static function incompleteRelatedInvoice($order){
    
        $query = static::where('invoice_id',$order->invoice_id)->where('id','!=',$order->id)->where('status_id','!=','4')->where('invoice_id','!=','1234567');

        return $query->get();
    }

    public function isAllPurchased(){
        $all_purchased = true;
        // dd($this->products);
        foreach($this->products as $product){
            if(!($product->product_id == 2 || $product->product_id == 3 ) && !$product->pivot->isPurchased){
                $all_purchased = false;
            }
        }
        return $all_purchased;
    }

    ////////////////////////////////////////////
    //
    // Search
    //
    //////////////////////////////////////////

    protected function getWhereClause($term)
    {
        // removing symbols used by MySQL
        $reservedSymbols = ['<', '>', '@', '(', ')', '~','\''];
        $term = str_replace($reservedSymbols, '', $term);
        $whereClause = '';

        $words = explode(' ', $term);

        foreach($words as $word_id=>$word){
            if($word_id > 0){
                $whereClause .= ") AND (";
            }else{
                $whereClause .= "(";
            }
            foreach ($this->searchable as $col_id=>$column){
                if($col_id==0){
                    $whereClause .= "$column LIKE '%$word%' "  ; 
                }else{
                    $whereClause .= "OR $column LIKE '%$word%' "  ; 
                }
            }

            if($word_id + 1 == sizeof($words)){
                    $whereClause .= ")";
                }
        }
        return $whereClause;
    }

    public function scopeSearch($query, $term)
    {
        $columns = implode(',',$this->searchable);
        
        $whereClause = $this->getWhereClause($term);

        $query->selectRaw(' `orders`.*, 
                            `order_product`.`product_id`, 
                            `order_product`.`name` AS product_name')
                ->leftJoin('order_product','orders.id','=','order_product.order_id')
                ->whereRaw($whereClause);
                
        return $query;
    }

    public function isSigned(){
        $file = Storage::disk('public')->exists('invoices/order'.$this->id.'.jpg');
        return $file;
     }

     public function lastSigned(){
        $url = url('/storage/invoices/order').$this->id.'.jpg';
        $c = curl_init($url);
        curl_setopt($c, CURLOPT_HEADER, 1); // Include the header    
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_FILETIME, 1);   
        curl_exec($c);
        $result = curl_getinfo($c);   

        return date('m/d/Y',(int)$result['filetime']);
     }

}
