@extends('frontend.layouts.main')

@include('layouts.datepicker')

@section('content')

<div class="container">

<h1 class="mt-2">Create RMA</h1>
    <form method="POST" action="{{route('rma.store')}}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name">Customer Name</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name" value="{{old('name')}}">
        </div>

        <div class="form-group">
            <label for="phone">Phone</label>
            <div class="input-group">
                <input type="text" class="form-control" id="phone" name="phone" placeholder="Enter Phone" value="{{old('phone')}}">
                <div class="input-group-prepend">
                    <div class="input-group-text"><span>Ext.</span></div>
                </div>
                <input type="text" class="form-control" id="phone_ext" name="phone_ext" placeholder="(optional)" value="{{ old('phone_ext') }}">
            </div>
        </div>

        <div class="form-group">
            <label for="email">Email</label>
            <input type="text" class="form-control" id="email" name="email" placeholder="Enter E-mail" value="{{old('email')}}">
        </div>

            <div class="form-group">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <b><label for="id_product">Product ID</label></b>
                        <div class="input-group">
                        <input class="form-control" type="number" id="id_product" name="id_product" placeholder="Enter Product ID" value="{{old('id_product')}}">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button" onclick="autofill()"><span data-feather="align-left"></span></button>
                            </div>
                        </div>
                    </div>
            
                    <div class="form-group col-md-6">
                        <b><label for="product_name">Product Name</label></b>
                        <input class='form-control' type="text" id="product_name" name="product_name" placeholder="Click on Button to autofill." value="{{old('product_name')}}">
                    </div>

                    <div class="form-group col-md-6">
                        <b><label for="product_vendor">Vendor Reference</label></b>
                        <input class='form-control' type="text" id="product_vendor" name="product_vendor" placeholder="Click on Button to autofill." value="{{old('product_vendor')}}">
                    </div>
                </div>
                <hr/>

            </div>

            <div class="form-group">
                <label for="qty">Quantity</label>
                <input type="text" class="form-control" id="qty" name="qty" placeholder="Enter Quantity" value="{{old('qty')}}" >

                <label for="comments">Comments</label>
                <input type="text" class="form-control" id="comments" name="comments" placeholder="Enter any information related to the RMA here" value="{{old('comments')}}" >
            </div>

            <div class="form-group">
                <label for="invoice_num">Invoice Number</label>
                <input type="text" class="form-control" id="invoice_num" name="invoice_num" placeholder="Enter Invoice Number" length="10" value="{{old('invoice_num')}}">

                <label for="invoice_date">Invoice Date</label>
                <input class="form-control" id="datepicker" name="invoice_date" placeholder="MM/DD/YYYY" value="{{old('invoice_date')}}" >
                
            </div>

        <button type="submit" class="btn btn-primary">Submit</button>

        @include('layouts.errors')

    </form>
</div>
@endsection

@push('bottom-scripts')

    <script>
        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap4'
        });
    </script>

<script>
    function autofill(){
                var pid = $('#id_product')[0].value;
                console.log(pid);
                $.ajax({
                    type: "get",
                    url: "{{ route('product.autofill') }}",
                    data: {pid: pid},
                    success: function( msg ) {
                        if(msg.status == "success"){
                            $('#product_name')[0].value = msg.name
                            $('#product_vendor')[0].value = msg.vendor
                        }else{
                            alert(msg.message);
                        }
                    },
                    error: function (request, status, error) {
                        alert(request.status + ':' + error + '. Please take picture and contact DevTeam.');
                    }
                });
            };
</script>
@endpush