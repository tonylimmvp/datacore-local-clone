<?php

use Illuminate\Database\Seeder;

class ProductNoteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_notes')->insert([
            'type_id' => '8',
            'product_id' => '432',
            'comments' => 'Commercial sale only.',
            'employee_id' => '1',
            'created_at' => NOW()
        ]);

        DB::table('product_notes')->insert([
            'type_id' => '8',
            'product_id' => '4280',
            'comments' => 'Commercial sale only.',
            'employee_id' => '1',
            'created_at' => NOW()
        ]);

        DB::table('product_notes')->insert([
            'type_id' => '8',
            'product_id' => '4280',
            'comments' => 'Commercial sale only.',
            'employee_id' => '1',
            'created_at' => NOW()
        ]);

        DB::table('product_notes')->insert([
            'type_id' => '8',
            'product_id' => '4284',
            'comments' => 'Commercial sale only.',
            'employee_id' => '1',
            'created_at' => NOW()
        ]);

        DB::table('product_notes')->insert([
            'type_id' => '8',
            'product_id' => '4328',
            'comments' => 'Commercial sale only.',
            'employee_id' => '1',
            'created_at' => NOW()
        ]);

        DB::table('product_notes')->insert([
            'type_id' => '8',
            'product_id' => '4865',
            'comments' => 'Commercial sale only.',
            'employee_id' => '1',
            'created_at' => NOW()
        ]);

        DB::table('product_notes')->insert([
            'type_id' => '8',
            'product_id' => '15483',
            'comments' => 'Commercial sale only.',
            'employee_id' => '1',
            'created_at' => NOW()
        ]);

        DB::table('product_notes')->insert([
            'type_id' => '8',
            'product_id' => '15524',
            'comments' => 'Commercial sale only.',
            'employee_id' => '1',
            'created_at' => NOW()
        ]);

        DB::table('product_notes')->insert([
            'type_id' => '8',
            'product_id' => '15525',
            'comments' => 'Commercial sale only.',
            'employee_id' => '1',
            'created_at' => NOW()
        ]);

        DB::table('product_notes')->insert([
            'type_id' => '8',
            'product_id' => '16319',
            'comments' => 'Commercial sale only.',
            'employee_id' => '1',
            'created_at' => NOW()
        ]);
    }
}