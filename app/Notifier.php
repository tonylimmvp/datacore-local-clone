<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Notifier extends Model
{
    use Traits\MultiplePrimaryKeys;
    protected $connection = "leeselectronic";
    protected $table = "ps_mailalert_customer_oos";
    protected $primaryKey = ['id_customer','customer_email','id_product','id_product_attribute','id_shop'];
    protected $guarded = [];

    public $incrementing = false;
    public $timestamps = false;

    public function product() {
        return $this->hasOne(Product::class,'id_product','id_product');
    }

    public function customer() {
        $customer = DB::connection('leeselectronic')->table('ps_customer')->where('id_customer',$this->id_customer)->first();
        return $customer;
    }
}
