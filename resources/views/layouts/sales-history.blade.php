@if($product->salesHistory)
    @php
        $quantity = array();
        $sales = array();
        $years = array();

        foreach($product->salesHistory as $yearly_sales){
            $year = $yearly_sales->year;
            $month = $yearly_sales->month;
            $qty = $yearly_sales->qty;
            $amount = $yearly_sales->total;
            
            $years[$year] = 1;
            $quantity[$year][$month] = $qty;
            $sales[$year][$month] = $amount;
        }

    @endphp

    {{-- Start of Table --}}
    <div class="table-responsive">
        <table class="table table-bordered table-hover table-dark">
            <thead>
                <th>Year</th>
                @for($month = 1; $month <=12 ; $month++)
                    <th>{{date("M", mktime(0, 0, 0, $month, 1))}} ({{$month}})</th>
                @endfor
                <th>Total</th>
            </thead>
            
            <tbody>
                @foreach($years as $year => $selectedYear)
            
                <tr>
                    <td> {{$year}} </td>
                    
                    @for($month = 1; $month <= 12; $month++)
                        <td>
                            @if(isset($quantity[$year][$month]))
                                {{$quantity[$year][$month] . " ($". $sales[$year][$month] .")"}}
                            @else
                                N/A
                            @endif
                        </td>
                    @endfor
                    
                    @php
                    $yearly_quantity = array_sum($quantity[$year]);
                    $yearly_sales = array_sum($sales[$year]);
                    @endphp
                    
                    <td>Totals: {{$yearly_quantity}} (${{$yearly_sales}})</td>
                
                </tr>
            
                @endforeach
            </tbody>

        </table>
    </div>
    {{-- End of table --}}
			
@endif