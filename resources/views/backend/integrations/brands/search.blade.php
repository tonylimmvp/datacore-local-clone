@extends('backend.layouts.main')

@include('layouts.datatables')

@push('css')
<style>
    button:disabled {
        background-color: grey;
        pointer-events: none;
        cursor: not-allowed;
    }

    #notes_table td {
        margin: 0;
        padding: 0;
    }

    #notes_table th {
        margin: 0;
        padding: 0;
    }

</style>
@endpush

@section('content')

<div class="row">
    <div class="col-12">
        <form method="post" action="{{route('brand.search.detail')}}">
            {{ csrf_field() }}
            <div class="form-group row text-center">
                <input type="text" class="form-control col-7 col-md-6 offset-1 offset-md-2" name="term" @if(isset($term)) value="{{$term}}" @endif required>
                <button type="submit" class="btn btn-primary col-3 col-md-3 col-lg-1" >Search</button>
            </div>
        </form>
    </div>
</div>

{{-- Desktop View --}}
<div class="table_responsive mt-5 d-none d-md-block">
<table id="products-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th class="th-sm">ID</th>
            <th class="th-sm">Image</th>
            <th class="th-sm">Name</th>
            @auth @if(Auth::user()->hasAccess('vendors')->read)<th class="th-sm">Vendor</th>@endif @endauth
            <th class="th-sm">Brand</th>
        </tr>
    </thead>
    <tbody>
        @if(isset($products))
            @foreach ($products as $product)
            <tr>
                <td>{{$product->id_product}}</td>
                <td width="80px">
                    @if($product->online)
                        <img class="lazy" data-src="{{$product->image_url()}}" width=80px>
                    @else
                        <p>This is a new item.</p>
                    @endif
                </td>
                <td>{{$product->name}}
                @auth @if(Auth::user()->hasAccess('vendors')->read)<td>{{$product->vendor}}</td>@endif @endauth
                  
                <td class="text-center" style="max-width:200px"> 
                    @if(Auth::user()->hasAccess('brands')->update)
                    @php 
                        $brands = App\Brand::orderBy('name','asc')->get();
                    @endphp
                    <form action="{{ route('brand.update',$product->id_product) }}" method="POST">
                        {{ csrf_field() }}
                        <small>Current: @if($product->online->brand){{ $product->online->brand->name }} @else --- @endif</small>
                        
                        <select class="form-control form-control-sm" name="brand" required>
                            <option @if(!$product->online->brand) selected @endif disabled>Select Brand...</option>
                        @foreach($brands as $brand){}
                            <option value="{{$brand->id_manufacturer}}" @if($product->online->id_manufacturer == $brand->id_manufacturer) selected @endif>{{$brand->name}}</option>
                        @endforeach
                        </select>

                        <button class="btn btn-primary btn-sm mt-2">Update</button>
                    </form>
                    @else
                    <small>Current: @if($product->online->brand){{ $product->online->brand->name }} @else --- @endif</small>
                    <div class="alert alert-info"><small>Note: You do not have permission to update brands.</small></div>
                    @endif
                </td>
                
            </tr>
            @endforeach
        @endif
    </tbody>
</table>
</div>
{{-- END OF DESKTOP TABLE --}}

{{-- Mobile View --}}
<div class="table_responsive mt-5 d-md-none">
    <table id="products-table" class="table table-striped " cellspacing="0" width="100%">
        @if(isset($products) && sizeof($products)>0)
        @foreach ($products as $product)
        <tr>
            <td><img class="lazy" data-src="{{$product->image_url()}}" width=150px></td>
            <td>
                <b>PID: </b><span>{{ $product->id_product }}</span>
                <hr>
                <span>{{$product->name}}</span>
                @auth
                @if(Auth::user()->hasAccess('vendors')->read)
                    <hr>
                    <small>{{ $product->vendor }}</small>
                @endif
                @endauth
            </td>
        </tr>
        <tr>
            <td><span><b>Price: $</b> {{$product->price}}</span></td>    
            <td><span><b>Quantity: </b> {{$product->quantity}}</span></td>      
        </tr>
        <tr>
            <td><span><b>Shelf: </b> {{$product->shelf_loc}}</span></td>    
            <td><span><b>Storage: </b> {{$product->fraser_loc}}</span></td>      
        </tr>   
        <tr><td></td><td></td></tr>
        
        @endforeach
        @else
            @if(isset($term))
                <p class="text-center">No results for <i>{{$term}}</i></p>
            @endif
        @endif
    
    </table>
</div>
{{-- END OF MOBILE TABLE --}}

@include('layouts.error-flash')

@endsection

@push('bottom-scripts')

<script type="text/javascript">
    $(document).ready(function () {
        $('#products-table').DataTable({
            scrollY:        "60vh",
            scrollX:        true,
            scrollCollapse: true,
            paging:         false,
            columnDefs: [ { orderable: false, targets: [
                1
            ] } ],
            "displayLength": 25,
            "language": {
                "search": "Filter records:"
            }
        });
        $('.dataTables_length').addClass('bs-select');
    });
</script>

@endpush
