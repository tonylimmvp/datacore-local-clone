<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\CategoryProduct;
use App\Product;
use App\ProductOnline;

use DB;

class CategoryController extends Controller
{
    
    public function index(){
        session()->flash('error','Error on ps_product');

        return back();
    }

    public function search() {
        return view('backend.integrations.categories.search');
    }

    public function searchDetail(Request $request) {
        $term = $request->term;
        $products = Product::search("$term")->get();
        return view('backend.integrations.categories.search',compact('products','term'));
    }

    /****************************************************
     * 
     *  API
     *    
    *****************************************************/
    public function autofill(Request $request){
        $category_id = $request->category_id;
        $category = Category::find($category_id);
        if($category){
            $response = array(
                'status' => 'success',
                'id' => $category->id_category,
                'name' => $category->name
                );
        }else{
            $response = array(
                'status' => 'failed',
                'message' => 'Category does not exist',
            );
        }
        
        return response()->json($response);
    }

    public function add(Request $request){
        //validate request

        if(!$request->pid || !$request->category){
            return json_encode(['status'=>'failed','message'=>'Missing Parameter(s).']);
        }

        $category = Category::find($request->category);

        if(!$category){
            return json_encode(['status'=>'failed','message'=>'Category does not exist.']);
        }

        // Add product to category if it doesnt exist
        $category_product = CategoryProduct::firstOrCreate(['id_product' => $request->pid,'id_category' => $request->category]);

        //change default category to newly added category
        // need to change in ps_product_shop
        $update_ps_product_shop = DB::connection('leeselectronic')->table('ps_product_shop')->where('id_product',$request->pid)->where('id_shop',1)->update(['id_category_default'=>$request->category]);

        // if(!$update_ps_product_shop){
        //     return json_encode(['status'=>'failed','message'=>'Error with ps_product_shop.']);
        // }

        // need to change in ps_product
        $update_ps_product = ProductOnline::where('id_product',$request->pid)->update(['id_category_default'=>$request->category]);

        // if(!$update_ps_product){
        //     return json_encode(['status'=>'failed','message'=>'Error with ps_product.']);
        // }

        return json_encode(['status'=>'success','message'=>'You have successfully updated the category of #'. $request->pid.' to '. $request->category .'.']);
        
    }

    public function remove(Request $request) {
        //validate request

        if(!$request->pid || !$request->category){
            return json_encode(['status'=>'failed','message'=>'Missing Parameter(s).']);
        }

        $category = Category::find($request->category);

        if(!$category){
            return json_encode(['status'=>'failed','message'=>'Category does not exist.']);
        }

        // Delete Category Product
        $category_product = CategoryProduct::where('id_product', $request->pid)->where('id_category', $request->category);
        if(!$category_product){
            return json_encode(['status'=>'failed','message'=>'Category Product does not exist.']);
        }

        $deleted_rows = $category_product->delete();

        return json_encode(['status'=>'success','message'=>'You have successfully deleted category ' . $request->category. ' from PID#'. $request->pid . '.']);

    }
}
