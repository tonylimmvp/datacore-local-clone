<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RMA_Status extends Model
{
    protected $table='rma_status';
    protected $primaryKey='id';
    protected $fillable = [
        'name','color','position'
    ];

    public function rmas(){
        return $this->hasMany(RMA::class);
    }
}
