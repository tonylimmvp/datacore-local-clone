<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\EmployeeRole;
use DB;

class Employee extends Authenticatable
{   
    use Notifiable;

    protected $connection = "mysql"; 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'employee_role_id','username'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /********************************************
    *
    *           Eloquent Relations
    *
    ********************************************/

    public function role(){
        return $this->belongsTo(EmployeeRole::class,'employee_role_id'); 
    }

    public function shifts(){
        return $this->belongsToMany(WorkShift::class)->orderBy('day_id','asc');
    }

    public function permissions(){
        return $this->belongsToMany(Permission::class,'employee_permission')->withPivot('create','read','update','delete');
    }

    public function events(){
        return $this->hasMany(Event::class);
    }

    public function sidePanelNote(){
        return $this->hasMany(SidePanelOnlineNote::class, 'employee_id');
    }

    // End of Eloquent Relations 

    /********************************************
    *
    *           Scopes
    *
    ********************************************/

    public function scopeIsActive($query) {
        return $query->where('isActive','1')->where('id','!=','1')->where('employee_role_id','!=','5');
    }

    public function scopeActiveForShift($query){
        $query->where('isActive',1)->whereNotIn('id',[1,4,5,7,20,30])->whereNotIn('employee_role_id',[5]);
    }

    /********************************************
    *
    *           Custom Functions
    *
    ********************************************/

    public function ignoreShift(){
        $ignore = false;
        $admins = array(1,4,5,7,20,30);
        foreach($admins as $admin){
            if($this->id == $admin){
                $ignore = true;
            }
        }
        return $ignore;
    } 

    public function hasRole($role){   
        return $role == $this->role->name;
    }

    public function hasAccess($table){

        $exist = DB::table('permissions')->where('name',$table)->first();

        if(!$exist){
            //TODO need to choose what to do when table does not exist
            echo "$table does not exist";
        }

        $permissions = $this->permissions->where('name',$table)->first()->pivot;

        if(!$permissions){
            echo "no permissions for $table";
        }

        return $permissions;
    }

    public function hasPermission($table,$permission){

        $exist = DB::table('permissions')->where('name',$table)->first();

        if(!$exist){
            //TODO need to choose what to do when table does not exist
            // echo "$table does not exist";
            return 0;
        }

        $result = $this->permissions->where('name',$table)->first()->pivot->$permission;
        return $result;
    }

    // Check if employee tapped in
    public static function hasTapped($employee_id,$date){
        $employee = Employee::with('events')
            ->whereHas('events', 
            function ($query) use($date){ 
                $query->where('type_id', 9)->whereDate("start",$date);
        })->where('id',$employee_id)->get();

        if(sizeof($employee)>0){
            return true;
        }else{
            return false;
        }
        
    }

}
