@extends('backend.layouts.main')

@section('content')
<h1 class="mt-2">Update Employee</h1>
<form method="POST" action="{{ route('coreui.employees.update',$employee->id) }}" enctype="multipart/form-data">
        <input type="hidden" name="_method" value="PATCH">
        {{ csrf_field() }}

    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" id="name" name="name" placeholder="Enter Employee's Full Name"  value="{{$employee->name}}">
    </div>

    <div class="form-group">
        <label for="username">Username</label>
        <input type="text" class="form-control" id="username" name="username" placeholder="Username"  value="{{$employee->username}}" disabled>
    </div>

    <div class="form-group">
        <label for="role">Employee Role</label>
        <select id="role" class="form-control" name="role">
            <option value="" disabled selected>Select Role ...</option>
            @foreach ($roles as $role)
                <option value="{{$role->id}}" @if($role->id == $employee->role->id) selected @endif>{{$role->name}}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label for="username">Card ID</label>
        <input type="text" class="form-control" id="cardid" name="cardid" placeholder="Card UID"  value="{{$employee->card_id}}" disabled>
    </div>

<div class="form-group">
    <label for="email">Email address</label>
    <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" value="{{ $employee->email }}" disabled>
</div>
<div class="form-group">
    <label for="password">Password</label>
    <input type="password" class="form-control" id="password" placeholder="Password" name="password" >
    <small id="passHelp" class="form-text text-muted">*Leave password blank if you do not want to change it.</small>
</div>
<div class="form-group">
    <label for="password_confirmation">Confirm Password</label>
    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Confirm Password">
</div>

{{-- <div class="form-group form-check">
    <input type="checkbox" class="form-check-input" id="exampleCheck1">
    <label class="form-check-label" for="exampleCheck1">Is this user active?</label>
</div>  --}}

<button type="submit" class="btn btn-primary">Update Employee</button>

@include('layouts.errors')

</form>


@endsection