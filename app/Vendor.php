<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $connection = 'mysql';
    protected $guarded = [];

    public $timestamps = false;

    public function products(){
        $database = $this->getConnection()->getDatabaseName();
        return $this->belongsToMany(Product::class, "$database.product_vendors",'vendor_id','product_id')->withPivot('recent_cost','avg_cost','remark');
    }

    public function shipment(){
        return $this->hasMany(Shipment::class);
    }

    public function rma(){
        return $this->hasMany(RMA::class);
    }

    public function saleSuggestion(){
        return $this->hasMany(SaleSuggestion::class);
    }
}
