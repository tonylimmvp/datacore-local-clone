<?php

use App\Http\Controllers\AnalyticsController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

date_default_timezone_set('America/Vancouver');

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::get('restricted', function () {
    return view('restricted');
})->name('restricted');

Route::get('/logout', 'Auth\LoginController@logout');

Route::get('/toc', function(){
    return view('frontend.toc-datacore');
})->name('toc');

Route::get('/development', function(){
    return view('development');
})->name('development');

/////////////////////////////////////////////////////////////////////
// Backend
////////////////////////////////////////////////////////////////////
Route::prefix('/coreui')->name('coreui.')->group(function () {
    // Dashboard
    Route::get('/', 'HomeController@index')->name('dashboard');
    // Stats
    Route::get('statistics',function(){
        return view('backend.stats.index');
    })->name('stats')->middleware('auth')->middleware('permission:statistics.read');
    // Integrations
    Route::get('integrations',function(){
        return view('backend.integrations.index');
    })->name('integrations')->middleware('auth');

    Route::get('integrations/banner_message', 'IntegrationController@bannerIndex')->name('banner-message')->middleware('auth')->middleware('permission:banner.read');

    // Employees
    Route::get('/employees', 'EmployeeController@index')->name('employees')->middleware('permission:employee.read');
    Route::get('/employees/create', 'EmployeeController@create')->name('employees.create')->middleware('permission:employee.create');
    Route::post('/employees', 'EmployeeController@store')->name('employees.store')->middleware('permission:employee.create');
    Route::get('/employees/edit/{employee}', 'EmployeeController@edit')->name('employees.edit')->middleware('permission:employee.update');
    Route::patch('/employees/edit/{employee}', 'EmployeeController@update')->name('employees.update')->middleware('permission:employee.update');
    Route::patch('/employees/activate/{employee}', 'EmployeeController@activate')->name('employees.activate')->middleware('permission:employee.update');
    Route::patch('/employees/deactivate/{employee}', 'EmployeeController@deactivate')->name('employees.deactivate')->middleware('permission:employee.update');
    Route::get('/employees/delete/{employee}', 'EmployeeController@destroy')->name('employees.delete')->middleware('permission:employee.delete');
    Route::get('/employees/{employee}/shift','EmployeeController@shifts')->name('employees.shifts')->middleware('permission:employee.update');
    
    // Profile
    Route::get('/profile', 'EmployeeController@profile')->name('profile')->middleware('auth');
    Route::patch('/profile/{employee}', 'EmployeeController@profile_update')->name('profile.update')->middleware('auth');
    
    //Employee Permissions
    Route::get('/employees/{employee}/permissions', 'EmployeePermissionController@edit')->name('employee-permission.edit')->middleware('permission:employee.update');
    Route::patch('/employees/{employee}/permissions', 'EmployeePermissionController@update')->name('employee-permission.update')->middleware('permission:employee.update');

    // Employee Roles
    Route::get('/roles', 'EmployeeRoleController@index')->name('employee-role')->middleware('permission:employee_role.read');
    Route::post('/roles', 'EmployeeRoleController@store')->name('employee-role.store')->middleware('permission:employee_role.create');
    Route::post('/roles/{employee_role}', 'EmployeeRoleController@update')->name('employee-role.update')->middleware('permission:employee_role.update');

    // Employee Role Permissions
    Route::get('/roles/{employee_role}/permissions', 'EmployeeRolePermissionController@edit')->name('employee-role-permission.edit')->middleware('permission:employee_role.update');
    Route::patch('/roles/{employee_role}/permissions', 'EmployeeRolePermissionController@update')->name('employee-role-permission.update')->middleware('permission:employee_role.update');

    // Permissions
    Route::get('/permissions', 'PermissionController@index')->name('permission')->middleware('permission:permission.read')->middleware('role:SuperAdmin');
    Route::post('/permissions', 'PermissionController@store')->name('permission.store')->middleware('permission:permission.create')->middleware('role:SuperAdmin');

    // Sales History

    Route::get('/sales_history/index', 'SalesHistoryController@index')
    ->name('sales-history')->middleware('role:SuperAdmin')->middleware('permission:sales_history.read');

    Route::get('/sales_history/upload', 'SalesHistoryController@upload')
    ->name('sales-history.upload')->middleware('permission:sales_history.create');

    Route::post('/sales_history', 'SalesHistoryController@store')
    ->name('sales-history.store')->middleware('permission:sales_history.create');

    Route::get('/sales_history/{product}', 'SalesHistoryController@show')
    ->name('sales-history.show')->middleware('role:SuperAdmin')->middleware('permission:sales_history.read');

    // Vendors

    Route::get('/vendors/index', 'VendorController@index')
    ->name('vendors')->middleware('permission:vendors.read');

    Route::get('/vendors/{vendor}/products', 'VendorController@products')
    ->name('vendors.products')->middleware('permission:vendors.read');

    Route::get('/vendors/upload', 'VendorController@upload')
    ->name('vendors.upload')->middleware('role:SuperAdmin')->middleware('permission:vendors.create');

    Route::post('/vendors', 'VendorController@store')
    ->name('vendors.store')->middleware('role:SuperAdmin')->middleware('permission:vendors.create');

    // Purchase Orders

    Route::get('/purchases/index', 'PurchaseOrderController@index')
    ->name('po')->middleware('permission:purchase_orders.read');

    Route::get('/purchases/upload', 'PurchaseOrderController@upload')
    ->name('po.upload')->middleware('role:SuperAdmin')->middleware('permission:purchase_orders.update');

    Route::post('/purchases', 'PurchaseOrderController@store')
    ->name('po.store')->middleware('role:SuperAdmin')->middleware('permission:purchase_orders.update');

    Route::get('/purchases/{po_id}', 'PurchaseOrderController@products')
    ->name('po.products')->middleware('permission:purchase_orders.read');

    Route::get('/purchases/product/upload', 'PurchaseOrderProductController@upload')
    ->name('po-product.upload')->middleware('role:SuperAdmin')->middleware('permission:purchase_orders.update');

    Route::post('/purchase_products', 'PurchaseOrderProductController@store')
    ->name('po-product.store')->middleware('role:SuperAdmin')->middleware('permission:purchase_orders.update');

    // Payroll
    Route::get('/payroll-report', 'PayrollController@index')->name('payroll')->middleware('auth');

    // Work Shifts
    Route::get('/shifts','WorkShiftController@index')->name('work-shifts')->middleware('permission:work_shifts.read');

    // Employee Shift
    Route::post('employee/shift/add','WorkShiftController@addEmployeeShift')->name('employee-shift.create')->middleware('permission:work_shifts.create');
    Route::post('employee/shift/delete','WorkShiftController@removeEmployeeShift')->name('employee-shift.delete')->middleware('permission:work_shifts.delete');

    // Settings
    Route::get('settings',function(){
        return view('backend.settings.index');
    })->name('settings')->middleware('auth');

    // Order Statuses
    Route::get('/order_statuses','OrderStatusController@index')->name('order-status')->middleware('role:SuperAdmin,Supervisor')->middleware('permission:order_status.read');
    Route::get('/order_statuses/create','OrderStatusController@create')->name('order-status.create')->middleware('role:SuperAdmin,Supervisor')->middleware('permission:order_status.create');
    Route::post('/order_statuses','OrderStatusController@store')->name('order-status.store')->middleware('role:SuperAdmin,Supervisor')->middleware('permission:order_status.create');
    Route::patch('/order_statuses/edit/{orderStatus}','OrderStatusController@update')->name('order-status.update')->middleware('role:SuperAdmin,Supervisor')->middleware('permission:order_status.update');
    Route::get('/order_statuses/delete/{orderStatus}','OrderStatusController@destroy')->name('order-status.delete')->middleware('role:SuperAdmin,Supervisor')->middleware('permission:order_status.delete');

    // Events/Schedule
    Route::get('/schedule-vue','EventController@vue')->name('schedule-vue');
    Route::get('/schedule','EventController@index')->name('schedule');
    Route::get('/schedule-admin','EventController@admin')->name('schedule-admin')->middleware('permission:events.read');
    Route::get('/schedule-employee/{employee}','EventController@employee')->name('schedule-employee')->middleware('permission:events.read');
    Route::post('/schedule/create','EventController@create')->name('event.create');
    Route::patch('/schedule/update','EventController@update')->name('event.update');
    Route::post('/schedule/delete','EventController@destroy')->name('event.delete');


});

////////////////////////////////////////////////////////////////////////////
// Frontend
///////////////////////////////////////////////////////////////////////////

/**************************************************************
 * 
 *   Search
 * 
 ***************************************************************/ 

Route::get('/search', 'ProductController@search')->name('search');

Route::get('/search/result', 'ProductController@searchResults')->name('search.detail');

Route::post('/orders/search', 'OrderController@search')->name('order.search');

/**************************************************************
 * 
 *   Product Notes
 * 
 ***************************************************************/ 
Route::get('/product/notes/show/{product}', 'ProductNoteController@show')
->name('notes.show')->middleware('permission:product_note.read');

Route::post('/product/notes/handle/{note}', 'ProductController@handleNote')
->name('notes.handle')->middleware('permission:product_note.update');

Route::post('/product/notes/save/', 'ProductNoteController@store')
->name('notes.store')->middleware('permission:product_note.create');

Route::post('/product/notes/missing/convert', 'ProductNoteController@convertMissingNotesFromFastsearch')
->name('notes.missing-convert')->middleware('role:SuperAdmin');

Route::post('/product/notes/relation/convert', 'ProductNoteController@convertRelationNotes')
->name('notes.relation-convert')->middleware('role:SuperAdmin');

Route::post('/product/notes/obsolete/convert', 'ProductNoteController@convertObsoleteNotes')
->name('notes.obsolete-convert')->middleware('role:SuperAdmin');

Route::post('/product/notes/restrict-sale/{product}', 'ProductNoteController@restrictSale')
->name('notes.restrictSale')->middleware('role:SuperAdmin');

Route::post('/product/notes/delete/{note}', 'ProductNoteController@deleteNote')
->name('notes.delete')->middleware('permission:product_note.delete');

/**************************************************************
 * 
 *   Profile
 * 
 ***************************************************************/ 
Route::get('/product/profile/{product}', 'ProductController@profile')
->name('product-profile.show')->middleware('auth');

Route::post('/product/profile/create-relationship/{product}', 'ProductController@createRelationship')
->name('product-relationship.create')->middleware('auth')->middleware('permission:product.create');

Route::post('product/profile/delete-relationship/{relation}', 'ProductController@deleteRelationship')
->name('product-relationship.delete')->middleware('permission:product.delete');

Route::patch('/product/profile/edit/{relatedProduct}', 'ProductController@editRelationship')
->name('product-relationship.edit')->middleware('permission:product.update');

/**************************************************************
 * 
 *   Quotations
 * 
 ***************************************************************/ 

Route::get('/quotations', 'QuotationController@index')
->name('quotations')->middleware('permission:quotation.read');

Route::get('/quotations/create', 'QuotationController@create')
->name('quotation.create')->middleware('permission:quotation.create');

Route::post('/quotations', 'QuotationController@store')
->name('quotation.store')->middleware('permission:quotation.create');

Route::patch('/api/quotations/expired', 'QuotationController@expire')
->name('quotation.expire');

Route::patch('/api/quotations/update/comment/{quotation}', 'QuotationController@updateComment')
->name('quotation.update.comment');

Route::patch('/api/quotations/update/lego/{quotation}', 'QuotationController@updateLego')
->name('quotation.update.lego');

// Quotation Product
Route::post('/api/quotation_product/add/{quotation}', 'QuotationProductController@add')
->name('quotation-product.add');

Route::patch('/api/quotation_product/update/{quotation_product}', 'QuotationProductController@update')
->name('quotation-product.update');

Route::post('/api/quotation_product/delete/{quotation_product}', 'QuotationProductController@destroy')
->name('quotation-product.delete');

/**************************************************************
 * 
 *   RMA
 * 
 ***************************************************************/ 
Route::get('/rma', 'RMAController@index')
->name('rma')->middleware('auth')->middleware('permission:rma.read');

Route::get('/rma/show/{rma}', 'RMAController@show')
->name('rma.show')->middleware('auth')->middleware('permission:rma.read');

Route::get('/rma/create', 'RMAController@create')->name('rma.create')->middleware('permission:rma.create');

Route::post('/rma', 'RMAController@store')->name('rma.store')->middleware('permission:rma.create');

// RMA Status History
Route::post('/rma_status_history', 'RMAStatusHistoryController@store')->name('rma-history.store')->middleware('auth')->middleware('permission:rma.update');

//Deactivating RMA
Route::patch('/rma/deactivate/{rma}', 'RMAController@deactivate')->name('rma.deactivate')->middleware('permission:rma.update');

Route::get('/rma/print/{rma}', 'RMAController@printLabel')->name('rma.printLabel');

/**************************************************************
 * 
 *   Order Suggestion
 * 
 ***************************************************************/ 
Route::get('/order-suggestion', 'OrderSuggestionController@index')
->name('order-suggestion')->middleware('auth')->middleware('permission:order_suggestion.read');

Route::post('/order-suggestion/insert/{product}', 'OrderSuggestionController@create')
->name('order-suggestion.create');

Route::post('/order-suggestion/purchase/{product}', 'OrderSuggestionController@purchase')
->name('order-suggestion.purchase')->middleware('permission:shipment.update');

Route::post('/order-suggestion/obsolete/{product}', 'OrderSuggestionController@obsolete')
->name('order-suggestion.obsolete')->middleware('role:SuperAdmin, Supervisor');

Route::post('/order-suggestion/purchase-and-ship/{product}', 'OrderSuggestionController@purchaseAndShip')
->name('order-suggestion.purchaseAndShip')->middleware('permission:shipment.update');

//Maintenance Routes
Route::get('/order-suggestion/maintenance2', 'OrderSuggestionController@removeWhenGreaterThanThreeMonths')
->name('order-suggestion.removeThreeMonthsOlder')->middleware('auth')->middleware('role:SuperAdmin');

Route::get('/order-suggestion/maintenance1', 'OrderSuggestionController@removeByQty')
->name('order-suggestion.removeByQty')->middleware('auth')->middleware('role:SuperAdmin');

//API for AJAX calls
Route::get('/api/order-suggestion/add', 'OrderSuggestionController@addToOrderSuggestion')->name('order-suggestion.add');
Route::get('/api/order-suggestion/ignore', 'OrderSuggestionController@ignoreSuggestionAJAX')->name('order-suggestion.ignore')->middleware('role:SuperAdmin');

//This is a one time consolidation integration run. Please see the function in the controller for details.
Route::get('/api/order-suggestion/consolidate', 'OrderSuggestionController@consolidateNotifierToOrderSuggestion')->middleware('role:SuperAdmin');

/**************************************************************
 * 
 *   Sale Suggestion
 * 
 ***************************************************************/ 
Route::get('/sale-suggestion', 'SaleSuggestionController@index')
->name('sale-suggestion')->middleware('auth')->middleware('role:SuperAdmin, Supervisor')->middleware('permission:statistics.read');

Route::Post('/sale-suggestion/generate', 'SaleSuggestionController@generate')
->name('sale-suggestion.generate')->middleware('auth')->middleware('role:SuperAdmin, Supervisor')->middleware('permission:statistics.create');

Route::Post('/sale-suggestion/deleteAll', 'SaleSuggestionController@deleteAll')
->name('sale-suggestion.deleteAll')->middleware('auth')->middleware('role:SuperAdmin, Supervisor')->middleware('permission:statistics.delete');

Route::get('/sale-suggestion/view/{suggestion}', 'SaleSuggestionController@display')
->name('sale-suggestion.view')->middleware('auth')->middleware('role:SuperAdmin, Supervisor')->middleware('permission:statistics.view');

Route::post('/sale-suggestion/view/calculateBreaks/{suggestion}', 'SaleSuggestionController@calculatePriceBreaks')
->name('sale-suggestion.calculateBreaks')->middleware('auth')->middleware('role:SuperAdmin');

Route::post('/sale-suggestion/view/storeQty/{suggestion}', 'SaleSuggestionController@storeQty')
->name('sale-suggestion.storeQty')->middleware('auth')->middleware('role:SuperAdmin');

/**************************************************************
 * 
 *   Shipment Cart
 * 
 ***************************************************************/ 

Route::post('/shipment-cart/insert/{note}', 'ShipmentCartController@insertToShipment')
->name('shipment-cart.insertToShipment')->middleware('role:SuperAdmin');

Route::patch('/shipment-cart/edit/{cart}', 'ShipmentCartController@editCart')
->name('shipment-cart.edit')->middleware('role:SuperAdmin');

Route::post('shipment-cart/delete/{cart}', 'ShipmentCartController@delete')
->name('shipment-cart.delete')->middleware('role:SuperAdmin');

Route::post('shipment-cart/create/', 'ShipmentCartController@create')
->name('shipment-cart.create')->middleware('role:SuperAdmin');

Route::post('shipment-cart/import/', 'ShipmentCartController@importPO')
->name('shipment-cart.importPO')->middleware('role:SuperAdmin');

Route::post('shipment-cart/clear/{shipment}', 'ShipmentCartController@clear')
->name('shipment-cart.clear')->middleware('role:SuperAdmin');

Route::Post('/sale-suggestion/deleteAll', 'SaleSuggestionController@deleteAll')
->name('sale-suggestion.deleteAll')->middleware('auth')->middleware('role:SuperAdmin');

/**************************************************************
 * 
 *   Ordered Products
 * 
 ***************************************************************/ 
Route::get('/ordered-products', function(){
    return view('frontend.ordered-products');
})->name('ordered-products')->middleware('permission:shipment.read');

Route::post('/ordered-products/delete/{note}', 'ShipmentController@delete')
->name('ordered-products.delete')->middleware('role:SuperAdmin');

Route::patch('/ordered-products/editOrder/{note}', 'OrderSuggestionController@editOrder')
->name('ordered-products.editOrder')->middleware('role:SuperAdmin');

Route::post('/ordered-products/insertToOP', 'OrderSuggestionController@insertToOrderedProducts')
->name('ordered-products.insertToOP')->middleware('role:SuperAdmin');

Route::post('/ordered-products/quickShip/{note}', 'ShipmentController@quickShip')
->name('ordered-products.quickShip')->middleware('role:SuperAdmin');

/**************************************************************
 * 
 *   Shipment List
 * 
 ***************************************************************/ 
Route::get('/shipment', 'ShipmentController@show')->name('shipment.show')->middleware('permission:shipment.read');

Route::get('/shipment/create', 'ShipmentController@create')->name('shipment.create')->middleware('role:SuperAdmin,Supervisor')->middleware('permission:shipment.create');
Route::post('/shipment', 'ShipmentController@store')->name('shipment.store')->middleware('role:SuperAdmin')->middleware('permission:shipment.create');

Route::get('/shipment/view/{shipment}', 'ShipmentController@view')->name('shipment.view')->middleware('permission:shipment.read');

// Store Shipment History
Route::post('/shipment_status_history', 'ShipmentController@storeHistory')->name('shipment-history.store')->middleware('role:SuperAdmin, Supervisor');

//Save shipment information
Route::patch('/shipment/{shipment}', 'ShipmentController@saveInformation')->name('shipment-detail.save')->middleware('role:SuperAdmin, Supervisor')->middleware('permission:shipment.update');

/**************************************************************
 * 
 *   Order Payment
 * 
 ***************************************************************/ 

Route::get('/payments', 'BamboraPaymentController@index')->name('payments');

Route::get('/checkout', 'BamboraPaymentController@payment')->name('checkout');

Route::post('/pay', 'BamboraPaymentController@pay')->name('pay');

Route::get('/payments/braintree', 'BraintreePaymentController@index')->name('payments.braintree');


/**************************************************************
 * 
 *   Orders
 * 
 ***************************************************************/ 
Route::get('/special_orders', 'OrderController@specialorders')->name('orders.special');
Route::get('/backorders', 'OrderController@backorders')->name('orders.backorder');
Route::get('/phone_orders', 'OrderController@phoneOrders')->name('orders.phoneorder');

Route::get('/orders/search', 'OrderController@index')->name('orders');
Route::get('/orders/{order}', 'OrderController@show')->name('orders.detail');
Route::get('/order/create/', 'OrderController@create')->name('orders.create');
Route::post('/orders', 'OrderController@store')->name('orders.store');
Route::get('/orders/edit/{order}', 'OrderController@edit')->name('orders.edit');
Route::patch('/orders/edit/{order}', 'OrderController@update')->name('orders.update');
Route::get('/orders/delete/{order}', 'OrderController@destroy')->name('orders.delete')->middleware('role:SuperAdmin');

Route::patch('/orders/{order}/update_invoice','OrderController@updateInvoice')->name('orders.update-invoice');
Route::patch('/orders/{order}/update_tracking','OrderController@updateTracking')->name('orders.update-tracking');
Route::patch('/orders/{order}/update_comment','OrderController@updateComment')->name('orders.update-comment');
Route::patch('/orders/update_payment','OrderController@updatePayment')->name('orders.update-payment');

//Order Status History
Route::post('/order_status_history', 'OrderStatusHistoryController@store')->name('order-history.store')->middleware('auth');

//Mobile Order Status History
Route::post('/mobile_order_status_history', 'MobileOrderStatusHistoryController@store')->name('mobile-order-history.store')->middleware('auth');

// Create order from quote
Route::post('/orders/convert_quote/{quotation}', 'OrderController@convertQuote')->name('orders.convert-quote')->middleware('permission:order.create');

// Order Product
Route::patch('/api/order_product/update/{order_product}', 'OrderProductController@updateVendor')
->name('order-product.update-vendor');

Route::post('/api/order_product/purchase/{order_product}', 'OrderProductController@purchase')
->name('order-product.purchase');

Route::post('/api/order_product/purchaseAndShip/{order_product}', 'OrderProductController@purchaseAndShip')
->name('order-product.purchaseAndShip');

/**************************************************************
 * 
 *   Online Orders
 * 
 ***************************************************************/ 

Route::get('/orders/online/{order}', 'OnlineOrderController@detail')->name('orders.online');

/**************************************************************
 * 
 *   Mobile Orders
 * 
 ***************************************************************/ 

Route::get('/orders/mobile/{order}', 'MobileOrderController@detail')->name('orders.mobile');
Route::post('/orders/mobile/updateTracking', 'MobileOrderStatusHistoryController@updateTracking')->name('mobile-order.updateTracking');

/**************************************************************
 * 
 *   Restock Page
 * 
 ***************************************************************/ 

Route::get('/restock', 'RestockProductController@index')->name('restock');
Route::post('/restock', 'RestockProductController@store')->name('restock.store');
Route::get('/restock/delete/{product}', 'RestockProductController@destroy')->name('restock.delete');

/**************************************************************
 * 
 *   Category
 * 
 ***************************************************************/ 
Route::get('/category/index', 'CategoryController@index')->name('categories');

Route::get('/category/search', 'CategoryController@search')->name('category.search');

Route::post('/category/search', 'CategoryController@searchDetail')->name('category.search.detail');

/**************************************************************
 * 
 *   Brands
 * 
 ***************************************************************/ 

Route::post('/brands/update/{product}', 'BrandController@update')->name('brand.update');

Route::get('/brands/search', 'BrandController@search')->name('brand.search');

Route::post('/brands/search', 'BrandController@searchDetail')->name('brand.search.detail');


/**************************************************************
 * 
 *   Issues Page
 * 
 ***************************************************************/ 

Route::get('/issues/store', function () {
    return view('frontend.issues.store');
})->name('issues.store')->middleware('auth');

Route::get('/issues/website', function () {
    return view('frontend.issues.website');
})->name('issues.website')->middleware('auth');

/**************************************************************
 * 
 *   Notifier
 * 
 ***************************************************************/ 

Route::get('/product/notifier', function(){
    return view('frontend.products.notifier');
})->name('product.notifier')->middleware('auth')->middleware('permission:notifier.read');

/**************************************************************
 * 
 *   Purchase and Sale Analytics
 * 
 ***************************************************************/ 

Route::get('/statistics/purchase-analytics', 'AnalyticsController@purchaseAnalytics')->name('purchase-analytics.index')->middleware('auth')->middleware('role:SuperAdmin');

Route::get('/statistics/sale-analytics', 'AnalyticsController@salesAnalytics')->name('sale-analytics.index')->middleware('auth')->middleware('role:SuperAdmin');

/**************************************************************
 * 
 *   Product Analytics
 * 
 ***************************************************************/ 

Route::get('/statistics/product-analytics', 'AnalyticsController@productAnalytics')->name('product-analytics.index')->middleware('auth')->middleware('role:SuperAdmin'); 

Route::get('/statistics/product-analytics/sales/', 'AnalyticsController@productSales')->name('product-analytics.sales')->middleware('auth')->middleware('role:SuperAdmin'); 

/**************************************************************
 * 
 *   Resistor 
 * 
 ***************************************************************/ 

Route::get('/resistor', 'ResistorController@index')->name('resistor');
Route::get('/api/resistor/update', 'ResistorController@update')->name('resistor.update'); 


 /**************************************************************
 * 
 *   Capacitor
 * 
 ***************************************************************/ 

Route::get('/capacitor', 'CapacitorController@index')->name('capacitor'); 
Route::get('/api/capacitor/update', 'CapacitorController@update')->name('capacitor.update'); 

 /**************************************************************
 * 
 *   Store Supply
 * 
 ***************************************************************/

Route::get('/store-supplies', 'StoreSupplyController@show')->name('store-supplies');

Route::post('/store-supplies/purchaseSupply', 'StoreSupplyController@store')
->name('store-supplies.purchaseSupply')->middleware('role:SuperAdmin');

Route::post('/store-supplies/delete/{supply}', 'StoreSupplyController@deleteSupply')
->name('store-supplies.deleteSupply')->middleware('role:SuperAdmin');

Route::patch('/store-supplies/editSupply/{supply}', 'StoreSupplyController@editSupply')
->name('store-supplies.editSupply')->middleware('role:SuperAdmin');

 /**************************************************************
 * 
 *   Store Supply
 * 
 ***************************************************************/

Route::get('/store-ips', 'StoreIPController@index')->name('store-ips');
Route::post('/store-ips/store', "StoreIPController@store")->name('store-ips.store');
Route::post('/store-ips/delete/{ip}', "StoreIPController@remove")->name('store-ips.delete');

 /**************************************************************
 * 
 *   Side Panel
 * 
 ***************************************************************/

Route::post('/side-panel/online/notes/delete/{sidePanelNote}', 'SidePanelController@deleteNote')
->name('sidepanel_online_notes.delete')->middleware('permission:product_note.delete');

Route::post('/side-panel/online/notes/create/', 'SidePanelController@createNote')
->name('sidepanel_online_notes.create');

Route::patch('/side-panel/online/notes/edit/{sidePanelNote}', 'SidePanelController@editNote')
->name('sidepanel_online_notes.edit');

/**************************************************************
 * 
 *   Phone Book
 * 
 ***************************************************************/ 
Route::get('/phonebook', "CompaniesController@index")->name('phonebook');
Route::get('/phonebook/create', "CompaniesController@create")->name('phonebook-create')->middleware("permission:companies.create");
Route::post('/phonebook/store', "CompaniesController@store")->name('phonebook-add')->middleware("permission:companies.create");
Route::get('/phonebook{company}/edit', "CompaniesController@edit")->name('phonebook-edit')->middleware("permission:companies.update");
Route::patch('/phonebook/{company}/patch', "CompaniesController@update")->name('phonebook-patch')->middleware("permission:companies.update");
Route::delete('/phonebook/{company}/delete', "CompaniesController@destroy")->name("phonebook-delete")->middleware("permission:companies.delete");

Route::post('/phonebook/filter', "ServiceTypesController@filter")->name('phonebook-filter');
Route::get('/phonebook/create-filter', "ServiceTypesController@create")->name('phonebook-create-filter')->middleware("permission:companies.create");
Route::post('/phonebook/store-filter', "ServiceTypesController@store")->name('phonebook-store-filter')->middleware("permission:companies.create");
Route::get('/phonebook/manage-filter', "ServiceTypesController@manage")->name('phonebook-manage-filter')->middleware("permission:companies.update");
Route::patch('/phonebook/patch-filters', "ServiceTypesController@update")->name('phonebook-manage-filter-patch')->middleware("permission:companies.update");

/**************************************************************
 * 
 *   Notes
 * 
 ***************************************************************/ 
Route::get('/notes/online/view/{onlineOrder}', 'NoteController@viewOnline')->name('online-notes.view');
Route::get('/notes/order/view/{storeOrder}', 'NoteController@viewOrder')->name('order-notes.view');
Route::get('/notes/mobile/view/{mobileOrder}', 'NoteController@viewMobile')->name('mobile-notes.view');

Route::post('/notes/order/save/{storeOrder}', 'NoteController@storeOrderNote')->name('order-notes.store');
Route::post('/notes/online/save/{onlineOrder}', 'NoteController@storeOnlineOrderNote')->name('onlineOrder-notes.store');
Route::post('/notes/mobile/save/{mobileOrder}', 'NoteController@storeMobileOrderNote')->name('mobileOrder-notes.store');

Route::post('/notes/order/delete/{note}', 'NoteController@deleteOrderNote')
->name('order_notes.delete')->middleware('role:SuperAdmin');
Route::post('/notes/online/delete/{note}', 'NoteController@deleteOnlineNote')
->name('online_notes.delete')->middleware('role:SuperAdmin');
Route::post('/notes/mobile/delete/{note}', 'NoteController@deleteMobileNote')
->name('mobile_notes.delete')->middleware('role:SuperAdmin');

/**************************************************************
 * 
 *   API Calls
 * 
 ***************************************************************/ 

Route::get('/api/product_autofill', 'ProductController@autofill')->name('product.autofill');
Route::post('/api/category_autofill', 'CategoryController@autofill')->name('category.autofill');

Route::get('/api/printers/index', 'PrinterController@printLabel')->name('print.index');
Route::get('/api/printers/quantity', 'PrinterController@printQuantityLabel')->name('print.quantity');
Route::get('/api/printers/noprice', 'PrinterController@printNoPriceLabel')->name('print.noprice');
Route::get('/api/printers/order-qr', 'PrinterController@printOrderQRCode')->name('print.orderQR');
Route::get('/api/printers/online-qr', 'PrinterController@printOnlineOrderQRCode')->name('print.onlineQR');
Route::get('/api/printers/mobile-qr', 'PrinterController@printMobileOrderQRCode')->name('print.mobileQR');
Route::get('/api/printers/postek/{product}', 'PrinterController@printPostek')->name('print.postek');
Route::get('/api/printers/postek-quantity/{product}', 'PrinterController@printPostekQuantity')->name('print.postekQuantity');
Route::get('/api/printers/postek-noprice/{product}', 'PrinterController@printPostekNoPrice')->name('print.postekNoPrice');
Route::get('/api/printers/postek-order-qr/{order}', 'PrinterController@printPostekOrderQRCode')->name('print.postekOrderQR');
Route::get('/api/printers/postek-online-qr/{order}', 'PrinterController@printPostekOnlineOrderQRCode')->name('print.postekOnlineQR');
Route::get('/api/printers/postek-mobile-qr/{order}', 'PrinterController@printPostekMobileOrderQRCode')->name('print.postekMobileQR');

Route::get('/api/export_new_products', 'ProductController@exportNewProducts')->name('product.export-new');

Route::post('/api/banner_message', 'IntegrationController@bannerMessage')->name('banner-message.update')->middleware('permission:banner.update');

Route::get('/api/online_order/fetch', 'OnlineOrderController@fetchOrder')->name('online-order.fetch');


Route::post('/api/online_product/updateBackorderStatus', 'OnlineProductController@updateBackorderStatus')->name('online-product.updateBO');
Route::post('/api/online_product/updateOnlineOnly', 'OnlineProductController@updateOnlineOnly')->name('online-product.updateOnlineOnly');
Route::get('/api/online_product/syncShortDescription', 'OnlineProductController@syncShortDescription')->name('online-product.syncShortDescription');
Route::get('/api/online_product/syncDescription', 'OnlineProductController@syncDescription')->name('online-product.syncDescription');

Route::get('/api/order/fetch', 'OrderController@fetchOrder')->name('order.fetch');

Route::get('/api/mobile_order/fetch', 'MobileOrderController@fetchOrder')->name('mobile-order.fetch');

Route::get('/api/restock/add', 'RestockProductController@addToBoard')->name('restock.add');
Route::get('/api/restock/remove', 'RestockProductController@removeFromBoard')->name('restock.remove');

Route::get('/api/notifier/fetch', 'NotifierController@fetchInterested')->name('notifier.fetch');
Route::get('/api/notifier/send', 'NotifierController@sendNotifier')->name('notifier.send');
Route::post('/api/notifier/add', 'NotifierController@addNotifier')->name('notifier.add');

Route::get('/api/vendors/update', 'ProductVendorController@updateVendor')->name('vendors.update');
Route::get('/api/vendors/updateRecentCost', 'ProductVendorController@updateRecentCost')->name('vendors.update-recent-cost');
Route::get('/api/vendors/updateAverageCost', 'ProductVendorController@updateAverageCost')->name('vendors.update-avg-cost');
Route::post('/api/vendors/updateRemark', 'ProductVendorController@updateRemark')->name('vendors.update-remark');
Route::post('/api/vendors/updateURL', 'VendorController@updateURL')->name('vendors.update-url');

Route::post('/api/category/add', 'CategoryController@add')->name('categories.add');
Route::post('/api/category/delete', 'CategoryController@remove')->name('categories.delete');

Route::post('/api/vendors/product/obsolete/{product}', 'ProductController@obsoleteAjax')
->name('product.obsolete')->middleware('permission:product.update');

Route::get('/api/analytics/', 'AnalyticsController@getMonthlyRevenue')
->name('analytics.monthRev')->middleware('role:SuperAdmin')->middleware('permission:statistics.read');

Route::post('/api/product/updateWeight/{product}', 'ProductController@updateWeight')->name('product.updateWeight');
Route::post('/api/product/updateUnity', 'OnlineProductController@updateUnity')->name('product.updateUnity');

Route::post('/api/saveOnlineSignature', 'SignatureController@saveOnlineSignature')->name('signature.saveOnline');
Route::post('/api/saveOrderSignature', 'SignatureController@saveOrderSignature')->name('signature.saveOrder');
Route::post('/api/saveMobileSignature', 'SignatureController@saveMobileSignature')->name('signature.saveMobile');

Route::get('/api/price-changes/update', 'ProductController@updatePriceChanges')->name('price-changes-update');