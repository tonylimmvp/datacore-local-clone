<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OrderStatusHistory;
use App\Order;
use App\Mail;
use App\Employee;

class OrderStatusHistoryController extends Controller
{
    
    public function store(Request $request)
    {
        $this->validate(request(), [
            'order' => 'required',
            'employee' => 'required',
            'status' => 'required'
        ]);

        // create order status history
    
        $status_history = OrderStatusHistory::create([
            'order_id' => request('order'),
            'employee_id' => request('employee'),
            'status_id' => request('status'),
            'comment' => request('comment')            
        ]);
    
        $status_history->save();

        // Once status history created, we want to change order status as well

        $order = Order::find($request->order);
        $order->status_id = $request->status; 

        $order->save();

        // check who opt in for emails
        $orderMailList = [];
            
        foreach(Employee::where('id','!=',$order->sales->id)->get() as $employee){
            if($employee->hasAccess('order_email')->read){
                array_push($orderMailList,$employee->email);
            }
        }

        if(config('mail.host','smtp.mailtrap.io') != 'smtp.mailtrap.io'){
            \Mail::to($order->sales->email)
                    ->bcc($orderMailList)
                    ->send(new \App\Mail\OrderStatusUpdate($order));
        }else{
            \Mail::to($order->sales->email)
            ->send(new \App\Mail\OrderStatusUpdate($order));
        }

        // Customer Emails
        // On Hold Email
        if($order->status_id == 5 && $order->email){
            \Mail::to($order->email)->send(new \App\Mail\OrderOnHoldCustomer($order));
        // Arrived Email
        }elseif($order->status_id == 6 && $order->email){
            \Mail::to($order->email)->send(new \App\Mail\OrderArrivedCustomer($order));
        }
            
        session()->flash('message','You have successfully updated your orders status.');
        return back();
    }

    public function destroy($id)
    {
        //
    }
}
