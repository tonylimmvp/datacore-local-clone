<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_vendors', function (Blueprint $table) {
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('vendor_id');
            $table->decimal('avg_cost',8,2)->nullable();
            $table->decimal('recent_cost',8,2)->nullable();
            $table->string('remark')->nullable();

            $table->primary(['product_id','vendor_id']);
            $table->foreign('vendor_id')->references('id')->on('vendors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_vendors');
    }
}
