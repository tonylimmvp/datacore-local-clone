@extends('frontend.layouts.main')

@section('content')
<div class="container">
    <h1> Backorders</h1>

    @include('frontend.layouts.orders')
</div>
@endsection