<?php

namespace App\Http\Controllers;

use App\Permission;
use App\Employee;
use App\EmployeeRole;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    
    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
        $permissions = Permission::all();
        return view('backend.permissions.index',compact('permissions'));
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'name' => 'required|unique:permissions',
        ]);

        $permission = new Permission();
        $permission->name = $request->name;
        $permission->save();

        // Add permission to all employees

        $employees = Employee::all();
        foreach ($employees as $employee){
            $permission->employees()->attach($employee->id);
        }

        // Add permissions to all employee roles
        $employee_roles = EmployeeRole::all();
        foreach ($employee_roles as $employee_role){
            $permission->roles()->attach($employee_role->id);
        }

        session()->flash('message', $permission->name . " has been successfully added.");

        return redirect()->route('coreui.permission');
    }


    public function update(Request $request, Permission $permission)
    {
        //
    }

    public function destroy(Permission $permission)
    {
        //
    }
}
