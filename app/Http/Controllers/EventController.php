<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Calendar;
use App\Event;
use App\EventType;
use App\Employee;
use \Auth;
use \Carbon\Carbon;

class EventController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $holidays = Event::where('type_id',3)->get();
        $events = Event::whereNotIn('type_id',[3])->where('employee_id',Auth::user()->id)->get();
        $events = $events->merge(Event::where('type_id',7)->get());
        return view('backend.calendar.index', compact('holidays','events'));
    }

    public function admin()
    {
        $holidays = Event::where('type_id',3)->get();
        $events = Event::whereNotIn('type_id',[2,3])->get();
        return view('backend.calendar.admin', compact('events','holidays'));
    }

    public function employee(Employee $employee)
    {
        $holidays = Event::where('type_id',3)->get();
        $events = Event::whereNotIn('type_id',[2,3])->where('employee_id',$employee->id)->get();
        $events = $events->merge(Event::where('type_id',7)->get());
        return view('backend.calendar.employee', compact('holidays','events','employee'));
    }

    // API CALLS
    public function create(Request $request)
    {  
        // Validate data
        $title = "";

        if(!isset($request->type) || !isset($request->employee) || !isset($request->start) || !isset($request->end) || !isset($request->all_day)){
            return json_encode([
                'status' => 'failed', 
                'message' => 'Parameter missing.'
            ]);
        }

        // check if title required
        if(EventType::find($request->type)){
            if(EventType::find($request->type)->title_required){
                if(!isset($request->title)){
                    return json_encode([
                        'status' => 'failed', 
                        'message' => 'Title not specified.'
                    ]);
                }
               $title = $request->title;
            }
        }else{
            return json_encode([
                'status' => 'failed', 
                'message' => 'Event Type not valid.'
            ]);
        }

        // check if all day
        if($request->all_day == "0"){
            if(!isset($request->start_time) || !isset($request->end_time) ){
                return json_encode([
                    'status' => 'failed', 
                    'message' => 'Time missing.'
                ]);
            }else if(   preg_match("/^(?:2[0-4]|[01][1-9]|10):([0-5][0-9])$/", $request->start_time) &&
                        preg_match("/^(?:2[0-4]|[01][1-9]|10):([0-5][0-9])$/", $request->end_time)){
                $request->start = $request->start . " " . $request->start_time;
                $request->end = $request->end . " " . $request->end_time;
            }else{
                return json_encode([
                    'status' => 'failed', 
                    'message' => 'Time format invalid.'
                ]);
            }
        }

        // check if end date is the future and not the past
        if($request->start > $request->end){
            return json_encode([
                'status' => 'failed', 
                'message' => 'Start/End date invalid.'
            ]);
        }

        // Check if employee exist
        if(!Employee::find($request->employee)){
            return json_encode([
                'status' => 'failed', 
                'message' => 'Employee not found.'
            ]);
        }

        // check last date
        if($request->all_day == "1"){
            $request->end = Carbon::parse($request->end)->addDay()->toDateString();
        }

        $event = Event::create([
            "type_id" => $request->type,
            "employee_id" => $request->employee,
            "title" => $title,
            "start" => $request->start,
            "end" => $request->end,
            "all_day" => $request->all_day,
        ]);

        $event->save();

        // Extra Variables
        if($event->type_id == 1 || $event->type_id == 5 || $event->type_id == 9){
            $event->title = $event->employee->name;
        }else if($event->type_id == 4 || $event->type_id == 6){
            $event->title = $event->type->name . ' - ' . $event->employee->name;
        }

        if(Carbon::parse($event->start)->format('H:i') != "09:00" && $event->type_id == 1 ){
            $event->backgroundColor = 'saddlebrown';
            $event->textColor = 'white';
        }else{
            $event->backgroundColor = $event->type->bg_color;
            $event->textColor = $event->type->text_color;
        }

        $event->name = $event->employee->name;

        return json_encode([
            'status' => 'success', 
            'message' => 'Event Created.',
            'event' => $event
        ]);
    }
     
 
    public function update(Request $request)
    {   
        // Validate data
        if(!isset($request->event_id)){
            return json_encode([
                'status' => 'failed', 
                'message' => 'Event not specified.'
            ]);
        }

        if(!isset($request->type) || !isset($request->employee) || !isset($request->start) || !isset($request->end) || !isset($request->all_day)){
            return json_encode([
                'status' => 'failed', 
                'message' => 'Parameter missing.'
            ]);
        }

        $event = Event::find($request->event_id);

        if(!$event){
            return json_encode([
                'status' => 'failed', 
                'message' => 'Event not found.'
            ]);
        }

        // check if title required
        if($event_type = EventType::find($request->type)){
            if($event_type->title_required){
                if(!isset($request->title)){
                    return json_encode([
                        'status' => 'failed', 
                        'message' => 'Title not specified.'
                    ]);
                }
               $event->title = $request->title;
            }
        }else{
            return json_encode([
                'status' => 'failed', 
                'message' => 'Event Type not valid.'
            ]);
        }

        // check if all day
        if($request->all_day == "0"){
            if(!isset($request->start_time) || !isset($request->end_time) ){
                return json_encode([
                    'status' => 'failed', 
                    'message' => 'Time missing.'
                ]);
            }else if(   preg_match("/^(?:2[0-4]|[01][1-9]|10):([0-5][0-9])$/", $request->start_time) &&
                        preg_match("/^(?:2[0-4]|[01][1-9]|10):([0-5][0-9])$/", $request->end_time)){
                $request->start = $request->start . " " . $request->start_time;
                $request->end = $request->end . " " . $request->end_time;
            }else{
                return json_encode([
                    'status' => 'failed', 
                    'message' => 'Time format invalid.'
                ]);
            }
        }

        // check if end date is the future and not the past
        if($request->start > $request->end){
            return json_encode([
                'status' => 'failed', 
                'message' => 'Start/End date invalid.'
            ]);
        }

        if($request->all_day == "1"){
            $request->end = Carbon::parse($request->end)->addDay()->toDateString();
        }     

        $event->type_id = $request->type;
        $event->start = $request->start;
        $event->end = $request->end;
        $event->employee_id = $request->employee;
        $event->all_day = $request->all_day;

        $event->touch();
        $event->save();

        // Extra Variables
        $event->name = $event->employee->name;

        if(Carbon::parse($event->start)->format('H:i') != "09:00" && $event->type_id == 1 ){
            $event->backgroundColor = 'saddlebrown';
            $event->textColor = 'white';
        }else{
            $event->backgroundColor = $event->type->bg_color;
            $event->textColor = $event->type->text_color;
        }

        return json_encode([
            'status' => 'success', 
            'message' => 'Event Updated.',
            'event' => $event
        ]);
    } 
 
 
    public function destroy(Request $request)
    {
        // Validate data
        if(!isset($request->event_id)){
            return json_encode([
                'status' => 'failed', 
                'message' => 'Event not specified.'
            ]);
        }

        $event = Event::find($request->event_id);

        if(!$event){
            return json_encode([
                'status' => 'failed', 
                'message' => 'Event not found.'
            ]);
        }

        $event->delete();

        return json_encode([
            'status' => 'success', 
            'message' => 'Event Deleted.',
            'event' => $event
        ]);
    } 

    /**
     * 
     * Vue Controller
     * 
     */

    public function vue()
    {
        return view('backend.schedule-vue');

    }
}
