<?php

use Illuminate\Database\Seeder;
use App\OrderStatus;
class OrderStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status = new OrderStatus();
        $status->name = "Created";
        $status->color = "#b09dfb";
        $status->position = 1;
        $status->save();

        $status = new OrderStatus();
        $status->name = "Ordered";
        $status->color = "#f7bb45";
        $status->position = 3;
        $status->save();

        $status = new OrderStatus();
        $status->name = "Called";
        $status->color = "#de2121";
        $status->position = 9;
        $status->save();

        $status = new OrderStatus();
        $status->name = "Done";
        $status->color = "#0019ff";
        $status->position = 10;
        $status->save();

        $status = new OrderStatus();
        $status->name = "On Hold";
        $status->color = "#ff3ad5";
        $status->position = 7;
        $status->save();

        $status = new OrderStatus();
        $status->name = "Arrived";
        $status->color = "#17a2b8";
        $status->position = 8;
        $status->save();
    }
}
