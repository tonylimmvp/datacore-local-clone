<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderStatusHistory extends Model
{
    protected $guarded = [];

    public function orders(){
        return $this->belongsTo(Order::class,'order_id');
    }
    public function status(){
        return $this->belongsTo(OrderStatus::class,'status_id');
    }
    public function sales(){
        return $this->belongsTo(Employee::class,'employee_id');
    }
}
