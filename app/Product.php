<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Product extends Model
{
    protected $connection = 'leeselectronic';
    protected $table = "table_1";
    protected $primaryKey = "id_product";

    public $timestamps = false;

    /********************************************
    *
    *           Search
    *
    ********************************************/

    /**
     * The columns of the full text index
     */
    protected $searchable = [
        'table_1.name',
        'vendor',
        'vendor_remarks',
        'table_1.id_product',
        'description_short',
        'vendors.remark',
        'vendor_name.reference',
        'table_1.shelf_loc'
    ];

    protected function getWhereClause($term)
    {
        // removing symbols used by MySQL
        $reservedSymbols = ['<', '>', '@', '~','\''];
        $term = str_replace($reservedSymbols, '', $term);
        $whereClause = '';

        $words = explode(' ', $term);

        foreach($words as $word_id=>$word){
            if($word_id > 0){
                $whereClause .= ") AND (";
            }else{
                $whereClause .= "(";
            }
            foreach ($this->searchable as $col_id=>$column){
                if($col_id==0){
                    $whereClause .= "$column LIKE '%$word%' "  ; 
                }else{
                    $whereClause .= "OR $column LIKE '%$word%' "  ; 
                }
            }

            if($word_id + 1 == sizeof($words)){
                    $whereClause .= ")";
                }
        }
        return $whereClause;
    }

    public function scopeSearch($query, $term)
    {
        $columns = implode(',',$this->searchable);
        
        $whereClause = $this->getWhereClause($term);

        $query  ->selectRaw('`table_1`.*, 
                            `ps_product_lang`.`description_short`, 
                            `ps_product_lang`.`description`')
                ->leftJoin('ps_product_lang','table_1.id_product','=','ps_product_lang.id_product')
                ->leftJoin('datacore.product_vendors as vendors','vendors.product_id','=','table_1.id_product')
                ->leftJoin('datacore.vendors as vendor_name','vendors.vendor_id','=','vendor_name.id')
                ->whereRaw($whereClause)
                ->where("table_1.id_product",">","6")
                ->where("table_1.id_product","!=","100")
                ->where("disabled","N")
                ->groupBy('table_1.id_product');

        return $query;
    }

    /********************************************
    *
    *           Locations
    *
    ********************************************/

    public function locations(){
    
        if($this->fraser_loc){                
            $fraser_loc = $this->fraser_loc;

            $fraser_loc = str_replace("1/4", "0.25", $fraser_loc);
            $fraser_loc = str_replace("1/2", "0.5", $fraser_loc);
            $fraser_loc = str_replace("//", "/", $fraser_loc);
            $fraser_loc = trim($fraser_loc);
            $keyterm_array = explode("/", $fraser_loc);
            if(count($keyterm_array)>0){

                for($i=0; $i<count($keyterm_array); $i++){
                        
                    $current_term = $keyterm_array[$i];

                    if(trim($current_term)!=""){
                        if($i>0){
                            echo "<hr>";
                        }
                        $current_term = str_replace("0.25", "1/4", $current_term);
                        $current_term = str_replace("0.5", "1/2", $current_term);
                        $current_term = trim($current_term);
                        echo "<p class='p-0 m-0'>".$current_term . "</p>";
                    }
                    
                }
            }
        }
                        
    }

     /********************************************
    *
    *           Images
    *
    ********************************************/

    // Get the first image url 
    public function image_url(){
        $url = "";
        $image = DB::connection("leeselectronic")->table("ps_image")->where("id_product",$this->id_product)->where('cover','1')->first();
        if(isset($image)){
            $url = $this->getImageByImageId($image->id_image);
        }
        return $url;
    }

    public function getImageByImageId($image_id){
        $image_id = (string)$image_id;
        $url = config("custom.web_url")."/img/p";
        for($i = 0; $i < strlen($image_id); $i++){
            $url .= "/" . $image_id[$i];
        }
        $url .= "/$image_id-large_default.jpg";

        return $url;
    }

    public function getAllImagesUrl(){

        $image_index = DB::connection('leeselectronic')->table("ps_image")->where("id_product", $this->id_product)->get();
        $image_urls = [];
        foreach($image_index as $image){
            array_push($image_urls, $this->getImageByImageId($image->id_image));
        }

        return $image_urls;
    }

     /********************************************
    *
    *           New Product
    *
    ********************************************/

    // Get new items
    public static function newProducts(){

        // $website = DB::connection("leeselectronic")->table("ps_product")->get();

        $new = Product::whereRaw('(`id_product`) NOT IN (SELECT `id_product` FROM `ps_product`) AND `price` > 0 AND `disabled` = "N"')->get();
        // $new = Product::whereRaw('(`id_product`) NOT IN (SELECT `id_product` FROM `ps_product`) AND `price` > 0')->get();

        // dd($new);
       return $new;
    }

     /********************************************
    *
    *           Category
    *
    ********************************************/

    public static function getCategoryUrl($category_id){
        $url = "";
        $category = Category::find($category_id);
        if($category){
            $url = config('custom.web_url') . "/category/" . $category_id . "-" . $category->link_rewrite;
        }
        return $url;
    }

    public static function getCategoryName($category_id){
        return Category::getName($category_id);
    }

    /********************************************
    *
    *           Get Product Info
    *
    ********************************************/
    public static function getProductName($product_id){
        return Product::find($product_id)->name;
    }

    public static function getProductPrice($product_id){
        return Product::find($product_id)->price;
    }

     /********************************************
    *
    *           Notes Restriction
    *
    ********************************************/
    public function isRestricted(){
        $restricted = false;
        foreach($this->notes as $note){
            if(($note->type_id == 8) &&($note->isHandled == 0)){
                $restricted = true;
            }
        }
        return $restricted;
    }

    public function isObsoleted(){
        $obsoleted = false;
        foreach($this->notes as $note){
            if(($note->type_id == 3) &&($note->isHandled == 0)){
                $obsoleted = true;
            }
        }
        return $obsoleted;
    }

    /********************************************
    *
    *           New Product
    *
    ********************************************/

    // Check if item in stock
    public static function checkStock($product_id,$quantity){

        $product = Product::where('id_product',$product_id)->where('quantity','>=',$quantity)->first();

        if($product){
            return true;
        }

        return false;
    }

    /********************************************
    *
    *           Eloquent Relations
    *
    ********************************************/


    public function orders(){
        return $this->belongsToMany(Order::class, 'datacore.order_product','product_id','order_id')->groupBy('id');
    }

    public function notes(){
        return $this->hasMany(ProductNote::class, 'product_id');
    }

    public function quotations(){
        return $this->belongsToMany(Quotation::class, 'datacore.quotation_product','product_id','quotation_id')->groupBy('id');
    }

    public function categories(){
        return $this->belongsToMany(Category::class, 'ps_category_product','id_product','id_category')->withPivot('position')->where('id_lang','1');
    }

    public function online(){
        return $this->belongsTo(ProductOnline::class, 'id_product');
    }

    public function salesHistory(){
        return $this->hasMany(SalesHistory::class, 'product_id')->orderBy('year','asc');
    }

    //return $this->belongsTo(ShipmentCart::class, 'foreign_key of the file model', 'id of the shipmentcart ');
    public function cart(){
        return $this->belongsTo(ShipmentCart::class, 'id_product', 'product_id');
    }

    public function relatedProduct(){
        return $this->hasMany(ProductRelation::class, 'product_id', 'id_product');
    }

    public function restock(){
        return $this->hasOne(RestockProduct::class,'product_id','id_product');
    }
    
    public function sale(){
        return $this->belongsTo(SaleSuggestion::class, 'id_product', 'product_id');
    }

    public function vendors(){
        return $this->belongsToMany(Vendor::class,'datacore.product_vendors','product_id','vendor_id')->withPivot('recent_cost','avg_cost','remark');
    }

    public function orderSuggestion(){
        return $this->belongsTo(OrderSuggestion::class, 'id_product', 'product_id');
    }

    public function onlineSales(){
        return $this->hasMany(OnlineSale::class, 'id_product');
    }

    public function saleSuggestion(){
        return $this->belongsTo(SaleSuggestion::class, 'id_product', 'product_id');
    }

    public function priceChanges(){
        return $this->hasMany(ProductPriceChange::class, 'product_id', 'id_product');
    }

    public function onSale(){
        $on_sale = false;
        if($this->onlineSales){
            foreach($this->onlineSales as $sale){
                if($sale->onSale()){
                    $on_sale = true;
                }
               
            }
        }
        return $on_sale;
    }

    public function quarterSale(){
        $sales = SalesHistory::selectRaw("quarter_table.product_id, quarter_table.year,
        MAX(CASE WHEN quarter_table.quarter = 1 THEN (total_qty) END) AS q1,
        MAX(CASE WHEN quarter_table.quarter = 2 THEN (total_qty) END) AS q2,
        MAX(CASE WHEN quarter_table.quarter = 3 THEN (total_qty) END) AS q3,
        MAX(CASE WHEN quarter_table.quarter = 4 THEN (total_qty) END) AS q4")
            ->join(
            DB::raw("(SELECT product_id, year,FLOOR((month-1)/3)+1 quarter, SUM(qty) as total_qty
                    FROM `sales_histories`
                    GROUP BY product_id, year, FLOOR((month-1)/3)+1) AS quarter_table"), function ($join) {
                         $join->on('sales_histories.product_id', '=', 'quarter_table.product_id')
                                ->on('sales_histories.year', '=', 'quarter_table.year');
                    })
            ->where('quarter_table.product_id',"=",$this->id_product)
            ->groupBy("quarter_table.product_id","quarter_table.year")
            ->get();
        return $sales;
    }

    public function quarterSaleByYear($year){
        $sales = SalesHistory::selectRaw("quarter_table.product_id, quarter_table.year,
        MAX(CASE WHEN quarter_table.quarter = 1 THEN (total_qty) END) AS q1,
        MAX(CASE WHEN quarter_table.quarter = 2 THEN (total_qty) END) AS q2,
        MAX(CASE WHEN quarter_table.quarter = 3 THEN (total_qty) END) AS q3,
        MAX(CASE WHEN quarter_table.quarter = 4 THEN (total_qty) END) AS q4")
            ->join(
            DB::raw("(SELECT product_id, year,FLOOR((month-1)/3)+1 quarter, SUM(qty) as total_qty
                    FROM `sales_histories`
                    GROUP BY product_id, year, FLOOR((month-1)/3)+1) AS quarter_table"), function ($join) {
                         $join->on('sales_histories.product_id', '=', 'quarter_table.product_id')
                                ->on('sales_histories.year', '=', 'quarter_table.year');
                    })
            ->where('quarter_table.product_id',"=",$this->id_product)
            ->groupBy("quarter_table.product_id","quarter_table.year")
            ->having("quarter_table.year","=",$year)
            ->first();
        return $sales;
    }
}
