@extends('backend.layouts.main')

@section('content')

<h1 class="mt-2">Add New Employee</h1>
<form method="POST" action="{{route('coreui.employees.store')}}" enctype="multipart/form-data">
    {{ csrf_field() }}

    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" id="name" name="name" placeholder="Enter Employee's Full Name" value="{{old('name')}}" required>
    </div>

    <div class="form-group">
        <label for="username">Username</label>
        <input type="username" class="form-control" id="username" name="username" placeholder="Enter Username" value="{{old('username')}}" required>
    </div>

    <div class="form-group">
        <label for="role">Employee Role</label>
        <select id="role" class="form-control" name="role" value="{{old('role')}}" required>
            <option value="" disabled selected>Select Role ...</option>
            @foreach ($roles as $role)
                <option value="{{$role->id}}" @if(old('role') == $role->id) selected @endif>{{$role->name}}</option>
            @endforeach
        </select>
    </div>

<div class="form-group">
    <label for="email">Email address</label>
    <input type="email" class="form-control" id="email" name="email" placeholder="Enter Email" value="{{old('email')}}" required>
</div>
<div class="form-group">
    <label for="password">Password</label>
    <input type="password" class="form-control" id="password" placeholder="Password" name="password" required>
    <small id="passHelp" class="form-text text-muted">Password must be at least 6 characters long.</small>
</div>
<div class="form-group">
    <label for="password_confirmation">Confirm Password</label>
    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Confirm Password" required>
</div>

{{-- <div class="form-group form-check">
    <input type="checkbox" class="form-check-input" id="exampleCheck1">
    <label class="form-check-label" for="exampleCheck1">Is this user active?</label>
</div>  --}}

<button type="submit" class="btn btn-primary">Submit</button>

@include('layouts.errors')

</form>


@endsection