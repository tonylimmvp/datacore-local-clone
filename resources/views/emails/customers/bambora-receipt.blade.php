@component('mail::message')

<h1 style="text-align:center">Lee's Electronic Online Sales Receipt</h1>


<h2 style="text-align:center;color:#74787E">ORDER DETAIL</h2>
@component('mail::table')
|||
|   :----------------   |   :------------------------------------------ |
| Order Date:           | {{ $result["created"] }}                      | 
| Order Number:         | {{ $result["order_number"] }}                 | 
| Order Total:          | ${{ number_format($result["amount"],2) }} CAD | 
@endcomponent

<h2 style="text-align:center;color:#74787E">CARD INFORMATION DETAILS</h2>

@component('mail::table')
|||
|:-------------------- |   :-------------|
| Cardholder Name:      | {{ $result["cardholder"] }}           | 
| Card Type:            | {{ $result["card"]["card_type"] }}    | 
| E-mail Address:       | {{ $result["email"] }}                | 
@endcomponent

<h2 style="text-align:center;color:#74787E">MERCHANT INFORMATION</h2>

@component('mail::table')
|||
|   :----------------   |   :-------------------    |
| Online Address:       | <a href="{{ config('custom.web_url') }}">{{ config('custom.web_url') }}</a>  | 
| Merchant Name:        | Lee's Electronic Component                                                   | 
| Address:              | 4131 Fraser Street                                                           | 
| City:                 | Vancouver                                                                    | 
| Province:             | BC                                                                           | 
| Postal Code:          | V5V 4E9                                                                      | 
| Country:              | Canada                                                                       | 
| Phone Number:         | <a href="tel:6048751993">(604) 875-1993</a>                                  | 
@endcomponent

@endcomponent
