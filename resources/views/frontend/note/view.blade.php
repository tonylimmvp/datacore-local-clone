    <link href="{{ asset('css/compiled/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/compiled/custom.css')}}">

	<link href="{{ asset('css/dashboard.css')}}" rel="stylesheet">
    <link href="{{ asset('css/compiled/frontend_navtop.css')}}" rel="stylesheet">
    <link href="{{asset('css/compiled/side-modal.css')}}" rel="stylesheet">

	{{-- Boolean cases to check order type:
			Online Order: $storeOrder == NULL AND $mobileOrder == NULL
			Store Order: $onlineOrder == NULL AND $mobileOrder == NULL
			Mobile Order: $onlineOrder == NULL AND $storeOrder == NULL --}}

	{{-- If order does not exist then create an alert --}}
	@if(empty($notes->first()))
		<div class="alert alert-dark">
			<p>Note is empty. Use add note button above to create.</p>
		</div>
	@else
		<div class="table-responsive">
			<table id="notesTable" class="table table-hover table-dark table-bordered">
				<thead>
					<tr>
						<th scope="col" class="col-md-1"><span data-feather="align-justify"></span> Notes</th>
						@if(Auth::user()->employee_role_id != 5)
							<th scope="col" class="col-md-1"><span data-feather="list"></span> Actions</th>    
						@endif               
					</tr>
				</thead>
				<tbody>
					@foreach ($notes as $note)
						<tr>
							<td>{{$note->created_at}} - [{{$note->employee->name}}] : {{ $note->notes }}</td>
							<td>
								{{-- The if statements here is used to distinguish order type --}}
								{{-- If the wrong order is found, null will be returned --}}
								@if(!empty($note->order->first()))
									<form class="form-inline" method="POST" action="{{route('order_notes.delete', $note->id)}}" enctype="multipart/form-data">
										<input type="hidden" name="_method" value="POST">
										{{ csrf_field() }}
										<button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')" title="Delete Note"><span data-feather="trash"></i></button><br>
									</form>
								@elseif(!empty($note->online->first()))
									<form class="form-inline" method="POST" action="{{route('online_notes.delete', $note->id)}}" enctype="multipart/form-data">
										<input type="hidden" name="_method" value="POST">
										{{ csrf_field() }}
										<button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')" title="Delete Note"><span data-feather="trash"></i></button><br>
									</form>
								@elseif(!empty($note->mobile->first()))
									<form class="form-inline" method="POST" action="{{route('mobile_notes.delete', $note->id)}}" enctype="multipart/form-data">
										<input type="hidden" name="_method" value="POST">
										{{ csrf_field() }}
										<button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')" title="Delete Note"><span data-feather="trash"></i></button><br>
									</form>
								@endif
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	@endif
	<hr/>

    {{-- Order Note Modal --}}
	@if(!empty($storeOrder))
		<div class="modal fade" id="createNoteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<form action="{{ route('order-notes.store', $storeOrder->id)}}" method="POST">
						{{ csrf_field() }}
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Create New Note</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>

						<div class="modal-body">
							<label>Order ID: </label>
							<input class="form-control" type="integer" name="orderid" id="note-orderid" value="{{$storeOrder->id}}" readonly>
							<hr/>

							<br/>

							<label>Comments:</label>
							<textarea class="form-control" type="text" name="comments" id="note-comments"></textarea>
						</div>

						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
							<a id="note-cancel" href="#">
							<button type="submit" class="btn btn-primary">Create Note</button>
							</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	@endif

    {{-- Online Order Note Modal --}}
	@if(!empty($onlineOrder))
		<div class="modal fade" id="createOnlineNote" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<form action="{{ route('onlineOrder-notes.store', $onlineOrder->id_order)}}" method="POST">
						{{ csrf_field() }}
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Create New Note</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>

						<div class="modal-body">
							<label>Order ID: </label>
							<input class="form-control" type="integer" name="orderid" id="note-orderid" value="{{$onlineOrder->id_order}}" readonly>
							<hr/>

							<br/>

							<label>Comments:</label>
							<textarea class="form-control" type="text" name="comments" id="note-comments"></textarea>
						</div>

						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
							<a id="note-cancel" href="#">
							<button type="submit" class="btn btn-primary">Create Note</button>
							</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	@endif

    {{-- Mobile Order Note Modal --}}
	@if(!empty($mobileOrder))
		<div class="modal fade" id="createMobileNote" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<form action="{{ route('mobileOrder-notes.store', $mobileOrder->order_id)}}" method="POST">
						{{ csrf_field() }}
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Create New Note</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>

						<div class="modal-body">
							<label>Order ID: </label>
							<input class="form-control" type="integer" name="orderid" id="note-orderid" value="{{$mobileOrder->order_id}}" readonly>
							<hr/>

							<br/>

							<label>Comments:</label>
							<textarea class="form-control" type="text" name="comments" id="note-comments"></textarea>
						</div>

						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
							<a id="note-cancel" href="#">
							<button type="submit" class="btn btn-primary">Create Note</button>
							</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	@endif

{{-- Datatable --}}
    <script type="text/javascript">
        $(document).ready(function () {
            $('#notesTable').DataTable({
                "scrollY":        "60vh",
                "scrollCollapse": true,
                "paging":         true,
				"pageLength": 5,
				"aLengthMenu": [[5, 10, 20, -1], [5, 10, 20, "All"]],
                columnDefs: [ { orderable: false, targets: [
                    5
                ]}],
				"language": {
                    "search": "Filter records:"
                },
				"order": [[ 0, "asc" ]]
            });
        });
    </script>