@extends('backend.layouts.main')

@section('content')
<div class="container d-flex justify-content-center" style="height:80vh">
    <div class="my-auto text-center">
        <h3>Import Sales History</h3>
        <form method="POST" action="{{route('coreui.sales-history.store')}}" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{-- COMPONENT START --}}
            <div class="form-group">
                <div class="input-group input-file">
                    
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                        <span data-feather="paperclip"></span>
                        </div>
                    </div>
                    
                    <input type="text" class="form-control" placeholder='Choose a file...' />
                    <div class="input-group-append">
                            <button class="btn btn-warning btn-reset" type="button">Reset</button>
                    </div>
                </div>
            </div>

            <div class="form-row">
                <div class="col">
                    @php $month_current = Carbon\Carbon::now()->month; @endphp
                    <select class="form-control mb-2" name="month" required>
                        <option value="" selected disabled>Select Period</option>
                        @for($month=1;$month<=12;$month++)
                        <option value="{{$month}}" @if($month==$month_current) selected @endif>
                            {{ date("F", mktime(0, 0, 0, $month, 10)) }}
                        </option>
                        @endfor
                    </select>
                </div>
                <div class="col">
                    <input type="text" class="form-control" placeholder='Enter Year...' name="year" value="{{Carbon\Carbon::now()->year}}"/>
                </div>
            </div>       

            {{-- COMPONENT END --}}
            <div class="form-group mt-2">
                <button id="upload-form-button" type="submit" class="btn btn-primary pull-right">Upload</button>
            </div>
        </form>

        <div class="alert alert-info pb-1">
            <u>File Location<br></u>
            <span><i>WinPOS > Report > Sales > Summary > Summary By Year </i></span>
        </div>

        @include('layouts.errors')
    </div>
</div>
@endsection

@push('css')
<style>
    
</style>
@endpush
          

@push('bottom-scripts')
<script>
function bs_input_file() {
	$(".input-file").before(
		function() {
			if ( ! $(this).prev().hasClass('input-ghost') ) {
				var element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0' name='csv_file'>");
				element.attr("name",$(this).attr("name"));
				element.change(function(){
					element.next(element).find('input').val((element.val()).split('\\').pop());
				});
				$(this).find(".btn-choose").click(function(){
					element.click();
				});
				$(this).find("button.btn-reset").click(function(){
					element.val(null);
					$(this).parents(".input-file").find('input').val('');
				});
				$(this).find('input').css("cursor","pointer");
				$(this).find('input').mousedown(function() {
					$(this).parents('.input-file').prev().click();
					return false;
				});
				return element;
			}
		}
	);
}
$(function() {
    bs_input_file();
});

$('form').on('submit',function(){
    $('#upload-form-button').attr("disabled",true);
    $('#loader').toggle();
});

</script>

@endpush