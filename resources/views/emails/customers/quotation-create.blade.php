@component('mail::message')

Hello {{$quotation->name}},

This is a confirmation email on the quotation that you have requested. Please review and keep this as reference. If you see any mistakes please let us know right away by emailing <a href="mailto:support@leeselectronic.com">support@leeselectronic.com</a> or call us at <a href="tel:6048751993">(604) 875-1993</a>. 

@component('mail::table')
| PID        | Name           | Price per Unit  | Quantity | 
| ---------- |:--------------:| :--------------:| :------: |
@foreach($quotation->products as $product)
| {{$product->id_product}} | {{$product->pivot->name}} | ${{$product->pivot->price}} | {{$product->pivot->quantity}} |
@endforeach
@endcomponent

@if($quotation->phone != "6048751993")
The phone number registered with this quotation is {{$quotation->phone}} @if($quotation->phone_ext), Ext.{{$quotation->phone_ext}} @endif.
@endif

Cheers,<br>
Lee's Electronic Components

---

<small>*Note: Quotations **expires** after **7 days**.*</small>

@endcomponent
