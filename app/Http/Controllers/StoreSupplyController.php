<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StoreSupply;

class StoreSupplyController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function show()
    {   
        $supplies = StoreSupply::all();
        return view('frontend.store-supply.index', compact('supplies'));
    }

    //Create page.
    public function create(Request $request)
    {
        return view('frontend.store-supply.create');
    }

    //Store function from create page.
    public function store(Request $request){
        // Validate form
        $this->validate(request(), [
            'supply_name' => 'required',
            'qty' => 'required|numeric|min:0',
        ]);

        $supply = StoreSupply::create([
            'name' => request('supply_name'),
            'qty' => request('qty'),
            'unit' => request('unit'),
            'created_at' => NOW(),
            'isActive' => '1',
        ]);

        $supply->save();
                
        $supplies = StoreSupply::all()->where('isActive', 1);
        return view('frontend.store-supply.index', compact('supplies'));
    }

    public function deleteSupply(StoreSupply $supply)
    {
        $activeSupply = StoreSupply::find($supply->id)->delete();
        return redirect()->back();
    }

    public function editSupply(StoreSupply $supply, Request $request)
    {
        $this->validate(request(), [
            'qty' => 'required|numeric|min:1',
            'unit' => 'required'
        ]);

        $supply->name = $request->name;
        $supply->qty = $request->qty;
        $supply->unit = $request->unit;
        $supply->updated_at = NOW();
        $supply->save();

        return redirect()->route('store-supplies');
    }
}