<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrderProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_order_products', function (Blueprint $table) {
            $table->string('po_id');
            $table->unsignedInteger('product_id');
            $table->string('name');
            $table->integer('po_qty');
            $table->integer('rec_qty');
            $table->decimal('unit_cost',8,2);
            $table->decimal('unit_qty_cost',8,2)->nullable();
            $table->decimal('other_cost',8,2)->nullable();
            $table->decimal('avg_cost',8,2);
            $table->decimal('total_cost',8,2);

            $table->primary(['po_id','product_id','name']);
            $table->foreign('po_id')->references('po_id')->on('purchase_orders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_order_products');
    }
}
