<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Resistor;


class ResistorController extends Controller
{

    public function index(Request $request){
        if(isset($request->wattage) && isset($request->watt_sign) && isset($request->resistance) && isset($request->ohm_sign)){
            $resistors = Resistor::filter(request(['wattage','watt_sign','resistance','ohm_sign']))->get();
        }elseif(isset($request->wattage) && isset($request->watt_sign)){
            $resistors = Resistor::filter(request(['wattage','watt_sign']))->get();
        }elseif(isset($request->resistance) && isset($request->ohm_sign)){
            $resistors = Resistor::filter(request(['resistance','ohm_sign']))->get();
        }else{
            $resistors = Resistor::all();
        }

        return view('frontend.resistor',compact('resistors'));
    }

    public function update(){
        $resistors = Product::whereRaw("name LIKE '%RESISTOR%' AND
                                        (name LIKE '%OHM%' OR
                                        name LIKE '%K%' OR
                                        name LIKE '%MEG%')")
                    ->where("id_product",">","6")
                    ->where("id_product","!=","100")
                    ->where("disabled","N")->get();

        foreach($resistors as $resistor){
            $this->parseResistor($resistor);
        }

        return back();
    }

    // Function
    public function parseResistor($resistor){
        $name_array = explode(' ', $resistor->name);
        
        $has_watt = false;
        $has_ohm = false;
        $has_tolerance = false;

        $wattage = '';
        $resistance = '';
        $tolerance = null;

        $resistors = [];
        
        foreach($name_array as $i=>$token){
            // Wattage
            if(preg_match("/^[0-9]*\.*[0-9]*W/i",$token)){
                $has_watt = true;
                if(preg_match_all('/^(\d+)\.*[0-9]*/m', $token, $m)){
                    $wattage = floatval($token);
                }
            }else if(strpos($token,'/4')){ 
                $has_watt = true;
                $wattage = 0.25;
            }else if(strpos($token,'/8')){
                $has_watt = true;
                $wattage = 0.125;
            }else if(strpos($token,'/2')){ 
                $has_watt = true;
                $wattage = 0.5;
            }

            //Ohm
            if(preg_match("/^[0-9]*\.*[0-9]*(ohm|OHM)/i",$token)){
                $has_ohm = true;
                if (preg_match_all('/^(\d+)\.*[0-9]*/m', $token, $m)){
                        $resistance = floatval($token);
                        // dd($name_array[$i-1]);
                        
                }else if($token == "OHM"){
                    $resistance = floatval($name_array[$i-1]);
                }
            }

            
            // kOhm
            if(preg_match("/^[0-9]*\.*[0-9]*(k|K)/i",$token)){
                $has_ohm = true;
                if (preg_match_all('/^(\d+)\.*[0-9]*/m', $token, $m)){
                    $resistance = floatval($token) * 1000;
                }
            }

            // MOhm
            if(preg_match("/^[0-9]*\.*[0-9]*(MEG|M)/i",$token)){
                $has_ohm = true;
                if (preg_match_all('/^(\d+)\.*[0-9]*/m', $token, $m)){
                    $resistance = floatval($token) * 1000000;
                }
            }

            // Tolerance
            else if(preg_match("/^[0-9]*%$/i",$token)){
                $has_tolerance = true;
                if (preg_match_all('/^(\d+)/m', $token, $m)){
                    $tolerance = floatval($token);
                }
            }

            if($has_watt && $has_ohm){
                $resist = Resistor::updateOrCreate(
                    [   'product_id' => $resistor->id_product   ],
                    [
                        'wattage' => $wattage,
                        'resistance' => $resistance,
                        'tolerance' => $tolerance
                    ]
                );
        
                $resist->save();
            }
        } 
    }
}
