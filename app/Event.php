<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $guarded = [];
    
    public function employee(){
        return $this->belongsTo(Employee::class,'employee_id');
    }
    
    public function type(){
        return $this->belongsTo(EventType::class,'type_id');
    }

    public function parsedDate(){
        return \Carbon\Carbon::parse($this->start)->format('Y-m-d');
    }

    public function parsedStartDate(){
        return \Carbon\Carbon::parse($this->start)->format('Y-m-d');
    }

    public function parsedEndDate(){
        return \Carbon\Carbon::parse($this->end)->subDay()->format('Y-m-d');
    }

    public function scopeDaysOffBetweenFor($query,$start,$end,$employee_id){
        $query->where('type_id',6)
                ->whereBetween('start',[$start,"$end 23:59:59"])
                ->where('employee_id',$employee_id)
                ->with('employee');
    }

    public static function vacationsBetweenFor($start,$end,$employee_id){
        $periods = Event::where('type_id',4)
                ->whereRaw('((start BETWEEN "'. $start . '" AND "'. $end .  ' 23:59:59") OR (end BETWEEN "' . $start . '" AND "' . $end . ' 23:59:59"))')
                ->Where('employee_id',$employee_id)
                ->with('employee')->get();
        $count = 0;

        foreach($periods as $period){
            // check range of date and Grab all dates
            if($period->parsedStartDate() < $start){
                $vacation_start = $start;
            }else{
                $vacation_start = $period->parsedStartDate();
            }

            if($period->parsedEndDate() > $end){
                $vacation_end = $end;
            }else{
                $vacation_end = $period->parsedEndDate();
            }

            $vacations = \Carbon\CarbonPeriod::create($vacation_start, $vacation_end);

            // check if they normally work on those days
            foreach($vacations as $vacation){
                $day = $vacation->dayOfWeek;
                $days = WorkShift::where('day_id',$day)->get();
                foreach(Employee::find($employee_id)->shifts as $shift){
                    if($days->contains($shift)){
                        $count++; 
                    }
                }
            }
        }

        // return total amount of days took off according to their normal schedule.
        return $count;
    }
}
