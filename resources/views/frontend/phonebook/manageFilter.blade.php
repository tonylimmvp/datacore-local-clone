@extends('frontend.layouts.main')

@push('css')
<link rel="stylesheet" href="{{asset("css/phonebook.css")}}">   
@endpush
@section('content')
<div class="title m-b-md">
    Phonebook
</div>
@auth
        @if(Auth::user()->hasAccess('companies')->create)
    <form class ="row filters company-form manage-form" method='post' action= {{route('phonebook-manage-filter-patch')}}>

        {{csrf_field()}}
        {{ method_field('PATCH') }}
        <div class="row">
            <label class ='col-md-8 col-sm-7'><h3>Filters:</h3></label>
            <label class ='col-md-4 col-sm-5'><h3>Active:</h3></label>
            <hr>
        </div>
        @foreach ($filters as $filter)
                <div class= "input-group-txt row manage-filters">
                    <label for="{{$filter->name}}" class= "col-md-6 col-sm-6">{{$filter->name}}</label>
                    <input type="checkbox" id="{{$filter->name}}"name= "{{$filter->name}}" class= "col-md-6 col-sm-6" value = "{{$filter->id}}" 
                    @php
                        if($filter->isActive) echo "checked='true'";
                    @endphp
                    >
                </div>
        @endforeach
        <div class = 'row'>
            <div class= 'col-md-6 col-sm-6'>
                <input class="btn btn-primary" value ='Revert Changes' type='reset'>
            </div>
            <div class = "col-md-6 col-sm-6">
                <input class="btn btn-primary" value ='Submit' type='submit'>
            </div>
        </div>
    </form>
    @endif
    @endauth
    
    
 @endsection

 @push("bottom-scripts")
 <script type="text/javascript" src = "{{asset('js/phonebook.js')}}"></script>
 @endpush