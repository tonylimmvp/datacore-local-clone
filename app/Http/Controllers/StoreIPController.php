<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StoreIP;

class StoreIPController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $ips = StoreIP::all();
        return view('frontend.ip-address',compact('ips'));
    }

    public function store(){
        // Validate form
        $this->validate(request(), [
            'ip' => 'required|unique:store_ips,ip_address',
        ]);

        $ip = new StoreIP();
        $ip->ip_address = request('ip');
        $ip->save();

        session()->flash('message','You have successfully added a new IP');
   
        return back();
    }

    public function remove($ip)
    {   
        if(!StoreIP::hasIP($ip)){
            session()->flash('error-message','IP '. $ip->ip_address . ' does not exist.');
            return back();
        }
        
        $ip = StoreIP::where('ip_address',$ip)->first();
        $ip->delete();
        session()->flash('message','You have successfully deleted ' . $ip->ip_address .'.');
        return back();
    }
}
