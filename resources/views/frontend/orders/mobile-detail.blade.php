@extends('frontend.layouts.main')

@section('content')
    <div class="container">
        <div class="text-center pb-3">
            <h1>Mobile Order #{{$order->order_id}}</h1>
            <h3>({{$order->reference}})</h3>
            <p><span class="badge" style="background-color:{{$order->status->color}}">{{$order->status->name}}</span></p>
            <p>Ordered on: {{$order->created_at}}</p>
            <button class="btn btn-primary btn-sm d-lg-none no-print" style="position: absolute; right:5vw; top:100px"
                    type="button"
                    data-toggle="modal"
                    data-target="#printMobileQRModal"
                    data-id= "{{$order->order_id}}">Print QR</button>
            <button class="btn btn-primary btn-sm d-none d-lg-block no-print" style="position: absolute; right:25vw; top:100px" 
                    type="button"
                    data-toggle="modal"
                    data-target="#printMobileQRModal"
                    data-id= "{{$order->order_id}}">Print QR</a>    
        </div>

        {{-- Address Information --}}
        @if($order->addressBilling() || $order->addressShipping())
        <div class="row">
            <div class="address-box col-sm-6 mt-2">
                <div class="card">
                    <div class="card-header">
                        <b>Billing Address</b>
                    </div>
                    <div class="card-body">
                        <p>{{$order->addressBilling()->firstname}} {{$order->addressBilling()->lastname}}</p>
                        @if(isset($order->addressBilling()->company))<p>{{$order->addressBilling()->company}}</p>@endif
                        <p>{{$order->addressBilling()->address1}}</p>
                        <p>{{$order->addressBilling()->city}} {{App\MobileOrder::stateToISO($order->addressBilling()->state_id)}} {{$order->addressBilling()->postcode}}</p>
                        <p>{{$order->addressBilling()->phone}}</p>
                    </div>
                </div>
            </div>
            <div class="address-box col-sm-6 mt-2">
                <div class="card">
                    <div class="card-header">
                        <b>Shipping Address</b>
                    </div>
                    <div class="card-body">
                        <p>{{$order->addressShipping()->firstname}} {{$order->addressShipping()->lastname}}</p>
                        @if(isset($order->addressShipping()->company))<p>{{$order->addressShipping()->company}}</p>@endif
                        <p>{{$order->addressShipping()->address1}}</p>
                        <p>{{$order->addressShipping()->city}} {{App\MobileOrder::stateToISO($order->addressShipping()->state_id)}} {{$order->addressShipping()->postcode}}</p>
                        <p>{{$order->addressShipping()->phone}}</p>
                    </div>
                </div>
            </div>
        </div>
        @else
        <div class="row">
            <div class="address-box offset-sm-3 col-sm-6 mt-2">
                <div class="card">
                    <div class="card-header">
                        <b>Customer Information</b>
                    </div>
                    <div class="card-body">
                        <p>{{$order->customer()->firstname}} {{$order->customer()->lastname}}</p>
                        @if(isset($order->customer()->company))<p>{{$order->customer()->company}}</p>@endif
                        <p>{{$order->customer()->email}}</p>
                        <p>{{$order->customer()->phone}}</p>
                    </div>
                </div>
            </div>
        </div>
        @endif

        {{-- Cart --}}
        <h2 class="mt-3">Cart</h2>
        <div class="table-responsive">
            <table class="table table-bordered">
                @foreach($order->products as $product)
                <tr>
                    <td>{{$product->id_product}}</td>
                    <td width="70px"><img src="{{ App\Product::find($product->id_product)->image_url() }}" width="70px"></td>
                    <td>{{$product->name}}</td>
                    <td>${{$product->price}}</td>
                    <td>{{$product->pivot->qty}}</td>
                    {{-- <td>${{number_format($product->price * $product->quantity,2)}}</td> --}}
                    <td class="no-print text-center">
                        <button class="btn btn-sm btn-secondary"
                        type="button"
                        data-toggle="modal"
                        data-target="#printModal"
                        data-id= "{{$product->id_product}}"><span data-feather="printer"></span></button>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
        <div class="row">
            <div class="col-sm-8 mt-2">
                <div class="alert border">
                    <p>Type: {{$order->type}}</p>
                    {{-- Tracking Section --}}
                    @if($order->shipping_number)
                    <p>Courier: {{$order->courier->name}}</p>
                    <label>Tracking #: </label><a href="{{$order->getTrackingURL()}}"> {{$order->shipping_number}} </a>
                    @endif
                    <div>
                        <form action="{{route('mobile-order.updateTracking')}}" method="POST">
                            <div class="input-group">
                                {{ csrf_field() }}
                                <div class="input-group-prepend">
                                    <select class="form-control form-control-sm" name="courier">
                                        @foreach(App\Courier::all() as $courier)
                                            <option value="{{$courier->id}}" @if($courier->id == $order->courier_id) selected @endif>{{$courier->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            <input class="form-control form-control-sm" name="tracking" value="@if($order->shipping_number){{$order->shipping_number}}@else{{old('tracking')}}@endif">
                                <div class="input-group-append">
                                    <button class="btn btn-primary btn-sm">Submit</button>
                                </div>
                            </div>
                            
                            <input type="hidden" name="order" value="{{$order->order_id}}">
                            <input type="hidden" name="employee" value="{{Auth::user()->id}}">

                        </form>
                    </div>
                    {{-- End of tracking --}}
                </div>
                
            </div>
            <div class="col-sm-4 offset-sm-0 col-8 offset-4 mt-2">

                    <table class="table table-bordered">
                        <tr>
                            <td>Total Products</td>
                            <td>${{number_format($order->total_paid, 2)}}</td>
                        </tr>
                        <tr>
                            <td>Delivery</td>
                            @if($order->total_shipping == 0)
                            <td>Pickup</td>
                            @else
                            <td>${{number_format($order->total_shipping, 2)}}</td>
                            @endif
                        </tr>
                        <tr>
                            <td>Total Tax</td>
                            <td>${{number_format($order->total_paid_tax + $order->total_shipping_tax,2)}}</td>
                        </tr>
                        @if($order->discount_cart_amount > 0)
                        <tr>
                            <td>Discount</td>
                            <td style="color:red">$({{number_format($order->discount_cart_amount, 2)}})</td>
                        </tr>
                        @endif
                        <tr>
                            <td><b>Total Paid</b></td>
                            <td><b>${{number_format($order->total_paid + $order->total_paid_tax + $order->total_shipping + $order->total_shipping_tax - $order->discount_cart_amount,2)}}</b></td>
                        </tr>
                    </table>

            </div>
        </div>

        {{-- Notes and Comments --}}
        <div id="orderNotes" class="no-print">
            @php $notes = $order->note; @endphp
            <h5>NOTES AND COMMENTS:</h5>
            <div style="text-align:right">
				<button class="btn btn-primary mb-1"
                        title="Create New Note"
						data-toggle="modal"
						data-target="#createNoteModal"><span data-feather="file-plus"></i>
				</button>
			</div>
            @include('frontend.note.view', ['notes' => $notes], ['order' => $order])
        </div>

        <div class="no-print">
            <h5>STATUS HISTORY:</h5>
            <div class="table-responsive">
            <table class="table table-bordered">
                <tr>   
                    <th>Employee</th>
                    <th class="text-center">Status</th>
                    <th>Date</th>
                </tr>
                @foreach($order->statusHistory as $status)
                <tr>
                    {{-- {{dd($status)}} --}}
                    <td>{{$status->sales->name}}</td>
                    <td class="text-center"><span class="order-status" style="background:{{$status->status->color}};">{{$status->status->name}}</span></td>
                    <td>{{$status->date_stamp}}</td>      
                </tr>
                @endforeach
            </table>
            </div>
        </div>

        {{-- Status History Form --}}
        @auth
        <form class="form-inline no-print" method="POST" action="{{route('mobile-order-history.store')}}" enctype="multipart/form-data">
            {{ csrf_field() }}

                <div class="input-group mr-sm-2 mb-2">
                    <div class="input-group-prepend">
                    <div class="input-group-text"><span data-feather="users"></i></div>
                    </div>
                                        
                    {{-- Sales Rep --}}
                    @if(Auth::user()->role->id==5)
                    
                    <select class="form-control custom-select employee-dropdown" name="employee" required>
                        <option value="" selected disabled>Please Select An Employee...</option>
                        @foreach(App\Employee::isActive()->get() as $employee)
                            <option value="{{$employee->id}}">{{$employee->name}}</option>
                        @endforeach
                    </select>

                    @else

                    <input class="form-control" type="text" value="{{Auth::user()->name}}" disabled>
                    <input type="hidden" name="employee" value="{{Auth::user()->id}}">

                    @endif

                    <input type="hidden" name="order" value="{{$order->order_id}}">
                </div>

                <select class="custom-select mr-sm-2 mb-2" name="status" required>
                    <option value="" selected disabled>Please Select Status...</option>
                    @foreach(App\MobileOrderStatus::all() as $status)
                        <option value="{{$status->order_state_id}}">{{$status->name}}</option>
                    @endforeach
                </select>
                
                <button type="submit" class="btn btn-primary mb-2" onclick="validateSubmission()" >Submit</button>
                </form>
                
        @endauth
        @guest
                <p class="alert alert-info no-print">Note: To change the status please log in: <a class="btn btn-primary btn-sm ml-2" href="{{ route('login')}}">Login Here</a></p>
        @endguest

        {{-- Signature --}}
        <div id="signatureDiv" class="mt-3">
            <h5 @if(!$order->isSigned()) class="no-print" @endif>CUSTOMER SIGNATURE:</h5>
            @if($order->isSigned())
                <img class="signature-img pb-3" src="{{ url('storage/invoices/mobile'. $order->order_id . '.jpg') }}" style="border-bottom: #c7c7c7 1px solid">
                <p class="mt-2" style="color:#8a8a8a">Picked up by: {{$order->picked_up_by}} on {{$order->lastSigned()}}</p>
            @else
                <button class='btn btn-sm btn-primary no-print'
                    type='button' 
                    data-toggle='modal' 
                    data-target='#signatureModal' 
                    data-id='{{$order->order_id}}'
                    data-name='{{$order->customer()->firstname}} {{$order->customer()->lastname}}'
                    data-type='mobile'
                    >Order Pick Up</button>
            @endif
        </div>

        {{-- Error Message --}}
        @include('layouts.errors')

        {{-- QR Modal --}}
        @include('layouts.printers-mobile-qr')

        {{-- Footer --}}
        <div class="footer text-center pt-5">
            <img class="img-fluid" src="{{asset('img/lees_logo_large.png')}}" width="300px;">
            <h2>Thank you!</h2>

            <p>We will contact you when the order is ready. Please keep this as your receipt!</p>

            <p>Please note, we only keep orders for three (3) months after the first initial call. We do not hold liability for discarding orders older than 3 months.</p>

            <p>4131 Fraser St. Vancouver BC V5V 4E9 Canada | 604 - 875 - 1993 | www.leeselectronic.com </p>
        </div>
    </div>

    {{-- Order Note Modal --}}
    <div class="modal fade" id="createNoteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{ route('mobileOrder-notes.store', $order->order_id)}}" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create New Note</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <label>Order ID: </label>
                        <input class="form-control" type="integer" name="orderid" id="note-orderid" value="{{$order->order_id}}" readonly>
                        <hr/>

                        <br/>

                        <label>Comments:</label>
                        <textarea class="form-control" type="text" name="comments" id="note-comments"></textarea>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <a id="note-cancel" href="#">
                        <button type="submit" class="btn btn-primary">Create Note</button>
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('bottom-scripts')
<style>
    .address-box p{
        margin: 0;
    }
</style>

<script>
    function validateSubmission() {
        var submit = true;
        if($('select[name="employee"] option:selected').val() == "" || $('select[name="status"] option:selected').val() == ""){
            submit = false;
        }
        if(submit){
            openLoader();
        }
        return submit;
    }
</script>

@endpush