<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\OrderStatus;
use App\OrderStatusHistory;
use App\OrderProduct;
use App\Quotation;
use App\Employee;
use App\Mail;
use App\Product;
use App\OnlineOrder;
use App\MobileOrder;

class OrderController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth',['except' => ['index','backorders','specialorders','show']]);
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        if(request(['status']) != null){
            $status = request(['status'])['status'];
        }else{
            $status = "";
        }

        if(request(['order']) != null){
            $unpaid = request('order');
        }else{
            $unpaid = "";
        }
    
        if($status == "4" || $unpaid){
            $orders = Order::with('products')->filter(request(['status','order']))->get();
        }else{
            $orders = Order::with('products')->filter(request(['status','order']))->where('status_id','!=','4')->get();
        }

        $type = "1";

        $unpaid_size = sizeof(Order::unpaid()->get());

        return view('frontend.orders.index',compact('orders','type','unpaid_size'));
    }

    public function backorders(){
        if(request(['status']) != null){
            $status = request(['status'])['status'];
        }else{
            $status = "";
        }

        if(request(['order']) != null){
            $unpaid = request('order');
        }else{
            $unpaid = "";
        }
    
        if($status == "4" || $unpaid){
            $orders = Order::with('products')->filter(request(['status','order']))->where('type','backorder')->get();
        }else{
            $orders = Order::with('products')->filter(request(['status','order']))->where('type','backorder')->where('status_id','!=','4')->get();
        }
        $unpaid_size = sizeof(Order::backorder()->unpaid()->get());
        $type = "3";
        return view('frontend.orders.backorder',compact('orders','type','unpaid_size'));
    }

    public function specialOrders(){
        if(request(['status']) != null){
            $status = request(['status'])['status'];
        }else{
            $status = "";
        }

        if(request(['order']) != null){
            $unpaid = request('order');
        }else{
            $unpaid = "";
        }
    
        if($status == "4" || $unpaid){
            $orders = Order::with('products')->filter(request(['status','order']))->where('type','special')->get();
        }else{
            $orders = Order::with('products')->filter(request(['status','order']))->where('type','special')->where('status_id','!=','4')->get();
        }

        $unpaid_size = sizeof(Order::special()->unpaid()->get());
        $type = "2";        
        return view('frontend.orders.special',compact('orders','type','unpaid_size'));
    }

    public function phoneOrders(){
        if(request(['status']) != null){
            $status = request(['status'])['status'];
        }else{
            $status = "";
        }

        if(request(['order']) != null){
            $unpaid = request('order');
        }else{
            $unpaid = "";
        }
    
        if($status == "4" || $unpaid){
            $orders = Order::with('products')->filter(request(['status','order']))->where('type','phone')->get();
        }else{
            $orders = Order::with('products')->filter(request(['status','order']))->where('type','phone')->where('status_id','!=','4')->get();
        }
        $unpaid_size = sizeof(Order::phoneorder()->unpaid()->get());
        $type = "4";
        return view('frontend.orders.phoneorder',compact('orders','type','unpaid_size'));
    }

    public function create(Request $request)
    {  
        $statuses = OrderStatus::orderBy('position','asc');
        $employees = Employee::isActive()->get();
        return view('frontend.orders.create',compact('statuses','employees','request'));
    }

    public function store(Request $request)
    {
        $rules = [
            'product_id.*' => 'required|exists:leeselectronic.table_1,id_product',
            'product_price.*' => 'required|between:0,9999.99',
            'product_quantity.*' => 'required',
            'invoice' => 'required|string|min:7|max:8|regex:/^[\w-]*$/',
            'name' => 'required',
            'phone' => 'required|min:10',
            'employee' => 'required'
        ];

        $customMessages = [
            'product_id.required' => "There must be at least 1 product.",
            'product_id.exists' => "One or more item(s) do not exist."
         ];

        $this->validate($request, $rules, $customMessages);

        preg_match_all('/((?:[0-9]+,)*[0-9]+(?:\.[0-9]+)?)/', $request->invoice, $invoice);

        if($invoice[0][0] < 2000000 || $invoice[0][0] > 2999999){
            if($invoice[0][0] != 1234567){
                session()->flash('error','You have entered an invalid invoice number ['. $request->invoice . '].');
                return back()->withInput($request->all());
            }
        }

        // END OF VALIDATIONS
        $backorder = false;
        $special = false;
        $phoneorder = false;
        $orders = [];

        foreach($request->product_id as $key=>$product_id){
            if($product_id == 100){
                $special = true;
            }
            if($product_id != 100 && $product_id != 2 && $product_id != 3){
                if(Product::checkStock($product_id,$request->product_quantity[$key])){
                    $phoneorder = true;
                }else{
                    $backorder = true;
                }
            }

        }

        // dd($special." / " . $backorder . " / " . $phoneorder);

        $phone = preg_replace('/\D+/', '', $request->phone);

        // Check if there are 100 items
        if($special){
             // Special Order Creation
            $order = new Order();
            $order->name = $request->name;
            $order->phone = $phone;
            $order->phone_ext = $request->phone_ext;
            if($request->email != null){
                $order->email = $request->email;
            }
            $order->employee_id = $request->employee;
            if($request->comments){
                $order->comments = Crypt::encryptString($request->comments);
            }
            if($request->payment_id){
                $order->payment_id = $request->payment_id;
            }
            $order->status_id = 1;
            $order->invoice_id = $request->invoice;
            $order->type = "special";
            
            $order->save();
            
            array_push($orders,$order->id);

            // Products
            for($i = 0 ; $i < sizeof($request->product_id) ;$i++){
                if($request->product_id[$i] == 100 || $request->product_id[$i] == 2 || $request->product_id[$i] == 3){
                    $product = OrderProduct::create([
                        'product_id' => $request->product_id[$i],
                        'name' => $request->product_name[$i],
                        'price' => $request->product_price[$i],
                        'quantity' => $request->product_quantity[$i],
                        'vendor' => $request->product_vendor[$i],
                        'vendor_remark' => $request->product_vendor_remark[$i],
                        'order_id' => $order->id
                    ]);
                    $product->timestamps = false;
                    $product->save();
                }
            }

            // After saving products, we must save status history as well
            $status_history = new OrderStatusHistory();
            $status_history->order_id = $order->id;
            $status_history->status_id = $order->status_id;
            $status_history->employee_id = $order->employee_id;
            $status_history->comment = "Order Created by " . $order->sales->name;
            $status_history->save();

            if($request->email){
                \Mail::to($order->email)->send(new \App\Mail\OrderCreateCustomer($order));
            }

            // check who opt in for emails
            $orderMailList = [];
            
            foreach(Employee::where('id','!=',$order->sales->id)->get() as $employee){
                if($employee->hasAccess('order_email')->read){
                    array_push($orderMailList,$employee->email);
                }
            }

            if(config('mail.host','smtp.mailtrap.io') != 'smtp.mailtrap.io'){
                \Mail::to($order->sales->email)
                        ->bcc($orderMailList)
                        ->send(new \App\Mail\OrderCreateEmployee($order));
            }else{
                \Mail::to($order->sales->email)
                ->send(new \App\Mail\OrderCreateEmployee($order));
            }
        }
        
        // Create backorder
        if($backorder){
             // Order Creation
             $order = new Order();
             $order->name = $request->name;
             $order->phone = $phone;
             $order->phone_ext = $request->phone_ext;
             if($request->email != null){
                $order->email = $request->email;
             }
             $order->employee_id = $request->employee;
             if($request->comments){
                $order->comments = Crypt::encryptString($request->comments);
            }
            if($request->payment_id){
                $order->payment_id = $request->payment_id;
            }

            $order->status_id = 1;
            $order->invoice_id = $request->invoice;
            $order->type = "backorder";
             
            //  dd("creating backorder");
            $order->save();

            array_push($orders,$order->id);

            // Products
            for($i = 0 ; $i < sizeof($request->product_id) ;$i++){
                if($request->product_id[$i] != 100){
                    if(!$special && ($request->product_id[$i] == 2 || $request->product_id[$i] == 3)){
                        $product = OrderProduct::create([
                            'product_id' => $request->product_id[$i],
                            'name' => $request->product_name[$i],
                            'price' => $request->product_price[$i],
                            'quantity' => $request->product_quantity[$i],
                            'vendor' => $request->product_vendor[$i],
                            'vendor_remark' => $request->product_vendor_remark[$i],
                            'order_id' => $order->id
                        ]);
                        $product->timestamps = false;
                        $product->save();
                    }
                    else if(!Product::checkStock($request->product_id[$i],$request->product_quantity[$i])){
                        $product = OrderProduct::create([
                            'product_id' => $request->product_id[$i],
                            'name' => $request->product_name[$i],
                            'price' => $request->product_price[$i],
                            'quantity' => $request->product_quantity[$i],
                            'vendor' => $request->product_vendor[$i],
                            'vendor_remark' => $request->product_vendor_remark[$i],
                            'order_id' => $order->id
                        ]);
                        $product->timestamps = false;
                        $product->save();
                    }
                }
            }

            // After saving products, we must save status history as well
            $status_history = new OrderStatusHistory();
            $status_history->order_id = $order->id;
            $status_history->status_id = $order->status_id;
            $status_history->employee_id = $order->employee_id;
            $status_history->comment = "Order Created by " . $order->sales->name;
            $status_history->save();

            if($request->email != null){
            \Mail::to($order->email)->send(new \App\Mail\OrderCreateCustomer($order));
            }

            // check who opt in for emails
            $orderMailList = [];

            foreach(Employee::where('id','!=',$order->sales->id)->get() as $employee){
                if($employee->hasAccess('order_email')->read){
                    array_push($orderMailList,$employee->email);
                }
            }

            if(config('mail.host','smtp.mailtrap.io') != 'smtp.mailtrap.io'){
                \Mail::to($order->sales->email)
                        ->bcc($orderMailList)
                        ->send(new \App\Mail\OrderCreateEmployee($order));
            }else{
                \Mail::to($order->sales->email)
                ->send(new \App\Mail\OrderCreateEmployee($order));
            }
        }
        // End of Backorder

        if($phoneorder){
            // Order Creation
            $order = new Order();
            $order->name = $request->name;
            $order->phone = $phone;
            $order->phone_ext = $request->phone_ext;
            if($request->email != null){
               $order->email = $request->email;
            }
            $order->employee_id = $request->employee;
            if($request->comments){
               $order->comments = Crypt::encryptString($request->comments);
            }

            if($request->payment_id){
                $order->payment_id = $request->payment_id;
            }

            $order->status_id = 1;
            $order->invoice_id = $request->invoice;
            
            $order->type = "phone";
            
            $order->save();

            array_push($orders,$order->id);

            // Products
            for($i = 0 ; $i < sizeof($request->product_id) ;$i++){
                if($request->product_id[$i] != 100){
                    if(!$special && !$backorder && ($request->product_id[$i] == 2 || $request->product_id[$i] == 3)){
                        $product = OrderProduct::create([
                            'product_id' => $request->product_id[$i],
                            'name' => $request->product_name[$i],
                            'price' => $request->product_price[$i],
                            'quantity' => $request->product_quantity[$i],
                            'vendor' => $request->product_vendor[$i],
                            'vendor_remark' => $request->product_vendor_remark[$i],
                            'order_id' => $order->id
                        ]);
                        $product->timestamps = false;
                        $product->save();
                    }
                    else if(Product::checkStock($request->product_id[$i],$request->product_quantity[$i])){
                        $product = OrderProduct::create([
                            'product_id' => $request->product_id[$i],
                            'name' => $request->product_name[$i],
                            'price' => $request->product_price[$i],
                            'quantity' => $request->product_quantity[$i],
                            'vendor' => $request->product_vendor[$i],
                            'vendor_remark' => $request->product_vendor_remark[$i],
                            'order_id' => $order->id
                        ]);
                        $product->timestamps = false;
                        $product->save();
                    }
                }
            }

            // After saving products, we must save status history as well
            $status_history = new OrderStatusHistory();
            $status_history->order_id = $order->id;
            $status_history->status_id = $order->status_id;
            $status_history->employee_id = $order->employee_id;
            $status_history->comment = "Order Created by " . $order->sales->name;
            $status_history->save();

            if($request->email != null){
               \Mail::to($order->email)->send(new \App\Mail\OrderCreateCustomer($order));
            }

           // check who opt in for emails
           $orderMailList = [];

           foreach(Employee::where('id','!=',$order->sales->id)->get() as $employee){
               if($employee->hasAccess('order_email')->read){
                   array_push($orderMailList,$employee->email);
               }
           }

           if(config('mail.host','smtp.mailtrap.io') != 'smtp.mailtrap.io'){
               \Mail::to($order->sales->email)
                       ->bcc($orderMailList)
                       ->send(new \App\Mail\OrderCreateEmployee($order));
           }else{
               \Mail::to($order->sales->email)
               ->send(new \App\Mail\OrderCreateEmployee($order));
           }
       }
    //  End of phone orders

        session()->flash('message','The order(s) have been successfully created');
        session()->flash('print',$orders);

       

        return redirect()->route('orders');
    }


    public function convertQuote(Request $request, Quotation $quotation){
        $this->validate(request(), [
            'invoice' => 'required|string|min:7|max:8|regex:/^[\w-]*$/'
        ]);

        preg_match_all('/((?:[0-9]+,)*[0-9]+(?:\.[0-9]+)?)/', $request->invoice, $invoice);

        if($invoice[0][0] < 2000000 || $invoice[0][0] > 2999999){
            if($invoice[0][0] != 1234567){
                session()->flash('error','You have entered an invalid invoice number ['. $request->invoice . '].');
                return back()->withInput($request->all());
            }
        }

        // END OF VALIDATIONS
        $backorder = false;
        $special = false;
        $phoneorder = false;
        $orders = [];

        foreach($quotation->products as $product){
            if($product->id_product == 100){
                $special = true;
            }
            if($product->id_product != 100){
                if(Product::checkStock($product->id_product,$product->pivot->quantity)){
                    $phoneorder = true;
                }else{
                    $backorder = true;
                }
            }

        }

        // Check if there are 100 items
        if($special){
             // Special Order Creation
            $order = new Order();
            $order->name = $quotation->name;
            $order->phone = $quotation->phone;
            $order->phone_ext = $quotation->phone_ext;
            $order->email = $quotation->email;
            $order->employee_id = $request->employee;
            $order->comments = $quotation->comments;
            $order->status_id = 1;
            $order->invoice_id = $request->invoice;
            $order->type = "special";

            if($request->payment_id){
                $order->payment_id = $request->payment_id;
            }
            
            $order->save();

            array_push($orders,$order->id);

            // Products
            foreach($quotation->products as $product){
                if($product->id_product == 100){
                    $order_product = OrderProduct::create([
                        'product_id' => $product->id_product,
                        'name' => $product->pivot->name,
                        'price' => $product->pivot->price,
                        'quantity' => $product->pivot->quantity,
                        'vendor' => $product->pivot->vendor,
                        'vendor_remark' => $product->pivot->vendor_remark,
                        'order_id' => $order->id
                    ]);
                    $order_product->timestamps = false;
                    $order_product->save();
                }
            }

            // After saving products, we must save status history as well
            $status_history = new OrderStatusHistory();
            $status_history->order_id = $order->id;
            $status_history->status_id = $order->status_id;
            $status_history->employee_id = $order->employee_id;
            $status_history->comment = "Order generated from quote #". $quotation->id. " by ". $order->sales->name;
            $status_history->save();

            if($order->email){
                \Mail::to($order->email)->send(new \App\Mail\OrderCreateCustomer($order));
            }

            // check who opt in for emails
            $orderMailList = [];

            foreach(Employee::where('id','!=',$order->sales->id)->get() as $employee){
                if($employee->hasAccess('order_email')->read){
                    array_push($orderMailList,$employee->email);
                }
            }

            if(config('mail.host','smtp.mailtrap.io') != 'smtp.mailtrap.io'){
                \Mail::to($order->sales->email)
                        ->bcc($orderMailList)
                        ->send(new \App\Mail\OrderCreateEmployee($order));
            }else{
                \Mail::to($order->sales->email)
                ->send(new \App\Mail\OrderCreateEmployee($order));
            }
        }
        
        // Create backorder
        if($backorder){
            // Order Creation
            $order = new Order();
            $order->name = $quotation->name;
            $order->phone = $quotation->phone;
            $order->phone_ext = $quotation->phone_ext;
            $order->email = $quotation->email;
            $order->employee_id = $request->employee;
            $order->comments = $quotation->comments;
            $order->status_id = 1;
            $order->invoice_id = $request->invoice;
            $order->type = "backorder";

            if($request->payment_id){
            $order->payment_id = $request->payment_id;
            }

            $order->save();

            array_push($orders,$order->id);

            // Products
            foreach($quotation->products as $product){
                if($product->id_product != 100){
                    $order_product = OrderProduct::create([
                        'product_id' => $product->id_product,
                        'name' => $product->pivot->name,
                        'price' => $product->pivot->price,
                        'quantity' => $product->pivot->quantity,
                        'vendor' => $product->pivot->vendor,
                        'vendor_remark' => $product->pivot->vendor_remark,
                        'order_id' => $order->id
                    ]);
                    $order_product->timestamps = false;
                    $order_product->save();
                }
            }
 
            // After saving products, we must save status history as well
            $status_history = new OrderStatusHistory();
            $status_history->order_id = $order->id;
            $status_history->status_id = $order->status_id;
            $status_history->employee_id = $order->employee_id;
            $status_history->comment = "Order generated from quote #". $quotation->id. " quoted by ". $quotation->employee->name;
            $status_history->save();

            if($order->email){
                \Mail::to($order->email)->send(new \App\Mail\OrderCreateCustomer($order));
            }

            // check who opt in for emails
            $orderMailList = [];

            foreach(Employee::where('id','!=',$order->sales->id)->get() as $employee){
                if($employee->hasAccess('order_email')->read){
                    array_push($orderMailList,$employee->email);
                }
            }

            if(config('mail.host','smtp.mailtrap.io') != 'smtp.mailtrap.io'){
                \Mail::to($order->sales->email)
                        ->bcc($orderMailList)
                        ->send(new \App\Mail\OrderCreateEmployee($order));
            }else{
                \Mail::to($order->sales->email)
                ->send(new \App\Mail\OrderCreateEmployee($order));
            }
        }

        // Phone Order
        if($phoneorder){
            // Order Creation
            $order = new Order();
            $order->name = $quotation->name;
            $order->phone = $quotation->phone;
            $order->phone_ext = $quotation->phone_ext;
            $order->email = $quotation->email;
            $order->employee_id = $request->employee;
            $order->comments = $quotation->comments;
            $order->status_id = 1;
            $order->invoice_id = $request->invoice;
            $order->type = "phone";

            if($request->payment_id){
                $order->payment_id = $request->payment_id;
            }
            
            $order->save();

            array_push($orders,$order->id);

            // Products
            foreach($quotation->products as $product){
                if($product->id_product != 100){
                    if(Product::checkStock($product->id_product,$product->pivot->quantity)){
                        $order_product = OrderProduct::create([
                            'product_id' => $product->id_product,
                            'name' => $product->pivot->name,
                            'price' => $product->pivot->price,
                            'quantity' => $product->pivot->quantity,
                            'vendor' => $product->pivot->vendor,
                            'vendor_remark' => $product->pivot->vendor_remark,
                            'order_id' => $order->id
                        ]);
                        $order_product->timestamps = false;
                        $order_product->save();
                    }
                }
            }

            // After saving products, we must save status history as well
            $status_history = new OrderStatusHistory();
            $status_history->order_id = $order->id;
            $status_history->status_id = $order->status_id;
            $status_history->employee_id = $order->employee_id;
            $status_history->comment = "Order generated from quote #". $quotation->id. " quoted by ". $quotation->employee->name;
            $status_history->save();

            if($quotation->email != null){
               \Mail::to($order->email)->send(new \App\Mail\OrderCreateCustomer($order));
            }

           // check who opt in for emails
           $orderMailList = [];

           foreach(Employee::where('id','!=',$order->sales->id)->get() as $employee){
               if($employee->hasAccess('order_email')->read){
                   array_push($orderMailList,$employee->email);
               }
           }

           if(config('mail.host','smtp.mailtrap.io') != 'smtp.mailtrap.io'){
               \Mail::to($order->sales->email)
                       ->bcc($orderMailList)
                       ->send(new \App\Mail\OrderCreateEmployee($order));
           }else{
               \Mail::to($order->sales->email)
               ->send(new \App\Mail\OrderCreateEmployee($order));
           }
       }
    //  End of phone orders

        $quotation->isExpired = true;
        $quotation->save();
        $quotation->touch();

        session()->flash('message', 'You have successfully converted the quotation into order(s)');
        session()->flash('print',$orders);

        return redirect()->route('orders');
    }

    public function show(Order $order)
    {   
        $status_history = $order->statuses;
        $statuses = OrderStatus::orderBy('position','asc')->get();
        $employees = Employee::isActive()->get();
        $related_phone = Order::incompleteRelatedPhone($order);
        $related_invoice = Order::incompleteRelatedInvoice($order);
        return view('frontend.orders.detail',compact('order','status_history','statuses','related_phone','related_invoice','employees'));
    }

    /**
     * 
     *      API
     * 
     */

    public function updateInvoice(Request $request, Order $order)
    {
        $this->validate(request(), [
            'invoice' => 'required|string|min:7|max:8|regex:/^[\w-]*$/'
        ]);

        preg_match_all('/((?:[0-9]+,)*[0-9]+(?:\.[0-9]+)?)/', $request->invoice, $invoice);

        if($invoice[0][0] < 2000000 || $invoice[0][0] > 2999999){
            if($invoice[0][0] != 1234567){
                session()->flash('error','You have entered an invalid invoice number ['. $request->invoice . '].');
                return back()->withInput();
            }
        }

        $order->invoice_id = $request->invoice;
        $order->save();
        $order->touch();

        session()->flash('message','You have successfully updated the invoice number.');

        return back();
    }

    public function updateTracking(Request $request, Order $order)
    {
        $this->validate(request(), [
            'tracking' => 'required'
        ]);

        $order->tracking_id= $request->tracking;
        $order->save();
        $order->touch();

        session()->flash('message','You have successfully updated the tracking number.');

        return back();
    }

    public function updateComment(Request $request, Order $order)
    {
        $this->validate(request(), [
            'comment' => 'required'
        ]);

        $order->comments = Crypt::encryptString($request->comment);
        $order->save();
        $order->touch();

        session()->flash('message','You have successfully updated the comment.');

        return back();
    }

    public function updatePayment(Request $request)
    {
        $this->validate(request(), [
            'payment_id' => 'required',
            'order_id' => 'required'
        ]);
        
        $order = Order::find($request->order_id);

        if($order) {
            $order->payment_id = $request->payment_id;
            $order->save();
            $order->touch();
        }else{
            session()->flash('error','Order not found');
            return back();
        }

        session()->flash('message','You have successfully update order #' . $order->id);

        return back();
    }

    public function fetchOrder(Request $request) {

        if($request->order){
            $order_id = $request->order;
        }else{
            return json_encode(array("status"=>"failed","message"=>"There was no order specified."));
        }
        
        $result = Order::find($order_id);

        if(!$result){
            return json_encode(array('status' => 'failed', 'message' => 'Order could not be found.'));
        }

        $cart_array = array();
        
        // dd($result->products);
        foreach($result->products as $product){
            $id_product = $product->id_product;
            $image = Product::find($id_product)->image_url();
            if($image){
                $product->img_url = $image; 
            }else{
                $product->img_url = asset('img/no-image.png');
            }
            
            array_push($cart_array, $product->toArray());
        }
        // dd($cart_array[0]);
        $result->cart_items = $cart_array;

        // add sales name
        $result->employee = $result->sales->name;

        // add status name
        $result->status_name = $result->currentStatus->name;
        
        $last_array = array('status' => 'success', 'result_data' => $result);
        return json_encode($last_array);
        
    }

    public function search(Request $request) {

        $unpaid_size = sizeof(Order::unpaid()->get());

        $term = $request->term;

        if($request->type == 1){
            $orders = Order::search("$term")->groupBy('order_id')->get();
            $online = OnlineOrder::search("$term")->groupBy('ps_orders.id_order')->get();
            $mobile = MobileOrder::search("$term")->groupBy('la_order.order_id')->get();
            return view('frontend.orders.index',compact('orders','term','type','unpaid_size','online','mobile'));
        }else if($request->type == 2){
            $orders = Order::search("$term")->where('type','special')->groupBy('order_id')->get();
            $online = OnlineOrder::search("$term")->groupBy('ps_orders.id_order')->get();
            $mobile = MobileOrder::search("$term")->groupBy('la_order.order_id')->get();
            return view('frontend.orders.special',compact('orders','term','type','unpaid_size','online','mobile'));
        }else if($request->type == 3){
            $orders = Order::search("$term")->where('type','backorder')->groupBy('order_id')->get();
            $online = OnlineOrder::search("$term")->groupBy('ps_orders.id_order')->get();
            $mobile = MobileOrder::search("$term")->groupBy('la_order.order_id')->get();
            return view('frontend.orders.backorder',compact('orders','term','type','unpaid_size','online','mobile'));
        }else{
            $orders = Order::search("$term")->groupBy('order_id')->get();
            $online = OnlineOrder::search("$term")->groupBy('ps_orders.id_order')->get();
            $mobile = MobileOrder::search("$term")->groupBy('la_order.order_id')->get();
            return view('frontend.orders.index',compact('orders','term','type','unpaid_size','online','mobile'));
        }

        
    }
}
