<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use DB;
use App\Note;

class OnlineOrder extends Model
{
    protected $connection = 'leeselectronic';
    protected $table = 'ps_orders';
    protected $primaryKey = 'id_order';

    public $incrementing = false;
    public $timestamps = false;

    protected $searchable = [
        'ps_orders.id_order',
        'ps_orders.reference',
        'ps_customer.firstname',
        'ps_customer.lastname',
        'ps_customer.email',
        'ps_order_detail.product_id',
        'ps_order_detail.product_name',
    ];

    public function sidePanelNote(){
        return $this->hasOne(SidePanelOnlineNote::class, 'online_id');
    }

    public function note(){
        return $this->belongsToMany('App\Note', 'datacore.online_notes', 'order_id', 'note_id');
    }

    /*************************************************************
     * 
     *          Order Status
     * 
     *************************************************************/

    public function statusName(){
        $status = DB::connection('leeselectronic')->table('ps_order_state_lang')->where('id_order_state',$this->current_state)->where('id_lang',1)->first();
        if($status){
            return $status->name;
        }else{
            return "N/A";
        }
    }

    public function statusColor(){
        $status = DB::connection('leeselectronic')->table('ps_order_state')->where('id_order_state',$this->current_state)->first();
        if($status){
            return $status->color;
        }else{
            return "";
        }
    }

    /*************************************************************
     * 
     *         Customer Information
     * 
     *************************************************************/

    public function customer(){
        $customer = DB::connection('leeselectronic')->table('ps_customer')->selectRaw('id_customer,firstname,lastname,email')->where('id_customer',$this->id_customer)->first();
        return $customer;
    }

     /*************************************************************
     * 
     *          Order Address
     * 
     *************************************************************/

    public function addressBilling(){
        $address = DB::connection('leeselectronic')->table('ps_address')->where('id_address',$this->id_address_invoice)->first();
        return $address;
    }

    public function addressShipping(){
        $address = DB::connection('leeselectronic')->table('ps_address')->where('id_address',$this->id_address_delivery)->first();
        return $address;
    }

    public static function stateToName($id_state){
        $state = DB::connection('leeselectronic')->table('ps_state')->where('id_state',$id_state)->first();
        return $state->name;
    }

    public static function stateToISO($id_state){
        $state = DB::connection('leeselectronic')->table('ps_state')->where('id_state',$id_state)->first();
        return $state->iso_code;
    }

    /*************************************************************
     * 
     *          Order Address
     * 
     *************************************************************/

     public function carrier() {
         $carrier = DB::connection('leeselectronic')->table('ps_carrier')->where('id_carrier',$this->id_carrier)->first();
        return $carrier;
     }

     /*************************************************************
     * 
     *          Order Functions
     * 
     *************************************************************/

     public static function newOrders(){
        $orders = static::whereIn('current_state',[2,9])->get();
        return $orders;
     }

      ////////////////////////////////////////////
    //
    // Search
    //
    //////////////////////////////////////////

    protected function getWhereClause($term)
    {
        // removing symbols used by MySQL
        $reservedSymbols = ['<', '>', '@', '(', ')', '~','\''];
        $term = str_replace($reservedSymbols, '', $term);
        $whereClause = '';

        $words = explode(' ', $term);

        foreach($words as $word_id=>$word){
            if($word_id > 0){
                $whereClause .= ") AND (";
            }else{
                $whereClause .= "(";
            }
            foreach ($this->searchable as $col_id=>$column){
                if($col_id==0){
                    $whereClause .= "$column LIKE '%$word%' "  ; 
                }else{
                    $whereClause .= "OR $column LIKE '%$word%' "  ; 
                }
            }

            if($word_id + 1 == sizeof($words)){
                    $whereClause .= ")";
                }
        }
        return $whereClause;
    }

    public function scopeSearch($query, $term)
    {
        $columns = implode(',',$this->searchable);
        
        $whereClause = $this->getWhereClause($term);

        $query->selectRaw(' `ps_orders`.*, 
                            `ps_order_detail`.*,
                            `ps_customer`.`firstname`,
                            `ps_customer`.`lastname`,
                            `ps_customer`.`email`,
                            `ps_customer`.`company`')
                ->join('ps_order_detail','ps_orders.id_order','=','ps_order_detail.id_order')
                ->join('ps_customer','ps_orders.id_customer','=','ps_customer.id_customer')
                ->whereRaw($whereClause);
                
        return $query;
    }

    // END SEARCH

     public function isSigned(){
        $file = Storage::disk('public')->exists('invoices/online'.$this->id_order.'.jpg');
        return $file;
     }

     public function lastSigned(){
        $url = url('/storage/invoices/online').$this->id_order.'.jpg';
        $c = curl_init($url);
        curl_setopt($c, CURLOPT_HEADER, 1); // Include the header    
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_FILETIME, 1);   
        curl_exec($c);
        $result = curl_getinfo($c);   

        return date('m/d/Y',(int)$result['filetime']);
     }

     public function getMessages(){
         $messages = DB::connection('leeselectronic')
                        ->table('ps_customer_thread')
                        ->join('ps_customer_message','ps_customer_thread.id_customer_thread','ps_customer_message.id_customer_thread')
                        ->select(   'ps_customer_thread.id_customer_thread',
                                    'id_employee',
                                    'id_order',
                                    'ps_customer_message.message',
                                    'ps_customer_message.private',
                                    'ps_customer_thread.date_add')
                        ->where('id_order',$this->id_order)->get();
         return $messages;
     }

}
