@component('mail::message')
# Status of Order {{$order->reference}}: Ready for Pick Up
This message is to inform you that your order is ready for pick up. Please bring a valid ID and show this email to a sales associate upon arrival.
If you are planning on sending someone else to pick up your order, please notify us of the name of the person.

You can find our address and our hours below: 

Our Address:<br>
<a href="https://g.page/leeselectronic">
4131 Fraser Street<br>
Vancouver, BC V5V 3E9<br>
</a>

Our Hours:<br>
Mon-Fri: 9:00AM-6:00PM<br>
Sat: 9:00AM-5:00PM<br>
Sun & Holidays: Closed<br>

To review your order, you can log in into the mobile application and view your order under "Orders".

If you have any questions or additional instructions regarding your order, please contact us at <a href="tel:6048751993">604-875-1993</a>.

Regards,<br>
Lee's Electronic Components
@endcomponent
