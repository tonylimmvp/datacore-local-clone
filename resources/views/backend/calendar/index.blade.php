@extends('backend.layouts.main')

@include('layouts.fullcalendar')
@include('layouts.datepicker')

@section('content')
    <div id="calendar-tools" class="card card-body mb-4">
        <div class="row">
            <div class="col-3">
                @auth
                    @if(Auth::user()->hasAccess('events')->read)
                    <a href="{{ route('coreui.schedule-admin') }}">
                        <button class="btn btn-primary" type="button">View All</button>
                    </a>
                    @endif
                @endauth
            </div>
        <div class="text-center col-6">
            <h2>My Schedule</h2>
        </div>
        <div class="text-right col-3">
            <button class="btn btn-primary" data-toggle="modal" data-target="#createEventModal">Create Event</button>
        </div>
        </div>
    </div>
    <div id="calendar"></div>
    
    @include('backend.calendar.schedule')

@endsection