<?php

namespace App\Http\Controllers;

use App\OrderStatus;
use Illuminate\Http\Request;

class OrderStatusController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $statuses = OrderStatus::all();
        return view('backend.settings.order-status.index',compact('statuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.settings.order-status.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        // dd($request->all());
        // Validate form
        $this->validate(request(), [
            'name' => 'required|unique:order_statuses',
            'color' => 'required|unique:order_statuses',
            'position' => 'required|integer|unique:order_statuses'
        ]);

        // create OrderStatus

        $status = OrderStatus::create([
                'name' => request('name'),
                'color' => request('color'),
                'position' => request('position')            ]
        );

        $status->save();

        session()->flash('message','You have successfully the status:' . $status->name . '.');
        return redirect()->route('coreui.order-status');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrderStatus  $orderStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderStatus $orderStatus)
    {
        // $current = \Auth::user();
        // if($employee->id!=$current->id){
        //     if(!$current->hasRole("SuperAdmin")){
        //         session()->flash('message','The page you tried to reach is restricted');
        //         return redirect()->route('coreui.dashboard');
        //     }
        // }

        // Validate
        $this->validate(request(), [
            'name' => 'required|unique:order_statuses,name,'.$orderStatus->id,
            'color' => 'required|unique:order_statuses,color,'.$orderStatus->id,
            'position' => 'required|integer|unique:order_statuses,position,'.$orderStatus->id
        ]);

        // update status

        
        $orderStatus->name = $request->name;
        $orderStatus->color = $request->color;
        $orderStatus->position = $request->position;

        $orderStatus->save();
        
        session()->flash('message','You have successfully updated ' . $orderStatus->name);
    
        return redirect()->route('coreui.order-status');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrderStatus  $orderStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderStatus $orderStatus)
    {
        $orderStatus->delete();
        session()->flash('message','You have successfully deleted the status: ' . $orderStatus->name .'.');
        return redirect()->route('coreui.order-status');
    }
}
