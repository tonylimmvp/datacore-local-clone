@extends('backend.layouts.main')

@section('content')
    <div class="container d-flex justify-content-center" style="height:80vh">
        <div class="my-auto text-center">
            <h5>WinPos Integrations/Tools</h5>
            
            @if(Auth::user()->hasAccess('sales_history')->create)
            <a href="{{route('coreui.sales-history.upload')}}"><button class="btn btn-primary">Import Monthly Sales</button></a> <br>
            @endif

            @if(Auth::user()->hasAccess('vendors')->create)
            <a href="{{route('coreui.vendors.upload')}}"><button class="btn btn-primary  mt-2">Import Vendors</button></a>
            <a href="{{route('vendors.update')}}"><button class="btn btn-primary  mt-2" onclick="openLoader()">Update Product Vendors</button></a> <br>
            @endif

            @if(Auth::user()->hasAccess('purchase_orders')->create)
            <a href="{{route('coreui.po.upload')}}"><button class="btn btn-primary  mt-2">Import Purchase Orders</button></a> <br>
            <a href="{{route('coreui.po-product.upload')}}"><button class="btn btn-primary  mt-2">Import Purchase Order Products</button></a><br>
            @endif

            @if(Auth::user()->hasAccess('cost')->update)
            <a href="{{route('vendors.update-recent-cost')}}"><button class="btn btn-primary  mt-2" onclick="openLoader()">Update Recent Cost for Vendors</button></a>
            <a href="{{route('vendors.update-avg-cost')}}"><button class="btn btn-primary  mt-2" onclick="openLoader()">Update Average Cost for Vendors</button></a>
            @endif

            @if(!Auth::user()->hasAccess('sales_history')->create && !Auth::user()->hasAccess('vendors')->create && !Auth::user()->hasAccess('purchase_orders')->create && !Auth::user()->hasAccess('cost')->update)
            <div class="alert alert-info">
                <p>You have no permissions for WinPos integrations</p>
            </div>
            @endif

            <hr>

            <h5>Prestashop Integrations/Tools</h5>
            @if(Auth::user()->hasAccess('category')->update)
            <a href="{{route('category.search')}}"><button class="btn btn-primary">Force Categories</button></a> <br>
            @endif
            @if(Auth::user()->hasAccess('brands')->update)
            <a href="{{route('brand.search')}}"><button class="btn btn-primary mt-2">Update Manufacturer/Brand</button></a> <br>
            @endif
            @if(Auth::user()->hasAccess('banner')->update)
            <a href="{{route('coreui.banner-message')}}"><button class="btn btn-primary mt-2">Update Banner Message</button></a><br>
            @endif

            @if(Auth::user()->hasAccess('product')->update)
            <a href="{{ route("online-product.syncDescription") }}"><button class="btn btn-primary mt-2" onclick="openLoader()">Sync All Product Description</button><br>
            <a href="{{ route("online-product.syncShortDescription") }}"><button class="btn btn-primary mt-2" onclick="openLoader()">Sync All Product Short Description</button><br>
            @endif

            @if(!Auth::user()->hasAccess('category')->update && !Auth::user()->hasAccess('brands')->update && !Auth::user()->hasAccess('banner')->update)
            <div class="alert alert-info">
                <p>You have no permissions for prestashop integrations</p>
            </div>
            @endif

            {{-- <hr/> --}}
            
            
            {{-- <h5>Convert Comments from FS to DC</h5>
            @if(Auth::user()->hasRole('SuperAdmin')) --}}
            {{-- <h5>Convert Comments from FS to DC</h5>
            @if(Auth::user()->hasRole('SuperAdmin')) --}}
            {{-- <form method="POST" action="{{route('notes.missing-convert')}}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <button type="submit" class="btn btn-primary" onclick="openLoader()">Convert Missing Notes</button>
                </div>
            </form>

            <form method="POST" action="{{route('notes.relation-convert')}}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <button type="submit" class="btn btn-primary"  onclick="openLoader()">Convert Relation Notes to General</button>
                </div>
            </form>

            <form method="POST" action="{{route('notes.obsolete-convert')}}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Convert Obsolete Notes</button>
                </div>
            </form> --}}

            <form method="POST" action="{{route('price-changes-update')}}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <button type="submit" class="btn btn-primary mt-2" onclick="openLoader()">Update Price Change Table</button>
                </div>
            </form>

            {{-- @else
            <div class="alert alert-info">
                    <p>You have must be an Admin in order to convert. <br>Please contact support if transfer is required.</p>
                </div>
            @endif --}}

        </div>
    </div>

@endsection

@push('bottom-scripts')

<script>
    function openLoader(){
        $('#loader').toggle();
    }
</script>

@endpush