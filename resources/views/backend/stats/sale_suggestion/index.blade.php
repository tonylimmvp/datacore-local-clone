@extends('backend.layouts.main')

@include('layouts.datatables')

@push('top-scripts')
<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
@endpush

@push('css')
<style>
    h1 {
        text-align: center;
        font-family: 'Nunito', sans-serif;
        font-weight: 200;
    }

    h2 {
        font-family: 'Nunito', sans-serif;
        font-weight: 200; 
        text-align: center;
    }

    h3 {
        font-family: 'Nunito', sans-serif;
        font-weight: 200; 
        font-size: 20px;
        text-align: center;
    }
    
    .product-image-thumbnail {
        display: block;
        width: 100px;
        height: auto;
    }

    td img {
        display: block;
        margin-left: auto;
        margin-right: auto;
    }
</style>
@endpush

@section('content')

    <br/>
    <h1>Sale Promotions Suggestion List</h1>
    <hr/>
    <h3>All sales suggestion are based on SRP margin. Discount are set to from [5% to 50%] with 5% increments.</h3>

    <div class="table-responsive">
        <table id="mainTable" class="table table-hover table-dark table-bordered" width="100%">
            <thead>
                <tr>
                    <th scope="col"><span data-feather="hash"></span> Product ID</th>
                    <th scope="col"><span data-feather="list"></span> Name</th>
                    <th scope="col"><span data-feather="cart"></span> Vendor</th>
                    <th scope="col"><span data-feather="check-circle"></span>Qty</th>
                    <th scope="col"><span data-feather="dollar-sign"></span> Price</th>
                    <th scope="col"><span data-feather="dollar-sign"></span> Cost</th>
                    <th scope="col"><span data-feather="up-arrow"></span> SRP Margin</th>
                
                    <th scope="col"><font color="#00cc66"><span data-feather="down-arrow"></span> Sale Percentage 1</font></th>
                    <th scope="col"><font color="#00cc66"><span data-feather="dollar-sign"></span> Adjusted Price 1</font></th>
                    <th scope="col"><font color="#00cc66"><span data-feather="check"></span> Profit Margin 1</font></th>

                    <th scope="col"><font color="#ffcc00"><span data-feather="down-arrow"></span> Sale Percentage 2</font></th>
                    <th scope="col"><font color="#ffcc00"><span data-feather="dollar-sign"></span> Adjusted Price 2</font></th>
                    <th scope="col"><font color="#ffcc00"><span data-feather="check"></span> Profit Margin 2</font></th>

                
                    <th scope="col"><font color="#ff5050"><span data-feather="down-arrow"></span> Sale Percentage 3</font></th>
                    <th scope="col"><font color="#ff5050"><span data-feather="dollar-sign"></span> Adjusted Price 3</font></th>
                    <th scope="col"><font color="#ff5050"><span data-feather="check"></span> Profit Margin 3</font></th>                
                </tr>
            </thead>
            <tbody>
                @foreach($sale_suggestions as $suggestion)
                    <tr>
                        <td><a href="{{route('sale-suggestion.view', $suggestion)}}" style='text-decoration: none; color: white;'>{{$suggestion->product_id}}</a></td>
                        <td>{{$suggestion->product->name}}</td>
                        @if($suggestion->vendor_id != 0)
                            <td>{{$suggestion->vendor->reference}}</td>
                        @else
                            <td>N/A</td>
                        @endif
                        <td>{{$suggestion->product->qty_available}}</td>
                        <td>${{$price = $suggestion->price}}</td>
                        <td>${{$cost = $suggestion->cost}}</td>
                        <td>{{(number_format((($price-$cost)/$price)*100, 0))}}%</td>
                        
                        <td><font color="#00cc66">{{$suggestion->sale_percentage1 * 100}}%</font></td>
                        <td><font color="#00cc66">${{$suggestion->adj_price1}}</font></td>
                        <td><font color="#00cc66">{{$suggestion->profit_margin1 * 100}}%</font></td>
                        
                        <td><font color="#ffcc00">{{$suggestion->sale_percentage2 * 100}}%</font></td>
                        <td><font color="#ffcc00">${{$suggestion->adj_price2}}</font></td>
                        <td><font color="#ffcc00">{{$suggestion->profit_margin2 * 100}}%</font></td>

                        <td><font color="#ff5050">{{$suggestion->sale_percentage3 * 100}}%</font></td>
                        <td><font color="#ff5050">${{$suggestion->adj_price3}}</font></td>
                        <td><font color="#ff5050">{{$suggestion->profit_margin3 * 100}}%</font></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <p>*Disabled / Non-existent / Non-existent cost / Non-existent price product(s) are not shown above.</p>
    <hr/>

    <div class="container" style="text-align:center">
        <form method="POST" action="{{route('sale-suggestion.generate')}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group input-group-append">
                <input type="text" class="form-control" id="pid_min" name="pid_min" placeholder="Minimum PID" value="{{old('pid_min')}}">
                <input type="text" class="form-control" id="pid_max" name="pid_max" placeholder="Maximum PID (optional)" value="{{old('pid_max')}}">
            </div>
            <div class="form-group">
                <label>Ignore Zero Quantities: </label>
                <input type="checkbox" name="ignore_zeroqty_checkbox" value="yes" checked>
            </div>

            <div class="form-group">
                <label for="vendor">Vendor</label>
                @php $query = App\Vendor::all() @endphp
                <select class="custom-select mr-sm-2 mb-2" name="vendor">
                    <option value="">All</option>
                    @foreach($query as $q)
                        <option value="{{$q->id}}">{{$q->id}} | {{$q->reference}}</option>
                    @endforeach
                </select>
            </div>

            <button type="submit" class="btn btn-info btn-sm ml-2" style="width: 25%">Generate Sales</button>

            @include('layouts.errors')
        </form>
    </div>
    <hr/>
    <div class="" style="text-align:center">
        <form method="POST" action="{{route('sale-suggestion.deleteAll')}}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <button type="submit" class="btn btn-info btn-sm">Delete All</button>
        </form>
    </div>
    <hr/>

@endsection

@push('bottom-scripts')
    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"> </script>

    <script>
        jQuery(document).ready(function($) {
            $(".clickable-row").click(function() {
                window.location = $(this).data("href");
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#mainTable').DataTable({
                "scrollY":        "80vh",
                scrollX:        true,
                "scrollCollapse": true,
                "paging":         false,
                columnDefs: [ { orderable: false, targets: [
                    
                ]}],
                "language": {
                    "search": "Filter records:"
                }
            });
        });
    </script>
@endpush