<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\EmployeePermission;

class Permission extends Model
{
    protected $table = "permissions";
    public $timestamps = false;

    public function employees(){
        return $this->belongsToMany(Employee::class, 'employee_permission')->withPivot('create','read','update','delete');
    }

    public function roles(){
        return $this->belongsToMany(EmployeeRole::class, 'employee_role_permission')->withPivot('create','read','update','delete');
    }
    

}
