@extends('frontend.layouts.main')

@push('top-scripts')
<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
@endpush

@push('css')
<style>
    h1 {
        text-align: center;
        font-family: 'Nunito', sans-serif;
        font-weight: 200;
    }

    h2 {
        font-family: 'Nunito', sans-serif;
        font-weight: 200; 
        text-align: center;
    }

    .flex-center {
        align-items: center;
        display: flex;
        justify-content: center;
    }

    .position-ref {
        position: relative;
    }

</style>
@endpush

@section('content')

    <br/>
    <h1>RMA Lists</h1>
    <hr/>

    <div class="table-responsive">
        <table class="table table-hover table-dark table-bordered" width="100%">
            <thead>
                <tr>
                    <th scope="col"><span data-feather="hash"></span> RMA ID</th>
                    <th scope="col"><span data-feather="user"></span> Customer Name</th>
                    <th scope="col"><span data-feather="phone-outgoing"></span> Phone Number</th>
                    <th scope="col"><span data-feather="hash"></span> Product ID</th>
                    <th scope="col"><span data-feather="align-justify"></span> Product Description</th>
                    <th scope="col"><span data-feather="list"></span> Quantity</th>
                    <th scope="col"><span data-feather="external-link"></span> Vendor</th>
                    <th scope="col"><span data-feather="activity"></span> Current Status</th>
                    <th scope="col"><span data-feather="user-check"></span> Staff Name</th>
                </tr>
            </thead>

            <tbody>
            @foreach ($rmas as $rma)
                <tr class="clickable-row" data-href="{{route('rma.show', $rma->id)}}">
                    <th scope="row">{{ $rma -> id }}</th>
                    <td>{{ $rma -> name }}</td>
                    <td>{{ $rma -> phone }} @if($rma->phone_ext) <span style="color:gray">Ext.{{$rma->phone_ext}}</span> @endif</td>
                    <td>{{ $rma -> id_product }}</td>
                    <td>{{ $rma -> product_name}}</td>
                    <td>{{ $rma -> qty}}</td>
                    @if ($rma->vendor)
                        <td>{{$rma->vendor->name}}</td>
                    @else
                        <td>N/A</td>
                    @endif
                    <td><font color="#{{$rma->status->color}}">{{ $rma->status->name}}</font></td>
                    <td>{{$rma->employee->name}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <hr/>

    {{-- Actions List --}}
    <div class="container">
        <div class="flex-center position-ref">
            @if(Auth::user()->hasAccess('rma')->create)
            <div class="btn-group mr-2">
                <a href="{{route('rma.create')}}"><button class="btn btn-xs btn-info" >Add RMA</button></a>
            </div>
            @endif

            @if ($showAllState == 0)
                <div class="btn-group mr-2">
                    <a href="{{route('rma').'?showAllState=1'}}"><button class="btn btn-xs btn-info" >{{$buttonText}}</button></a>
                </div>
            @elseif ($showAllState == 1)
                <div class="btn-group mr-2">
                    <a href="{{route('rma').'?showAllState=0'}}"><button class="btn btn-xs btn-info" >{{$buttonText}}</button></a>
                </div>
            @endif

            
        </div>
    </div>
@endsection

@push('bottom-scripts')
<script>
    jQuery(document).ready(function($) {
        $(".clickable-row").click(function() {
            window.location = $(this).data("href");
        });
    });
</script>
@endpush