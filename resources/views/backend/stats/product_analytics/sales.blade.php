@extends('backend.layouts.main')

@include('layouts.datatables')

@push('top-scripts')
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
@endpush

@push('css')
    <style>
        h1 {
            text-align: center;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
        }

        h2 {
            font-family: 'Nunito', sans-serif;
            font-weight: 200; 
            text-align: center;
        }

        h3 {
            font-family: 'Nunito', sans-serif;
            font-weight: 200; 
            font-size: 20px;
            text-align: center;
        }
    </style>
@endpush

@section('content')

{{-- Variable Initialization --}}
@php 
    $currentYear = date('Y') + 0;
    $lastYear = date('Y') - 1;
    $currentMonth = date('m') + 0;

    $qtrSales2 = App\SalesHistory::quarterlyByYearRank($currentYear);
@endphp

<h2>{{$currentYear}} Quarterly Sales Analytics</h2>
<hr>
    <div class="table-responsive">
        <table id="rankTable" class="table table-hover table-dark table-bordered" width="100%">
            <thead>
                <tr>
                    <th scope="col"><span data-feather="hash"></span> Product ID</th>
                    <th scope="col"><span data-feather="hash"></span> Product Name</th>
                    <th scope="col"><span data-feather="list"></span> Q1</th>
                    <th scope="col"><span data-feather="list"></span> Q2</th>
                    <th scope="col"><span data-feather="list"></span> Q3</th>
                    <th scope="col"><span data-feather="list"></span> Q4</th>
                    <th scope="col"><span data-feather="list"></span> Q1 -> Q2</th>
                    <th scope="col"><span data-feather="list"></span> Q2 -> Q3</th>
                    <th scope="col"><span data-feather="list"></span> Q3 -> Q4</th>
                </tr>
            </thead>
            <tbody>
                @foreach($qtrSales2 as $result)
                    @if($result->products)
                    @if(!$result->products->isObsolete)
                    <tr>
                        <td>{{$result->product_id}}</td>
                        <td>{{$result->products->name}}</td>
                        <td>{{$result->q1}}</td>
                        <td>{{$result->q2}}</td>
                        <td>{{$result->q3}}</td>
                        <td>{{$result->q4}}</td>

                        @php 
                        
                            if($result->q1 > 0){ //Used to prevent division by 0
                                $firstComparison = ($result->q2)/($result->q1); 
                                $firstComparisonP = abs(round((1-$firstComparison)*100));
                            }
                            else {
                                $firstComparisonP = 0;
                            }
            
                            if($result->q2 > 0){
                                $secondComparison = ($result->q3)/($result->q2);
                                $secondComparisonP = abs(round((1-$secondComparison)*100));
                            }
                            else {
                                $secondComparisonP = 0;
                            }

                            if($result->q3 > 0){
                                $thirdComparison = ($result->q4)/($result->q3);
                                $thirdComparisonP = abs(round((1-$thirdComparison)*100));
                            }
                            else {
                                $thirdComparisonP = 0;
                            }
                        @endphp

                        @if(($result->q2) > ($result->q1))
                            <td style="color: green;"><span data-feather="arrow-up" style="stroke: green;"></span>{{$firstComparisonP}}%</td>
                        @elseif (($result->q2) == ($result->q1))
                            <td><span data-feather="arrow-right" style="stroke: white;"></span></td>
                        @else
                            <td style="color: red;"><span data-feather="arrow-down" style="stroke: red;"></span>{{$firstComparisonP}}%</td>
                        @endif
                        
                        @if(($result->q3) > ($result->q2))
                            <td style="color: green;"><span data-feather="arrow-up" style="stroke: green;"></span>{{$secondComparisonP}}%</td>
                        @elseif (($result->q3) == ($result->q2))
                            <td><span data-feather="arrow-right" style="stroke: white;"></span></td>
                        @else
                            <td style="color: red;"><span data-feather="arrow-down" style="stroke: red;"></span>{{$secondComparisonP}}%</td>
                        @endif

                        @if(($result->q4) > ($result->q3))
                            <td style="color: green;"><span data-feather="arrow-up" style="stroke: green;"></span>{{$thirdComparisonP}}%</td>
                        @elseif (($result->q4) == ($result->q3))
                            <td><span data-feather="arrow-right" style="stroke: white;"></span></td>
                        @else
                            <td style="color: red;"><span data-feather="arrow-down" style="stroke: red;"></span>{{$thirdComparisonP}}%</td>
                        @endif
                    </tr>
                    @endif
                    @endif
                @endforeach
            </tbody>
        </table>
    </div>

@endsection

@push('bottom-scripts')
    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"> </script>

    <script>
        jQuery(document).ready(function($) {
            $(".clickable-row").click(function() {
                window.location = $(this).data("href");
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#rankTable').DataTable({
                "scrollY":        "100vh",
                scrollX:        true,
                "scrollCollapse": true,
                "paging":         true,
                "pagingType": "simple_numbers",
                columnDefs: [ { orderable: false, targets: [
                    1, 6, 7, 8
                ]}],
                "language": {
                    "search": "Filter records:"
                }
            });
        });
    </script>
@endpush