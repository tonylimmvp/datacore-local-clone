<html>
<head>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

    <style>
        body {
            margin: 0;
            padding: 0;
            background-color: #FAFAFA;

        }
        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }
        .page {
            width: 8.2cm;
            height: 8.3cm;
            margin: 0cm auto;
            border: 1px #D3D3D3 solid;
            border-radius: 0px;
            background: white;
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
            line-height: 4px;
        }

        @page {
            size: 5.5cm 6.3cm;
            margin: 0;
        }

        @media print {
            .page {
                margin: 0;
                border: initial;
                border-radius: initial;
                width: 6.3cm;
                min-height: 5.5cm;
                box-shadow: initial;
                background: initial;
                page-break-after: always;
            }
        }

        #container_style1{
            width: 301px;
            text-align: center;
            line-height: 0px;
            position: relative;
        }

        .pid {
            text-align: center;
            font: 36pt "Nunito";
            font-weight: bold;
        }

        .product_name {
            text-align: center;
            font: 18pt "Nunito";
            height:70px;
        }

        .bottom_left{
            position: absolute;
            font: 14pt "Nunito";
            left: 20px;
            bottom: 40px;
        }

        .bottom_right{
            position: absolute;
            font: 11pt "Nunito";
            top: 210px;
            right: 12px;
            width:100px;
        }

        .date{
            position: absolute;
            font: 14pt "Nunito";
            left: 100px;
            bottom: 0px;
        }

    </style>
</head>

<body>
    <div id='container_style1' class='page'>
        <h1 class="pid">#{{$product->id_product}}</h1>
        <h2 class="product_name">{{$product->name}}</h2>
        <hr>
        <span class="bottom_left"> {!! DNS1D::getBarcodeSVG($product->id_product, "C39", 1.25, 37.5) !!}</span>
        @php
            $now = now()->format('Y-m-d'); 
        @endphp
        <h3 class="bottom_right">LOC: {{$product->fraser_loc}}</h3>
        <small class="date">{{$now}}</small>
    </div>
    
</body>