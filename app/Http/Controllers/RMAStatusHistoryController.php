<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RMAStatusHistory;
use App\RMA;

class RMAStatusHistoryController extends Controller
{
    public function store(Request $request)
    {
        $this->validate(request(), [
            'rma' => 'required',
            'employee' => 'required',
            'status' => 'required'
        ]);

        // create order status history
    
        $status_history = RMAStatusHistory::create([
            'rma_id' => request('rma'),
            'employee_id' => request('employee'),
            'status_id' => request('status'),
            'comment' => request('comment')            
        ]);
    
        $status_history->save();

        // Once status history created, we want to change order status as well

        $rma = RMA::find($request->rma);
        $rma->status_id = $request->status; 

        $rma->save();

        if ($rma->status_id >= '3'){
            $rma->isActive = '0';
            $rma->save();
        }
            
        session()->flash('message','You have successfully updated your RMA status.');
        return back();
    }

    public function destroy($id)
    {
        //
    }
}
