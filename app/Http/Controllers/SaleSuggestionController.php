<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SaleSuggestion;
use App\Category;
use App\CategoryProduct;
use App\Product;
use App\Vendor;
use App\SalePriceBreak;

class SaleSuggestionController extends Controller
{
    public function index(){
        $sale_suggestions = SaleSuggestion::all();

        return view('backend.stats.sale_suggestion.index', compact('sale_suggestions'));
    }

    //Function to generate sales suggestion.
    public function generate(Request $request){
        $this->validate(request(), [
            'pid_min' => 'required|integer',
        ]);

        $vendorFromRequest = Vendor::where('id', $request->vendor)->first();
        if($request->ignore_zeroqty_checkbox == 'yes' && $request->vendor == NULL)
        {
            if($request->pid_max != NULL){
                $products = Product::where('id_product', '>=', $request->pid_min)->where('id_product', '<=', $request->pid_max)->where('disabled', 'N')
                ->where('recent_cost', "!=", '0.00')->where('price', "!=", '0.00')->where('quantity', '>=', '1')->get();
            }
            else{
                $products = Product::where('id_product', '>=', $request->pid_min)->where('id_product', '<=', $request->pid_min)->where('disabled', 'N')
                ->where('recent_cost', "!=", '0.00')->where('price', "!=", '0.00')->where('quantity', '>=', '1')->get();
            }
        }
        elseif ($request->ignore_zeroqty_checkbox != 'yes' && $request->vendor == NULL){
            //If pid_max is set:
            if($request->pid_max != NULL){
                $products = Product::where('id_product', '>=', $request->pid_min)->where('id_product', '<=', $request->pid_max)->where('disabled', 'N')
                ->where('recent_cost', "!=", '0.00')->where('price', "!=", '0.00')->get();
            }
            else{
                $products = Product::where('id_product', '>=', $request->pid_min)->where('id_product', '<=', $request->pid_min)->where('disabled', 'N')
                ->where('recent_cost', "!=", '0.00')->where('price', "!=", '0.00')->get();
            }
        }
        elseif ($request->ignore_zeroqty_checkbox != 'yes' && $request->vendor != NULL){
            //If pid_max is set:
            if($request->pid_max != NULL){
                $products = Product::where('id_product', '>=', $request->pid_min)->where('id_product', '<=', $request->pid_max)->where('disabled', 'N')
                ->where('recent_cost', "!=", '0.00')->where('price', "!=", '0.00')->where('vendor', $vendorFromRequest->reference)->get();
            }
            else{
                $products = Product::where('id_product', '>=', $request->pid_min)->where('id_product', '<=', $request->pid_min)->where('disabled', 'N')
                ->where('recent_cost', "!=", '0.00')->where('price', "!=", '0.00')->where('vendor', $vendorFromRequest->reference)->get();
            }
        }

        elseif($request->ignore_zeroqty_checkbox == 'yes' && $request->vendor != NULL)
        {
            if($request->pid_max != NULL){
                $products = Product::where('id_product', '>=', $request->pid_min)->where('id_product', '<=', $request->pid_max)->where('disabled', 'N')
                ->where('recent_cost', "!=", '0.00')->where('price', "!=", '0.00')->where('quantity', '>=', '1')->where('vendor', $vendorFromRequest->reference)->get();
            }
            else{
                $products = Product::where('id_product', '>=', $request->pid_min)->where('id_product', '<=', $request->pid_min)->where('disabled', 'N')
                ->where('recent_cost', "!=", '0.00')->where('price', "!=", '0.00')->where('quantity', '>=', '1')->where('vendor', $vendorFromRequest->reference)->get();
            }
        }

        foreach($products as $product){
            if ($product->disabled == "N" && $product->price != '0.00' && $product->recent_cost != '0.00'){

                $var = Vendor::where('reference', $product->vendor)->first();
                $vend_id = "";
                
                if($var){
                   $vend_id = $var->id;
                }

                $sale_suggestion = SaleSuggestion::updateOrCreate([
                    'product_id' => $product->id_product,
                    'price' => $product->price,
                    'cost' => $product->recent_cost,
                    'vendor_id' => $vend_id,
                ]);

            
                $msrpMargin = floatval((($product->price) - ($product->recent_cost)) / ($product->price)); 

                if ($product->sale->sale_percentage1 == NULL || $product->sale->adj_price1 == NULL || $product->sale->profit_margin1 == NULL){
                    $count = 0;
                    $this->calculateLevel1Discount($product, $sale_suggestion, $msrpMargin, $count);
                }
    
                if ($product->sale->sale_percentage2 == NULL || $product->sale->adj_price2 == NULL || $product->sale->profit_margin2 == NULL){
                    $this->calculateLevel2Discount($product, $sale_suggestion, $msrpMargin, $count);
                }
    
                if ($product->sale->sale_percentage3 == NULL || $product->sale->adj_price3 == NULL || $product->sale->profit_margin3 == NULL){
                    $this->calculateLevel3Discount($product, $sale_suggestion, $msrpMargin, $count);
                }
            }
            else{}
        }

        return redirect()->route('sale-suggestion');
    }

    public function calculateLevel1Discount(Product $product, SaleSuggestion $sale_suggestion, float $msrpMargin, int $count){
        //Bypassing rand where it gives only an integer
        $rng = floatval(rand(5, 50));
        while($rng % 5 != 0){
            $rng = floatval(rand(5, 50));
        }

        $discount = 1 - ($rng/100);
        $sale_price = ($product->price) * $discount;
        $profitMargin = floatval((($sale_price - ($product->recent_cost)) / $sale_price));

        //$profitMargin is in range of desired sales level:
        if(($profitMargin >= floatval($msrpMargin*0.75)) && ($profitMargin <= floatval($msrpMargin*0.9))){
            $sale_suggestion->sale_percentage1 = 1 - $discount;
            $sale_suggestion->adj_price1 = $product->price * $discount;
            $sale_suggestion->profit_margin1 = $profitMargin;
            $sale_suggestion->save();

        }
        else{
            //Allows n passes of loopings.
            if ($count < 40){
                $count++;
                $this->calculateLevel1Discount($product, $sale_suggestion, $msrpMargin, $count);
            }
            else{
                $sale_suggestion->sale_percentage1 = '0';
                $sale_suggestion->adj_price1 = '0';
                $sale_suggestion->profit_margin1 = '0';
                $sale_suggestion->save();
            }
        }
    }

    public function calculateLevel2Discount(Product $product, SaleSuggestion $sale_suggestion, float $msrpMargin, int $count){
        //Bypassing rand where it gives only an integer
        $rng = floatval(rand(5, 50));
        while($rng % 5 != 0){
            $rng = floatval(rand(5, 50));
        }

        $discount = 1 - ($rng/100);
        $sale_price = ($product->price) * $discount;
        $profitMargin = floatval((($sale_price - ($product->recent_cost)) / $sale_price));

        //$profitMargin is in range of desired sales level:
        if(($profitMargin >= floatval($msrpMargin*0.50)) && ($profitMargin <= floatval($msrpMargin*0.75))){
            $sale_suggestion->sale_percentage2 = 1 - $discount;
            $sale_suggestion->adj_price2 = $product->price * $discount;
            $sale_suggestion->profit_margin2 = $profitMargin;
            $sale_suggestion->save();
        }
        else{
            //Allows n passes of loopings.
            if ($count < 40){
                $count++;
                $this->calculateLevel2Discount($product, $sale_suggestion, $msrpMargin, $count);
            }
            else{
                $sale_suggestion->sale_percentage2 = '0';
                $sale_suggestion->adj_price2 = '0';
                $sale_suggestion->profit_margin2 = '0';
                $sale_suggestion->save();
            }
        }
    }

    public function calculateLevel3Discount(Product $product, SaleSuggestion $sale_suggestion, float $msrpMargin, int $count){
        //Bypassing rand where it gives only an integer
        $rng = floatval(rand(5, 50));
        while($rng % 5 != 0){
            $rng = floatval(rand(5, 50));
        }

        $discount = 1 - ($rng/100);
        $sale_price = ($product->price) * $discount;
        $profitMargin = floatval((($sale_price - ($product->recent_cost)) / $sale_price));

        //$profitMargin is in range of desired sales level:
        if(($profitMargin >= floatval($msrpMargin*0.35)) && ($profitMargin <= floatval($msrpMargin*0.50))){
            $sale_suggestion->sale_percentage3 = 1 - $discount;
            $sale_suggestion->adj_price3 = $product->price * $discount;
            $sale_suggestion->profit_margin3 = $profitMargin;
            $sale_suggestion->save();
        }
        else{
            //Allows n passes of loopings.
            if ($count < 40){
                $count++;
                $this->calculateLevel3Discount($product, $sale_suggestion, $msrpMargin, $count);
            }
            else{
                $sale_suggestion->sale_percentage3 = '0';
                $sale_suggestion->adj_price3 = '0';
                $sale_suggestion->profit_margin3 = '0';
                $sale_suggestion->save();
            }
        }
    }

    public function deleteAll(){
        //Delete all rows from database table.
        SaleSuggestion::truncate();
    
        return redirect()->route('sale-suggestion');
    }

    public function display(SaleSuggestion $suggestion){
        return view('backend.stats.sale_suggestion.view', compact('suggestion'));
    }

    public function calculatePriceBreaks(SaleSuggestion $suggestion){
        // Get Static Value to be used in price break calculation
        $regular_static_1 = '0.20';
        $regular_static_2 = '0.40';
        $regular_static_3 = '0.60';

        $wholesale_static_1 = '0.40';
        $wholesale_static_2 = '0.60';
        $wholesale_static_3 = '0.80';

        // Grab $suggestion Information
        $price = $suggestion->price;
        $cost = $suggestion->cost;
        $srpMargin = ($price-$cost) / $price;
    
        $regular_suggestion1 = (number_format($price - ($srpMargin*$regular_static_1*$price), 2));
        $regular_suggestion2 = (number_format($price - ($srpMargin*$regular_static_2*$price), 2));
        $regular_suggestion3 = (number_format($price - ($srpMargin*$regular_static_3*$price), 2));

        $wholesale_suggestion1 = (number_format($price - ($srpMargin*$wholesale_static_1*$price), 2));
        $wholesale_suggestion2 = (number_format($price - ($srpMargin*$wholesale_static_2*$price), 2));
        $wholesale_suggestion3 = (number_format($price - ($srpMargin*$wholesale_static_3*$price), 2));

        $breaks = SalePriceBreak::where('product_id', $suggestion->product_id)->first();
        if($breaks){
            $breaks->regular_suggestion1 = $regular_suggestion1;
            $breaks->regular_suggestion2 = $regular_suggestion2;
            $breaks->regular_suggestion3 = $regular_suggestion3;
            $breaks->wholesale_suggestion1 = $wholesale_suggestion1;
            $breaks->wholesale_suggestion2 = $wholesale_suggestion2;
            $breaks->wholesale_suggestion3 = $wholesale_suggestion3;
    
            $breaks->save();
        }   
        else{
            SalePriceBreak::updateOrCreate([
                'product_id' => $suggestion->product_id,
                'regular_suggestion1' => $regular_suggestion1,
                'regular_suggestion2' => $regular_suggestion2,
                'regular_suggestion3' => $regular_suggestion3,
                'wholesale_suggestion1' => $wholesale_suggestion1,
                'wholesale_suggestion2' => $wholesale_suggestion2,
                'wholesale_suggestion3' => $wholesale_suggestion3,
            ]);
        }

        return view('backend.stats.sale_suggestion.view',compact('suggestion'));
    }

    public function storeQty(SaleSuggestion $suggestion, Request $request){
        // Validate form
        $this->validate(request(), [
        
            'reg_qty1' => 'integer',
            'reg_qty2' => 'integer',
            'reg_qty3' => 'integer',

            'wholesale_qty1' => 'integer',
            'wholesale_qty2' => 'integer',
            'wholesale_qty3' => 'integer'

        ]);

        $breaks = SalePriceBreak::where('product_id', $suggestion->product_id)->first();
        if($breaks){
            $breaks->regular_qty1 = $request->reg_qty1;
            $breaks->regular_qty2 = $request->reg_qty2;
            $breaks->regular_qty3 = $request->reg_qty3;
            $breaks->wholesale_qty1 = $request->who_qty1;
            $breaks->wholesale_qty2 = $request->who_qty2;
            $breaks->wholesale_qty3 = $request->who_qty3;
    
            $breaks->save();
        }
        else {
            SalePriceBreak::updateOrCreate([
                'product_id' => $suggestion->product_id,
                'regular_qty1' => $request->reg_qty1,
                'regular_qty2' => $request->reg_qty2,
                'regular_qty3' => $request->reg_qty3,
                'wholesale_qty1' => $request->who_qty1,
                'wholesale_qty2' => $request->who_qty2,
                'wholesale_qty3' => $request->who_qty3,
            ]);
        }

        return view('backend.stats.sale_suggestion.view', compact('suggestion'));
    }
}
