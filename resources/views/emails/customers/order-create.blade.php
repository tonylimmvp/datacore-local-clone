@component('mail::message')

Hello {{$order->name}},

This is a confirmation email on the order you have placed. Please review and keep this as reference. If you see any mistakes please let us know right away by emailing <a href="mailto:support@leeselectronic.com">support@leeselectronic.com</a> or call us at <a href="tel:6048751993">(604) 875-1993</a>. 

@component('mail::table')
| PID        | Name           | Price per Unit  | Quantity | 
| ---------- |:--------------:| :--------------:| :------: |
@foreach($order->products as $product)
| {{$product->id_product}} | {{$product->pivot->name}} | ${{$product->pivot->price}} | {{$product->pivot->quantity}} |
@endforeach
@endcomponent

@if($order->phone != "6048751993")
The phone number registered with this order is {{$order->phone}} @if($order->phone_ext), Ext.{{$order->phone_ext}} @endif.
@endif

Cheers,<br>
Lee's Electronic Components

---

@if($order->type == "backorder")
<small>*Note: Backorders are subjected to no refund or exchange.*</small>
@elseif($order->type == "phone")
<small>*Note: Phone orders are subjected to no refund or exchange.*</small>
@elseif($order->type == "special")
<small>*Note: All special orders are **FINAL SALE**. No refund or exchange.*</small>
@endif
@endcomponent
