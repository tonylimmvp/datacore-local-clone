@extends('frontend.layouts.main')

@include('layouts.datatables')

@section('content')

{{-- Resistor Filters  --}}
<div class="card mb-5">
        <div class="card-body ml-5 mr-5">
            <form action="{{route('resistor')}}" method="GET">
                <div class="row">
                    <div class="input-group col-md-6 mb-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text">Watt: </label>
                        </div>
    
                        <select class="form-control" name="watt_sign" multiple>
                            <option value="="  @if(request()->get('watt_sign') == "=") selected @endif>equal</option>
                            <option value=">=" @if(request()->get('watt_sign') == ">=") selected @endif>greater than</option>
                            <option value="<=" @if(request()->get('watt_sign') == "<=") selected @endif>less than</option>
                        </select>
    
                        <select class="form-control" name="wattage" multiple>
                            @foreach(App\Resistor::groupBy('wattage')->get() as $resistor)
                                <option value="{{$resistor->wattage}}" 
                                    @if(request()->get('wattage') == $resistor->wattage) selected @endif>
                                    {{$resistor->wattage}}W
                                </option>
                            @endforeach
                        </select>
                    
                    </div>
                    <div class="input-group col-md-6 mb-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text">Ohm: </label>
                        </div>
    
                        <select class="form-control" name="ohm_sign" multiple>
                            <option value="="  @if(request()->get('ohm_sign') == "=") selected @endif>equal</option>
                            <option value=">=" @if(request()->get('ohm_sign') == ">=") selected @endif>greater than</option>
                            <option value="<=" @if(request()->get('ohm_sign') == "<=") selected @endif>less than</option>
                        </select>
    
                        <select class="form-control" name="resistance" multiple>
                            @foreach(App\Resistor::groupBy('resistance')->get() as $resistor)
                                <option value="{{$resistor->resistance}}"
                                    @if(request()->get('resistance') == $resistor->resistance) selected @endif>
                                   {{$resistor->resistance}} Ω
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                
                <button class="btn btn-primary btn-sm">Submit</button>
                <a href={{url()->current()}}><button type="button" class="btn btn-primary btn-sm">Clear Filter</button></a>

            </form>
        </div>
    </div>
    
    @if((isset($_GET['wattage']) && isset($_GET['watt_sign'])) || (isset($_GET['resistance']) && isset($_GET['ohm_sign'])))
    <div class="alert alert-info text-center">
        @if(isset($_GET['wattage']) && isset($_GET['watt_sign']))
        <span>
            Wattage {{$_GET['watt_sign']}} {{$_GET['wattage']}}W
        </span>
        @endif
        @if((isset($_GET['wattage']) && isset($_GET['watt_sign'])) && (isset($_GET['resistance']) && isset($_GET['ohm_sign'])))
            <span>|</span>
        @endif
        @if(isset($_GET['resistance']) && isset($_GET['ohm_sign'])) 
        <span>
            Resistance {{$_GET['ohm_sign']}} {{$_GET['resistance']}} Ω 
        </span>
        @endif
    </div>
    @endif
    
    {{-- Resistor table --}}
<table id="resistor-table" class="table table table-bordered table-hover" width="100%">
    <thead>
        <th>PID</th>
        <th>Name</th>
        <th class="d-none d-md-table-cell">Wattage (W)</th>
        <th class="d-none d-md-table-cell">Resistance (ohm)</th>
        <th class="d-none d-md-table-cell">Tolerance (%)</th>
        <th>Price</th>
        <th>Quantity</th>
        <th>Location</th>
    </thead>
    <tbody>
        @foreach($resistors as $resistor)
        <tr>
            <td>{{$resistor->product_id}}</td>
            <td>{{$resistor->product->name}}</td>
            <td class="d-none d-md-table-cell">{{$resistor->wattage}}W</td>
            <td class="d-none d-md-table-cell">{{$resistor->resistance}} Ω</td>
            <td class="d-none d-md-table-cell">@if($resistor->tolerance){{$resistor->tolerance}}%@endif</td>
            <td>${{$resistor->product->price}}</td>
            <td>{{$resistor->product->quantity}}</td>
            <td class="text-center">{{$resistor->product->locations()}}</td>
        </tr>
        @endforeach
    </tbody>
    
</table>

<div class="text-center">
    <a href="{{ route('resistor.update') }}"><button class="btn btn-primary" onclick="openLoader()">Update Resistor Table</button></a>
</div>

@endsection

@push('bottom-scripts')
<script>
    
    $('#resistor-table').DataTable(
        {
            scrollY:        "60vh",
            scrollX:        true,
            scrollCollapse: true,
            paging:         false,
            displayLength: 25,
            language: {
                "search": "Filter records:"
            },
            columnDefs: [
                { type: 'any-number', targets : [2,3,4,5,6] } 
            ]  

        }
    );
</script>
@endpush