<?php

use Illuminate\Database\Seeder;
use App\EmployeeRole;
use App\Permission;

class EmployeeRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = Permission::all();

        $role = new EmployeeRole();
        $role->name = "SuperAdmin";
        $role->save();
        foreach($permissions as $permission){
            $role->permissions()->attach($permission->id, [
                'create' => 1,
                'read' => 1,
                'update' => 1,
                'delete' => 1,
                ]);
        }

        $role = new EmployeeRole();
        $role->name = "Supervisor";
        $role->save();
        foreach($permissions as $permission){
            $role->permissions()->attach($permission->id, [
                'create' => 1,
                'read' => 1,
                'update' => 1,
                'delete' => 0,
                ]);
        }

        $role = new EmployeeRole();
        $role->name = "Staff";
        $role->save();
        foreach($permissions as $permission){
            $role->permissions()->attach($permission->id, [
                'create' => 1,
                'read' => 1,
                'update' => 1,
                'delete' => 0,
                ]);
        }

        $role = new EmployeeRole();
        $role->name = "Coop/Intern";
        $role->save();
        foreach($permissions as $permission){
            $role->permissions()->attach($permission->id, [
                'create' => 0,
                'read' => 1,
                'update' => 1,
                'delete' => 0,
                ]);
        }

        $role = new EmployeeRole();
        $role->name = "Terminal";
        $role->save();
        foreach($permissions as $permission){
            $role->permissions()->attach($permission->id, [
                'create' => 0,
                'read' => 1,
                'update' => 0,
                'delete' => 0,
                ]);
        }

    }
}
