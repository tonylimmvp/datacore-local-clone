<nav class="d-block bg-light sidebar">
    
    <div id="sidebar1" class="sidebar-sticky">
        {{-- MAIN --}}

        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span class="d-none d-md-block">Main</span>
        </h6>

        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link d-none d-md-block" href="{{route('coreui.dashboard')}}">
                <span data-feather="home"></span>
                <span>Dashboard <span class="sr-only">(current)</span></span>
                </a>
                <a class="nav-link d-md-none" href="{{route('coreui.dashboard')}}">
                    <span data-feather="home"></span>
                </a>
            </li>
            @if(Auth::user()->hasAccess('statistics')->read)
            <li class="nav-item">
                <a class="nav-link" href="{{route('coreui.stats')}}">
                <span data-feather="bar-chart-2"></span>
                <span class="d-none d-md-inline">Statistics</span>
                </a>
            </li>
            @endif
        </ul>

        {{-- Purchases --}}

        @if(Auth::user()->hasAccess('purchase_orders')->read || Auth::user()->hasAccess('vendors')->read)
            <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                <span class="d-none d-md-inline">Purchases</span>
            </h6>

            <ul class="nav flex-column mb-2">

                @if(Auth::user()->hasAccess('purchase_orders')->read)

                <li class="nav-item">
                    <a class="nav-link" href="{{route('coreui.po')}}">
                    <span data-feather="shopping-bag"></span>
                    <span class="d-none d-md-inline">Purchase Orders</span>
                    </a>
                </li>
                
                @endif

                @if(Auth::user()->hasAccess('vendors')->read)
                <li class="nav-item">
                    <a class="nav-link" href="{{route('coreui.vendors')}}">
                    <span data-feather="globe"></span>
                    <span class="d-none d-md-inline">Vendors</span>
                    </a>
                </li>
                @endif

            </ul>
        @endif

        {{-- Users --}}

        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span class="d-none d-md-inline">Users</span>
        </h6>

        <ul class="nav flex-column mb-2">
            @if(Auth::user()->hasAccess('employee')->read)
            <li class="nav-item">
                <a class="nav-link" href="{{route('coreui.employees')}}">
                <span data-feather="users"></span>
                <span class="d-none d-md-inline">Employees</span>
                </a>
            </li>
            @endif

            @if(Auth::user()->hasAccess('employee_role')->read)
            <li class="nav-item">
                <a class="nav-link" href="{{route('coreui.employee-role')}}">
                <span data-feather="list"></span>
                <span class="d-none d-md-inline">Employee Roles</span>
                </a>
            </li>
            @endif

            <li class="nav-item">
                <a class="nav-link" href="{{route('coreui.schedule')}}">
                <span data-feather="calendar"></span>
                <span class="d-none d-md-inline">Schedule</span>
                </a>
            </li>

            @if(Auth::user()->hasAccess('permission')->read && Auth::user()->hasRole('SuperAdmin') && (Auth::user()->id == 2 || Auth::user()->id == 3))
            <li class="nav-item">
                <a class="nav-link" href="{{route('coreui.permission')}}">
                <span data-feather="settings"></span>
                <span class="d-none d-md-inline">Permissions</span>
                </a>
            </li>
            @endif
        </ul>

        {{-- ADMIN --}}
        @if(Auth::user()->hasAccess('events')->read || Auth::user()->hasAccess('payroll')->read || Auth::user()->hasAccess('work_shifts')->read)
            <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                <span class="d-none d-md-inline">ADMIN</span>
            </h6>

            <ul class="nav flex-column">
                @if(Auth::user()->hasAccess('work_shifts')->read)
                <li class="nav-item">
                    <a class="nav-link" href="{{route('coreui.work-shifts')}}">
                    <span data-feather="trello"></span>
                    <span class="d-none d-md-inline">Work Shifts</span>
                    </a>
                </li>
                @endif

                @if(Auth::user()->hasAccess('events')->read)
                <li class="nav-item">
                    <a class="nav-link" href="{{route('coreui.schedule-admin')}}">
                    <span data-feather="calendar"></span>
                    <span class="d-none d-md-inline">Admin Schedule</span>
                    </a>
                </li>
                @endif

                @if(Auth::user()->hasAccess('payroll')->read)
                <li class="nav-item">
                    <a class="nav-link" href="{{route('coreui.payroll')}}">
                    <span data-feather="dollar-sign"></span>
                    <span class="d-none d-md-inline">Payroll Report</span>
                    </a>
                </li>
                @endif
            </ul>
        @endif

        {{-- SETTINGS --}}

        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span class="d-none d-md-inline">Settings</span>
        </h6>

        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link" href="{{route('coreui.settings')}}">
                <span data-feather="settings"></span>
                <span class="d-none d-md-inline">Settings</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{route('coreui.integrations')}}">
                <span data-feather="layers"></span>
                <span class="d-none d-md-inline">Integrations</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="{{route('logout')}}">
                <span data-feather="log-out"></span>
                <span class="d-none d-md-inline">Sign Out</span>
                </a>
            </li>
        </ul>
    </div>
    </nav>